---
title: "#28 PrintScrn"
author: Felix
date: 2022-01-28
tags: ["ashpd", "gjs", "libhandy", "libadwaita", "glib", "gnome-tour", "webkitgtk", "health", "gnome-shell", "fractal", "gnome-builder", "vala"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 21 to January 28.<!--more-->

# Core Apps and Libraries

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) announces

> After thorough reviews from Tobias Bernard, Jonas Dreßler and Georges Stavracas, the main part of the [new GNOME Shell screenshot UI](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/1954) has been merged for GNOME 42! 🎉🎉 Since the last time, the screenshot UI has got another round of design review, more polish and a big close button in the top right.
> 
> Screencasts and a few other minor additions have been split into separate, smaller merge requests for easier review and should hopefully also make it in time for the GNOME 42 UI freeze next week.
> {{< video src="62683d3c97e2e5c74a76b1103e459a5fa63f2ba1.webm" >}}

### WebKitGTK [↗](https://webkitgtk.org/)

GTK port of the WebKit rendering engine.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) announces

> WebKitGTK has had a few touchscreen-related fixes and supports using the accent colors from the app or system when rendering scrollbars and other widgets now

### Vala [↗](https://gitlab.gnome.org/GNOME/vala)

An object-oriented programming language with a self-hosting compiler that generates C code and uses the GObject system

[Prince781](https://matrix.to/#/@Prince781:matrix.org) says

> [Vala 0.55.2 has been released with new features and bug fixes.](https://gitlab.gnome.org/GNOME/vala/-/commit/e13e0d6894abb17189f1846b8727b38388b739cc)
> 
> Last week we [added support for `async main()`](https://gitlab.gnome.org/GNOME/vala/-/commit/45fe8523ad392f1aaa0f24d83b63e91774938775), allowing you to call asynchronous methods from `main` using `yield`.
> 
> ```vala
> // ls.vala
> async int main (string[] args) {
>   string dir = args.length == 2 ? args[1] : ".";
>   var file = File.new_for_commandline_arg (dir);
>   try {
>     FileEnumerator enumerator =
>       yield file.enumerate_children_async (
>         "standard::*,time::*",
>         FileQueryInfoFlags.NOFOLLOW_SYMLINKS
>       );
>     List<FileInfo> children =
>       yield enumerator.next_files_async (int.MAX);
>     print ("total %lu\n", children.length ());
>     foreach (var info in children) {
>       // <file-type> <access-date> <size> <name>
>       print ("%26s %24s %10"+int64.FORMAT+" B %s\n",
>         info.get_content_type (),
>         info.get_access_date_time ().to_string (),
>         info.get_size (),
>         info.get_name ());
>     }
>   } catch (Error e) {
>     printerr ("failed to enumerate files - %s\n", e.message);
>     return 1;
>   }
>   return 0;
> }
> ```
> 
> Similarly, we added support for using [`yield` within main blocks](https://gitlab.gnome.org/GNOME/vala/-/commit/eea3a2ac0e6ef4e88df5e9424b486f399d7ff036).
> 
> This week, we added support for using `foreach` with [`GLib.Array`](https://gitlab.gnome.org/GNOME/vala/-/commit/0db2ebd9035c95efa062ecab310bca88a70d8faf) and [`GLib.Sequence`](https://gitlab.gnome.org/GNOME/vala/-/commit/44195a02c9d26453dc698282deb4947425a4b0b1).
> 
> All these features are available in Vala 0.55.2 and you can use them in your code now.

### Libhandy [↗](https://gitlab.gnome.org/GNOME/libhandy)

Building blocks for modern GNOME apps using GTK3.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> libhandy development branch has been renamed from master to main. If you're using it as a subproject or in a flatpak manifest, you may need to update it

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> Chris 🌱️ has implemented suffix widget support for AdwPreferencesGroup. Apps like Settings have been using a similar pattern for a while but we never had a proper support for it
> ![](465c22222ad76d572f0dac227cd0fbe2fd29c72a.png)

### GNOME Tour [↗](https://gitlab.gnome.org/GNOME/gnome-tour)

A guided tour and greeter for GNOME.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> I have landed a slightly redesigned Tour and also dropped the video feature as it is no longer used by distributors
> ![](04459f542b3de98e11a32a5706f124bfceac5c7b.png)

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[gwagner](https://matrix.to/#/@gwagner:gnome.org) reports

> GNOME Builder resolves files now to a toolbox or podman container correctly and therefore can offer clang completions, symbol resolution and hover information from the container. This should make working in C/C++ with a container much more enjoyable. Also, Christian tweaked the colorschemes in Builder therefore the Adwaita colorscheme has much better contrast and a nice color palette to distinguish the elements better. Veli took the work and wrote a plugin to enable Builder to lint reStructured Text. And we fixed a little bug in our CMake plugin about the target path resolution.
> ![](c60f78ad46957dca1e6d2634ad600243b8fd5c4d.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Emmanuel Fleury has also been continuing his campaign against old tests and incomplete test coverage in GLib, moving a number of old tests to the newer test framework and improving code coverage as he goes

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> I extended GLib's gdbus-codegen tool to take a D-Bus interface XML file and produce a reStructuredText file that can be used with multiple documentation generators, like Python docutils and Sphinx, and simplify the process of documenting D-Bus API.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Marc-André Lureau and Loïc Le Page have been landing various fixes and improvements to GLib on Windows, including static linking support, `AF_UNIX` socket support, various test fixes, and `GMemoryMonitor` support

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> In GJS this week, we have a [bug fix](https://gitlab.gnome.org/GNOME/gjs/-/merge_requests/710/diffs) from Nasah Kuma and a [documentation fix](https://gitlab.gnome.org/GNOME/gjs/-/merge_requests/711/diffs) from Andy Holmes.

# Circle Apps and Libraries

### Health [↗](https://gitlab.gnome.org/World/Health)

Collect, store and visualise metrics about yourself.

[Cogitri](https://matrix.to/#/@cogitri:cogitri.dev) reports

> [Health](https://gitlab.gnome.org/World/Health)'s notifications have been reworked - instead of a separate daemon launching, Health itself is DBus-activatable now and sends notifications via a GApplication service. Additionally, Health's UI files have been ported to [Blueprint](https://gitlab.gnome.org/jwestman/blueprint-compiler) and the overview of activities has been switched over to `AdwExpanderRow`.
> ![](VCgJvZYEdNjqSnMdBlDRqEsq.png)
> ![](iedmWBVkNEQpRHrJiikbMyih.png)

### ashpd [↗](https://github.com/bilelmoussaoui/ashpd)

Rust wrapper around freedesktop portals.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> After months of work I have finally released [ashpd](https://lib.rs/ashpd) 0.2. The release consists of a ton of changes that happened during the last 6 alpha releases and one beta. It consists of mostly:
> * Async only API using [zbus](https://lib.rs/zbus)
> * A new almost fully featured demo of the API available on https://flathub.org/apps/details/com.belmoussaoui.ashpd.demo
> * Integration with both GTK4/GTK3 for x11/Wayland window handle retrieval (optional)
> * Integration with PipeWire for retrieving the camera NodeID when possible (optional)
> * Integration with raw-window-handle crate to allow creating a WindowIdentifier from it, very useful for cross-platform crates that wants to make use of the portals on Linux (optional)
> * Library calls logging using the tracing API (optional)
> * Docs aliases for finding the Rust alternatives to what libportal offers or for finding the proxy to use from the DBus interface
> * Enjoy!

# Third Party Projects

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) reports

> I forgot to report it last week, but there's a new release of [Nostalgia](https://flathub.org/apps/details/im.bernard.Nostalgia), my tiny app to set historic GNOME wallpapers! It's been ported to GTK4 and Libadwaita thanks to Maximiliano, Alexander Mikhaylenko, and Julian Sparber 🎉
> ![](233bb5da656eecb1dbd848f17a9f6be39cc56657.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Julian Sparber](https://matrix.to/#/@jsparber:gnome.org) announces

> Hello folks, it's been a while since we last spoke! We have been focused on the code, but we're long overdue for an update. A lot has happened since November. Fractal-Next is getting closer to feature parity with current Fractal, and even supports new things:
> * Timeline
> 
>     * Fractal-Next now allows you to open and save sent files
> 
>     * It also displays images, videos and stickers in the timeline
> 
>     * You can also get a better view of media send to the room thanks to the built-in media viewer
> 
>     * It (finally!) supports reactions (displaying them and sending new ones)
> 
> * User verification
> 
>     * Fractal-Next now supports verification of other users by scanning their QR code, or via emoji
> 
>     * When a user is verified, an icon is displayed next to their username in the list of room members
> 
> * Room details
> 
>     * The room details show now the members of the room including the power level
> 
> * General UX
> 
>     * Fractal-Next is better integrated with GNOME's secret management service Seahorse
> 
>     * It supports room upgrades
> 
>     * It also supports inviting users to a room
> 
>     * Users can change the category of rooms in the sidebar  via drag and drop or by using the context menu
> 
> 
>     If you want a more lengthy writing about what we've been up to and a note about the NLnet grant, please head out to [my blog post "A Long Overdue Update — Fractal Next"](https://blogs.gnome.org/jsparber/2022/01/28/a-long-overdue-update-fractal-next/)
> ![](8e54c94c7d76b8e8a7f321487c586cd2f3943bf0.png)
> ![](ccb673ae54d9481d042bae1ea8e02eb462a58cc9.png)
> ![](7a631abacba98b5286db9a8d3e1885194bf40606.png)
> {{< video src="6dc69e2663fb5e76b198eb1e8ed732842b151469.webm" >}}

# GNOME Shell Extensions

[firox263](https://matrix.to/#/@firox263:matrix.org) says

> [Extension Manager](https://github.com/mjakeman/extension-manager/), a simple app for browsing and installing Shell Extensions, had its second release. This adds much-requested support for screenshots, as well as a new dark theme and many other quality of life improvements.
> 
> The highlights are:
> * Displays screenshots from extensions.gnome.org
> * Dark theme support
> * GNOME Shell version compatibility check
> * Sort search results by Popularity, Recently Updated, etc
> * Global extension on/off toggle
> * Shows user and system extensions separately
> ![](nJkEYRfvbiOrIFhlfUFagVSF.png)
> ![](oCenGKjBlRsWskuOpaWDhfMF.png)

# GNOME Foundation

[Neil McGovern](https://matrix.to/#/@nmcgovern:matrix.org) says

> FOSDEM returns, virtually. At this year's virtual booth we have planned themed chat times with a variety of GNOME team, staff, and board members. Be sure to check out our booth page for a full schedule, fun GNOME links, and to join us in chat: [https://stands.fosdem.org/stands/the_gnome_foundation/](https://stands.fosdem.org/stands/the_gnome_foundation/). In true FOSDEM tradition, we're also hosting a GNOME Beers social! Bring your beverage of choice and join us for this informal virtual event on Feb 5th at 18:00 UTC. Let us know you're coming by [registering online](https://events.gnome.org/event/96/).

[Neil McGovern](https://matrix.to/#/@nmcgovern:matrix.org) says

> The [Foundation announced](https://foundation.gnome.org/2022/01/21/further-investments-in-desktop-linux/) three new projects that focus on improving our developer experience, making more applications available on the GNOME platform and creating a more equitable and sustainable revenue models for developers within our ecosystems.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

