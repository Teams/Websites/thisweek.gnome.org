---
title: "#35 Software Reviews"
author: Felix
date: 2022-03-18
tags: ["gnome-software", "vala"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 11 to March 18.<!--more-->

# Core Apps and Libraries

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Richard Hughes has [deployed some updates](https://gitlab.gnome.org/Infrastructure/odrs-web/-/merge_requests/15) to the ODRS server (which stores ratings and reviews for apps in GNOME Software) which should fix submission of reviews and voting in GNOME Software, and should [improve HTTP caching and download size](https://gitlab.gnome.org/Infrastructure/odrs-web/-/merge_requests/9) for clients

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Milan Crha has fixed an incredibly pervasive and annoying CI failure in gnome-software, which should speed development up: https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/1298

### Vala [↗](https://gitlab.gnome.org/GNOME/vala)

An object-oriented programming language with a self-hosting compiler that generates C code and uses the GObject system

[lwildberg](https://matrix.to/#/@lw64:gnome.org) announces

> This week Vala 0.56 was released. It includes lots of new and useful features and advancements. To find out more, read the [release notes](https://lw64.gitlab.io/vala/2022/03/18/Vala-0.56).

# Third Party Projects

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> Boatswain, a Stream Deck controller app for GNOME / Linux, and the side project I've been working on for the past few weeks, [was finally publicly announced](https://feaneron.com/2022/03/17/boatswain-your-stream-deck-app-for-linux/). It already supports playing sound effects, start and stop streaming and recording on OBS Studio, switching OBS Studio scenes, folders, multiple profiles, and launching applications.
> ![](cc425585284a7bb0d5c106c89250d0ccdb95fce6.png)

[marhkb](https://matrix.to/#/@marcusb86:matrix.org) announces

> I've been working on a application for Podman called Symphony for a little over a month now. At the moment it is still very much in development. So there are still a lot of missing features and certainly one or two bugs to be expected. Anyone who is interested can have a look at the [repository](https://github.com/marhkb/symphony)
> ![](eviFhZJuQVyBjNRHWToxkBWX.png)

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> I have released a GTK4/libadwaita port of [Symbolic Preview](https://gitlab.gnome.org/World/design/symbolic-preview). It is a design utility that helps the designers design & deploy symbolic icons.
> ![](a2be1230c3df40a73e2f8611b193eb5bc49471bb.png)
> ![](0678603a266a4c91ecc16bb69a4f2e96f1f75f9e.png)

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) says

> [Login Manager Settings](https://github.com/realmazharhussain/gdm-settings) (gdm-settings): A settings app for GNOME's Login Manager (GDM) received a new release (v0.4). It fixes a lot of bugs, adds translations for Urdu and Dutch, adds some keyboard shortcuts and adds options to extract default shell theme, apply user's display settings to GDM, and reset settings.
> 
> Still no prebuilt packages though (except, of course, AUR packages and an AppImage without any dependencies)
> ![](VdZFYXessIjHKoiNFogzQdiZ.png)

# GNOME Shell Extensions

[lupantano](https://matrix.to/#/@lupantano:matrix.org) announces

> [ReadingStrip](https://github.com/lupantano/readingstrip) is a extension for Gnome-Shell with an equivalent function to a reading guide on the computer, that's really useful for people with dyslexia.
> ![](zgMHAihtbJxdwdxzMlZOabOM.jpeg)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

