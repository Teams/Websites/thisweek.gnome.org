---
title: "#31 Bit Windy"
author: Felix
date: 2022-02-18
tags: ["gtk-rs", "telegrand", "webfont-kit-generator", "gnome-shell"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 11 to February 18.<!--more-->

# Core Apps and Libraries

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> Evan Welsh has ported Fonts and Weather to GTK4 + libadwaita. The Fonts port reworks how we load font previews, allowing us to recolor them so that they work with libadwaita's dark style preference.
> ![](5d57da5d40b6f57d78b53f6e0dc4b866b9fe9cd4.png)
> ![](735d0bb3582936114622aa4fc1e19ebdd11d571f.png)

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> In the next GNOME 42 release Clocks, Maps, Calendar & Weather will be using the location portal when sandboxed. Which means they can no longer access the location without the user's consent

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) announces

> Light/dark preference now uses a uniform crossfade transition
> {{< video src="51b298f775148d7c7a3e07d185983f7998c602ab.mp4" >}}

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) says

> The new GNOME Shell screenshot UI has landed in full for GNOME 42. Since the last update, a new screen recording indicator has been added to the top panel, which shows the recording duration and lets you stop the recording in a single click. The UI itself has seen a few design tweaks: for example, the close button has moved to the top-right corner of the panel. Additionally, the icons on the screenshot UI notifications have been refreshed. Of course, there were a few bugfixes, too: overview drag-and-drop now works again during screen recording, and the screenshot UI will automatically close when the screen is locked from inactivity.
> {{< video src="2b491eeb234326a75837e9cadcd83249ebc0c24a.webm" >}}

# Circle Apps and Libraries

### Webfont Kit Generator [↗](https://github.com/rafaelmardojai/webfont-kit-generator)

Create @font-face kits easily.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) announces

> Webfont Kit Generator now includes a tool that allows you to download and import fonts from Google Fonts using a CSS API url, handy for self-hosting them.
> ![](jZnpyDVeCbQFYQUDIVnAcxGc.png)

# Third Party Projects

[Forever](https://matrix.to/#/@foreverxmld:matrix.org) announces

> Today I released Random 1.2. It completely changes the UI of the app to fit current UI guidelines. You can download it on [Flathub](https://flathub.org/apps/details/page.codeberg.foreverxml.Random).
> ![](mrzaZPZkvSrkoPLwwLuWNqhh.png)

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> I have released flatpak-vscode 0.0.17
> * New output terminal for less output delay and working terminal colors
> * New status bar item for current build and run status
> * New rust-analyzer integration to run runnables within the sandbox
> * Improved build and runtime terminal integration
> * Trigger documents portal in activate (May still be problematic when other extensions, like-rust-analyzer, startups earlier)
> * Display the "Flatpak manifest detected" dialog only once
> 
> Huge thanks to SeaDve for most of the changes that landed this release!

### Telegrand [↗](https://github.com/melix99/telegrand/)

A Telegram client optimized for the GNOME desktop.

[Marco Melorio](https://matrix.to/#/@melix99:gnome.org) announces

> It's been a while since I last wrote about Telegrand, but the development hasn't been stopped at all! Here's a short list of what's new since the last update:
> 
> * Implemented chats and contacts search
> * We now show more information in the chat list, like the mention badge and draft message
> * We now support the photo message type
> * Added multi account support (thanks to Marcus Behrendt!)
> * General improvements to the style

# Documentation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> I have added a "Getting Started" tutorial to the Developer Documentation website; you can follow various lessons to go from the basic GNOME application template in Builder to a working simple text viewer. At the end of the process you'll know how to load and save files asynchronously, how to update your UI in response to changes, and how to save and load settings: https://developer.gnome.org/documentation/tutorials/beginners/getting_started.html
> ![](b9c07cc311e4fde12e32ff4074dd4fa22a0b6e23.png)
> ![](7339f89fa2c0a677f838e48e5481b9fd5d2a73df.png)
> ![](f269a5aac9be24578644fe0fad05e21bead4c6fd.png)
> ![](6cbb58eb59895230dbd9033bd0f866adedadd6f7.png)

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> The GNOME developer documentation now contains JavaScript examples. Here is the [merge request](https://gitlab.gnome.org/Teams/documentation/developer-www/-/merge_requests/58).
> ![](uaLSTOCpuEChkaCMzaNdGFUd.png)

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Julian Hofer](https://matrix.to/#/@julianhofer:gnome.org) announces

> I've refactored the gtk4-rs book and touched most chapters during this process. The biggest changes are:
> * The [signals](https://gtk-rs.org/gtk4-rs/stable/latest/book/gobject_signals.html) chapter includes the new `glib::closure_local!` macro.
> * The interface builder chapter has been renamed to [composite templates](https://gtk-rs.org/gtk4-rs/stable/latest/book/composite_templates.html)
> * The composite templates chapter dropped the section about`gtk::Builder` in favor of introducing`gio::Resource`.
> * The composite templates chapter app uses the newly introduced support for template callbacks.
> * The [second todo app](https://gtk-rs.org/gtk4-rs/stable/latest/book/todo_app_2.html) chapter now uses automatic resources to add the shortcut window.
> * The second todo app chapter uses`gio::Settings` instead of`serde_json` to save its state.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

