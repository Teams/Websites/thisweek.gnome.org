---
title: "#30 Fragmented"
author: Felix
date: 2022-02-11
tags: ["gtk-rs", "just-perfection", "pika-backup", "fragments", "gnome-software", "gnome-control-center", "vala"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 04 to February 11.<!--more-->

# Core Apps and Libraries

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Adrien Plazas has [updated the appearance of app reviews in gnome-software](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/1101), as well as a number of other improvements across the UI

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> Backgrounds can now change depending on the light/dark preference. Thanks Jakub Steiner for making dark versions of all default backgrounds and Georges Stavracas (feaneron) for reviews
> ![](e268c933560e14de1b12153f7fdcbc8f5db11e00.png)
> ![](7161d8924ee8df4911f1e6883a4e163210d98bf3.png)

### Vala [↗](https://gitlab.gnome.org/GNOME/vala)

An object-oriented programming language with a self-hosting compiler that generates C code and uses the GObject system

[lwildberg](https://matrix.to/#/@lw64:gnome.org) says

> The examples in the [beginners tutorial](https://developer.gnome.org/documentation/tutorials/beginners/components.html) are also now available in Vala. So the perfect time to start your GNOME app in Vala! Thanks also to Nahu for supporting. [This](https://gitlab.gnome.org/Teams/documentation/developer-www/-/merge_requests/48) is the merge request.
> ![](4f27e40564ee12cb41f43a62f8f1367935707340.png)

# Circle Apps and Libraries

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) says

> Fragments 2.0 is now available! You can download the latest version from [Flathub](https://flathub.org/apps/details/de.haeckerfelix.Fragments) now. If you want to learn more, you should read my [blog post](https://blogs.gnome.org/haeckerfelix/2022/02/07/the-road-to-fragments-2-0/).
> ![](xhpIITFGaShWDUcbwojWmypD.png)

### Pika Backup [↗](https://wiki.gnome.org/Apps/PikaBackup)

Simple backups based on borg.

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) announces

> This week Pika Backup got an interface for deleting old backup archives. The deletion happens based on user-defined prune rules. A lot of refactoring happened to make it easy to show progress for BorgBackup processes apart from backup creation.
> 
> Support for pruning old backup archives is vital for scheduled backup support. Otherwise, schedule settings like 'Hourly' could quickly fill up the backup storage.
> ![](ae372b809f382be6e51f03d6bb9b9fcecadc479e.png)

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) announces

> Bilal Elmoussaoui and I made Rust bindings 🦀 for [libsecret](https://gitlab.gnome.org/GNOME/libsecret/), they can be found [here](https://crates.io/crates/libsecret). Docs [here](https://world.pages.gitlab.gnome.org/Rust/libsecret-rs/stable/latest/docs/libsecret/index.html).

# Third Party Projects

[dabrain34](https://matrix.to/#/@dabrain34:matrix.org) says

> GstPipelineStudio 0.2.3 is available on [flathub](https://flathub.org/apps/details/org.freedesktop.dabrain34.GstPipelineStudio) with a few bug fixes related to the first flatpak release.

[Forever](https://matrix.to/#/@foreverxmld:matrix.org) announces

> I released Random 1.1. This release includes a new icon, libadwaita 1.0, and some improved translations and behind-the-scenes things.

# GNOME Shell Extensions

[aunetx](https://matrix.to/#/@aunetx:matrix.org) announces

> I released version 3 of [panel-corners extension](https://extensions.gnome.org/extension/4805/panel-corners/), which permits the user to keep rounded corners for the panel, following their recent removal.
> 
> In addition, and thanks to Alexander Mikhaylenko, this extension also adds rounded corners to the screen; and permits you to configure the roundness settings (through gsettings for the moment).
> 
> It is widely based on gnome-shell's own ancient code, and is already compatible with GNOME 40 and 41 for those who want to have rounded corners on the bottom of the screen!

### Just Perfection [↗](https://extensions.gnome.org/extension/3843/just-perfection/)

A tweak tool to customize the GNOME Shell and to disable UI elements.

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) reports

> Just Perfection extension version 17 has been released with [some bug fixes](https://gitlab.gnome.org/jrahmatzadeh/just-perfection/-/blob/main/CHANGELOG.md#1700-roslin-2022-02-11).
> Also _panel corner size_ option has been removed for GNOME Shell 42.
> This version is named after _Alexander Roslin_ (Swedish portrait painter).
> ![](01622d8a6328f76a3eef0a582b65001015cdab35.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
