---
title: "#63 Experiments and Prototypes"
author: Felix
date: 2022-09-30
tags: ["gaphor", "workbench", "fractal", "newsflash", "kooha", "komikku"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 23 to September 30.<!--more-->

# Circle Apps and Libraries

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) says

> This week, [Workbench](https://apps.gnome.org/app/re.sonny.Workbench/) joined GNOME Circle. Workbench lets you experiment with GNOME technologies, whether tinkering for the first time or building and testing a GTK user interface. Congratulations!
> ![](39dc01c9cbc8c915c0903f2b9ae6b74902fbd989.png)

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) reports

> [Workbench](https://apps.gnome.org/app/re.sonny.Workbench/) 43 is out!
>
> * Display CSS errors inline
> * Blueprint 0.4.0
> * VTE 0.70.0
> * Use AdwAboutWindow
> * Fix responsiveness when working on large Blueprint files
> * Various bug and crash fixes
> * Use GNOME 43 platform/SDK
>
> Make sure to check what's [new for developers in GNOME 43](https://release.gnome.org/43/developers/index.html) and give it a try in Workbench 43.

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[Jan Lukas](https://matrix.to/#/@jangernert:matrix.org) reports

> After the 2.0 release of NewsFlash last week followed a quick 2.0.1 to fix a nasty database migration issue.
> But now development of version 2.1 has started with more fixes and two new features already merged:
> 1. Tags are now also displayed in the article list. So now you can directly see which article has which tags assigned.
> 2. A simple share mechanism. Nothing fancy with logins etc. Just a auto-generated URL. But this means you can add your own share service easily.
> ![](vkxszKZTYWdnRgJPkvoxOvwA.png)
> ![](eUXLYlIMIoZAawNkzVhiNjMZ.png)
> ![](LQnJipxoiNosHKKhETOWqjOY.png)

### Kooha [↗](https://github.com/SeaDve/Kooha)

A simple screen recorder with a minimal interface. You can simply click the record button without having to configure a bunch of settings.

[SeaDve](https://matrix.to/#/@sedve:matrix.org) announces

> I am pleased to announce [Kooha](https://www.flathub.org/apps/details/io.github.seadve.Kooha) 2.2.0. This release introduces fresh new features and bug fixes from over a hundred commits. Here's the summary of some of the most significant changes:
>
> * New area selection UI inspired from GNOME Shell
> * Added option to change the frame rate through the UI
> * Improved delay settings flexibility
> * Added preferences window for easier configuration
> * Added `KOOHA_EXPERIMENTAL` env var to show experimental (unsupported) encoders like VAAPI-VP8 and VAAPI-H264
> * Added the following experimental (unsupported) encoders: VP9, AV1, and VAAPI-VP9
> * Unavailable formats/encoders are now hidden from the UI
> * Fixed broken audio on long recordings
> ![](HiQmxeIUGsmqQrCnlGKeKDCd.png)

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[danyeaw](https://matrix.to/#/@Yeaw:matrix.org) says

> Excited to announce Gaphor, the simple UML and SysML tool, version 2.12.0 is released!
> * GTK4 is now the default for Flatpak
> * Save folder is remembered across save actions
> * State machine functionality has been expanded, including support for regions
> * Resize of partition keeps actions in the same swimlane
> * Activities (behaviors) can be assigned to classifiers
> * Stereotypes can be inherited from other stereotypes
> * Many GTK4 fixes: rename, search, instant editors
> * Many translation updates
> ![](sMYUpenonwMvEbXlFiWKStjb.png)

# Third Party Projects

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> [Tagger](https://beta.flathub.org/apps/details/org.nickvision.tagger) V2022.9.2 is finally here! This release mainly adds support for automatically downloading and applying tag metadata from MusicBrainz with support for retriving the album art, if avaliable, as well!
>
> Here's a full changelog:
> * Added support for downloading tag metadata from MusicBrainz
> * Fixed an issue where Tagger would not allow opening more than about 1024 files
> * Fixed an issue where the chromaprint fingerprint contained an extra alien unicode character
> * Rewrote the MusicFile model used by Tagger to be faster and better support a large music library
> * Various UX improvements (Tagger should feel much more snappier and responsive)
> ![](RImFqVibJqGehWDhsClKNMCf.png)

### Komikku [↗](https://gitlab.com/valos/Komikku)

A manga reader for GNOME.

[Valéry Febvre (valos)](https://matrix.to/#/@valos:matrix.org) reports

> Pleased to announce the release of version 1.0.0 of [Komikku](https://flathub.org/apps/details/info.febvre.Komikku), the manga reader (but not only).
>
> After several months of efforts, the porting of Komikku on GTK4 and libadwaita is finished.
>
> * Refreshing of the UI to follow the GNOME HIG as much as possible
> * Library has now two display modes: Grid and Compact grid
> * Faster display of the chapters list, whether there are few or many chapters
> * Full rewriting of the Webtoon reading mode
> * Modern ‘About’ window
> * [Preferences] Reader: Add 'Landscape Pages Zoom' setting
> * [Preferences] Reader: Add 'Maximum Width' setting
> * [Servers] Add Grisebouille by @gee [FR]
> * [Servers] MangaNato (MangaNelo): Update
> * [Servers] Mangaowl: Update
> * [Servers] Read Comic Online: Update
> * [L10n] Update French, German, Spanish and Turkish translations
> ![](XPOJRNkmXNarupYiCioRgOQI.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Julian Sparber](https://matrix.to/#/@jsparber:gnome.org) reports

> This week we tagged Fractal as 5.alpha1. This is our first release since Fractal has been rewritten to take advantage of GTK 4 and the Matrix Rust SDK. It is the result of eighteen months of work.
> Currently supported features are:
>
> Currently supported features are:
>  * Sending and receiving messages and files
>  * Sending files via Drag-n-Drop and pasting in the message entry
>  * Rendering of rich formatted (HTML) messages, as well as media
>  * Displaying edited messages, redacting messages
>  * Showing and adding reactions
>  * Tab completion of user names
>  * Sending and displaying replies
>  * Sharing the current location
>  * Exploring the room directory
>  * Sorting the rooms by category
>  * Joining rooms
>  * Sending and accepting invitations
>  * Logging into multiple accounts at once
>  * Logging in with Single-Sign On
>  * Sending and reading encrypted messages
>  * Verifying user sessions using cross-signing
>  * Exporting and importing encryption keys
>  * Managing the connected devices
>  * Changing the user profile details
>  * Deactivating the account
>
> Major missing features are:
>  * Notifications
>  * Read markers
>
> As the name implies, this is still considered alpha stage and is not ready for general use just yet. If you want to give this development version a try, you can get it from the [GNOME Apps Nightly flatpak repository] (https://wiki.gnome.org/Apps/Nightly).
> A list of known issues and missing features for a 5.0 release can be found in the [Fractal v5 milestone](https://gitlab.gnome.org/GNOME/fractal/-/milestones/18) on Gitlab.
>
> We also published a blogpost about the security quick scan performed by Radically Open Security as part of the NLnet grant https://blogs.gnome.org/jsparber/2022/09/27/fractal-security-audit/
> ![](3a48fa5b0fd4b0e1f67fdf5dd8c51c683a6b7573.png)

# Documentation

[Julian 🍃](https://matrix.to/#/@julianhofer:gnome.org) says

> I've published the final section of the Libadwaita chapter in [GUI development with Rust and GTK 4](https://gtk-rs.org/gtk4-rs/stable/latest/book/todo_4.html).
> It has been reviewed by Ivan Molodetskikh and Alexander Mikhaylenko.
> {{< video src="332ad1070e6debdf9dbdadbd86c639599619fb58.webm" >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!