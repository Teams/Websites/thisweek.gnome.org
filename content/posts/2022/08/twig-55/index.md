---
title: "#55 Wallpapers & Screenshots"
author: Chris 🌱️
date: 2022-08-05
tags: ["solanum", "newsflash", "amberol", "epiphany", "gnome-photos", "gnome-shell"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 29 to August 05.<!--more-->

# Core Apps and Libraries



### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) reports

> I [optimized](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2394) GNOME Shell's layout performance a little bit, and then [a tiny bit more](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2395). This code runs every frame during the overview animations so it's important that it's fast.

### Photos [↗](https://gitlab.gnome.org/GNOME/gnome-photos)

Access, organize and share your photos on GNOME

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> GNOME Photos now uses the Wallpaper portal for setting a picture as your background / lockscreen.

### Web [↗](https://gitlab.gnome.org/GNOME/epiphany)

Web browser for the GNOME desktop.

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) announces

> GNOME Web gets a more friendly way to get screenshots. It is now possible to do this through the context menu by choosing the Take Screenshot option or pressing Shift + Ctrl + S.
> ![](fhZkyJCfOUmQSfOdIyBOEiHP.png)
> ![](HTCsEZjbAcDaYOuvOooUavQX.png)

# Circle Apps and Libraries



### Solanum [↗](https://gitlab.gnome.org/World/Solanum)

Balance working time and break time.

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> Solanum now has the ability to completely reset sessions without restarting the app. New contributor Dani Rodríguez is responsible for implementing this. Thank you, Dani!

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[Jan Lukas](https://matrix.to/#/@jangernert:matrix.org) says

> NewsFlash recently gained support for Nextcloud News. It only supports API v1.3 which will be available in their next release 18.1.1. So Make sure to update you Nextcloud News if you plan to sync with NewsFlash.
> Since my last update on TWIG was some time ago I should also mention the updated tagging workflow: you can create tags & tag articles now in one step without a single click. The login & reset pages have gotten a libadwaita makeover and are now more mobile friendly. And of course the new libadwaita about dialog has to be mentioned.
> ![](hcdtJhLyMXSdRIzsLhdWIYpB.png)
> ![](xXPRlevmcJTBAVSRJuhIQUzA.png)
> ![](kkjpGtgctRPmJnufZJmRumEe.png)

### Amberol [↗](https://gitlab.gnome.org/ebassi/amberol/)

Plays music, and nothing else.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> Amberol 0.9.0 is out, now with fuzzy matching when searching inside the playlist, loads of bug fixes in the code, style, and dependencies that deal with loading metadata, and translation updates. You can get the new release from [Flathub](https://flathub.org/apps/details/io.bassi.Amberol) or the software center application nearest to you.

# Third Party Projects

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> Loupe now has the ability to show file properties. NOTE: These are based on the file's basic info and not exif data currently. Properties based on exif data will come later.
> ![](59789fcb7d0f457c125376adeefa6630da7de1db.png)

[ranfdev](https://matrix.to/#/@ranfdev:matrix.org) announces

> I'm announcing **Lobjur**, a simple GTK4 client for https://lobste.rs.
> 
> **Features**:
> 
> * Browse the hottest and most recently active stories
> * View the comments of each story
> * Browse stories by tag, by domain, or by user.
> * View some information about the user who just commented below that awesome story
> 
> Also, as far as I know, this is the first application on flathub to use **ClojureScript** and GTK.
> 
> Get this application from [Flathub](https://flathub.org/apps/details/com.ranfdev.Lobjur) !
> ![](RtesUIaIQpdXlqhxdSkieRoH.png)

[knuxify](https://matrix.to/#/@knuxify:cybre.space) announces

> The first version (and first minor patch) of [Ear Tag](https://github.com/knuxify/eartag) has been released!
> Ear Tag is a small and simple music tag editor meant to be used for editing single files, rather than entire music collections like many other tagging programs. You can [get it on FlatHub](https://flathub.org/apps/details/app.drey.EarTag), or get the code from [the release page](https://github.com/knuxify/eartag/releases/tag/0.1.1).
> ![](ODIthXeWVKFDdkBMsJHEtfiP.png)

[cwunder](https://matrix.to/#/@ovflowd:gnome.org) says

> One of our GSoC students Anupam Kumar had great progress on the development of Chromecast (Cast v2) protocol support on GNOME Network Displays. You can read it more [here](https://kyteinsky.github.io/p/chromecast-protocol/).

# Documentation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> Two new tutorials are now available on the GNOME developers documentation website: [how to implement drag and drop with GTK](https://developer.gnome.org/documentation/tutorials/drag-and-drop.html) and [how and when to use composite widget templates](https://developer.gnome.org/documentation/tutorials/widget-templates.html). The full project code for the [Getting Started tutorial](https://developer.gnome.org/documentation/tutorials/beginners/getting_started.html) is also now [available on GitLab](https://gitlab.gnome.org/Teams/documentation/getting-started-tutorial).

# GNOME Shell Extensions

[lupantano](https://matrix.to/#/@lupantano:matrix.org) reports

> [ReadingStrip](https://github.com/lupantano/readingstrip) is an extension for Gnome-Shell. It works as a reading guide for computer and this is really useful for people affected by dyslexia. It works great in helping children focusing to read very well, it marks the sentence that they are reading and hides the previous and the next one. It's already used in education projects at schools, it puts the attention on screen but it's also really useful for programmers and graphic designers who want to check their works.
> ![](pnUzwoPKfXZVeeyvVvsMfHUV.png)

# Miscellaneous

[Vojtěch Perník](https://matrix.to/#/@pervoj:matrix.org) reports

> The new [Czech GNOME community website](https://cz.gnome.org/), along with the [Czech Matrix room](https://matrix.to/#/#gnome-cz:matrix.org) and [Mastodon](https://mastodon.social/@gnome_cz)/[Twitter](https://twitter.com/gnomecz) accounts, has been released! Thanks to the GNOME Infrastructure team for their help with publishing the page, and to Martin Thibault and Felix Häcker for their help with creating the Matrix room!

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> GNOME Nightly has now the necessary infrastructure for building and deploying your nightly apps targetting aarch64. More details on the following blog post. https://blogs.gnome.org/alatiera/2022/08/04/aarch64-for-gnome-nightly-apps/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
