---
title: "#56 Refined Documentation"
author: Chris 🌱️
date: 2022-08-12
tags: ["gnome-calls", "just-perfection", "tracker", "newsflash", "gnome-contacts", "gjs"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 05 to August 12.<!--more-->

# Core Apps and Libraries



### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) announces

> Carlos Garnacho landed a major performance fix in Tracker Miners. Directory renames should now be processed within a few seconds even when many files are indexed. See https://gitlab.gnome.org/GNOME/tracker-miners/-/merge_requests/400 for details.

### GNOME Contacts [↗](https://gitlab.gnome.org/GNOME/gnome-contacts)

Keep and organize your contacts information.

[nielsdg](https://matrix.to/#/@nielsdg:gnome.org) says

> Contacts can now [import and export](https://gitlab.gnome.org/GNOME/gnome-contacts/-/merge_requests/183) contacts in the vCard format (usually recognized by their .vcf file extension). This was the most popular feature request in the Contacts issue tracker for quite a while, so this should make quite a few people happy 😁

### Calls [↗](https://gitlab.gnome.org/GNOME/calls)

A phone dialer and call handler.

[Evangelos](https://matrix.to/#/@evangelos.tzaras:talk.puri.sm) announces

> Calls now allows [sending SMS](https://gitlab.gnome.org/GNOME/calls/-/merge_requests/579) from the call history 🎉
> and starts up faster and has more fluid scrolling  with [large histories](https://gitlab.gnome.org/GNOME/calls/-/merge_requests/576) of more than 1k entries (which should especially help on weaker hardware) \o/
> ![](XVLbgVHHUVokGDpzYMtUCgaC.png)
> {{< video src="cLvboPZNROxEGGbOWiNYeOGj.webm" >}}
> {{< video src="rUMezyqZvnEbmKNzWcgPgpsA.webm" >}}

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> This week, a new version of GJS was released for GNOME 43.beta.
> * It pulls in the JS engine from Firefox 102, bringing such APIs as `Object.hasOwn()` and `Intl.supportedValuesOf()`. With contributions from Evan Welsh, Chun-wei Fan, and myself.
> * It's now possible to use `GObject.BindingGroup.prototype.bind_full()` with JS functions. Previously this method was unusable in JS. Thanks to Florian Müllner.
> * Gio.FileEnumerator is now iterable, both synchronously (with for-of or array spread syntax) and asynchronously (with for-await-of). Thanks to Sonny Piers.
> * Updates to the in-repo [documentation](https://gitlab.gnome.org/GNOME/gjs/-/tree/master/doc) and [examples](https://gitlab.gnome.org/GNOME/gjs/-/tree/master/examples), by Andy Holmes and Sonny Piers.

# Circle Apps and Libraries

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) announces

> This week, [File Shredder](https://apps.gnome.org/app/com.github.ADBeveridge.Raider/) joined GNOME Circle. File Shredder allows you to securely delete files that should not be recoverable. Congratulations!
> ![](bff7d7b214356ccef3eb22d30401fd1f6864ef1b.png)

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[Jan Lukas](https://matrix.to/#/@jangernert:matrix.org) announces

> NewsFlash gained support for basic highlighting of code blocks.
> ![](jHVPqgfJlsUyaigwSUpuCWHM.png)

# Third Party Projects

[Daudix UFO](https://matrix.to/#/@daudix_ufo:matrix.org) reports

> [Adwaita Manager](https://github.com/AdwCustomizerTeam/AdwCustomizer) was significantly redesigned, Thanks for hard work of @0xMRTT who maked it real and @daudix-UFO for providing design mockups.
> 
> Adwaita Manager is a tool that allows you to customize Libadwaita applications and the adw-gtk3 theme with simple UI, as well with Material You Palette generation from wallpaper, Thanks @avanishsubbiah for porting Monet engine to Python!
> 
> The name was changed to Gradience to remove any confusions. The project still available at the same [URL](https://github.com/AdwCustomizerTeam/AdwCustomizer)
> 
> Also, @ArtyIF has migrated Adwaita Manager to Adwaita Manager Team, which allowed to add new maintainers, @0xMRTT, @daudix-UFO and @AndroGR.
> 
> More amazing changes will come in near future!
> 
> Note: Adwaita Manager is NOT an official GNOME app, the name will change in v0.2.0
> ![](pVokPdQJQeoGsxmwDnqGEadk.png)
> ![](BDRWloyUNOKcdiYqbLUHLldJ.png)

# Documentation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> Pages on the [GNOME Developers Documentation website](https://developer.gnome.org/documentation) now have a link to let you easily view and edit the source text in GitLab; if you spot an issue, or if you want to improve the documentation, you can now do that directly from your web browser.

[lwildberg](https://matrix.to/#/@lw64:gnome.org) reports

> This week the port of the whole [getting started tutorial](https://developer.gnome.org/documentation/tutorials/beginners/getting_started.html) to Vala on developer.gnome.org was merged and is now available online! Also the widget template tutorial was ported thanks to Vojtěch Perník .
> ![](a3a41b06fcbef33f0f2e9f003ff3d3c4d335e23e.png)

# GNOME Shell Extensions



### Just Perfection [↗](https://extensions.gnome.org/extension/3843/just-perfection/)

A tweak tool to customize the GNOME Shell and to disable UI elements.

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) says

> Just Perfection extension version 21 (code name Reynolds) has been added 7 new features including:
> 
> * OSD position
> * Alt Tab icon size
> * Alt Tab window preview size
> * Alt Tab window preview icon size
> * Dash separator visibility
> * Looking glass size
> * Take screenshot button visibility in window menu
> 
> We also had some [bug fixes](https://gitlab.gnome.org/jrahmatzadeh/just-perfection/-/blob/main/CHANGELOG.md#2100-reynolds-2022-08-06).
> 
> This version named after English painter Joshua Reynolds.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
