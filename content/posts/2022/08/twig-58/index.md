---
title: "#58 Cartographer's Delight"
author: Chris 🌱️
date: 2022-08-26
tags: ["gnome-maps"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 19 to August 26.<!--more-->

# Core Apps and Libraries



### Maps [↗](https://wiki.gnome.org/Apps/Maps)

Maps gives you quick access to maps all across the world.

[mlundblad](https://matrix.to/#/@mlundblad:matrix.org) reports

> Upcoming Maps GTK 4 and libshumate port with the last touch-ups by Chris 🌱️ for GNOME 43, also sporting porting from libsoup 2 to libsoup 3 and using the OAuth 2 protocol instead of OAuth 1.1a for signing up editing points-of-interest in OpenStreetMap
> ![Maps GTK4](KWaKKUcxSoBLhzaWkbFITqdR.png)

# Third Party Projects

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) announces

> [Login Manager Settings](https://realmazharhussain.github.io/gdm-settings) v1.0 is in beta now.
> 
> * Thanks to [Thales Bindá](https://github.com/thalesruan), the app has a new icon that follows [GNOME HIG](https://developer.gnome.org/hig).
> * It got a few new dialogs to show errors to the user instead of panicking on the terminal.
> * Previously, the app froze after hitting 'Apply' until it was done applying settings. That has been fixed.
> * The app now shows a log-out dialog (if required) after applying settings.
> * When applying current display settings, scaling is also applied (may not work on all systems).
> * The app is now DBusActivatable
> 
> But many of the changes happened to the code itself, making it easier to work on the app in future. One of those changes was how the settings were being saved; which means the users will lose some of their settings. I will refrain from making such user-experience breaking changes in the future though. I only let it happen because the app was new, incomplete, still in a version below 1.0 and the change would make coding experience dramatically better.
> ![Login Manager Settings main screen](wSLGUtdgfazMKEOkDRNfOEIb.png)

[Daudix UFO](https://matrix.to/#/@daudix_ufo:matrix.org) announces

> This week, [Gradience](https://github.com/GradienceTeam/Gradience) v0.2.0 released 🚀 here are some exciting updates:
> 
> * Added a presets manager, with it you can rename, delete and download other users' presets
> * Added a welcome screen
> * Improved monet theme generation
> * Small UI improvements
> 
> You can download v0.2.0 from [GitHub](https://github.com/GradienceTeam/Gradience/releases/tag/0.2.0). Gradience will be available on Flathub very soon 🎉
> ![Gradience startup tour](sJsiZFLekBPcGFEamosGgkFX.png)
> ![Gradience preset search](jPOXMlWPstAaxjFxsWSJzBWc.png)
> ![Gradience color customization](pWJpWcPqhfCogPIWDYNXtrca.png)

# Documentation

[federico](https://matrix.to/#/@federicomena:matrix.org) announces

> Librsvg now has a development guide, intended for interns and newcomers to the code.  It's available at https://gnome.pages.gitlab.gnome.org/librsvg/devel-docs/index.html

# GNOME Shell Extensions

[oae](https://matrix.to/#/@oae:gnome.org) announces

> I have released the first version of [Pano](https://github.com/oae/gnome-shell-pano). An extension that manages the clipboard history. Currently, it supports;
> 
> * Code blocks (w/ syntax highlighting)
> * Color codes (hex/rgb)
> * Images (w/ size and resolution information)
> * Links (w/ previews)
> * Texts
> * File Operations (Cut/Copy)
> ![GNOME Shell with the Pano extension](b49ad8aec622bcdd49361acf6d3df9ff036f28ef.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
