---
title: "#71 Increased Circle"
author: Felix
date: 2022-11-25
tags: ["money", "boatswain", "loupe", "gradience", "tagger"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 18 to November 25.<!--more-->

# Circle Apps and Libraries

### Boatswain [↗](https://gitlab.gnome.org/World/boatswain/)

A guiding hand when navigating through streams.

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) announces

> This week, [Boatswain](https://apps.gnome.org/app/com.feaneron.Boatswain/) joined GNOME Circle. Boatswain allows you to control Elgato Stream Deck devices. Congratulations!
> ![](f9826921b72cebebdbaa69270795fd47c9a6fd73.png)

# Third Party Projects

[martinszeltins](https://matrix.to/#/@martinszeltins:matrix.org) announces

> Introducting a new game written in C, GTK4, Blueprint and Libadwaita - Who Wants To Be a Millionaire. Test your knowledge to see if you can answer all questions and become a virtual millionaire! This is the very first release and I am open to contributions to make this game even better if anyone wants to contribute.
> 
> The game has landed on Flathub - https://flathub.org/apps/details/lv.martinsz.millionaire

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> If you like the Perl programming language, and you wish to use to write your GNOME applications, you can now do so by using these two modules:
> 
> * [Gtk4](https://gitlab.gnome.org/ebassi/perl-gtk4)
> * [Adwaita](https://gitlab.gnome.org/ebassi/perl-adwaita)
> 
> They are currently pretty bare bones, but the plan is to improve them with additional, more idiomatic overrides for the C API, before being published on CPAN. Help is welcome!

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

An easy-to-use music tag (metadata) editor.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Tagger [V2022.11.2](https://github.com/nlogozzo/NickvisionTagger/releases/tag/2022.11.2) is here! This is a small release that fixes some issues users were facing.
> Here's the full changelog:
> * Tagger will now properly set an album art's mime type to be properly displayed in some music players
> * Changed 'Delete Tags' shortcut to Shift+Delete to allow for Delete button to work in entry widgets
> * Added Croatian translation (Thanks @milotype!)
> ![](JxbhZajClkNnRGZDyxYpekMo.png)

### Money [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Money [V2022.11.1](https://github.com/nlogozzo/NickvisionMoney/releases/tag/2022.11.1) is here! This release features a brand new look for Money! This new redesign features a new way of organizing groups and transactions, allowing for a quick overview of all transactions and an easy way to filter your account view. The TransactionDialog was also redesigned to make creating and editing transactions easier. Besides the big redesign, we also added the ability to transfer money from one account to another with ease.
> Here's the full changelog:
> * We completely redesigned the application to provide an easier and more efficient way to manage your accounts, groups, and transactions
> * Added the 'Transfer Money' action to allow for transferring money to another account file
> * Added support for filtering transactions by type, group, or date
> * You can now double-click a .nmoney file and it will open directly in Money
> * The CSV delimiter has been changed to a semicolon (;)
> * Fixed an issue where some monetary values were displayed incorrectly
> * Fixed an issue where repeated transactions would not assign themselves to a group
> ![](xUUcDIgBNAIqzoTwZhlSDzPC.png)
> ![](mEOeMmiZOwzJZqPUdBhPyOkS.png)

### Loupe [↗](https://gitlab.gnome.org/BrainBlasted/loupe)

A simple and modern image viewer.

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) reports

> Loupe gained support for zooming and scrolling in images via many different input types, including touchpad and touchscreen gestures. Combined with some cleanups and added keyboard shortcuts, Loupe now provides the basic features of an image viewer.
> {{< video src="c3cc75d78bbfcc3eb36c61a256de8e0c798d321e.mp4" >}}

### Gradience [↗](https://github.com/GradienceTeam/Gradience)

Change the look of Adwaita, with ease.

[Daudix UFO](https://matrix.to/#/@daudix_ufo:matrix.org) reports

> Gradience [0.3.2](https://github.com/GradienceTeam/Gradience/releases/tag/0.3.2) is out! This version fixes some major issues and introduces some under-the-hood improvements, as well as some new features, some of them are:
> 
> * Issues with the [Firefox GNOME theme](https://github.com/rafaelmardojai/firefox-gnome-theme) plugin under Flatpak are fixed
> * CSS now loads correctly after applying a preset
> * Fixed an issue with presets always being saved as `User.json`
> * Presets are now removed correctly
> * The internal structure was refactored
> * Various typos were fixed
> * The README was fully rewritten
> * All screenshots are now in high resolution
> * New and updated translations

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

