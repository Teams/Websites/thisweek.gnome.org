---
title: "#87 Editable Shortcuts"
author: Felix
date: 2023-03-17
tags: ["denaro", "fractal", "portfolio", "tubeconverter", "gnome-builder", "libadwaita"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 10 to March 17.<!--more-->

# GNOME Development Tools

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[hergertme](https://matrix.to/#/@hergertme:gnome.org) says

> Just in time for GNOME 44, Builder has gained support for editable shortcuts. You can individually override a number of shortcuts from the Preferences dialog. Popover menus and the Keyboard Shortcuts window will reflect changes made by the user.
> ![](7b7e97703cbcfe2ecd6cb593ad793c05a2edd5bf.png)

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alex (they/them)](https://matrix.to/#/@alexm:gnome.org) says

> libadwaita 1.3 is out: https://blogs.gnome.org/alexm/2023/03/17/libadwaita-1-3/

# GNOME Circle Apps and Libraries

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) reports

> This week, [Clairvoyant](https://apps.gnome.org/app/com.github.cassidyjames.clairvoyant/) joined GNOME Circle. Clairvoyant gives you psychic answers to your questions. For free. Congratulations!
> ![](144a7b97fdcfa603c6d37817323f5b5aba76d72d.png)

# Third Party Projects

[Casper Meijn](https://matrix.to/#/@caspermeijn:matrix.org) reports

> This week I [released](https://www.caspermeijn.nl/posts/read-it-later-0.3.0/) version 0.3.0 of Read It Later.
> This is a client for [Wallabag](https://www.wallabag.it/en), which allows you to save web articles and read them later.
> The significant changes are the upgrade to GTK 4, some fixed bugs, and new translations.
> Download on [Flathub](https://flathub.org/apps/details/com.belmoussaoui.ReadItLater).

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

An easy-to-use video downloader (yt-dlp frontend).

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Tube Converter [V2023.3.0](https://github.com/nlogozzo/NickvisionTubeConverter/releases/tag/2023.3.0) is here! Introducing the all-new Tube Converter completely rewritten in C#. First off I must thank @fsobolev and @DaPigGuy because this release would not be possible without them ❤️ Besides being rewritten in C#, this release includes many new features compared to the previous C++ version: Users can now download playlists, there is a new queue system to more easily manage downloads, plus a redesigned interface with support for smaller screens. We can't wait for all of you to try this release 🥳
> ![](hPWmnMiLoGeWPdCJGBkFmUWD.png)
> ![](WUwieGeRicIFjScTlwjIRjck.png)

### Portfolio [↗](https://github.com/tchx84/Portfolio)

A minimalist file manager for those who want to use Linux mobile devices.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) says

> After a long hiatus, a new [release](https://blogs.gnome.org/tchx84/2023/03/15/portfolio-0-9-15/) of Portfolio is out 📱🤓. This new release comes with important bug fixes, small-detail additions and a few visual improvements. Download on [Flathub](https://flathub.org/apps/details/dev.tchx84.Portfolio).
> ![](NuErnXPXSkwNhwtSqENunOgU.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) says

> As predicted 3 weeks ago, Fractal 4.4.2 has been released and is available on Flathub.
> 
> As a reminder, there are no new features, but it makes Fractal compatible with newer versions of our dependencies. Which means our Flatpak has been rebased on top of the GNOME 43 runtime just in time for the release of GNOME 44! Big thanks to everyone who tested the beta version and feel free to uninstall it.
> 
> In another news, let's talk a bit about Fractal 5…
> 
> We have finally implemented one of the most annoying missing features: sending read receipts and updating the fully-read marker. What is doubly great about this, is that we are now only one feature away from being regression-free compared to our stable release (and a merge request is opened for the last one)!
> 
> This means that a beta release is around the corner, but we also have serious performance issues to resolve first. We hope that switching to the new store backend currently developped in the Matrix Rust SDK will fix some of it, but we still need to investigate properly how we can improve the situation.
> 
> That's all for this week, don't hesitate to come say hi in [#fractal:gnome.org](https://matrix.to/#/#fractal:gnome.org). If you would like to help us, take a look at our [newcomers issues](https://gitlab.gnome.org/GNOME/fractal/-/issues/?label_name%5B%5D=4.%20Newcomers) (and don't forget to read our `CONTRIBUTING.md` first).

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> [Denaro V2023.3.0-beta2](https://github.com/nlogozzo/NickvisionMoney/releases/tag/2023.3.0-beta2) is here! This release features more customization for an account's custom currency, the ability to add a password to an exported PDF file, and many fixes when importing QIF and OFX files :)
> Here's the full changelog across betas:
> * Added the ability to customize the decimal and group separators used per account
> * Added the ability to password protect a PDF file
> * Fixed an issue where OFX files with security could not be imported
> * Fixed an issue where QIF files could not be imported on non-English systems
> * Fixed an issue where editing a transaction with a receipt would crash the application
> * The UI is now built using blueprint
> ![](EXIFbWlSDrDnSUQDTXzYvjwN.png)
> ![](azmRFVUwYxoJDKecRNiBnArh.png)

# Shell Extensions

[sereneblue](https://matrix.to/#/@serenedev:matrix.org) announces

> [Colosseum](https://extensions.gnome.org/extension/4207/colosseum/) and [krypto](https://extensions.gnome.org/extension/1913/krypto/) extensions have received support for GNOME Shell 44.
> ![](JbKrBWKMwSjvdibwWcplpxAJ.png)

# GNOME Foundation

[Kristi Progri](https://matrix.to/#/@kristiprogri:gnome.org) announces

> I am Kristi Progri, the Program Manager at the GNOME Foundation, where I am in charge of conferences, events and ensuring that everything is run efficiently in our community.
> 
> The Linux App Summit is approaching and we have just finished organising the conference schedule, sending emails to all speakers, opening the call for volunteers and gathering the supplies we will need to make the conference run smoothly, and made sure we have enough people present to support us. Talking and communicating with sponsors is another aspect of my duties.
> If you or your business is interested in sponsoring the Linux App Summit monetarily, please contact us by email at kprogri@gnome.org.
> 
> After Linux App Summit the other conference that we have is GUADEC.
> GUADEC is the GNOME community’s largest conference and this year is happening in Riga, Latvia from 26th-31st of July. The call for papers is open until March 27th, so here's your chance to submit your proposals.
> 
> Here are the tasks we have been working on as a team:
> * Reviewing the papers submission page
> * Updating the website with the latest information about the city, attractions and transportation
> * Constantly talking with the venue people making sure we have the rooms we want to rent for the event
> * Forming the papers team, etc.
> 
> In addition to these activities, I am working on the Annual Report, for which I would appreciate some help. 
> These are the tasks that we need help with: 
> * Contacting and sending reminders to everyone who is writing the articles;
> * Collecting all these materials and creating a document;
> * Update the Gitlab issues with the latest update on annual report progress
> 
> If you are interested in volunteering to help me with that, please ping me either in matrix @kristiprogri:gnome.org or by email.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

