---
title: "#98 Fast Searching"
author: Felix
date: 2023-06-02
tags: ["tubeconverter", "phosh", "nautilus", "libadwaita"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 26 to June 02.<!--more-->

# GNOME Core Apps and Libraries

### Files [↗](https://gitlab.gnome.org/GNOME/nautilus)

Providing a simple and integrated way of managing your files and browsing your file system.

[antoniof](https://matrix.to/#/@antoniof:gnome.org) says

> Files search is faster. A series of performance optimizations by Carlos Garnacho gave momentum to a testing and developing team effort. There are further optimizations ahead, but the difference can already be felt in the [Nightly flatpak](https://wiki.gnome.org/Apps/Nightly).

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/they)](https://matrix.to/#/@alexm:gnome.org) announces

> I just landed [`AdwNavigationSplitView`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.NavigationSplitView.html) - the other part of replacing `AdwLeaflet`. This widget displays sidebar and content side by side or inside an `AdwNavigationView`. Meanwhile, `AdwHeaderBar` automatically hides redundant window buttons when used inside a split view.
> 
> `AdwNavigationSplitView` also manages sidebar width as a percentage of the full width when possible, as well as finally allows to implement the style from the original mockups that was impossible with `AdwLeaflet`
> {{< video src="4c0947f12f11414f24ad918c3a657961d86ea249.mp4" >}}

# Third Party Projects

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) announces

> I just released Forge Sparks, a simple git forges (Github, Gitea, Forgejo) notifier app.
> 
> You can get it on [Flathub](https://flathub.org/apps/com.mardojai.ForgeSparks), and help translate it on [Weblate](https://hosted.weblate.org/engage/forge-sparks/).
> ![](SNJpymGpImWkmHapWfwLkkFW.png)

[gregorni](https://matrix.to/#/@gregorni:gnome.org) reports

> This week, I released [Calligraphy](https://flathub.org/apps/io.gitlab.gregorni.Calligraphy), an app that turns your text into large ASCII banners. Spice up your online conversations and add that extra oomph to your messages!
> ![](c6ed22c886365d8d2d9dd43052cfd4fce6bffeb0.png)
> ![](470d17f50ed096dce14556989d873b63bfdb3394.png)
> ![](848b375c7d339999f9b362259b85ffc0033de97e.png)
> ![](0bd7af1f03b2eeffb0c4c2995bb2b3887d1e3117.png)

[Iman Salmani](https://matrix.to/#/@imansalmani:matrix.org) announces

> [IPlan 1.3.0](https://github.com/iman-salmani/iplan) is now out!
> Changes this week contain Code refactoring and UI improvements:
> * New widgets for selecting Date and Time
> * Changing duration of the new record by setting end time
> * Add Brazilian Portuguese Translation thanks to Fúlvio Leo
> You can get it on [flathub](https://flathub.org/apps/ir.imansalmani.IPlan).
> ![](lxaHsJpOrRVbVHhnCtVfSTCS.png)

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Get video and audio from the web.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Tube Converter [V2023.6.0](https://github.com/NickvisionApps/TubeConverter/releases/tag/2023.6.0-p1) is here! We have been hard at work implementing many new features, configuration options, user interface improvements, and plenty of bug fixes for this update.
> 
> I'd like to especially thank @fsobolev (my right-hand man), @soumyaDghosh (our wonderful snap maintainer), @DaPigGuy (our continuous tester and feature implementer), and all of the other testers, contributors and especially translators who put in countless time to make Tube Converter be the best that it can be! We wouldn't be here if it wasn't for all of your support ❤️
> 
> Here's the full changelog:
> 
> * Added the ability to upload a cookie file to use for media downloads that require a login
> * Added support for downloading media as M4A
> * Added more configurable options for aria2 downloader
> * Added options to configure when completed download notifications are shown
> * Added the ability to clear completed downloads
> * Added the ability to disallow conversions and simply download the appropriate video/audio format for the selected quality without converting to other formats
> * Overwrite Existing Files is now a global option in Preferences instead of an individual setting in the AddDownloadDialog
> * Tube Converter will check the clipboard for a valid media url and when AddDownloadDialog is opened
> * Fixed an issue that prevented downloading m3u8 streams
> * Fixed an issue that prevented downloading media to NTFS drives
> * Updated UI/UX
> * Updated translations (Thanks everyone on Weblate!)
> ![](zJJFtyBXAqXlNDbeCJKbkfpo.png)
> ![](arssdDsXWMvRKTFfWMZsmozK.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) 0.28.0 is out. This is a "small things matter" release: Several transitions have been smoothed out.  Notifications can unblank the screen and you can set at which urgency this happens. `<super>`-key can open the overview. Pressing and holding down the volume button works now (no more tapping multiple times). The lockscreen works on smaller displays and libcall-ui was updated to 0.1.0 bringing in some visual improvements.
> ![](sepafXXOHQeTFVxQxKFVzpLh.png)

# Miscellaneous

[feborges](https://matrix.to/#/@felipeborges:gnome.org) says

> GNOME will have an Outreachy intern working on "Make GNOME platform demos for Workbench"!
> 
> We are happy to announce that GNOME is sponsoring an [Outreachy](https://outreachy.org) internship project for the May-August cohort where the intern will be working on “[Make GNOME platform demos for Workbench](https://gitlab.gnome.org/Teams/Engagement/internship-project-ideas/-/issues/27)".
> 
> [Jose Hunter](https://josecodes.hashnode.dev/) will be working with mentor [Sonny Piers](https://sonny.re/).
> 
> Stay tuned to [Planet GNOME](https://planet.gnome.org/) for future updates on the progress of this project!

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) announces

> This week has been a lot of churn of what used to be called paperwork. With the help with some expert bookkeepers we were able to get the info the accountant needed to file our taxes by their deadline. I am always grateful when we can get expert help; it saves me a lot of time trying to figure it out on my own and I can be more confident in the results.
> 
> Speaking of deadlines, today is also the deadline for Executive Director applicants. I've been collecting the applications and collating them for the search committee to go through. Will spend this weekend and early next week going through them before the next committee meeting.
> 
> The travel committee had a lot more requests this year for GUADEC than our budget to handle. Because GUADEC is such a priority for us and being able to send interns is so fundamental to getting them to integrate with our community, I liaised between the committee and the Board to request additional funding. Hoping to be able to process the rest of the travel sponsorships soon.
> 
> Speaking of GUADEC, the call for volunteers is still ongoing. If you are attending and have ever wondered what is necessary behind the scenes to get everything running smoothly, this is a great opportunity. Quick form is located here: https://events.gnome.org/event/101/surveys/14 — Make sure you also have registered for GUADEC first!
> 
> Deadline for GUADEC BoFs or Workshops: June 12 — If you want to run a BoF or Workshop at GUADEC this year but have not submitted a request yet, please do so by June 12. More details here: https://foundation.gnome.org/2023/05/25/guadec-2023-call-for-bofs/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

