---
title: "#99 Overlaid Sidebars"
author: Chris 🌱️
date: 2023-06-09
tags: ["gnome-builder", "furtherance", "libadwaita", "loupe", "denaro", "gnome-control-center", "solanum"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from June 02 to June 09.<!--more-->

# GNOME Core Apps and Libraries



### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/they)](https://matrix.to/#/@alexm:gnome.org) says

> libadwaita now has [`AdwOverlaySplitView`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.OverlaySplitView.html) as an overlay sidebar counterpart to `AdwNavigationSplitView` and a future replacement of `AdwFlap` that works with breakpoints. It provides a cleaned up API and the same dynamic sizing, styling and automatic window button handling as `AdwNavigationSplitView`
> ![Screenshot of a window with a utility pane at desktop sizes. There is a button at the top left with an icon mirroring the shape of the window. This button controls the visibility of the sidebar. It is currently toggled, and the sidebar shows next to the main content.](b62b886927b22ee22ff8b1fabfab156e5b6d912b.png)
> ![Screenshot of a window with a utility pane at a small width. There is a button at the top left with an icon mirroring the shape of the window. This button controls the visibility of the sidebar. It is currently toggled, and the sidebar overlays the main content.](7c08ab5a4ccdafe4041d7ae2655e5b22d7de9236.png)

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Pedro Sader Azevedo](https://matrix.to/#/@toluene:matrix.org) announces

> As part of an ongoing GSoC internship to integrate screen casting into GNOME, we're running a user survey to better understand the most common use-cases for this feature. Answer our poll on [reddit](https://www.reddit.com/r/gnome/comments/143wfnh/what_would_you_use_screen_casting_for_in_gnome/) and leave a comment if you have the time, it'll be greatly appreciated!

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[hergertme](https://matrix.to/#/@hergertme:gnome.org) announces

> Now that GJS has merged support for running scripts within a given mozjs Realm, libpeas has gained support for loading plugins written in JavaScript. Christian's blog post at https://blogs.gnome.org/chergert/2023/06/02/gjs-plugins-for-libpeas-2-0/ has more details on how to use this and how it will be integrated with GNOME Builder.

# GNOME Development Tools

[hergertme](https://matrix.to/#/@hergertme:gnome.org) announces

> An initial alpha release of libmks is available at https://gitlab.gnome.org/chergert/libmks/-/releases/0.1.0. Libmks provides a "Mouse, Keyboard, and Screen" implementation for QEMU utilizing the D-Bus display device. It supports importing DMA-BUF into GdkTexture with damages for more efficient rendering as part of a GTK 4-based application.
> 
> We expect a number of new features to land in the future ranging from touchpad integration, clipboard, and sound devices.

[hergertme](https://matrix.to/#/@hergertme:gnome.org) says

> Libdex, an async/await/futures library for GLib based applications, gained support for performing async I/O on traditional file-descriptors in the public API. This was always available internally and utilizes `io_uring` on Linux. Additionally, a number of new GIO abstractions were provided for `GDBusConnection` and `GSubprocess`. Fixes were also added to improve GObject Introspection integration.

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[hergertme](https://matrix.to/#/@hergertme:gnome.org) announces

> Hot on the heels of `libpeas-2` gaining support for JavaScript-based plugins powered by GJS, GNOME Builder has switched to JavaScript as it's dynamic language for plugins. That means you can write third-party plugins for Builder in C, C++, Rust, Vala, JavaScript, or any other language which supports both GObject Introspection and compiling to a shared library (`.so`).

# GNOME Incubating Apps



### Loupe [↗](https://gitlab.gnome.org/Incubator/loupe)

A simple and modern image viewer.

[Sophie (busy)](https://matrix.to/#/@sophieherold:gnome.org) says

> Loupe is now using [glycin](https://gitlab.gnome.org/sophie-h/glycin) to decode images. Glycin is a new library that loads images via sandboxed processes. This will allow adding support for additional image formats via additional loaders, similar to GdkPixbuf. The sandboxed image loading will increase security and neatly separate image loading from the Loupe UI. There will be more details announced on this in the future. You can try the changes via [GNOME Nightly](https://nightly.gnome.org/).
> 
> There have also been a bunch of smaller fixes and tweaks by new contributors. If you are interested in contributing as well, we are trying to keep a list of [“Newcomers” issues](https://gitlab.gnome.org/GNOME/Incubator/loupe/-/issues/?label_name%5B%5D=4.%20Newcomers) now.

# GNOME Circle Apps and Libraries



### Solanum [↗](https://gitlab.gnome.org/World/Solanum)

Balance working time and break time.

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> Solanum 4.0.0 is out with the feature to restart your pomodoro sessions from the beginning and some fixes for long-standing bugs.

# Third Party Projects

[ByteSeb](https://matrix.to/#/@byteseb:matrix.org) says

> I made a simple Rock, Paper, Scissors Libadwaita application. It serves as a demo for my free Libadwaita App Development video tutorial. You can inspect the source code at: https://github.com/ByteSeb/Duel
> And watch the tutorial at: https://youtu.be/WtvObZHhdf0

[Khaleel Al-Adhami](https://matrix.to/#/@adhami:matrix.org) announces

> In under 24 hours, I made Impression; the simplest app you could imagine to create bootable devices with a clean interface. All while following safe flatpak permissions!
> 
> https://flathub.org/apps/io.gitlab.adhami3310.Impression
> ![Screenshot of Impression's main window. It shows the name of the .iso file the user selected above a list of two devices with the top device selected. Below the list there's a big red "Flash!" button and a warning that all data on the selected device will be erased.](pBVyhEMGDZYQoccvivbuyctL.png)

[tfuxu](https://matrix.to/#/@tfuxu:matrix.org) reports

> Halftone 0.3.0 is out! It comes with a slightly revised look to the dithering page, new controls for brightness and contrast, and a button for opening previews in external image viewers. You can check it out on [Github](https://github.com/tfuxu/halftone) or download directly from [Flathub](https://flathub.org/apps/io.github.tfuxu.Halftone)
> ![Screenshot of Halftone's main window. The image selected is a picture of a frog on a plant, looking toward the viewer. Below it is a list of dithering options.](OjgMWpDCchbgKwLUuhWQxvSl.png)

[Iman Salmani](https://matrix.to/#/@imansalmani:matrix.org) says

> [IPlan 1.4.0](https://github.com/iman-salmani/iplan) is now out!
> 
> New features and updates
> * Backing up system
> * Calendar for Exploring tasks by date
> * Tasks can have date and reminders (Application will remain in background for handling reminders)
> * When a task doesn't have record, Timer button will move to options
> * Record edit window
> * Now users able to edit record duration by changing start time
> * Bug fixes, and UI improvements
> 
> You can get it from [flathub](https://flathub.org/apps/ir.imansalmani.IPlan).
> ![Screenshot of IPlan's main window, showing various projects and their associated tasks.](gusbhqivhPpXlKonosGpIpuq.png)
> ![Screenshot of IPlan's task configuration window, showing options to configure on the task and subtasks.](gkyQLKoXewKcxlBCgKEBOVrJ.png)

### Furtherance [↗](https://github.com/lakoliu/Furtherance)

Track your time without being tracked.

[Ricky Kresslein](https://matrix.to/#/@rickykresslein:matrix.org) reports

> [Furtherance](https://github.com/lakoliu/Furtherance) v1.8.0 was released and includes lots of new features:
> 
> * Today's total time ticks up with the current timer (optional)
> * Added 'This week' and 'Last week' options to Reports
> * Exported CSVs have a total seconds column
> * Total time is displayed across from the date in the task list
> * The task input and history list expand with the window
> * Preferences is sorted into three sections
> ![Screenshot of Furtherance's main window, showing tasks across multiple days. The timer is not running.](twMylpVrXhnUhhHZJYsEynzO.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Denaro [V2023.6.0-beta2](https://github.com/NickvisionApps/Denaro/releases/tag/2023.6.0-beta2) has been released this week! Version 2023.6.0 is shaping up to be a big release with many new features and fixes! 
> 
> Here's the current changelog:
> * Added a new account setup dialog to make it easier to configure new accounts
> * Added the ability to remove recent accounts from the list
> * Denaro will now suggest autocompletions for transaction descriptions
> * Moved deleting groups and transactions from their rows to their dialogs
> * Changed the default sorting order of new accounts to last to first by date
> * Amounts shown in the sidebar will now reflect that of the transactions shown in view
> * Fixed an issue importing CSV files
> * Improved UI/UX
> * Updated translations (Thanks to everyone on Weblate)!
> ![Screenshot of Denaro's account setup dialog.](nRKbhTYcCHnRYPsWPOyNHmYL.png)
> ![Screenshot of Denaro's transaction dialog.](EFIPkcWGaaSWBBHuneEtyPbI.png)

# Shell Extensions

[andyholmes](https://matrix.to/#/@andyholmes:gnome.org) announces

> Shell Extensions now support donation URLs!
> 
> Thanks to work by Martin Zurowietz and Javad Rahmatzadeh, extension developers can now add donation links for several popular services, as well as custom URLs.
> 
> See the documentation at https://gjs.guide/extensions/overview/anatomy.html#donations for details on how to get set up.

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) says

> This week, the GNOME Foundation has been getting everything ready for GUADEC 2023! The conference is only a month and a half away and there are still lots of tasks to do. The Riga team has planned two fun social outing options for our last conference day and we’re currently working on getting all the details up on the website as well as opening registration for both. In other event news, we will also be attending [FOSSY](https://2023.fossy.us/), hosted by the Software Freedom Conservancy, in July in Portland, OR (USA). If you’re in the area or planning to attend be sure to stop by our booth!
> 
> In addition to event planning, we’re working on finalizing all of the articles for last year’s annual report. Once all of the writing is complete I’ll be designing the report and preparing it for online and print publication in time for GUADEC. Something we can always use help with is collecting photos for annual reports. If you have any GNOME photos from 2021-2022 that you would like to share please take a look at our [GitLab issue](https://gitlab.gnome.org/Teams/Engagement/AnnualReport/-/issues/73) and consider submitting them. 
> 
> Reminders:
> The GUADEC 2023 call for BoFs and Workshops is open for a few more days. If you would like to host a BoF in Riga make sure to [submit your application](https://events.gnome.org/event/101/surveys/) by June 12.
> 
> We’re still looking for GUADEC 2023 sponsors! If you or your company would like to sponsor this year’s conference, you can find our brochure and learn more on [guadec.org](https://events.gnome.org/event/101/page/167-sponsors).

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
