---
title: "#80 Different Locales"
author: Felix
date: 2023-01-27
tags: ["glib", "phosh", "crosswords"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 20 to January 27.<!--more-->

# Third Party Projects

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> Maybe it's already known that Phosh's testsuite runs the phone shell under different locales. When doing so it takes screenshots so we can make sure translations fit under the size constraints of mobile devices and designers have an easy way to validate contributor changes. This weeks news is that we doubled the amount of screenshots taken covering most of the modal dialogs now.
> This is how it looks in Ukranian (the untranslated strings are from the tests itself which we don't bother translators with):
> ![](RoHCFbimCqbvgDKEYMPENLAV.png)

[slomo](https://matrix.to/#/@slomo:matrix.org) announces

> GStreamer 1.22 was released this Monday, including the improvements of one year of development. Some of the highlights of the release are
> 
>  * New gtk4paintablesink and gtkwaylandsink renderers
>  * AV1 video codec support improvements
>  * New HLS, DASH and Microsoft Smooth Streaming adaptive streaming clients
>  * Qt6 support for rendering video inside a QML scene
>  * Minimal builds optimised for binary size, including only the individual elements needed
>  * Playbin3, Decodebin3, UriDecodebin3, Parsebin enhancements and stabilisation
>  * WebRTC simulcast support and support for Google Congestion Control
>  * WebRTC-based media server ingestion/egress (WHIP/WHEP) support
>  * New easy to use batteries-included WebRTC sender plugin
>  * Easy RTP sender timestamp reconstruction for RTP and RTSP
>  * ONVIF timed metadata support
>  * New fragmented MP4 muxer and non-fragmented MP4 muxer
>  * New plugins for Amazon AWS storage and audio transcription services
>  * New videocolorscale element that can convert and scale in one go for better performance
>  * High bit-depth video improvements
>  * Touchscreen event support in navigation API
>  * H.264/H.265 timestamp correction elements for PTS/DTS reconstruction before muxers
>  * Improved design for DMA buffer sharing and modifier handling for hardware-accelerated video decoders/encoders/filters and capturing/rendering on Linux
>  * Video4Linux2 hardware accelerated decoder improvements
>  * CUDA integration and plugin improvements
>  * New H.264 / AVC, H.265 / HEVC and AV1 hardware-accelerated video encoders for AMD GPUs using the Advanced Media Framework (AMF) SDK
>  * New "force-live" property for audiomixer, compositor, glvideomixer, d3d11compositor etc.
>  * Lots of new plugins, features, performance improvements and bug fixes
> 
> See the release notes for more details https://gstreamer.freedesktop.org/releases/1.22/

### Crosswords [↗](https://gitlab.gnome.org/jrb/crosswords)

A simple Crossword player and Editor.

[jrb](https://matrix.to/#/@jblandford:matrix.org) announces

> GNOME Crosswords 0.3.7 was released. This release features a massive internal rewrite, unlocking new functionality and giving a performance boost. Among the user visible features:
> 
> * Custom game widget for adaptive layout, supporting animations
> * New supported puzzle type: Arrowwords
> * Support puzzles with zero or one column of clues, such as alphabet crosswords
> * New options for preferences dialog:
> 
>     - Preference: Hide puzzle sets by default and let the user select the ones they want.
>     - Preference: Hide puzzles after they're solved
> * Add tagging to puzzle-sets to provide more information to users
> * Add count of unsolved puzzles
> * Fix zoom for all game UI elements
> * Support horizontal and vertical cell dividers
> * Fixes for Onscreen Keyboard support
> 
> This release is also marked for mobile compatability.  Read the full [release announcement](https://blogs.gnome.org/jrb/2023/01/23/crosswords-0-3-7-adaptive-layout-animations-and-arrows/) for more information. It's available for download in Fedora and flathub.
> ![](uExeDZQUkqbZTCxFnLcIyFzX.png)
> {{< video src="dlqUPJzhgWVIyqMMbFTySBnt.webm" >}}

# Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Michael Catanzaro](https://matrix.to/#/@mcatanzaro:gnome.org) announces

> Natanael Copa deleted GLib's slice allocator, which will now internally use g_malloc() and g_free(). - https://gitlab.gnome.org/GNOME/glib/-/merge_requests/2935

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

