---
title: "#79 Research Results"
author: Felix
date: 2023-01-20
tags: ["denaro", "bottles", "tangram"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 13 to January 20.<!--more-->

# Miscellaneous

[Allan Day](https://matrix.to/#/@aday:gnome.org) announces

> I published the results of the gnome-info-collect research that was done last August - https://blogs.gnome.org/aday/2023/01/18/gnome-info-collect-what-we-learned/

# Circle Apps and Libraries

### Tangram [↗](https://github.com/sonnyp/Tangram)

A browser for your pinned tabs.

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> Tangram 2.0 is out and [available on Flathub](https://flathub.org/apps/details/re.sonny.Tangram).
> 
> * Ported to GTK4 / libadwaita
> * Responsive/mobile UI
> * New and clearer onboarding experience
> * Improved Web performance
> * Follow system light/dark theme
> 
> Thank you to everyone involved in the GTK4 port of WebKit ♥️
> ![](7ad3adaf00c0ba37237a99add7c0437e836d3490.png)
> ![](b249fb19ef566eba4694549433cae4911c2ecbae.png)
> ![](355d9096d239749c3d037bdab8d5ae04349d2a51.png)
> {{< video src="5e2ac303a6d6935cd8b0ab0574bec3287892668f.mp4" >}}

# Third Party Projects

[Cyber Phantom](https://matrix.to/#/@cyberphantom2527:matrix.org) reports

> Sudoku Solver, a new application written in rust using GTK4, libadwaita and blueprint has landed its first release this week on Flathub with version 1.0.1. It's a brand new way to pass time for all the Sudoku enthusiasts or people who like looking at random numbers on a grid.
> 
> This app was built with the sole purpose of learning about the the technologies behind GNOME and getting familiar with rust along the way. If you have any suggestions you can leave them on our gitlab, we'd appreciate that.
> 
> The app can be downloaded on Flathub - https://flathub.org/apps/details/io.gitlab.cyberphantom52.sudoku_solver
> ![](YbkdQgPyuIJsfhAyFicpPfUn.png)
> ![](LtqBAEtZYLgtcydSNgvruOlJ.png)
> ![](gzpjQRrIEWxvVhzBvnDvCnjn.png)
> ![](LQVyLGNwNxUWQoITbkkPxiJz.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Denaro [V2023.1.0 stable](https://github.com/nlogozzo/NickvisionMoney/releases/tag/2023.1.0) is here!
> For those of you who don't know, Money was rewritten in C# and with all it's great new features and rewrite comes a new name: Denaro (Money in Italian)
> 
> We have been hard at work over the past two months developing and constantly testing this release, bringing our users highly-requested features and providing an overall better experience when using the app.
> 
> I cannot thank enough all those who have translated the application, contributed new code, tested betas, and provided bug reports, including:
> 
> * My right-hand man: @fsobolev
> * @Suork
> * @chuangzhu
> * @oscfdezdz
> * @bordam
> * And everyone on @weblate
> 
> Here's a full changelog:
> 
> * Denaro is now available to translate on [Weblate](https://hosted.weblate.org/engage/nickvision-money/)!
> * Money has been completely rewritten in C# and now has a new name: Denaro! With the C# rewrite, there is now a new version of Denaro available on Windows!
> * Added an Account Settings dialog to allow users to customize their accounts better
> * Added an "Ungrouped" row to the groups section to allow filtering transactions that don't belong to a group
> * Added the ability to attach a jpg/png/pdf of a receipt to a transaction
> * Reworked the repeat transaction system and added support for a biweekly interval
> * Added the ability to export an account as a PDF
> * Added the ability to sort transactions by id or date
> * Added the ability to hide the groups section
> * Made a group's description an optional field
> * Performance improvements and better handling of large accounts
> ![](qZycfhpAIysfQxZorbhJqCUy.png)
> ![](BeFVWvmauhMOjMcQXSWVdclR.png)
> ![](VtvRfonJrdGFbIXYrFTiFfzI.png)

### Bottles [↗](https://usebottles.com/)

Easily run Windows software on Linux with Bottles!

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) says

> Bottles 50.0 was just released! First of, we ditched the monthly release cycle to one where we release a new version whenever we deem ready. This gives more time for us to implement and polish upcoming features without being pressured with time. So, we changed the version number, starting from 50.0, to avoid confusion as much as possible.
> 
> In this update, we mainly focused on fixing bugs and polishing the interface. Based on our testing and user feedback, we found that previous versions had a lot of performance issues and inconsistent interfaces throughout the app, so we decided to freeze feature additions to focus mainly on fixing bugs and improving the interface. Furthermore, we also agreed that we will mainly focus on them for the next couple of versions. The next versions won't be as "fun" as they used to be, but the improves will definitely provide a better and more polished experience!
> 
> To summarize the main improvements:
> 
> * Unfortunately, Bottles requires the internet to download data to provide the latest information for runners and other metadata, which slowed down the startup drastically. We spent a lot of time to optimize this part to make the startup load quicker. Now, the startup went from 3 seconds to about 1.5 seconds if you have a connection of 50KB/s!
> * Not only was startup slow, we also realized that loading each bottle had a slight delay and sluggish transition, which felt unpleasant. We also spent the time to improve the performance of loading information of a bottle, so it should now feel performant again!
> 
> Other changes include:
> 
> * Gamescope improvements and fixes
> * Dependency installation is faster and more stable
> * The health check has more information for faster debugging
> * NVAPI has a lot of fixes and is more stable, should now work properly
> * Fix crash when downloading a component
> * Backend code improvement by avoiding spin-lock
> * More variables for installer scripting
> * Fix onboard dialog showing “All ready” while it was in fact not ready
> * Improvement to build system
> * Enabling VKD3D by default when creating bottles for gaming
> * Fix crashes when reading Steam files with bad encodings
> * Fix components not updated correctly in the UI after installation/uninstallation
> * FSR fixes
> * Fix the issue when a program closes after it was launched from “Run executable”
> * Filter types of files when opening the file chooser
> 
> Again, this update isn't as fun as the other updates, but the experience should still feel much more responsive and snappier!
> 
> To read more about the update, feel free to read our [release post](https://usebottles.com/blog/release-50/)!

# GNOME Shell Extensions

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) announces

> A new version of **Weather O'Clock** has been released.
> An extension to display the current weather inside the pill next to the clock. 
> _GNOME Weather is required for this extension to function._
> 
> [Get it on extensions.gnome.org.](https://extensions.gnome.org/extension/5470/weather-oclock/)
> [Release Notes](https://github.com/CleoMenezesJr/weather-oclock/releases/tag/3)
> {{< video src="hPxVQOUGOqbwRGpPaIvjBQLx.mp4" >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

