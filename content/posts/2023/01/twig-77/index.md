---
title: "#77 Happy New Year!"
author: Felix
date: 2023-01-06
tags: ["gnome-shell", "eyedropper", "tangram", "gnome-sound-recorder", "money"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 30 to January 06.<!--more-->

# Core Apps and Libraries

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) announces

> The design team has been discussing alternatives to the app menu for indicating window focus in GNOME Shell, and we are seeking feedback on a prototype of the proposed new behavior.
> 
> ### The Why
> 
> Back in 2018, [we removed the unique menu items from the app menu](https://blogs.gnome.org/aday/2018/10/09/farewell-application-menus/). However, we kept the menu itself, so that it could be used as a window focus indicator, and so it can show a loading spinner for apps that are slow to show a window.
> 
> However, when doing user research over the past few years, we've noticed that people are often confused by the app menu in the top bar. Often, they think that it's a task switcher, a shortcut to a specific app, or don't understand what it is at all. It seems to be a trip hazard for new users.
> 
> Additionally, we've realized that the app menu doesn't work very well as a window focus indicator. It doesn't differentiate multiple windows of the same app, it's only present on the primary monitor, and it's sometimes located a long way from the window that it's indicating.
> 
> We're therefore investigating an alternative design for indicating window focus, which would both improve the focus indicator experience, and potentially enable us to stop showing the app menu in the top bar. The new design adds a subtle scale effect to newly focused windows when switching workspaces, super+tabbing, or closing a window.
> 
> For the loading spinner, we are still exploring alternatives, but feel that this is a relatively easy design problem to solve. Showing a spinner in the top bar is one obvious option.
> 
> ### Call for Testing
> 
> Thanks to @Leleat we have a prototype of the new focus indicator behaviour, in the form of a GNOME Shell extension. We're looking for people to test this extension and give feedback about whether it works for them as a replacement for the app menu as a focus indicator. If you never use the app menu it'd be still be helpful for you to test it, to know whether you notice it being gone.
> 
> To participate, install this extension: https://extensions.gnome.org/extension/5612/focus-indicator
> 
> Feedback can be provided as [comments on the Discourse announcement](https://discourse.gnome.org/t/window-focus-call-for-testing/13277) or on Matrix in #gnome-design.

# Circle Apps and Libraries



### Tangram [↗](https://github.com/sonnyp/Tangram)

A browser for your pinned tabs.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) announces

> [Tangram](https://apps.gnome.org/app/re.sonny.Tangram/) had 2 new releases
> 
> 1.5 available on Flathub is an update of Tangram GTK3 with an up to date GNOME runtime and WebKit engine
> 
> 2.0 beta available on flathub beta repository is a GTK4 / libadwaita port.
> 
> 🧪 It's note quite ready but if you'd like to test or use; follow these instructions:
> 
> ```sh
> flatpak kill re.sonny.Tangram
> flatpak remove re.sonny.Tangram
> flatpak --user remote-add --if-not-exists flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
> flatpak --user install flathub-beta re.sonny.Tangram
> flatpak run --branch=beta re.sonny.Tangram
> ```
> 
> ⚠️ NVIDIA GPU: There is a bug affecting graphics – expertise needed https://bugs.webkit.org/show\_bug.cgi?id=228268

# Third Party Projects

[دانیال بهزادی](https://matrix.to/#/@danialbehzadi:mozilla.org) reports

> 
> Carbuteror 4.0 released with porting the main window to libadwaita. It is a graphical application for connecting to Tor mainly designed with GNU/Linux mobile phones in mind. Under the hood it uses a CLI program named "Tractor" which lets users to connect to Tor in userspace.
> 
> It is already available in English, Persian, Spanish, Turkish, Croatian, Portuguese, German, French and Norwegian Bokmål
> 
> Source: https://framagit.org/tractor/carburetor/
> {{< video src="aad1dfd5929eb03d4e027330c5a43dd8b49a3f99.mp4" >}}

[Sjoerd Broekhuijsen](https://matrix.to/#/@sjoerdb93:matrix.org) announces

> This week I released version 1.3.4 of Graphs, a data manipulation and graphing tool written for the GNOME environment. The latest release adds the ability to get the indefinite integral or the derivative of your data, as well as the possibility to perform Fourier Transforms. Since the last time I wrote about Graphs, there's been a bunch of other exciting new features as well such as the ability to plot data on different axes, as well as a ton of bug fixes. As always, the latest version of Graphs is [available on Flathub](https://flathub.org/apps/details/se.sjoerd.DatMan)
> ![](vHAJKXUMgcoMiTPVAQDrBnvo.png)

### Money [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Money [V2023.1.0-beta3](https://github.com/nlogozzo/NickvisionMoney/releases/tag/2023.1.0-beta3) is here! This beta features a brand new repeat transactions system (with support for biweekly transactions), as well as some design tweaks and performance improvements.
> 
> [Money is also now available to translate on Weblate!](https://hosted.weblate.org/engage/nickvision-money/) We welcome all users to come and translate Money is their language :)
> 
> Continued thanks to all the developers, testers, and translations (especially Fyodor Sobolev) for all their hard work as we gear up for the stable release next week!
> 
> (Excuse the missing icons in the video, this is from a manual build on WSL. When running from Builder/flatpak this does not happen 🫠)
> {{< video src="NcgsXGQCOtwQfbkRMcrNAlPM.mp4" >}}

### Sound Recorder [↗](https://wiki.gnome.org/Apps/SoundRecorder)

A simple, modern sound recorder.

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> The Sound Recorder TypeScript port has been merged. TypeScript is easy to use on top of our GJS ecosystem, and provides compile-time type checking.

### Eyedropper [↗](https://flathub.org/apps/details/com.github.finefindus.eyedropper)

A powerful color picker and formatter.

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) announces

> I've released the 0.5.0 version of Eyedropper. The biggest improvements are the ability to change how a color is formatted (for example customize `rgb(r, g, b)` to `Color::rgb(r, g, b)`), two new color formats: LMS and Hunter-Lab, and a placeholder page when no color is picked.
> 
> Eyedropper can be downloaded from [flathub](https://flathub.org/apps/details/com.github.finefindus.eyedropper).

# Miscellaneous

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) reports

> Romain [fixed an issue in xdg-desktop-portal-gnome](https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome/-/merge_requests/38) where opening a folder would necessarily be in read-only. Which this change – the default will be read/write and the user can select “read only” – just like for files. As an app developer, it means you'll be able to use the document portal to let users choose save locations.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) announces

> The [OpenQA tests for GNOME](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/wikis/openqa/OpenQA-for-GNOME-developers#running-the-openqa-tests) are now set to run against every change to 'master' in the GNOME integration repo [gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta/). Thanks to everyone who helped us get to this point!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

