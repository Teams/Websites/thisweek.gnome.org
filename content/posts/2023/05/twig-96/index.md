---
title: "#96 Polished Settings"
author: Felix
date: 2023-05-19
tags: ["gaphor", "blueprint-compiler", "gnome-control-center", "gnome-software", "phosh", "workbench", "fractal", "escambo"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 12 to May 19.<!--more-->

# GNOME Core Apps and Libraries

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Allan Day](https://matrix.to/#/@aday:gnome.org) announces

> A collection of nice polish improvements landed in Settings this week:
> 
> * An information popup was added to the Users panel, to explain the autologin setting.
> * The user name setting was changed to use [AdwEntryRow](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.EntryRow.html) instead of a custom UI solution.
> * In the Sharing panel, descriptions were added to each of the sharing features.
> 
> There were also some code cleanups, with the introduction of a [new information button widget](https://gitlab.gnome.org/GNOME/gnome-control-center/-/commit/00a71d46da7f2097e67873d250e5a3a389cc261c).
> ![](22dd9c2d533f16739c53daa02dc7276f510ed4a6.png)
> ![](c560ddbd0a3f1e4a8a91d714a9b60e20c0f2c28a.png)

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Allan Day](https://matrix.to/#/@aday:gnome.org) reports

> In Software, Milan Crha added the ability to delete app data when uninstalling Flatpaks. When other types of apps are uninstalled, a reminder is shown that app data is retained.
> ![](5748d3369fa767313cb586b54db3fab633ae3d1a.png)
> ![](186f9e2958f1aebb0da1d2e53a38e3345db26d73.png)

### Document Scanner

[Bartłomiej Maryńczak (Poly)](https://matrix.to/#/@poly_meilex:matrix.org) reports

> The latest batch of patches, pertaining to the transition from GTK3 to GTK4, has now been applied to Gnome Document Scanner. This development instills enough confidence in me to deem the porting process complete.
> This includes:
> * GTK4 & libadwaita port
> * Multithreaded image resize, that improves window resize performance ~10 fold
> * Introduction of a new design for page reordering, drivers installation, and authentication dialogs, which are expected to be more user-friendly and visually appealing.
> 
> User testing (aka. hunt for regressions) on a wide variety of scanners would be appreciated.
> ![](GPzBbAisNLeqvOOvUyzxikvf.png)

# GNOME Circle Apps and Libraries

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> Following up James Westman's announcement, I have released Workbench 44.1
> 
> - Update icon-development-kit; adds 143 new icons
> - Blueprint 0.8
> - VTE 0.72.1
> - Rome tools 12.1.1
> 
> As soon as Blueprint hits 1.0, it will lose its experimental status and become the default UI syntax in Workbench.
> Please help us by testing Blueprint 0.8 in your apps or in Workbench.

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) reports

> Did you ever wonder how ants would build an espresso machine? Dan Yeaw extended the Gaphor documentation with a [tutorial](https://docs.gaphor.org/en/latest/coffee_machine.html) showcasing the modeling features of Gaphor. You can read it at https://docs.gaphor.org/en/latest/coffee_machine.html.

# Third Party Projects

[0xMRTT](https://matrix.to/#/@0xmrtt:envs.net) says

> [Bavarder](https://bavarder.codeberg.page) 0.2.2 has been released with the ability to have multiple windows open at the same time, an improved UI (the provider selector has been moved to the menu), a new mechanism for warning users if a provider isn't working because of a remote change (the ability to use local models will come soon) and now, Hugging Chat has been disabled and replaced with the model behind Hugging Chat, Open-Assistant SFT-1 12B Model. 
> 
> You can download Bavarder from [Flathub](https://flathub.org/apps/io.github.Bavarder.Bavarder) or from either [Github](https://github.com/Bavarder/Bavarder) or [Codeberg](https://codeberg.org/Bavarder/Bavarder).
> ![](064785a758555f504d3c945f13854e48375a30f8.png)
> ![](ffc397da4fb5541a7ee9cf7ff874536976b98249.png)

[0xMRTT](https://matrix.to/#/@0xmrtt:envs.net) says

> [Imaginer](https://imaginer.codeberg.org) 0.2.1 has been released with [support of custom provider](https://imaginer.codeberg.page/help/custom), an improved UI, a faster loading mechanism for Preferences, and an improved mechanism for saving credentials.
> 
> You can download Imaginer from [Flathub](https://flathub.org/apps/page.codeberg.Imaginer.Imaginer) or from either [Github](https://github.com/ImaginerApp/Imaginer) or [Codeberg](https://codeberg.org/Imaginer/Imaginer).
> ![](22264831e749328296ed81cff016906090d6a13b.png)
> ![](11e19c98840823684deeb84e3ec07e8345ccdeaf.png)

[Iman Salmani](https://matrix.to/#/@imansalmani:matrix.org) announces

> [IPlan 1.1](https://github.com/iman-salmani/iplan) is out.
> this is a solution for managing your personal life and workflow (its a goal).
> you can download it from [flathub](https://flathub.org/apps/ir.imansalmani.IPlan).
> 
> **Features**:
> * Grouping tasks with project and list
> * Timer for tasks
> * Global search
> * Arranging projects, lists and tasks by drag and drop
> 
> **changes in this version**:
> * Lazy loading for project tasks and stat
> * Records window for create, edit and delete records
> ![](MFwEHOZgEsByXnwfBfiMCBtD.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) can now unblank the screen on incoming notifications based on category and urgency. That allows you to e.g. have it only unblank on critical notifications and/or only on instant messages.
> 
> We also improved the idle-inhibit support in [phoc](https://gitlab.gnome.org/World/Phosh/phoc) so that we can now show the same amount of information when an application uses that Wayland protocol rather than DBus to prevent the screen from locking (as e.g. mpv does).
> 
> Finally [Alistair Francis](https://gitlab.gnome.org/alistair23) made it possible to use the super-key to bring up the overview in docked mode which required changes in phosh and phoc.
> ![](BDuvZKISTihKoOWTTPiHdgZF.png)
> {{< video src="iYbLsIcEcHoBcMCdjQLPUlNx.webm" >}}

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) says

> Fractal 5.beta1 is out!
> 
> Fractal 5.beta1 is the first beta release since the rewrite of Fractal to take advantage of GTK 4 and the Matrix Rust SDK. It is the result of over two years of work.
> 
> New features since Fractal 5.alpha1:
> 
> * Joining room by ID, by alias or with a Matrix URI
> * Creation of direct chats
> * Sending and displaying read receipts, read markers and typing notifications
> * Notifications on new messages
> * Highlight messages with mentions
> * Added media file history viewers in the room details, thanks to our GSoC intern Marco Melorio
> * Displaying the other user's avatar as a room avatar for direct chats
> 
> Of course, there are a also a lot of less visible changes, fixes and translations thanks to all our contributors, and our upstream projects.
> 
> As the version implies, this is still considered beta stage and might trigger crashes or other bugs but overall should be pretty stable. It is available to install via Flathub Beta, see the [instructions in our README](https://gitlab.gnome.org/GNOME/fractal#beta-version).
> 
> _The GNOME 44 runtime used by the beta version Flatpak was shipped with a GTK version that has a regression which messes with room order in the sidebar: some rooms can appear twice and some rooms are missing. This is a known issue so there is no need to report it, there is nothing for us to do but wait until the fix is backported and shipped in an update of the GNOME runtime. In the meantime, the nightly version is unaffected by that._
> 
> A list of blocking issues for the release of version 5 can be found in the [Fractal 5 milestone](https://gitlab.gnome.org/GNOME/fractal/-/milestones/18) on GitLab. All contributions are welcome !
> ![](bc16f37540152ed388f6750efc5e402a3505395f.png)
> ![](483203c8955e423bb6847870bbd311af891f5ef3.png)

### Escambo [↗](https://github.com/CleoMenezesJr/escambo)

Test and develop APIs

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) reports

> Introducing **Escambo**, an HTTP-based API testing application for GNOME.
> 
> Some cool features are:
> 
> * _API Testing_: The main objective of Escambo is to facilitate the testing of HTTP-based APIs. It provides an interface where users can specify API endpoints, parameters, headers, and other information relevant to executing various types of API requests.
> 
> * _Request Configuration_: Escambo allows users to configure different types of HTTP requests such as GET, POST, PUT, DELETE, etc. Users can define request headers, authentication credentials, request bodies, and other request-specific parameters.
> 
> * _Authentication and security_: The app can support authentication methods, API keys, or basic authentication.
> 
> 
> **Escambo** is now [available on Flathub](https://flathub.org/apps/io.github.cleomenezesjr.Escambo)
> Follow its development on [Github](https://github.com/CleoMenezesJr/escambo) or [Codeberg](https://codeberg.org/CleoMenezesJr/escambo).
> ![](UuDWlHlHOgLxPsEQPIDHolYa.png)
> ![](NyZhZxHnzpbHzifKIULDjXFC.png)
> {{< video src="KvLTtQIRFSmvongVSHqiRpqQ.mp4" >}}

### Blueprint [↗](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/)

A markup language for app developers to create GTK user interfaces.

[James Westman](https://matrix.to/#/@flyingpimonster:matrix.org) announces

> I've released Blueprint ~~0.8.0~~ 0.8.1, a big release that contains some syntax changes and a bunch of newly supported features. Even more exciting, this is a release candidate for Blueprint 1.0! Check out the full release notes [on the Releases page](https://gitlab.gnome.org/jwestman/blueprint-compiler/-/releases).

# Shell Extensions

[Marcin Jahn](https://matrix.to/#/@mnjahn:matrix.org) says

> I have created 3 Gnome Shell extensions:
> 
>  - [Quick Settings Audio Devices Hider](https://github.com/marcinjahn/gnome-quicksettings-audio-devices-hider-extension) - allows you to hide selected devices from the Quick Settings audio devices panel
> * [Dim Completed Calendar Events](https://github.com/marcinjahn/gnome-dim-completed-calendar-events-extension) - styles calendar events in the top panel to make it clear which events are completed, ongoing, or upcoming.
> * [Do Not Disturb While Screen Sharing Or Recording](https://github.com/marcinjahn/gnome-do-not-disturb-while-screen-sharing-or-recording-extension) - automatically switches on the "Do Not Disturb" mode while screen sharing or screen recording
> 
> Thanks to the great documentation at [gjs.guide](https://gjs.guide) and awesome folks at [#extensions:gnome.org](https://matrix.to/#/#extensions:gnome.org), developing your own extension is (almost) a breeze!

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) reports

> A new version of **Weather O'Clock** has just arrived. In this new version, the extension creates its own instance of WeatherClient from GWeather instead of recycling the existing one from dateMenu. This caused weather to be forced to update every time it was clicked on the clock.
> 
> Kudos to [runverzagt](https://github.com/runverzagt) who helped with this task.
> 
> [Get it on GNOME Extensions](https://extensions.gnome.org/extension/5470/weather-oclock/)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

