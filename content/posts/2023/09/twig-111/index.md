---
title: "#111 Number One"
author: Felix
date: 2023-09-01
tags: ["daikhan", "workbench", "portfolio", "cartridges", "fretboard", "flare"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 25 to September 01.<!--more-->

# GNOME Circle Apps and Libraries



### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> [Akshay Warrier](https://akshaywarrier.medium.com) made a beautiful offline documentation viewer.
> It will ship with Workbench 45 and probably turn into a standalone app in the future.
> 
> So far it supports gi-docgen docs, searching for pages and back/forward navigation.
> ![](8d1495df54fb81bb5c08be6476e8e588e90b2d6a.png)
> ![](b3cafa4c4d828afd909bd74a0db94dafba6654e0.png)

### Cartridges [↗](https://github.com/kra-mo/cartridges)

Launch all your games

[kramo](https://matrix.to/#/@kramo:matrix.org) says

> This week, I released Cartridges 2.3!
> 
> * New import source: desktop entries!
> * You can now choose executables directly via a file picker.
> * Short covers added manually will now be padded with a blurred background instead of stretched.
> 
> Check it out on [Flathub](https://flathub.org/apps/hu.kramo.Cartridges)!

# Third Party Projects

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> A new minor release of [libmks](https://gitlab.gnome.org/GNOME/libmks) is out. Mostly containing small fixes that should make the rendering perfectly smooth. The next step is to get clipboard sharing and USB redirection working. The library is also available on https://download.gnome.org/sources/libmks/0.1/ if people wish to start testing/packaging it.

[gountal](https://matrix.to/#/@gountal:matrix.org) announces

> I released the version 0.1.0 of Package Transporter.
> 
> This application is meant to facilitate migration from an installation to another. Currently, the application can only backup Flatpak applications, their settings and data.
> 
> This is only a preview release as you cannot restore a backup of your apps yet.
> 
> Check it out on [Flathub](https://flathub.org/apps/io.gitlab.gwendalj.package-transporter)
> ![](ePDKkHvoedTXeQbKPMHBOWHH.png)

### Portfolio [↗](https://github.com/tchx84/Portfolio)

A minimalist file manager for those who want to use Linux mobile devices.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) reports

> I am happy to announce the release of Portfolio 1.0.0! This new release is the first step in the modernization process to GTK 4 and Libadwaita. It’s also a continuation to my efforts of bringing a minimalist file manager to the mobile Linux community, with a few important bug fixes.
> 
> https://blogs.gnome.org/tchx84/2023/08/29/portfolio-1-0-0/
> ![](hNEGgLlbEJwAmLCFXLNzTImn.gif)

### Fretboard [↗](https://github.com/bragefuglseth/fretboard)

Look up guitar chords

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:matrix.org) says

> This week I released Fretboard 2.0. This major release of Fretboard brings a bunch of exciting improvements:
> 
> * View different ways to play the same chord, to find the one that suits your situation the best
> * Bookmark chords to save them for a later practice session or gig
> * An additional fret in the chord diagram, to help you practice those really tricky positionings
> * Hover over positions in the chord diagram to see their respective note names
> * Smarter and more precise chord detection
> 
> If you would like to come with suggestions, report bugs, translate the app, or contribute otherwise, feel free to reach out!
> 
> You can get Fretboard from [Flathub](https://flatpak.app/fretboard).
> ![](pgdvxgSMDQxCdNbtYChVlpYc.png)
> ![](CAravRQKAfyiQoEnNFTGeAkT.png)

### Flare [↗](https://flathub.org/apps/details/de.schmidhuberj.Flare)

Chat with your friends on Signal.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) says

> Flare 0.10.0 got released. There were tons of features, UI improvements and fixes compared to the last TWIG (with also 4 minor releases), so I will just list for the most notable ones. For all the changes, consult the changelog.
> 
> * One can now receive notifications when someone reacts to a message (if one wants that to happen).
> * Added support for libspelling.
> * Fixed some major performance issues.
> * Deleting messages of a channel locally.
> * Added blurhash support, such that even unloaded images and videos look great.
> * Tons of UI improvements.
> * Many fixes.
> 
> There were some organizational changes made to my Linux mobile related apps in general:
> 
> * Updated descriptions and a new common namespace. You can now find all my projects for Linux mobile in this GitLab group.
> * We now also have an Mastodon Account. We will post any updates to the apps right here.
> ![](JPDBvhUZkBDPxwtZvxUxlRUA.png)
> {{< video src="xBQhxSJiQdggMVAFDleLgcKT.webm" >}}

### Daikhan [↗](https://flathub.org/apps/io.gitlab.daikhan.stable)

Play Videos/Music with style.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) reports

> Daikhan received
> 
> * A welcome page
> * Support for more codecs.
> * New translations
>   - Czech
>   - French
>   - Russian
>   - Turkish (previously translated; included in the flatpak now)
> 
> and
> 
> * It disables fullscreen when no media is open.
> * It remembers fullscreen state of the player and restores it when opening files.
> ![](DbfNOmixXPUqEVRPwClGRqjP.png)

# Miscellaneous

[Cassidy James](https://matrix.to/#/@cassidyjames:gnome.org) says

> I’ve been editing down the day-long GUADEC 2023 streams into individual talks. To help share how I approached this using some excellent apps designed for GNOME, I provided instructions and a couple of videos on the [GitLab issue](https://gitlab.gnome.org/Teams/Engagement/Events/GUADEC/guadec-2023/guadec-2023/-/issues/30). Hopefully this can help people in the future for GUADEC and other conferences.
> Huge thanks to HappiePlant NL from YouTube for providing comprehensive timestamps for each GUADEC 2023 talk! This type of work is often too invisible in the community, and yet is an excellent way to contribute. The next step is to get the videos actually uploaded to the channel with proper titles and descriptions—look forward to that soon.

# GNOME Foundation

[mwu](https://matrix.to/#/@mwu:gnome.org) says

> The Foundation team is ramping up for GNOME  ASIA. The call for papers is available at:  https://events.gnome.org/event/170/abstracts/ and we are excited for your submission. Be on the look out for registration, hotel info and more!  And, as always, we are on the look out for sponsors! Please reach out to us if you or your company would like to participate.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

