---
title: "#114 Forty-five!"
author: Felix
date: 2023-09-22
tags: ["cavalier", "gnome-calendar", "identity", "turtle", "cartridges", "letterpress", "workbench", "fretboard", "newsflash"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 15 to September 22.<!--more-->

**This week we released GNOME 45!**

![](45_banner.jpg)

This new major release of GNOME is full of exciting changes, such as a new activity indicator, a brand new image viewer and camera app, faster search, new styled sidebars, additional privacy indicators, and so much more! See the [GNOME 45 release notes](https://release.gnome.org/45/) for more information.

Readers who have been following this site for a few weeks will already know some of the new features. If you want to follow the development of GNOME 46 (Spring 2024), keep an eye on this page - we'll be posting exciting news every week!


# GNOME Core Apps and Libraries

### Calendar [↗](https://gitlab.gnome.org/GNOME/gnome-calendar/)

A simple calendar application.

[Dallas Strouse (she/her) 🧑‍🏫](https://matrix.to/#/@orowith2os:fedora.im) says

> [Calendar](https://apps.gnome.org/Calendar/) has completely rethought its scrolling behavior; in version 44 and prior, the scrolling lacked overall smoothness and was too sensitive to scrolling, and was especially annoying to use on touchpads. Calendar 45 now features smooth scrolling that scrolls between weeks, rather than whole months, which also helped to improve performance! You can view the details [here](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/351), courtesy of [Georges](https://feaneron.com/).
> 
> For the past three months, [Hari Rana (TheEvilSkeleton)](https://theevilskeleton.gitlab.io/) has been working on more Calendar bugfixes to improve accessibility and otherwise improve the user experience. Calendar has also become more keyboard navigable, with some assistance from [Lukáš Tyrychtr](https://gitlab.gnome.org/tyrylu) to ensure usability, and many more fixes are planned for the future. If you'd like to see the work that Hari has worked on for Calendar so far, you can do so [here](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests?scope=all&state=all&author_username=TheEvilSkeleton).
> 
> Calendar has also been updated to [use the *dazzling* new adaptive widgets](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/260), thanks to [Christopher Davis (BrainBlasted)](https://gitlab.gnome.org/BrainBlasted).
> 
> If you'd like to help contribute to Calendar and make the experience better for yourself and others, see if you can help nab any [Accessibility](https://gitlab.gnome.org/GNOME/gnome-calendar/-/issues/?label_name=8.%20Accessibility) issues, and ask about how you can help in the [#gnome-calendar:gnome.org](https://matrix.to/#/!koZzDOwNHEzEoeuNok:matrix.org) channel. [Jeff Fortin](https://fortintam.com) has also created a tracking issue which you can read [here](https://gitlab.gnome.org/GNOME/gnome-calendar/-/issues/1036). 
> 
> In other news, Calendar's number of open issues has been slimmed all the way down to 283, from 570 back in February!
> 
> _Oh, hey, Georges, you still haven't given Skel that candy you offered :P_
> {{< video src="bef9f098b53a7699e684d05b5dc83626df39e5f7.mp4" >}}

# GNOME Circle Apps and Libraries

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) announces

> Forge Sparks 0.2.0 is now available on [Flathub](https://flathub.org/apps/com.mardojai.ForgeSparks).
> 
> This release includes several user interface improvements thanks to the GNOME Circle review.
> 
> The initial view has been improved for a better experience, and account management has been moved to a separate window for easier interaction. It has also been updated to match the style of GNOME 45.
> 
> A few bugs have also been fixed, making it a bit more robust.

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> Workbench 45 is out and [available on Flathub](https://flathub.org/apps/re.sonny.Workbench) 🎉
> 
> This is a massive release with many new features such as Rust support, 50 new Library demos (Map, Spelling, Camera, ...), multi-windows, offline documentation viewer etc.
> See the release announcement https://github.com/sonnyp/Workbench/releases/tag/v45.0
> ![](aaa96ca09da261f38a1a22ec2eb68d6713cd427c.png)
> ![](1501cb164177f3d0ea8a166d1fbaca4ea87e8084.png)
> ![](5b721c82bb1b4071195053cc14f0b79b46c4eae9.png)

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[Jan Lukas](https://matrix.to/#/@jangernert:matrix.org) announces

> NewFlash 3.0 was just released. There is so much new stuff in this release that I wrote a blog post about it: https://blogs.gnome.org/jangernert/2023/09/22/newsflash-3-0/
> ![](eostKgHKGKaytARvsgGOukts.png)
> ![](ilyXInFnKrULMywaEZMcOkSt.png)
> ![](uvWXdgRZBSZbblJPWNIOZGrp.png)

### Identity [↗](https://gitlab.gnome.org/YaLTeR/identity)

Compare images and videos.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) says

> A new version of Identity is out, ready to go with visual tweaks to match the GNOME 45 design. Major improvements include pixelated, rather than blurry, zoomed-in media display; memory leak fixes, and improved format support, like AV1, WebP, semitransparent, and 16-bit images and videos.
> ![](7b4da44e5ef289f0466e172e12beca8b234b8528.png)

### Cartridges [↗](https://github.com/kra-mo/cartridges)

Launch all your games

[kramo](https://matrix.to/#/@kramo:matrix.org) announces

> Cartridges 2.4 is out, updated with a sidebar to filter sources and better adaptivity thanks to the newest Libadwaita!
> 
> Check it out on [Flathub](https://flathub.org/apps/hu.kramo.Cartridges)!
> ![](oReHVEOniNXgCAmNuudMMHCJ.png)
> ![](xxkzCxnnPLglMqadevmxRStl.png)

# Third Party Projects

[baarkerlounger](https://matrix.to/#/@baarkerlounger:one.ems.host) reports

> This week I released Jogger - a fitness tracker for Gnome Mobile. With it you can:
> 
> * Track workouts such as running, cycling, rowing and more using Geoclue based location
> * Get time or distance based voice announcements during your workouts (local or higher quality network based)
> * Manually enter past workouts
> * View workout details such as route, pace, calories burned etc
> * Import workouts from Fitotrack (Android) for users switching to mobile Linux.
> 
> Thanks to [kramo](https://kramo.hu) for the icon design and to everyone who has translated Jogger so far. If you would like to contribute with design suggestions, code, translations, bug reports or ideas you can do so on [Codeberg](https://codeberg.org/baarkerlounger/jogger)
> 
> You can get Jogger on [Flathub](https://flathub.org/apps/xyz.slothlife.Jogger).
> 
> Note. that if you're installing on a Librem 5 you'll need to update to a newer version of `xdg_desktop_portal` than is available in Pure OS Byzantium to get an accurate location fix. See this [issue](https://codeberg.org/baarkerlounger/jogger/issues/8) for more details.
> 
> ![](0e4f62d0cd4add64689381e66af7971d83eff631.png)
> ![](409cfd2c6b978cf0da4c61d78266c73cd26ccfa1.png)

[dabrain34](https://matrix.to/#/@scerveau:igalia.com) announces

> GstPipelineStudio version 0.3.3 is finally out on flathub, MacOS and Windows.
> 
> You can have a quick overview of the project on its website: https://dabrain34.pages.freedesktop.org/GstPipelineStudio/
> 
> The main features are:
> 
>   - Windows and MacOS installer
>   - Open a pipeline with its command line description
>   - Multiple graphview tabs allowing to play with multiple pipeline at once.
>   - Capsfilter support on the link
>   - gstreamer-1.0 wrap support
>   - Bugs fixing

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) says

> Railway (previously DieBahn) version 2.0.0 was released! Railway allows you to plan your travel by public transport like train or bus, and furthermore allows you to keep up-to-date on the latest information during your journey. Railway supports numerous providers, for example from Germany, Austria, Luxembourg but also some providers from America. Version 2.0.0 is a massive overhaul of the UI, for which I have to thank Tobias Bernard  for the mockups and feedback and also camelCaseNick for helping with the implementation. Railway is available on [Flathub](https://flathub.org/apps/de.schmidhuberj.DieBahn).
> ![](pWMAHrJThGITjiRdZFRzITNI.png)

### Turtle [↗](https://gitlab.gnome.org/philippun1/turtle)

Manage git repositories in Nautilus.

[Philipp](https://matrix.to/#/@philippun:matrix.org) announces

> Turtle is finally available on [flathub](https://flathub.org/apps/de.philippun1.turtle)!
> 
> I also released [Turtle 0.5](https://gitlab.gnome.org/philippun1/turtle/-/releases/0.5) recently, which adds more settings (filter remotes in log, commit default behaviour) and plugins for Thunar, Nemo and Caja.
> 
> [Turtle 0.5.1](https://gitlab.gnome.org/philippun1/turtle/-/releases/0.5.1) adds some minor flatpak tweaks.
> 
> If you want to use the Nautilus plugin combined with flatpak, you have to install it manually. Please follow the instructions in the [readme](https://gitlab.gnome.org/philippun1/turtle/-/blob/main/README.md?ref_type=heads#flatpak).
> ![](KoyGeEHJvTUDDFvpTxgaEmEV.png)
> ![](YVqCMbxypwWbYGxQPGczmOwE.png)

### Letterpress [↗](https://gitlab.com/gregorni/Letterpress)

Create beautiful ASCII art

[gregorni](https://matrix.to/#/@gregorni:gnome.org) reports

> This week, I've released version 2.0 of Letterpress! We've simplified the versioning scheme (as you might have already noticed) and changed the app ID to be more consistent with the app name. The app icon has been revised (props to kramo), and the interface itself is now even uses a matching accent color! We've also updated to the latest libadwaita widgets, which allow for a nice and clean looking flat header bar!
> 
> A Tips dialog has been added that explains what kind of pictures are suitable for ASCII art.
> 
> The conversion to ASCII art has been improved, meaning the output will no longer have inverted colors in light mode! Pictures taken on a phone in portrait mode will also no longer appear rotated by 90°.
> 
> The output view itself now centers the ASCII output by default, and you can change the font size to zoom in and out of the view! For changing output width and zoom level, there are now also keyboard shortcuts.
> 
> The app has also been translated into Ukranian (by @volkov).
> 
> You can get the new release from [Flathub](https://flathub.org/apps/io.gitlab.gregorni.Letterpress). If you already have the app installed, just update, no need to worry about the new app ID.
> ![](8d4e3ea5fc37126614ac85db6d961792a1bb30b0.png)
> ![](de00b78ce3b02c5bdca0a3aa7025504d83f198e7.png)

### Fretboard [↗](https://github.com/bragefuglseth/fretboard)

Look up guitar chords

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) announces

> This week I released Fretboard 3.0, with the following improvements:
> 
> * Refined visuals taking advantage of the new capabilities of GNOME 45
> * Keyboard shortcuts for common actions
> * Italian, Russian, and Dutch translations, making Fretboard available in a total of 4 different languages
> 
> If you would like to come with suggestions, report bugs, translate the app, or contribute otherwise, feel free to [reach out](https://github.com/bragefuglseth/fretboard)! The app is now translatable through [Weblate](https://hosted.weblate.org/engage/fretboard/), which I hope will make it more straightforward to contribute.
> 
> You can get Fretboard on [Flathub](https://flatpak.app/fretboard).
> ![](5cce0d93e6b828aaeb40f7f27b91032b35a8addd.png)

### Cavalier [↗](https://flathub.org/apps/details/org.nickvision.cavalier)

Visualize audio with CAVA.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Cavalier [V2023.9.0](https://github.com/NickvisionApps/Cavalier/releases/tag/2023.9.0) is here! In this release a feature to set foreground image was added to Cavalier! You can also now set transparency on both background and foreground images. Combine beautiful pictures with gradients to give Cavalier unique look that fits your mood!
> 
> Here's the full changelog:
> * Added ability to set foreground image for Box modes
> * Transparency can now be set both for background and foreground image
> * Cavalier switched back to using PulseAudio by default. You can still switch audio backend to whatever is supported by CAVA using CAVALIER_INPUT_METHOD environment variable
> * Updated translations (Thanks everyone on Weblate!)
> ![](xfcwXWGDkmTOsSvWofEioxaF.png)
> ![](ZMJIuEfrHCXzOdLCSaYuSDnP.png)

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) announces

> This week GNOME 45 was released! Along with assisting with the release announcements, sharing the news on social channels, and updating the website, one of my jobs as Foundation staff is coordinating the [GNOME 45 release video](https://youtu.be/47aZgF6xmS0). Big thanks to [Freehive](https://freehive.com/) for their work producing this video!
> 
> Reminder:
> We’re still looking for sponsors for GNOME.Asia 2023! If you or your company would like to sponsor this year's summit reach out to [mwu@gnome.org](mailto:mwu@gnome.org) for more information.
> 
> Volunteer Opportunity:
> We’re looking for people to help manage the Foundation’s social media channels. If you’re interested in getting involved reach out to [chenriksen@gnome.org](mailto:chenriksen@gnome.org) for more information.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

