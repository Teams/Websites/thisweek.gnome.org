---
title: "#90 Enabling Feedback"
author: Felix
date: 2023-04-07
tags: ["cartridges", "denaro", "crosswords", "loupe", "gaphor", "tubeconverter"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 31 to April 07.<!--more-->

# GNOME Incubating Apps

### Loupe [↗](https://gitlab.gnome.org/Incubator/loupe)

A simple and modern image viewer.

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) says

> Loupe is now available as a [preview on Flathub](https://flathub.org/apps/details/org.gnome.Loupe). One of the ideas behind the Incubation process is to enable feedback from the broader community before an app lands in GNOME Core and all features are set in stone. So don't expect everything to work yet.
> 
> Since our last update, we have continued to get some of the more important features done:
> 
> * Loupe now uses mipmaps in rendering, avoiding rendering artifacts.
> * We have merged HighDPI support.
> * I have added an algorithm that detects whether a zoom or a rotate gesture is intended. Quite a tricky task, but necessary for touch gestures to work correctly. However, more work on the rotate gestures is still necessary.
> * Loupe now uses an algorithm that detects if a transparent image will be too dark to be visible on the default background. In that case, Loupe will choose a lighter background (screenshot below).
> * And as usual, we have fixed a bunch of issues.
> 
> Thanks for everyone contributing feedback and issue reports. Special thanks go to Matthias Clasen and Benjamin Otte for keeping up with all my attempts to break GTK/GDK and Ivan Molodetskikh for the fruitful cooperation in our joint goal to get images on computer screens.
> ![](117482068c08e10d264f10d5c2396fb7a6d59512.png)

# GNOME Circle Apps and Libraries

[Dave Patrick](https://matrix.to/#/@sedve:matrix.org) reports

> Introducing [Mousai v0.7](https://github.com/SeaDve/Mousai/releases/tag/v0.7.0)!
> 
> If you have been following Mousai on TWIG, nothing should come as a surprise. After almost two years of development, this version comes packed with exciting new features, including a new sleek and intuitive UI, fuzzy search on the history, recognition cancellation, and MPRIS support. You can now also easily copy the title and artist of any song from the UI, remove individual songs from your history, and seek through the player. And for those times when you don't have an internet connection, we've got you covered. Plus, we've fixed a ton of bugs and improved stability across the board, along with a full Rust rewrite.
> ![](MvrwMjneTTovDMcrBDsPJarz.png)

[hugoposnic](https://matrix.to/#/@huluti21:matrix.org) says

> I spent the last two weeks improving Curtail. Mainly porting it to Gtk 4 and Libadwaita.
> I made three new versions to improve the app incrementally with the indispensable help of Maximiliano and Tobias.
> It was also the opportunity to add some new features and fixed some bugs.
> 
> Here is a small tour of the main changes:
> * SVG support
> * Port to GTK 4 and Libadwaita
> * More modern results page
> * No more UI freezes during compression
> * Configurable compression timeout
> * Dependencies updates (OptiPNG -> Oxipng)
> 
> The latest update has even been featured on "OMG! Linux": [Curtail Image Compressor Can Now Crush SVGs](https://www.omglinux.com/curtail-app-adds-svg-support)
> 
> Really happy with the current state of Curtail. Thanks for your great feedback!
> ![](UnjNTvSBBTAdLCIUUDXjORHc.png)

[Plum Nutty (she/they)](https://matrix.to/#/@plum-nutty:matrix.org) reports

> Chess Clock v0.5 was [released](https://gitlab.gnome.org/World/chess-clock/-/releases/v0.5.0)!  This new release brings a major overhaul to the time control selection screen, simplifying the available presets and emphasizing the manual time entry controls.  There's now only a single button to start the game, and the presets just set the time on the manual time entry.
> ![](cZdXAZeHRiraTZzmMIHUDToN.png)

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) reports

> I'm happy to announce a new release of [Gaphor](https://gaphor.org).
> 
> * Gaphor is now 100% GTK4 on all supported platforms: Linux (Flatpak + AppImage), Windows, and macOS
> * Gaphor now has a graphical merge conflict resolver
> * Diagrams can be added to diagrams
> * Enable middle-click mouse scrolling of diagrams
> * The language used in the model can be changed independent from your system language
> * And many, many bug fixed and UI improvements
> ![](hUtdoQIAOHzgyxswCBRszNVO.png)

# Third Party Projects

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> Took a bit too long but finally did a new release of [flatpak-vscode](https://github.com/bilelmoussaoui/flatpak-vscode), it includes various improvements inspired by GNOME Builder as usual: 
> 
> * Mount fonts directories
> * Support running inside a container like toolbox
> * Expose session accessibility bus
> * Fix remote development support

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> A new release of [flatpak-github-actions](https://github.com/flatpak/flatpak-github-actions) is out with various new configuration options. Details can be found at https://github.com/flatpak/flatpak-github-actions/releases/tag/v6

[nxyz](https://matrix.to/#/@nxyz:matrix.org) announces

> This week I released [Resonance](https://github.com/nate-xyz/resonance), an intuitive music player application written in Rust & Python with a clean user interface. Resonance lets you effortlessly manage and play your music collection. 
> 
> Features: 
> 
> * UI updates to reflect currently playing track's cover art colors
> * Playlist creation & modification
> * Control the player through MPRIS
> * Discord Rich Presence integration
> * Last.fm scrobbling
> 
> GH repo: https://github.com/nate-xyz/resonance
> 
> Flathub page: https://beta.flathub.org/apps/io.github.nate_xyz.Resonance
> ![](YrnyXssJnBpefrgwNvIGdwqi.png)

[Fyodor Sobolev](https://matrix.to/#/@fsobolev:matrix.org) announces

> This week Time Switch, a small app to run a task after a timer, got [an update](https://github.com/fsobolev/timeswitch/releases/tag/2023.04.03): 
> * Added ability to create presets to allow saving and restoring settings
> * New shortcuts were added to make the app easier to use with keyboard
> * When running in the background, the app shows timer information in GNOME 44
> * The app will now remember window size and notification text
> * Small UI improvements
> 
> Download from [Flathub](https://flathub.org/apps/details/io.github.fsobolev.TimeSwitch)
> ![](hrcZAvbSuNJkRQhTAglCrojC.png)
> ![](ZCXYttAuxKErIDPMSZgHMlwG.png)

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

An easy-to-use video downloader (yt-dlp frontend).

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> [Tube Converter V2023.4.0](https://github.com/nlogozzo/NickvisionTubeConverter/releases/tag/2023.4.0) is here! This week's release features the ability to run downloads in the background (taking advantage of GNOME 44's new background apps API!) as well as many bugs fixed!
> 
> Here's the full changelog:
> 
> * Added the ability to run downloads in the background (*off by default*)
> * Fixed an issue where extra escape characters were added in video titles
> * Improved the UX of adding a download
> * Updated translations (Thanks everyone on Weblate!)
> ![](KmANkJcxAMHwGbyROjuPpqXN.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Denaro [V2023.4.0-beta1](https://github.com/nlogozzo/NickvisionMoney/releases/tag/2023.4.0-beta1) got released this week! We have been hard at work the past month and a half improving Denaro's stability and adding many many new features to the application. This beta is a continuation of the V2023.3.0-beta series, but since we are in a new month, the beta number was reset.
> 
> This beta includes a fix for the random crashing many users were experiencing when working with transactions, finally! It also includes a new preference option to automatically backup account files as CSV files to a specific folder.
> We have a few more features left to polish up and are expecting a stable release next week :)
> 
> Here's the full changelog:
> 
> * Added the ability to customize the decimal and group separators used per account
> * Added the ability to password protect a PDF file
> * Added a preference option to automatically backup account files to a specific folder
> * Fixed an issue where OFX files with security could not be imported
> * Fixed an issue where QIF files could not be imported on non-English systems
> * Fixed an issue where editing a transaction with a receipt would crash the application
> * Updated to GNOME 44 platform and fixed many GTK crashes users were experiencing
> * The UI is now built using blueprint
> * Updated translations (Thanks to everyone on Weblate)!
> ![](SogQCguShaYMBindtiqWtLJB.png)

### Crosswords [↗](https://gitlab.gnome.org/jrb/crosswords)

A crossword puzzle game and creator.

[jrb](https://matrix.to/#/@jblandford:matrix.org) announces

> GNOME Crosswords 0.3.8 has been [released](https://blogs.gnome.org/jrb/2023/04/02/crosswords-0-3-8-change-management/).
> 
> This release adds the following improvements:
> * "New Puzzle" greeter for the editor.
> • Fully adaptive sizing. Crosswords will shrink to fit available space
> • Fix end-of-game bugs where you could still edit the puzzle
> • Use tags instead of labels for puzzle-set metainfo
> • Enumeration rendering fixes
> • Miscelaneous bugfixes
> 
> This release marks a change in focus. For the next few months, we are going to work more on the Editor than the game. Stay tuned for more changes!
> ![](yBbVSnHnXQScHUAZJoqkopSg.png)
> ![](hKUlvhIPUBGhXwmIKRoMtMBJ.png)
> {{< video src="hRbRVEJmnGTEoxJIpEotVQyZ.webm" >}}

### Cartridges [↗](https://github.com/kra-mo/cartridges)

Launch all your games

[kramo](https://matrix.to/#/@kramo:matrix.org) says

> Last week I released Cartridges, a simple Libadwaita game launcher for all your games. This week it received a [huge update](https://github.com/kra-mo/cartridges/releases/tag/v1.3):
> * You can now download cover art from SteamGridDB automatically!
> * It gained two new import sources: Lutris and itch.
> * There is better feedback for launching and hiding games.
> 
> Check it out on [Flathub](https://flathub.org/apps/details/hu.kramo.Cartridges)!
> ![](xEzdzRXICjGsFNeVWxQrDIof.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

