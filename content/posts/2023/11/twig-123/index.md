---
title: "#123 Infrastructure Work"
author: Felix
date: 2023-11-24
tags: ["girens", "glib", "dialect", "gnome-maps", "switcheroo", "extension-manager", "solanum", "fractal"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 17 to November 24.<!--more-->

# Sovereign Tech Fund

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) says

> As part of our infrastructure [initiative funded by the Sovereign Tech Fund](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/), a number of community members have been hard at work for the past weeks. Starting now we will send a weekly update of our progress on thisweek.gnome.org.
> 
> Some highlights of our progress so far:
> 
> ### Home Encryption
> 
> Adrian Vovk added support for systemd homed in AccountService. This is the first step towards safer encryption and a nicer user experience.
> 
> Sam Hewitt is working with Adrian on the designs and task definitions for home encryption.
> 
> ### XDG Desktop Portal
> 
> Georges Stavracas and Hubert Figuière are improving existing portals, doing various maintenance tasks on the project, and introducing some exciting new portal APIs, such as the USB portal. Things that have already landed include:
> 
> * Drag and drop of folders will now work with sandboxed applications (Drop a folder from Nautilus onto Amberol)
> * Some bug fixes in the document portal, notably the one preventing ejection of USB flash drives (thanks Alex) and the one preventing using sqlite3 databases
> * Expanded [website](https://flatpak.github.io/xdg-desktop-portal/) and [developer documentation](https://flatpak.github.io/xdg-desktop-portal/docs/)
> 
> {{< video src="portal" >}}
>
> The new USB portal is already capable of the following:
> 
> * Creating and monitoring apps using USB devices through the portal
> * It has a permission to control which apps are allowed to use the portal
> 
> In the works is accessing USB devices with per-device, per-app permissions.
> 
> ### GLib
> 
> Philip Withnall is doing lots of maintenance tasks on GLib, including [porting documentation to gi-docgen](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3724).
> 
> ### Secret Management
> 
> Dhanuka Warusadura ported the PAM module [from gnome-keyring to libsecret](https://gitlab.gnome.org/GNOME/libsecret/-/merge_requests/128).
> 
> This is the first step towards replacing gnome-keyring with [oo7](https://github.com/bilelmoussaoui/oo7).
> 
> ### CSS Variables in GTK
> 
> Alice is [adding CSS variables (aka custom properties) support to GTK](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/6540).
> 
> ![](css.png)
>
> ### Shell and Compositor Performance
> 
> Ivan Molodetskikh is improving the GNOME Shell/Mutter profiling instrumentation and [adding integration for the Tracy profiler](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3391). As part of this, he had already discovered several actionable performance bugs. Thanks to Mutter maintainers, [some](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3001) of [those](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3004) have [fixes](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3389) pending or [merged](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3406).
> 
> ![](tracy.png)
>
> ### Notifications
> 
> Julian Sparber and Jonas Dreßler are working on improving notifications, both in terms of platform APIs and user interface. As part of this Julian is implementing grouping notifications by app in GNOME Shell.
> 
> {{< video src="shell" >}}
>
> ### GNOME Online Accounts
> 
> Andy Holmes is improving GNOME Online Accounts, including [CalDAV/CardDAV support](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/issues/1), [port to GTK4](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/142), and [replacing the embedded web view](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/139) in favor of using the preferred browser for OAuth2. We're also discussing more general improvements to the GOA architecture.
> 
> ### Accessibility
> 
> Sam Hewitt has fixed some accessibility issues in the GNOME Shell stylesheet. Joanie Diggs is reducing technical debt in the Orca screen reader, improving documenation, and rewriting table and table cell support.
> 
> ### Hardware Support
> 
> Jonas Dreßler is working on hardware-accelerated screencasts and improvements to the Linux Bluetooth stack.
> 
> Emmanuel Gil Peyrot (Link Mauve) is adding support for GL\_KHR\_robustness in Mutter. It allows the session to recover from a GPU driver crash. This is a requirement to unlock newer graphical features that aren't well tested yet with GPU drivers.

# GNOME Core Apps and Libraries

### Maps [↗](https://wiki.gnome.org/Apps/Maps)

Maps gives you quick access to maps all across the world.

[mlundblad](https://matrix.to/#/@mlundblad:matrix.org) reports

> Maps is now showing the "mode of transportation" icon instead of the generic filled circle start icon in the turn instruction list for turn-by-turn navigation as an extra detail and to act as a reminder as to what kind of routing it is showing
> ![](tLPkvxvucDnuhUpSMFxhQbUC.png)
> ![](apRKejyuvEpbprnnCPtjYeVt.png)
> ![](CcZpBdrTpNQRNGvhCQoQNNZi.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Alexander Slobodeniuk has added a [new `g_log_writer_default_set_debug_domains()` function](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3710) in GLib which allows enabled debug logging domains to be changed at runtime without modifying `G_MESSAGES_DEBUG` in the environment (which can never be threadsafe)

# GNOME Circle Apps and Libraries

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) announces

> This week [Switcheroo](https://apps.gnome.org/Converter) was accepted into GNOME Circle. Switcheroo lets you convert images to different file types and resize them. Congratulations!
> ![](d988285077cd1891db0b23aabdd85b2c2c7bd34e1728153399062429696.png)

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) says

> The second app to enter Circle this week is [Decibels](https://apps.gnome.org/Decibels/). Decibels lets you play audio files in a beautiful, convenient, and intuitive way. Congratulations!
> ![](5ac66004c1e9b31cadf0a4d1b6ba1620d2aab7321728154266184450048.png)

### Solanum [↗](https://gitlab.gnome.org/World/Solanum)

Balance working time and break time.

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> Solanum 5.0.0 has been released and is available on [Flathub](https://flathub.org/apps/org.gnome.Solanum).
> 
> This release brings a minor visual refresh and few other changes:
> 
> - Added the ability to view the release notes for the latest version
> - Swapped to a new audio player
> - Fixed the notification buttons
> - Miscellaneous translation updates

### Dialect [↗](https://github.com/dialect-app/dialect/)

Translate between languages.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) reports

> Dialect 2.2.0 has been released and is available on [Flathub](https://flathub.org/apps/app.drey.Dialect).
> 
> This release contains various improvements and bug fixes, these are the most destacable:
> 
> Now you can use Lingva Translate text-to-speech in addition to Google's. There are also new translation providers for Bing and Yandex.
> 
> The preferences window has been redesigned to make it easier to change the settings for each provider.
> ![](ajDuQxJnqRySkbQqqyHVjOPn.png)

# Third Party Projects

[Akshay Warrier](https://matrix.to/#/@akshaywarrier:matrix.org) says

> Biblioteca 1.2 is now available on [Flathub](https://flathub.org/apps/app.drey.Biblioteca)!
> 
> Biblioteca now supports tabs! Middle-clicking on the sidebar or documentation links now opens them in new tabs. It also received some other improvements and bug fixes such as
> 
> * Fixed an issue where shortcuts window prevents app from closing
> * Fixed an issue where main window CSS leaks into other windows
> * Shows a status page in sidebar when no results found
> * Fixed layout with "Large Text" accessibility option
> ![](xXVLoQsLvPsyauaKYPyzlxeh.png)

### Girens for Plex [↗](https://flathub.org/apps/details/nl.g4d.Girens)

Girens is a Plex Gtk client for playing movies, TV shows and music from your Plex library.

[tijder](https://matrix.to/#/@tijder:matrix.org) says

> This week Girens version 2.0.3 is released. This is a small update. In this version the old sidebar is replaced for an overlay sidebar. This will make the appearance of the application more inline with other Gnome applications.
> ![](rgzoEUtLFEqwTQVCiDLeAyRR.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) announces

> We just released Fractal 5! It is a full rewrite compared to Fractal 4, that now leverages GTK 4, libadwaita, and the Matrix Rust SDK. The two-and-a-half-year effort brings a new interface that fits all screens, big 🖥️ or small 📱, but should still look familiar to users of earlier versions.
> 
> It still offers the same old features you know and love, with a few additions. Highlights (*italics* is new✨ in 5):
> 
> * Find rooms to discuss your favorite topics, or talk privately to people, securely thanks to *end-to-end encryption*
> * Send rich formatted messages, files, or *your current location*
> * *Reply* to specific messages, *react* with emoji, *edit* or remove messages
> * View images, and play audio and video directly in the conversation
> * See *who has read messages*, and who is typing
> * Log into *multiple accounts* at once (with *Single-Sign On* support)
> 
> It will be available in a few hours on [Flathub](https://flathub.org/apps/org.gnome.Fractal).
> 
> For our next version, we intend to add missing key features, like notification settings, room settings and moderation… We also plan on improving the accessibility and polish the rough edges. [Any help](https://gitlab.gnome.org/GNOME/fractal/-/issues/?label_name%5B%5D=4.%20Newcomers) is greatly appreciated!
> ![](4361d4deff3cfaf68067789b6c190a88fdd22cf01728117788301590528.png)

### Extension Manager [↗](https://github.com/mjakeman/extension-manager)

Browse and install GNOME Shell extensions.

[firox263](https://matrix.to/#/@firox263:matrix.org) says

> Extension Manager 0.4.3 has been released and is now available on [Flathub](https://flathub.org/apps/com.mattjakeman.ExtensionManager). 
> 
> This release contains numerous fixes to sorting and search: 
>  * Updated search to be fully compatible with the latest version of extensions.gnome.org
>  * Fixed a long running issue where correct search results were overwritten by a previous query
>  * Fixed an issue where the global extensions toggle became 'stuck'
>  * Uses new libadwaita widgets and GNOME 45
> 
> In special news, Extension Manager has reached its one millionth download on Flathub. Thanks to everyone for the support! 🎉

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
