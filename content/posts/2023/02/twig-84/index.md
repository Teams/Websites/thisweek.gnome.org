---
title: "#84 Polished Circle"
author: Felix
date: 2023-02-24
tags: ["tubeconverter", "eyedropper", "gdm-settings", "gjs", "glib", "denaro", "bottles", "fractal", "gnome-builder", "pika-backup", "workbench"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 17 to February 24.<!--more-->

# GNOME Circle Apps and Libraries

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) reports

> This week we are welcoming our 50th app into [GNOME Circle](https://circle.gnome.org/). With only existing for 2½ years, the GNOME Circle project has been an enormous success.
> 
> We thank all the app maintainers and other contributors for their dedicated work on all those apps. We will do our best to help the community to keep our apps at such good quality.
> ![](909e7c1019570773e61fb0802ed5e0caef5f7886.png)

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) says

> This week, [Chess Clock](https://apps.gnome.org/app/com.clarahobbs.chessclock/) joined GNOME Circle. Chess Clock allows you to time games of over-the-board chess. Congratulations!
> ![](991fa8de954d44e2e3ed8ea3642976992e728644.png)

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) says

> This week, [Komikku](https://apps.gnome.org/app/info.febvre.Komikku/) joined GNOME Circle. Komikku allows you to read your favorite manga. Congratulations!
> ![](50979fdba9b2a0ba0110dba8d092b29432a00650.png)

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) says

> This week, [Eyedropper](https://apps.gnome.org/app/com.github.finefindus.eyedropper/) joined GNOME Circle. Eyedropper allows you to pick colors and generate palettes. Congratulations!
> ![](b4a08ca1214d0dee5a0862b8b2a33bd5425d95e6.png)

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> Workbench news
> 
> Workbench is now fully sandboxed. Starting with the next release, it will protect users from accidents or malicious code. Made possibly by Flatpak ❤️. [Read more](https://floss.social/@sonny/109886525574894538)
> 
> Our project proposal "Make GNOME platform demos for Workbench" has been accepted as a Google Summer of Code project. I will be mentoring along with Andy Holmes and Tobias Bernard. [Read more](https://floss.social/@sonny/109899494637821665)
> 
> I made a guide in how to get started contributing to Workbench. [Read more](https://github.com/sonnyp/Workbench/blob/main/CONTRIBUTING.md)

### Pika Backup [↗](https://apps.gnome.org/app/org.gnome.World.PikaBackup/)

Keep your data safe.

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) says

> Version 0.5 of [Pika Backup](https://apps.gnome.org/app/org.gnome.World.PikaBackup/) is now available. This version includes the following:
> 
> * Different presets for commonly excluded folders that can be activated independently.
> * Support for adding exclusion rules based on regular expressions or shell patterns.
> * Ability to manually delete specific archives.
> * More automated help with finding and mounting USB drives when starting backups.
> * More than 20 other changes, including small interface improvements and bug fixes.
> 
> I'm also happy to share that Fina joins Pika Backup as a second maintainer. Further, it is now possible to [support the development and maintenance on Open Collective](https://opencollective.com/pika-backup). Both steps will help to give the project a stable future.
> ![](88ae916a4d06e6160f946781ff1e7966539910b0.png)

# GNOME Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Luca Bacci has added support for getting pretty names of UWP apps on Windows in GLib, so the GTK app chooser dialogue is now a lot more useful! See screenshots on https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3168

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) reports

> GJS 1.75.2 was released today, the beta release for the GNOME 44 series. Here's a selection of the new features:
> * There are new `Gio.Application.prototype.runAsync()` and `GLib.MainLoop.prototype.runAsync()` methods which do the same thing as `run()` but return a Promise which resolves when the main loop ends, instead of blocking while the main loop runs. Use one of these methods (by awaiting it) if you use async operations with Promises in your application. Previously, it was easy to get into a state where Promises never resolved if you didn't run the main loop inside a callback. Thanks to Evan Welsh for working on this.
> * There are new `Gio.InputStream.prototype.createSyncIterator()` and `Gio.InputStream.prototype.createAsyncIterator()` methods which allow easy iteration of input streams in consecutive chunks of bytes, either with a for-of loop or a for-await-of loop. This addition is from Sonny Piers.
> * DBus proxy wrapper classes now have a static `newAsync()` method, which returns a Promise that resolves to an instance of the proxy wrapper class on which `initAsync()` has completed. Marco Trevisan laid the groundwork for this one.
> * DBus property getters can now return GLib.Variant instances directly, if they have the correct type, instead of returning JS values and having them be packed into GLib.Variants. A quick quality-of-life improvement from Andy Holmes.
> * Some long-existing typos in Cairo enums were spotted by Vítor Vasconcellos.
> * More Cairo improvements, tuberry created `Cairo.SVGSurface.prototype.finish()` and `Cairo.SVGSurface.prototype.flush()` because previously SVG surfaces were only written to disk when the SVGSurface
>   object was garbage collected, making it uncertain to rely on them.

# GNOME Development Tools

[Felipe Borges (afk)](https://matrix.to/#/@felipeborges:gnome.org) says

> GNOME Boxes now has a more modern virtual machine creation dialog which is more aligned to the GNOME Human Interfaces Guidelines and simplifies the former approach that used pagination. https://gitlab.gnome.org/GNOME/gnome-boxes/-/merge_requests/571

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[hergertme](https://matrix.to/#/@hergertme:gnome.org) says

> Builder gained a toggle to allow you to start your application with the GTK inspector active. This helps in situations where you cannot otherwise activate it with keyboard shortcuts.
> ![](742987e8c8ea68a13a19b633d7b6633bc5b5fd2f.png)

[hergertme](https://matrix.to/#/@hergertme:gnome.org) says

> Builder now uses a PTY for cloning operations to provide more reliable messages from the git server.
> ![](0c0a876737d72d148e41c7dc97594dafce10b70b.png)

[hergertme](https://matrix.to/#/@hergertme:gnome.org) reports

> Builder got a revamped messages panel this week which allows users to troubleshoot issues with their project more easily. Various subsystems in Builder are being updated to raise awareness of operations to users.
> ![](6066965a6b11847a66773aa0661de1d2a4fe4da7.png)

# Third Party Projects

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) reports

> I finally released Elastic - an editor for libadwaita spring animations: https://blogs.gnome.org/alexm/2023/02/24/introducing-elastic/
> ![](5be62f2a938ea2ac1a55847af7503cb4ff8ed7bf.png)

[Dave Patrick](https://matrix.to/#/@sedve:matrix.org) announces

> I am excited to announce that [Mousai](https://github.com/SeaDve/Mousai) now supports offline mode. Through this update, instead of an error, the recordings will be saved and then recognized once you're back online. This is perfect for anyone who needs to recognize audio on the go. Aside from that, there have been significant improvements in navigation to make it more natural and intuitive.
> ![](LKyogkqIgxbazlyXCqlkhXLW.png)

[nxyz](https://matrix.to/#/@nxyz:matrix.org) says

> This week I released Paleta v0.3.0, a complete rewrite in Rust! This release brings with it the advantage of using [colorthief-rs](https://github.com/RazrFalcon/color-thief-rs), which makes extracting colors blazingly fast! 
> 
> Download rewritten Paleta from flathub: https://beta.flathub.org/apps/io.github.nate_xyz.Paleta
> ![](FXjzcaetTosiIGCVdlLNHeAK.png)

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) reports

> Jonas published version 1.1 of Capsule, with support for mobile form factors!
> ![](2fc0dc946e44ac15911aa601e63d5db5fd592346.png)

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

An easy-to-use video downloader (yt-dlp frontend).

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> [Tube Converter's C# rewrite](https://github.com/nlogozzo/NickvisionTubeConverter/pull/50) continues to move along. This week we rewrote the backend using [pythonnet](http://pythonnet.github.io/). This framework allows us to embed python directly within our application to use `yt-dlp` calls directly from python instead of relying on the executable, giving us more fine-grain control of downloads.
> 
> Thanks to Brage we also have a new icon for this big release!
> 
> I know I said this last week 😆, but we are hoping to have a beta by the end of the weekend!
> ![](rzRegWlBjNNPSXNYlkeeuxnv.png)
> ![](yoqbIGWDQDpQzGUpnciYkqSB.png)

### Login Manager Settings [↗](https://realmazharhussain.github.io/gdm-settings/)

A settings app for the login manager GDM.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) announces

> [Login Manager Settings](https://realmazharhussain.github.io/gdm-settings) [v3.alpha.0](https://github.com/realmazharhussain/gdm-settings/releases/tag/v3.alpha.0) was released. This version is not available on Flathub but you can download the flatpak (and AppImage) from its [release page](https://github.com/realmazharhussain/gdm-settings/releases/tag/v3.alpha.0).
> 
> Some of the new features include
> * An "Always Show Accessibility Menu" toggle in Top Bar settings
> * Option to change cursor/pointer size
> * Release notes available in the About Window
> 
> Some other changes include
> * Proper names are shown for themes instead of name of their directory
> * Cursor only themes are not presented when choosing icon theme
> 
> There's also a one-time donation request along with a 'Donate' hamburger menu item.
> ![](uMzuCvOSlkMMiLxmXlaZDJDa.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) reports

> Hello all! Fractal 4.4.2-beta2 is available on Flathub Beta.
> 
> You read correctly, it's not Fractal 5 yet. This is a maintenance release that fixes the compilation of Fractal 4.4 with recent libraries and allows to build the Flatpak with the latest GNOME runtime. That means no more warning that the GNOME 41 runtime is outdated!
> 
> There are no new features, but since we updated a lot of Rust dependencies we preferred to release a beta version before using it to update our stable release to the latest runtime. So please test it and if no major regression is reported, we will release it as stable in two weeks.
> 
> To get it, run the following commands:
> 
> ```sh
> flatpak remote-add --if-not-exists flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
> flatpak install flathub-beta org.gnome.Fractal
> flatpak run --branch=beta org.gnome.Fractal
> ```
> 
> Note that if you always want to use this beta version instead of the stable one, and run it from the icon in your launcher, you will need to change its desktop file to use the command in the last line.
> 
> See you soon with some news regarding Fractal 5 hopefully!

### Eyedropper [↗](https://flathub.org/apps/details/com.github.finefindus.eyedropper)

Pick and format colors.

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) reports

> A new version of Eyedropper has been released, with many small UX and UI improvements. Most notably is a new icon, designed by [bertob](https://github.com/bertob), and the ability to export palettes as Adobe Swatch Exchange (ASE), hex, PAL or Paint.NET (txt) files. Minor improvements include an Undo toast after clearing the history and colors no longer displaying NaN. View the full changelog in-app after [downloading it from Flathub](https://flathub.org/apps/details/com.github.finefindus.eyedropper) or on the [GitHub release page](https://github.com/FineFindus/eyedropper/releases).

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Denaro [V2023.2.1](https://github.com/nlogozzo/NickvisionMoney/releases/tag/2023.2.1-p1) is here! This week we are bringing users some fixes to minor bugs and various UX improvements!
> 
> Here's the full changelog:
> 
> * New and improved icon (Thanks @daudix-UFO)!
> * Fixed an issue where the wrong group was selected in TransactionDialog
> * Fixed an issue where some LC vars could not be parsed
> * Various UX improvements
> ![](WbzHLPhIoXQrFmMAiVEhVVRM.png)

### Bottles [↗](https://usebottles.com/)

Easily run Windows software on Linux with Bottles!

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) says

> Bottles 51.0 was just released! As said last time, we focused on fixing bugs and quality of life changes.
> 
> We revamped the "New Bottle" interface, to address several issues that were raised from users, including but not limited to:
> 
> * There wasn’t a clear indicator for which environment was chosen, especially when the Custom environment was selected and then collapsed.
> * The window would increase and shrink in each page, and when the Custom environment was selected or collapsed. This caused a lot of distractions as the window would expand and shrink unexpectedly.
> 
> There are also minor quality of life changes that will hopefully improve the user experience:
> 
> * The Library icon has been changed from a heart to books, to represent a libary.
> * There is now a toast when launching executables with "Run Executable". Before, there wasn't a clear indicator when the program was launching.
> * Runners are now sorted according to a priority list. From the highest to the lowest priority: Soda, Caffe, Vaniglia, Lutris, and then the rest. Whenever you want to create a new bottle, or change the runner of an existing bottle, everything will be displayed in order.
> * Bottles can now be named without any character restrictions. This means, for example, "Assassin's Creed", "Sid Meier’s Civilization", or "Paw Patrol: On A Roll" are now valid names.
> * Bottles will now send a notification when a bottle has been created, but only when the window is unfocused.
> 
> There are also several bug fixes that include:
> 
> * Adding a shortcut to Steam wouldn't add and would throw an error.
> * Importing full backups wouldn't import and would throw an error.
> * A lot of various issues with the library, like empty covers and crashes.
> * Various bugs regarding text encoding, in games and in Bottles
> * Bottles list not being updated correctly after deleting a bottle.
> 
> You can find all information in the [release page](https://usebottles.com/blog/release-51.0/)!
> ![](09df7ba5ae7759920195a0ce85488eef87343ecb.png)

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) announces

> As the Brand Manager for the GNOME Foundation, I am involved in many of the Foundation’s projects, but most of my daily tasks involve graphic design, website maintenance, and social media management. This week I’ve been preparing for our booth at [SCaLE](https://www.socallinuxexpo.org/scale/20x) next month by ordering new GNOME t-shirts and writing messages about the event to share in our Friends of GNOME newsletter and on our social channels. I've also been working on building registration forms for [Linux App Summit (LAS)](https://linuxappsummit.org/) (look for event registration to open very soon), designing LAS merchandise (these will be for sale in person by per-order or online in the LAS shop), addressing lots of GUADEC 2023 planning tasks, and working on the GNOME 44 release video with [Freehive](https://freehive.com/).
> 
> Contributors welcome! If you would like to join me in working on GNOME social media or Foundation and Engagement Team graphic design projects find me on matrix @chenriksen.
> 
> Open Calls for Participation:
> We’re still looking for locations for both GNOMEAsia 2023, and GUADEC 2024. If you’re interested in learning more about hosting a GNOME event or want to submit a proposal for either of our open calls you can learn more about GNOMEAsia 2023 [here](https://foundation.gnome.org/2023/01/12/call-for-gnome-asia-2023-bids/) or GUADEC 2024 [here](https://foundation.gnome.org/2023/01/27/call-for-guadec-2024-bids/). Deadlines for both of these bids are coming up soon so submit your proposals asap!
> 
> Volunteer Opportunities:
> GNOME will be hosting a booth at SCaLE in Pasadena, CA on March 10-12. If you are in the Los Angeles area or planning to attend SCaLE, we could use your help at our booth! Contact [info@gnome.org](info@gnome.org) to volunteer.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

