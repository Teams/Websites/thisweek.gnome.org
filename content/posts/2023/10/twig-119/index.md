---
title: "#119 Stylish Websites"
author: Felix
date: 2023-10-27
tags: ["tagger", "glib", "gdm-settings", "gtk-rs", "vala", "workbench"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 20 to October 27.<!--more-->

# GNOME Core Apps and Libraries

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> The XDG Desktop Portal project has a [stylish new website](https://flatpak.github.io/xdg-desktop-portal/), thanks to Jakub Steiner!
> ![](a8e8a7a2e40dd81fcc72c056f57ec5c5f90e303b1715465173231206400.png)
> ![](5bdf03cc8837154dbd3c4970fb20281d5573865a1715465205082750976.png)

### Vala [↗](https://gitlab.gnome.org/GNOME/vala)

An object-oriented programming language with a self-hosting compiler that generates C code and uses the GObject system.

[lwildberg](https://matrix.to/#/@lw64:gnome.org) reports

> Recently port of the Vala Reference Manual was merged and is now accessible online [here](https://gnome.pages.gitlab.gnome.org/vala/manual/index.html). This was a longstanding effort to move away from a wiki-based solution. If you want to contribute to it now, you need to create a merge request in the Vala repository. Also it looks just so much more beautiful, doesn't it? ;)
> ![](06a17a882f40deace399248f505707f67c2f0b511717142743563632640.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> After more than 10 years, GLib is now [generating the introspection data](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3636) for its API instead of making it the responsibility of gobject-introspection. This is a requested step towards [changing the documentation generator](https://gitlab.gnome.org/GNOME/glib/-/issues/3037) for the GLib API references from gtk-doc to gi-docgen. Work is ongoing to [move libgirepository into GLib](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3642), as part of the overall goal of [simplifying the introspection build](https://gitlab.gnome.org/GNOME/glib/-/issues/2616). For more information, you can check [my blog](https://www.bassi.io/articles/2023/10/25/introspections-edge/).

# Miscellaneous

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) announces

> In our ongoing efforts to organize the GNOME pages a bit better, we have created a [Websites Team](https://gitlab.gnome.org/Teams/Websites). The Websites Team is currently a loose association of people maintaining websites within the GNOME project. In this context, a bunch of website repositories have already been moved to the [new GitLab team](https://gitlab.gnome.org/Teams/Websites). You can also find us in [#pages:gnome.org](https://matrix.to/#/#pages:gnome.org).

# GNOME Circle Apps and Libraries

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> Marco Köpcke (Capypara) added Python support to Workbench 🎉
> 
> Are you learning Python or Gtk? Porting Library entries to Python is a great way to learn and contribute.
> Help welcome, see [our guide](https://github.com/sonnyp/Workbench/blob/main/CONTRIBUTING.md).
> {{< video src="829ad86e05d0aed84362b88088c9fb0bc197d0691717640397066338304.mp4" >}}

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Julian 🍃](https://matrix.to/#/@julianhofer:gnome.org) announces

> I've extended the chapter "The Main Event Loop" of the gtk-rs book to include more information on how to deal with `async` code.
> It shows how to:
> * embed blocking calls in an async context,
> * run async functions from external crates, and
> * integrate with tokio.
> 
> Thanks goes to Fina, Sabrina and Bilal Elmoussaoui for reviewing the changes.

# Third Party Projects

[d-k-bo](https://matrix.to/#/@d-k-bo:matrix.org) says

> Televido has been released and is available on [Flathub](https://flathub.org/apps/de.k_bo.Televido)!
> 
> It lets you livestream, search and play media from German-language public television services. Based on APIs provided by the [MediathekView](https://mediathekview.de/) project, the content is delivered directly from the respective television services and may not be accessible outside of Germany.
> ![](pkOKBFsnpKikAeBSEfabbCbf.png)

[Hunter Wittenborn](https://matrix.to/#/@hunter:hunterwittenborn.com) reports

> Celeste, a GTK file synchronization program that can connect to a plethora of cloud providers, has seen quite a few updates since last featured in This Week in GNOME.
> 
> Some notable changes include the addition of Proton Drive support, and new app icons that greatly help in showing what Celeste is.
> 
> Celeste is available on [Flathub](https://flathub.org/apps/com.hunterwittenborn.Celeste) and the [Snap Store](https://snapcraft.io/celeste) if you'd like to give it a try.

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

Tag your music.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Tagger [V2023.10.0](https://github.com/NickvisionApps/Tagger/releases/tag/2023.10.0) is here! We've been hard at work this month bringing you a release packed with features and fixes :)
> 
> First, this release introduces a brand-new Windows version of the app built on WindowsAppSDK and WinUI 3! For all those unfortunately stuck using Windows at times, they can now use this great app on their system with a 1:1 feature match with the GNOME version! 
> 
> In terms of features, this release introduces full support for playlists with relative paths, new tag properties, and a new information dialog for album art. Tagger will also watch a music folder library for changes on disk and prompt the user to reload the folder within the app. Furthermore, we fixed many issues and tweaked the design of some aspects of the app in which you can read all about below!
> 
> A special thanks to @kissthermite for bringing most of the changes in this release to our attention :)
> 
> Here's the full changelog:
> * Tagger is now available for Windows using Windows App SDK and WinUI 3
> * Added the option to use relative paths when creating a playlist. This means that Tagger also now supports opening playlists with relative paths
> * Added the Disc Number, Disc Total, and Publishing Date fields to additional properties
> * Added information dialog for album art
> * Added an option in Preferences to limit file name characters to those only supported by Windows
> * Tagger will now watch a music folder library for changes on disk and prompt the user to reload if necessary
> * Tagger will now display front album art within a music file row itself if available
> * Tagger will now remember previously used format strings for file name to tag and tag to file name conversions
> * Fixed an issue where downloaded lyrics would sometimes contain html encoded characters
> * Fixed an issue where file names containing the `<` character caused the music file row to not display
> * Fixed an issue where the duration displayed for multiple selected files was wrong
> * Improved create playlist dialog ux
> * Updated translations (Thanks everyone on Weblate!)
> ![](BlWfNnigPsXcRScQxuixkkRA.png)

### Login Manager Settings [↗](https://gdm-settings.github.io)

Customize your login screen.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) announces

> GDM Settings version 4 was released.
> 
> * It brings a new UI style, GNOME 45 support, and more. [Full release notes](https://github.com/gdm-settings/gdm-settings/releases/tag/v4.0) are available in [GitHub releases](https://github.com/gdm-settings/gdm-settings/releases).
> * Version 4 also brings the rename from "Login Manager Settings" to "GDM Settings". Internally, GDM Settings has always been known as gdm-settings. So, the rename should makes things clearer and more consistent.
> * GDM Settings now supports [GitHub Sponsors](https://github.com/sponsors/gdm-settings) as well.
> ![](xkGpURmhetNDWGINRtNMuKBz.png)
> ![](yILgWagVNnzLIjykJpyMsMtP.png)
> ![](dorRDBdSltsZcnBULSsXeakA.png)

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) reports

> This week the Foundation has been preparing for a few upcoming events! Now that our new Executive Director has started, we’re hosting a Meet and Greet to give everyone a chance to get to know Holly and ask questions about her plans for the Foundation. This event is open to everyone, you can register and submit your questions for Holly [here](https://events.gnome.org/event/172/). Please note that all question submissions are due by Nov 7th.
> 
> We’ve also signed up for a booth at [SCaLE x21](https://www.socallinuxexpo.org/scale/21x). The 2024 conference will take place on March 14-17 and the CfP is still open. The deadline for [submitting talks](https://www.socallinuxexpo.org/scale/21x/cfp) is Nov 1, 2023, so make sure to send yours in soon! 
> 
> In addition to these two events, GNOME Asia 2023 is just over a month away! The [full schedule of presentations](https://events.gnome.org/event/170/timetable/#20231201) is available on the event website and registration is open for both in-person and remote attendees. Please let us know you’re attending by [registering](https://events.gnome.org/event/170/registrations/126/). 
> 
> Reminder:
> We’re still looking for sponsors for GNOME Asia 2023! If you or your company would like to sponsor this year's summit reach out to [mwu@gnome.org](mailto:mwu@gnome.org) for more information.
> 
> Volunteer Opportunity:
> We’re looking for people to help manage the Foundation’s social media channels. If you’re interested in getting involved reach out to [chenriksen@gnome.org](mailto:chenriksen@gnome.org) for more information.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

