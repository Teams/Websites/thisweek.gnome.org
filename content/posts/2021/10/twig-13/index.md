---
title: "#13 It begins…"
author: Felix
date: 2021-10-08
tags: ["phosh", "gnome-software", "gnome-todo", "gtk-rs", "gnome-tour"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 01 to October 08.<!--more-->

# Core Apps and Libraries

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> Disk Usage Analyzer has been ported to using GTK4 and libadwaita.
> ![](b6b9cdde5efcc87a16b84a9925a35667ca887d23.png)

### GNOME Tour [↗](https://gitlab.gnome.org/GNOME/gnome-tour)

A guided tour and greeter for GNOME.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> Chris 🌱️'s port of [Tour](https://gitlab.gnome.org/GNOME/gnome-tour) to GTK 4 & libadwaita was merged today.
> ![](2f28c14fbc7a1d920a0bd9030bcb669dd78e8cc5.png)

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> GNOME Software was ported to GTK4 and libadwaita. This brings various improvements, such as improved performance, the restyled Adwaita, new widgets, and more.
> ![](b282e2e8dd453959e74a8ac0729c84a04bc16c27.png)
> ![](abebf8e4889b3dfab3889d36a04d859db52d2f4c.png)

# Circle Apps and Libraries

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) reports

> Chris 🌱️ and I have ported Commit to GTK4 / libadwaita https://apps.gnome.org/app/re.sonny.Commit/
> ![](ivAOzrwkoZIgOlSvVHGPaNfx.png)

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Julian Hofer](https://matrix.to/#/@julianhofer:gnome.org) announces

> I've added a [chapter](https://gtk-rs.org/gtk4-rs/stable/latest/book/actions.html) about actions to the gtk4-rs book. It was reviewed by Ivan Molodetskikh and Sabrina.
> ![](a2a12738e36281d68bee2a1fd2249750c5b5e3fc.gif)

# Third Party Projects

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> I've released a portals demo app on Flathub, it's meant as a test case for the various xdg portals. You can grab it from https://flathub.org/apps/details/com.belmoussaoui.ashpd.demo
> ![](6359ff3329883f0d00c7bfcf67121cc5a9c2da76.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) reports

> Phosh's CI pipeline [gitlab.gnome.org](https://gitlab.gnome.org/World/Phosh/phosh)
> now automatically takes screenshots in different languages (currently Arabic,
> Japanese and German). This hopefully makes it simpler for developers, designers
> and translators to check layout changes:
> ![](ijpXUnwRUjcLuuTdZeKRMshO.png)

### GNOME To Do [↗](https://gitlab.gnome.org/GNOME/gnome-todo)

An intuitive and powerful application to manage your personal tasks.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> GNOME To Do now uses the new color scheme API provided by libadwaita to handle dark and light modes. The style selector also received some visual improvements.
> ![](24751c7e2b6e6f8f05da9bcef7d5731fe956a36e.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

