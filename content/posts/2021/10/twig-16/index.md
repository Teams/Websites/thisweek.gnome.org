---
title: "#16 Card-Carrying"
author: Felix
date: 2021-10-29
tags: ["gnome-control-center", "libadwaita", "gnome-builder", "fragments", "newsflash", "phosh"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 22 to October 29.<!--more-->

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> libadwaita has new style classes:
> 
> * `.card` to help style standalone widgets similarly to boxed lists as seen in Software, Shortwave or Health
> * `.opaque` for making custom colored buttons
> 
> and a demo listing most of the available style classes (both the ones that it adds and the ones from GTK) that can be used as a reference
> ![](035ffa47b99df6eafea829698b0ae4904206fbd5.png)
> ![](db53c7902e2bbdd7c0f24a361e1b541c028ec4c8.png)

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> the `gnome-bluetooth` library was ported to GTK4. The GTK4 version can be installed in parallel to the GTK3 version.
> ![](d832a2784f544e587a1a0234d8e837d03fe6f4a1.png)

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[Marco Melorio](https://matrix.to/#/@melix99:gnome.org) announces

> I've added a GTK4 Rust template to GNOME Builder. Compared to the GTK3 template, it features composite templates, subclassing, about dialog, gactions and accelerators.

# Circle Apps and Libraries

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) says

> This week [Mousai](https://apps.gnome.org/app/io.github.seadve.Mousai/) entered GNOME Circle. Mousai can identify playing songs via microphone or from you device audio. Congratulations!
> ![](94077fcc1cdeb59b8f183d149543eff139d733a5.png)

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[jangernert](https://matrix.to/#/@jangernert:matrix.org) reports

> NewsFlash lost support for feedly due to the API secret expiring. The new release of version 1.5.0 removes the feedly option from the flathub builds. But the code is still present and custom builds with developer secrets are possible. As an alternative to feedly NewsFlash 1.5.0 now offers support for Inoreader. We are still looking to find a maintainer for the Inoreader integration who dog-foods NewsFlash together with Inoreader.

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> This week I implemented queue support in Fragments v2 (the upcoming Rust/Libadwaita rewrite). The torrent queue can be reordered so that torrents are downloaded in the desired order. Maximiliano [added support](https://gitlab.gnome.org/World/Fragments/-/merge_requests/87) for automatically detecting magnet links from the clipboard.
> ![](yYxsQrMRLePTOyhrFBqkmvsK.png)

# Third Party Projects

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) announces

> We released phosh 0.14.0 featuring
> 
> * Launch splash support
> * An improved media player widget with seek buttons
> * Less flicker on startup
> 
> and much more. Check the full [release notes](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.14.0)
> for more details and contributors.
> ![](RcRZcAbLCDebrtIKXvgdOzuQ.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
