---
title: "#7 Software Upgrade"
author: Felix
date: 2021-08-27
tags: ["health", "fractal", "gnome-software", "gnome-shell", "pango", "libadwaita"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 20 to August 27.<!--more-->

# Core Apps and Libraries

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) announces

> Over the past weeks there were a number of notable improvements in Software's interface that haven't gotten much coverage yet.
> 
> The [new category tiles](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/872), along with the [simplified set of categories](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/813) and [revamped category pages](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/721) make for a much-improved browsing experience.
> ![](fb395ed19b06a65d88b27ed176741d9b5a8b7685.png)

> The app details page has received a visual overhaul thanks to Philip Withnall, including the [page header](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/887) and the [metadata section](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/863#note_1241227).
> ![](ddd0a1d5139c332274c9ca5e82a9ac4835b0f796.png)
> ![](4888b9044e90a876f49ec3e60787565ae94292c1.png)

> The app details page also features [context tiles](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/770) now, a new way to get a quick overview of the most important information about an app.
> 
> There are tiles for Storage (download and install size), safety (sandboxing, license), hardware support (supported/required input and output devices), and age ratings. Clicking these tiles opens a dialog with more details.
> 
> All of this is courtesy of Philip Withnall, with additional UI polish by Adrien Plazas.
> ![](c13cc311f9dd1fe93f2cbaf5aa1177112cd58bfd.png)

> Most views in the app are fully adaptive now thanks to Adrien Plazas, including the [Explore page](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/902) and the [app details page](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/904).
> ![](f00bfc01b597bda6009b7f6ea9435d6e57c69a5e.png)
> ![](fffd98cb645cf46b1d0d9a417a32ac0688a3c391.png)
> ![](aaefb65ad1df9759a91e932a6959dd85b1cc41bb.png)

> Philip Withnall also [added an infobar and dialog](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/903) for the case that an app isn't translated to the system language, encouraging people to help translate it.

> When there aren't any updates the Updates page [now features a fun illustration](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/721) by Jakub Steiner.
> ![](77ed54702a6f7319894d1802aa379a9fe13bd823.png)

### Pango [↗](https://gitlab.gnome.org/GNOME/pango)

A library for layout and rendering of text, with an emphasis on internationalization.

[matthiasc](https://matrix.to/#/@matthiasc:gnome.org) reports

> Pango has seen some improvements recently. This blog post has some details: https://blogs.gnome.org/mclasen/2021/08/26/pango-updates-2/

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Manuel Genovés](https://matrix.to/#/@somas95:gnome.org) reports

> I've been working on an animation API for libadwaita this summer as a part of the GSoC program. So far I've landed the grounding work to easily implement animation support in the future, and started working on a timed-animation API. You can read more in this [blogpost](https://blogs.gnome.org/manugen/2021/08/23/wrapping-up-gsoc-2021/).
> ![](7e6f1ef74a34be2f948efd9b324d761c21a4ea0e.gif)

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) announces

> The new work-in-progress GNOME Shell [screenshot UI](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/1954) has got a few updates! It has finally learned to save screenshots to files, all while showing a shiny new notification. You can now take a window screenshot right from the window menu—a feature inspired by [elementary OS 6](https://blog.elementary.io/elementary-os-6-odin-released/#desktop-features). Finally, the screenshot UI can now capture GNOME Shell system dialogs, although currently with an odd transparency bug.
> {{< video src="3d71cde8e5f23d28f9ce15dab441c4f2302d72a6.webm" >}}

# Circle Apps and Libraries

### Health [↗](https://gitlab.gnome.org/World/Health)

Collect, store and visualise metrics about yourself.

[Cogitri](https://matrix.to/#/@cogitri:cogitri.dev) says

> Health has seen a multitude of improvements this last week. Visvesh’s last MR for this GSoC, an overview of how many calories you’ve burned, has been merged. I also worked on making Health’s notification daemon autostart during login.  Additionally, Health was moved to World/ today 🎉

# Third Party Projects

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Alexandre Franke](https://matrix.to/#/@afranke:matrix.org) announces

> Busy week in Fractal land! It is the end for GSoC, but it’s not really over. While some merge requests from our interns are still ongoing, Julian merged a bunch of them. On Kai’s side, the long awaited [Room details](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/789) are finally here. Alejandro, on the other hand, landed code to [get display name and avatar of accounts at startup](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/802).

# GNOME Shell Extensions

[Romain](https://matrix.to/#/@romainvigier:matrix.org) reports

> I released new versions of [Night Theme Switcher](https://nightthemeswitcher.romainvigier.fr/), a GNOME Shell extension that makes your desktop easy on the eye, day and night.
> 
> It comes with a brand new preferences window, is already compatible with GNOME 41, and since I moved translations to Weblate, it's now easier to [contribute your own](https://hosted.weblate.org/engage/night-theme-switcher/)!
> 
> (This is not an official dark mode, only hacks that emulate one)
> ![](rdOxXqNtYPiKukgxqUACnzdW.png)

# Miscellaneous

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) announces

> The _Apps for GNOME_ website is now available on [apps.gnome.org](https://apps.gnome.org/). It gives you an overview of apps in the GNOME ecosystem and allows you to learn more about specific apps.
> 
> The page content is generated from existing app metadata. This keeps the website up-to-date with little extra work. Since those metadata are covered by [GNOME's translation system](https://wiki.gnome.org/TranslationProject) we can also provide the website in a [variety of languages](https://apps.gnome.org/languages/).
> 
> You can check out my [blog posts about the project](https://blogs.gnome.org/sophieh/category/apps-for-gnome/) if you want to learn more.
> ![](59cf45bd4334dfbc588158aee6b1845de83fe3f2.png)

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) announces

> I've [started](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/224) working on a system-wide dark style preference in a way that allows apps to be aware of it and can be cross-desktop. (the settings UI is a placeholder)
> ![](1365cffede02761b1fb18d1c7f73d6e35cf64f9d.png)

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> flatpak-github-actions, a set of GitHub CI actions that helps you build and deploy your application as a Flatpak received the support of building against different CPU architectures in the new release V4. See the [README](https://github.com/bilelmoussaoui/flatpak-github-actions) for details on how to set it up.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
