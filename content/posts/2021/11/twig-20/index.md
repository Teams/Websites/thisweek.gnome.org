---
title: "#20 Colorful Characters"
author: Felix
date: 2021-11-26
tags: ["gnome-mahjongg", "gnome-characters", "fragments", "vala", "gnome-shell", "libadwaita", "flatseal", "just-perfection", "tangram"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 19 to November 26.<!--more-->

# Core Apps and Libraries

### Characters [↗](https://gitlab.gnome.org/GNOME/gnome-characters)

 A simple utility application to find and insert unusual characters.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> I have landed a GTK 4 / libadwaita port of Characters which includes a refactoring/cleanup of the whole codebase. Along with that Alexander Mikhaylenko ported the accompanying C library from using libunistring which allowed us to update the unicode database to the latest release.
> ![](d96f69cfe2b7d38c577f4dba35f8e4e48eb84d4c.png)
> ![](dfb3b9cedb521a2addd2d74d358bb1739ca3c8d8.png)

### Vala [↗](https://gitlab.gnome.org/GNOME/vala)

An object-oriented programming language with a self-hosting compiler that generates C code and uses the GObject system

[lwildberg](https://matrix.to/#/@lw64:gnome.org) announces

> With much help from Rico Tzschichholz I fixed multiline errors. Before, the location of errors which stretch over multiple lines were just not printed out. On the way, I also made the output a bit prettier and more useful by adding the line, where the error is located in the source file. Instead of
> ```
> int foo = bar();
>           ^^^
> ```
> 
> it outputs now
> ```
>     2 | int foo = bar();
>       |           ^~~
> ```
> 
> As you can see, the underline consists also now out of "~"'s and starts with a "^". This makes it easier to find the beginning of the error especially in multiline errors.

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Manuel Genovés](https://matrix.to/#/@somas95:gnome.org) says

> The work we've been doing these past months for the Timed Animation API has landed on libadwaita. I'm working to land the Spring Animation API soon
> ![](e4b36e9da4b2a1af895143aec43ee51929acad4e.gif)

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) reports

> The work-in-progress [screenshot UI](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/1954) got a bit of polish. I added hotkeys to switch between area, screen and window selection. The window selection now picks the currently focused window by default. The window border radius was increased to remove gaps for windows with rounded corners. Additionally, I made the "Screenshot captured" notification display the thumbnail with the correct aspect ratio to avoid ugly scaling. Finally, the window selection button is now disabled in screencast mode since it's not implemented yet.
> {{< video src="def3b1cec44c146493ac4bb5fe442aa06213d0da.webm" >}}

### xdg-desktop-portal

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> This week, a fascinating new feature landed in xdg-desktop-portal: restoring screencast sessions. We already implement this feature in the GNOME portal frontend, and together with that, we've done a round of polish and cleanups to these portal dialogs.
> ![](7e2e60b805d97a802c9fdc69afda4e4413dae3a8.png)

### Libgnome-desktop

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> Libgnome-desktop has been split into three separate shared libraries, and two of them (GnomeRR and GnomeBG) have been ported from GTK3 to GTK4. This will unblock the port to GTK4 of various system components. https://gitlab.gnome.org/GNOME/gnome-desktop/-/merge_requests/123

### GWeather

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> I released the first developers snapshot for GWeather-4.0, which is meant to be used by GTK4 applications showing weather or time zone information. The new API is not yet stable, but it is already useful for porting existing applications. You can find what changed in the [migration guide](https://gnome.pages.gitlab.gnome.org/libgweather/migrating-3to4.html).

# Circle Apps and Libraries

### Tangram [↗](https://github.com/sonnyp/Tangram)

A browser for your pinned tabs.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> Tangram 1.4.0 was released. It comes with
> 
> * per-tab notifications priority
> * middle-click / Ctrl+Click to open links in the default browser
> * a fix for popular websites with external sign-in
> 
> [See Tangram on GNOME Apps.](https://apps.gnome.org/app/re.sonny.Tangram/)

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) says

> I added authentication support to Fragments V2. It's possible now to connect to a remote username/password protected Transmission/Fragments session. The credentials are then automatically stored in the keyring so you don't have to enter them again the next time.
> {{< video src="JqOysMBMhAyBDSmSkiEmpMMt.webm" >}}

# Third Party Projects

### Mahjongg [↗](https://wiki.gnome.org/Apps/Mahjongg)

A solitaire (one player) version of the classic Eastern tile game. The objective is to select pairs of similar tiles.

[gwagner](https://matrix.to/#/@gwagner:gnome.org) says

> Reworked GNOME Mahjongg. Initial plan was to help out with the Dark Style Initiative (https://gitlab.gnome.org/GNOME/Initiatives/-/issues/32) but i ended up porting to Gtk4 and libadwaita. I also made some refactorings to make the codebase more modern (Composite Templates, more subclasses)
> ![](7e785fab1fc448540dee4b67c15e51d8e03b9f8a.png)

### Flatseal [↗](https://github.com/tchx84/Flatseal)

A graphical utility to review and modify permissions of Flatpak applications.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) announces

> A new Flatseal release is out 🥳🎉, and it comes with subtle visual improvements, a few bugs fixes, one more permission and a big quality of life improvement. [Check](https://blogs.gnome.org/tchx84/2021/11/20/flatseal-1-7-5/) it out!

# GNOME Shell Extensions

### Just Perfection [↗](https://extensions.gnome.org/extension/3843/just-perfection/)

A tweak tool to customize the GNOME Shell and to disable UI elements.

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) says

> Just Perfection extension is celebrating one year of active development with version 16:
> https://extensions.gnome.org/extension/3843/just-perfection/
> The extension added profile switcher to its prefs window and users can select between default, minimal and super minimal profiles.
> This version added ripple effects visibility, disabling double super key to app grid, panel in overview and more.
> https://www.youtube.com/watch?v=u8koWhtk5hg
> ![](54eef694290a19787662416dbb2c064907bf5ca9.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
