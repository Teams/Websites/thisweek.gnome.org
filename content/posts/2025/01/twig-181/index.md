---
title: "#181 Happy New Year!"
author: Felix
date: 2025-01-03
tags: ["phosh", "shortwave", "mutter"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 27 to January 03.<!--more-->

# GNOME Core Apps and Libraries

### Mutter [↗](https://gitlab.gnome.org/GNOME/mutter/)

A Wayland display server and X11 window manager and compositor library.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> Mutter can now be built without fonts rendering support allowing to get rid of Pango/Harfbuzz/Fribidi dependencies. When combined with the previously added options to disable X11/Xwayland, we should soon be able to drop Cairo as a dependency in this build configuration.
> This change has 0 impact on GNOME Shell extensions as GNOME Shell cannot be built without fonts rendering support. Compositors built using libmutter, might need to include `clutter/clutter-pango.h` header where they use any of the fonts rendering APIs.

# GNOME Circle Apps and Libraries

### Shortwave [↗](https://gitlab.gnome.org/World/Shortwave)

Internet radio player with over 30000 stations.

[Felix](https://matrix.to/#/@felix:haecker.io) reports

> Over the holidays I have significantly improved the recording feature of Shortwave:
> * Choice between different recording modes (“Save Everything”, “Decide for Each Track” and “Record Nothing”)
> * Directory in which recorded tracks are saved can now be changed
> * Minimum duration for tracks can be adjusted
> * New dialog window for tracks to display further information
> * New button to automatically save desired tracks after they have been completely recorded
> * Ongoing recordings can be canceled
> * Improved notifications, with direct option to automatically save the track or not to record
> ![](CWvcnvNLOZyDgXgGHvnnxnOW.png)
> ![](xWgaMzIOsrlJkvWACtJcbYfx.png)
> ![](YxYvVktMoGvAAYLCmeFXytcX.png)

# Third Party Projects

[Hari Rana | TheEvilSkeleton](https://matrix.to/#/@theevilskeleton:fedora.im) reports

> Introducing [Refine](https://tesk.page/refine), an app to tweak advanced and experimental settings in GNOME. It is an alternative to [GNOME Tweaks](<https://gitlab.gnome.org/GNOME/gnome-tweaks>), and is a pet project I'm currently working to experiment with [PyGObject](<https://pygobject.gnome.org/>) and [dconf](<https://gitlab.gnome.org/GNOME/dconf>), while following the [data-driven](<https://en.wikipedia.org/wiki/Data-driven_programming>), [object-oriented](<https://en.wikipedia.org/wiki/Object-oriented_programming>), and [composition](<https://en.wikipedia.org/wiki/Object_composition>) paradigms.
> 
> The entire codebase is made up of widgets that provide all the functionality needed to add an option. For example, instead of adding each option programmatically in Refine, the ultimate goal is to have it all done in the UI file.
> 
> For example, if we want to add an option to enable or disable middle click paste, all we need is the following code in the UI file:
> 
> ```
> $RefineSwitchRow {
>   title: _('Middle Click Paste');
>   schema-id: 'org.gnome.desktop.interface';
>   key: 'gtk-enable-primary-paste';
> }
> ```
> That's it. The `RefineSwitchRow` widget will do whatever it needs to do to ensure the option is available, grab the setting if it's available, and display it to the user. Many of these widgets provide extra functionality, such as a Reset button.
> 
> You can get Refine on Flathub: [flathub.org/apps/page.tesk.Refine](<https://flathub.org/apps/page.tesk.Refine>)
> 
> Everything else (source code, screenshot, etc.) is in the project website: [tesk.page/refine](<https://tesk.page/refine/>), as well as the Flathub link.
> ![](67b2ccc26a36da07391e3382153dc5ad04c895881874891840604340224.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) reports

> [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) 0.44.0 is out: 
> 
> Phosh now shows the configured wallpaper / background in the overview (instead of just classic black) and has a button to unfullscreen on app thumbnails. We have done more notification style fixes (e.g. banners now disappear with an animation) and the configuration dialogs now use `AdwPreferencesDialog` (to be more mobile friendly) and `GtkFileDialog` to use the portal.
> 
> There's more, see the full details at [here](https://phosh.mobi/releases/rel-0.44.0/)
> ![](PhSQWoVCBOQlSkycwcHAPqkZ.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
