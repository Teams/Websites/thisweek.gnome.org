---
title: "#185 Adwaita Sans"
author: Felix
date: 2025-01-31
tags: ["parabolic", "libadwaita", "graphs", "gnome-text-editor", "gaphor", "hieroglyphic", "gtk", "gnome-control-center", "fractal"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 24 to January 31.<!--more-->

# GNOME Core Apps and Libraries

[Allan Day](https://matrix.to/#/@aday:gnome.org) reports

> GNOME changed its UI and monospace fonts this week, in a long anticipated change that is planned for GNOME 48. The new fonts are called Adwaita Sans and Adwaita Mono. Adwaita Sans is a modified version of Inter, and replaces Cantarell as the UI font. Adwaita Mono is a modified version of Iosevka, and replaces Source Code Pro as the default monospace font. This feature was implemented by Jamie Gravendeel, with last-minute assistance from Florian Muellner.
> ![](b9867415e44767c7e6451e2f3c87635b359cc7bb1885375975693221888.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) announces

> libadwaita now provides API for accessing the system monospace and document fonts, both [programmatically](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/property.StyleManager.monospace-font-name.html) and from [CSS](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/css-variables.html#fonts).
> 
> Additionally, the `.monospace` style class uses the system font now, instead of `monospace`, so apps don't need to access it themselves from the settings portal and gsettings anymore

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> The GTK developers held a hackfest in Brussels, covering various topics:
> 
> * accessibility
> * text rendering
> * deprecations
> * new Android backend
> * GTK5 features
> 
> A full report will be published on the [GTK development blog](https://blog.gtk.org/), so keep a keen eye for it

### Text Editor [↗](https://gitlab.gnome.org/GNOME/gnome-text-editor/)

Text Editor is a simple text editor that focus on session management.

[Allan Day](https://matrix.to/#/@aday:gnome.org) announces

> Some news from a previous week: a collection of design updates have appeared in Text Editor, in time for the upcoming GNOME 48 release. The changes include a new document sidebar, which combines document properties and settings. There is also a new floating line/column indicator.
> ![](gte.png)

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Allan Day](https://matrix.to/#/@aday:gnome.org) reports

> Work continued on GNOME's new Digital Wellbeing features this week. Changes were made to allow screen time to be viewed independently of the screen time limit feature, and a setting was added to allow screen time recording to be disabled. The labels in the settings panel were also polished. Much of this work was made possible by an Endless grant to the GNOME Foundation.
> ![](3f6d4cf739e7dc5058fc484adba7cc190b8d8f9c1885378103539138560.png)

# GNOME Circle Apps and Libraries

[Brage Fuglseth (he/him)](https://matrix.to/#/@bragefuglseth:gnome.org) reports

> This week [Iotas](https://apps.gnome.org/Iotas/) was accepted into GNOME Circle. Iotas aims to provide distraction-free note taking, and lets you sync your notes across devices with Nextcloud. Congratulations!
> ![](0a1525ec7b5a33354a12a8d30139fddb3a1e426e1884872369315512320.png)

### Hieroglyphic [↗](https://github.com/FineFindus/Hieroglyphic)

Find LaTeX symbols

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) says

> A new Hieroglyphic update has been released, bringing a number of improvements:
>  - Improved classifier, which now includes the nearly 500 user-contributed symbols, thanks to everyone who helped.
>  - The backend infrastructure has been updated to make it easier for future improvements to the classifier.
>  - The drawing area has been rewritten to use GTK4's rendering capabilities instead of Cairo.
> ![](bYstNKunOVyHvnysqEoBdyGd.png)

### Graphs [↗](https://graphs.sjoerd.se)

Plot and manipulate data

[Sjoerd Stendahl](https://matrix.to/#/@sjoerdb93:matrix.org) reports

> This week we released version 1.8.4 of Graphs, it's a minor release primarily focusing on the update to the GNOME 47 runtime:
> 
> * Update to the GNOME 47 runtime, with support for accent colours.
> * The rubberband on the canvas has been improved, now it has rounded corners similar to Nautilus
> * Equation parsing has been improved, now handling edge-cases better. It's now also completely case-insensitive, meaning "Pi" and "PI" are both acceptable variants of the greek letter π.
> 
> Meanwhile we're working hard on the next major release. The latest main channel on our Gitlab now has support for actual equations spanning an infinite canvas, and operations are calculated analytically with the equations changing their names accordingly. We've also got a brand new style-editor with a live preview. Stay tuned for an announcement on this later on, in the meantime you can check out the upcoming release on the Flathub beta channel.
> ![](esnJYPHhelbriSojafMmOQRI.png)

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) announces

> This week Dan Yeaw  release Gaphor 3.0. This release is a major step forward. It contains a lot of UI updates. In addition Gaphor's internal data models has been updated and improved. More details you can find in [this blog post](https://gaphor.org/2025/01/26/gaphor-3.0/). You can find the latest version in Flatpak. macOS and Windows versions are available from [our website](https://gaphor.org/download/).
> ![](FdJKCYMRQhboWMfeeXXeglbG.png)

# Third Party Projects

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) announces

> How are you going to find your friends and coordinate end of day drinks when you’re lost in the middle of a large crowd in a big city? With the new version of your favorite Matrix client, of course! Here is Fractal 10.
> 
> * The QR code scanning code has been ported to [libaperture](https://crates.io/crates/aperture), the library behind GNOME Camera. This should result in better performance and more reliability.
> * [OAuth 2.0 compatibility](https://areweoidcyet.com/#next-gen-auth-aware-clients) was added, to make sure that we are ready for [the upcoming authentication changes for matrix.org](https://matrix.org/blog/2025/01/06/authentication-changes/).
> * Pills for users and rooms mentions show consistently in the right place instead of seemingly random places, getting rid of one of our oldest and most annoying bug.
> * Attachments go through the send queue, ensuring correct order of all messages and improving the visual feedback.
> * Videos were often not playing after loading in the room history. This was fixed, and we also show properly when an error occurred.
> * We were downloading too many different sizes for avatar images, which would fill the media cache needlessly. We now only download a couple of sizes. This has the extra benefit of fixing blurry or missing thumbnails in notifications.
> 
> 
> As usual, this release includes other improvements and fixes thanks to all our contributors, and our upstream projects.
> 
> We want to address special thanks to the translators who worked on this version. We know this is a huge undertaking and have a deep appreciation for what you’ve done. If you want to help with this effort, head over to [Damned Lies](https://l10n.gnome.org/).
> 
> This version is available right now on [Flathub](https://flathub.org/apps/org.gnome.Fractal).
> 
> We have a lot of improvements in mind for our next release, but if you want a particular feature to make it, the surest way is to implement it yourself! Start by looking at our [issues](https://gitlab.gnome.org/World/fractal/-/issues/) or just come say hello in [our Matrix room](https://matrix.to/#/#fractal:gnome.org).

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Parabolic [V2025.1.4](https://github.com/NickvisionApps/Parabolic/releases/tag/2025.1.4) was also released this week with some new features and fixes!
> 
> Here's the full changelog:
> * Added a new Embed Thumbnails option in Preferences to enable/disable Parabolic's downloading of thumbnails separate from metadata
> * Added a disclaimer about embedding thumbnails/subtitles when using generic file types
> * Fixed an issue where the incorrect previous video and/or audio format was selected
> * Fixed an issue where chapters were embedded even if the option was disabled
> * Fixed an issue where splitting media by chapters would result in incorrect media lengths in the split files
> * Fixed an issue where video and audio formats were not selectable on GNOME

# Events

[Kristi Progri](https://matrix.to/#/@kristiprogri:gnome.org) reports

> CFP for Linux App Summit 2025 is now open! 
> Join us in Tirana, Albania on April 25-26th.  Submit your paper by Feb 15th 
>  https://conf.linuxappsummit.org/event/7/
> 
> For more information and updates check our website: https://linuxappsummit.org/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
