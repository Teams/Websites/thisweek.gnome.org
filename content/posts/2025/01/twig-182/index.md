---
title: "#182 Updated Crypto"
author: Felix
date: 2025-01-10
tags: ["vala", "parabolic", "libadwaita", "loupe", "shortwave"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 03 to January 10.<!--more-->

# GNOME Core Apps and Libraries

[nielsdg](https://matrix.to/#/@nielsdg:gnome.org) reports

> [gcr](https://gitlab.gnome.org/GNOME/gcr/), a core library that provides a GObject-oriented interface to several crypto APIs, is preparing for the new 4.4 version with the alpha release 4.3.90. It contains some new APIs for `GcrCertificate`, such as the new [`GcrCertificateExtension`](https://gnome.pages.gitlab.gnome.org/gcr/gcr-4/class.CertificateExtension.html) class that allows you to inspect certificate extensions. 🕵️

[nielsdg](https://matrix.to/#/@nielsdg:gnome.org) says

> [GNOME Keyring](https://gitlab.gnome.org/GNOME/gnome-keyring/) has now finally [moved to Meson](https://gitlab.gnome.org/GNOME/gnome-keyring/-/merge_requests/25) and has dropped support for building with autotools. This will be part of the upcoming 48.alpha release.

### Vala [↗](https://gitlab.gnome.org/GNOME/vala)

An object-oriented programming language with a self-hosting compiler that generates C code and uses the GObject system.

[lorenzw](https://matrix.to/#/@lw64:gnome.org) says

> Many people might have seen it already, but a while ago we finally officailly moved our documentation from the old GNOME wiki to a new website: https://docs.vala.dev!
> This has been a long-standing task completed by Colin Kiama.
> The pages are hosted on https://github.com/vala-lang/vala-docs and everyone is welcome to contribute and improve them, we have already started to file tickets in the issue  tracker and assign labels, especially for newcomers, so its easy to start helping out! We want to port a lot more docs and code examples from other locations  to this new website, and thats not difficult at all!
> The website is built similar to all other new GNOME documentation websites using sphinx, so you don't even need to learn a new markup language. Happy docs reading and hacking! :D
> ![](efdb715f37502458391974ec11a416bfe77d8b2a1877603889130242048.png)

### Image Viewer (Loupe) [↗](https://apps.gnome.org/app/org.gnome.Loupe/)

Browse through images and inspect their metadata.

[Sophie 🏳️‍🌈 🏳️‍⚧️ (she/her)](https://matrix.to/#/@sophieherold:gnome.org) says

> Image Viewer (Loupe) 48.alpha is now [available](https://welcome.gnome.org/app/Loupe/#installing-a-nightly-build).  
> 
> This new release adds image editing support for PNGs and JPEGs. Images can be cropped ([tracking issue](https://gitlab.gnome.org/GNOME/loupe/-/issues/409)), rotated, and flipped. New zoom controls allow setting a specific zoom level and feature a more compact style. Support for additional metadata formats like XMP and new image information fields have been added as well.

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) reports

> adaptive preview has received a bunch of updates since the last time: for example it now shows device bezels and allows to take screenshot of the app along with the shell panels and bezels
> ![](72ce8aa7ae3e33fd6514e63e39ac7426de0aa0021877456056142004224.png)
> ![](f47a783986e619d5820243be6671c1a39d4d83d91877456045958234112.png)

# GNOME Circle Apps and Libraries

### Shortwave [↗](https://gitlab.gnome.org/World/Shortwave)

Internet radio player with over 30000 stations.

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> At the end of the festive season I was able to implement one more feature: Shortwave now supports [background playback](https://gitlab.gnome.org/World/Shortwave/-/issues/471), and interacts with the background portal to display the current status in the system menu!

# Third Party Projects

[Fabrix](https://matrix.to/#/@fabrixxm:kirgroup.net) announces

> Confy 0.8.0 has been released. Confy is a conference schedule companion. This release brings updated UI design, some quality of life improvements like recent opened schedules list, and fixes to schedule parsing. https://confy.kirgroup.net/
> ![](9d889bf35f814438cd8601c42c2dfd93f7c8d2c2fe70fdc2de7f6aa33a13ec0d.png)
> ![](f9f26166ead3099acd3d8e5f747d48678855a77f87667c12fcb96e64ee2e0aff.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Parabolic [V2025.1.0](https://github.com/NickvisionApps/Parabolic/releases/tag/2025.1.0) is here! This update contains various bug fixes for issues users were experiencing, as well as a new format selection system.
> 
> Here's the full changelog:
> * Parabolic will now display all available video and audio formats for selection by the user when downloading a single media
> * Fixed an issue where some video downloads contained no audio
> * Fixed an issue where progress was incorrectly reported for some downloads
> * Fixed an issue where downloads would not stop on Windows
> * Fixed an issue where paths with accent marks were not handled correctly on Windows
> * Fixed an issue where the bundled ffmpeg did not work correctly on some Windows systems
> ![](BZHxEjlwjuKfqOBHarYrNsWi.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
