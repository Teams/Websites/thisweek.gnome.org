---
title: "#184 Upcoming Freeze"
author: Felix
date: 2025-01-24
tags: ["crosswords"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 17 to January 24.<!--more-->

# GNOME Releases

[Sophie 🏳️‍🌈 🏳️‍⚧️ (she/her)](https://matrix.to/#/@sophieherold:gnome.org) reports

> In about one week from today, on February 1st, APIs, features, and user interfaces are frozen for GNOME 48. The release for GNOME 48 is planned for March 19th. More details and dates are available in the [release calendar](https://release.gnome.org/calendar/).

# Third Party Projects

[petsoi](https://matrix.to/#/@petso:matrix.org) reports

> This week, I released my very first app, Words!, a game inspired by Wordle. You can find it on [Flathub](https://flathub.org/apps/page.codeberg.petsoi.words).
> 
> Some features, like support for different dictionaries with varying word lengths and multiple languages, are still on my todo list.
> 
> Happy word hunting!
> ![](bJJmdhcqtYLAssmszeRQvMud.png)
> ![](ZXAzPBHbfcfraTGZcRdgNXsz.png)

[Giant Pink Robots!](https://matrix.to/#/@giantpinkrobots:matrix.org) announces

> Varia download manager got an update that's probably its biggest since the first release.
> 
> * The most important new feature is yt-dlp integration allowing for video and audio downloads from any supported website at any supported quality setting. These downloads are fully integrated into Varia and behave like any other download type.
> * New adaptive layout allows for smaller window sizes and supports mobile devices.
> * Way better handling of downloads for better performance and also to crush bugs. Downloads that were paused stay paused upon relaunch, which I think was one of the biggest issues.
> * More settings for torrents, allowing for adjustments to the seeding ratio and custom download directory. .torrent files can now be dragged onto the window.
> 
> You can get it here: https://giantpinkrobots.github.io/varia/
> ![](oBxoBcvVBKHQKhJXdOeOOXhj.png)
> ![](jDLWRlZDFxNcfveLAYzxeubA.png)

### Crosswords [↗](https://gitlab.gnome.org/jrb/crosswords)

A crossword puzzle game and creator.

[jrb](https://matrix.to/#/@jblandford:matrix.org) announces

> Crosswords 0.3.14 was released. For this version, almost all the changes were in the underlying code and Crossword Editor. Improvements include:
> 
> * libipuz is ported to GObject Introspection and has developer [documentation](https://libipuz.org/libipuz-1.0/). It's much closer to a stable API.
> * Autofill is massively improved. Full boards are now solvable, and tough corners will fill or fail quicker
> * Selection of cells is saner, removing the need for nested tabs.
> * A preloaded dictionary from Wiktionary is included to supplement the word lists.
> * Enhanced substring matching added for cryptic indicators.
> * A preview window is added to check on a puzzle in development.
> 
> This is the first version of the Editor that is generally usable. If you've ever wanted to write a crossword, please give it a try and let me know how it goes.
> 
> Read more at the [release notes](https://blogs.gnome.org/jrb/2025/01/22/crosswords-0-3-14/)
> 
> It's available on flathub ([game](https://flathub.org/apps/org.gnome.Crosswords), [editor](https://flathub.org/apps/org.gnome.Crosswords.Editor)) or in fedora
> {{< video src="YnpvccNKUxZqrABAaOkmWgyC.mp4" >}}
> {{< video src="WGxFKmvipWKHwMqQhIVhtcHx.mp4" >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
