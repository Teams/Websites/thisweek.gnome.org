---
title: "#189 Global Shortcuts"
author: Felix
date: 2025-02-28
tags: ["libmanette", "gir.core", "gameeky", "archives", "televido", "gtk"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 21 to February 28.<!--more-->

# GNOME Core Apps and Libraries

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> Thanks to [the work of many people](https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome/-/merge_requests/208) across multiple components, the GNOME desktop portal now supports the [Global Shortcuts interface](https://flatpak.github.io/xdg-desktop-portal/docs/doc-org.freedesktop.portal.GlobalShortcuts.html). Applications can register desktop-wide shortcuts, and users can edit and revoke them through the system settings.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> Lukáš Tyrychtr finished working on the keyboard monitoring support in [Mutter](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/4217), [Orca](https://gitlab.gnome.org/GNOME/orca/-/merge_requests/237) and [libatspi](https://gitlab.gnome.org/GNOME/at-spi2-core/-/merge_requests/178). This means that Orca shortcuts will just finally work, including Caps lock as the Orca key, under Wayland, closing one of the last major blockers for the full transition away from X11.

### Libmanette [↗](https://gitlab.gnome.org/GNOME/libmanette)

Simple GObject game controller library.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) reports

> after a long period of inactivity, libmanette has been ported to gi-docgen. The new docs are available at https://gnome.pages.gitlab.gnome.org/libmanette/doc/main/

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Matthias Clasen](https://matrix.to/#/@matthiasc:gnome.org) says

> Both [GTK](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/8247) and [mutter](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/4307) support the cursor shape [protocol](https://wayland.app/protocols/cursor-shape-v1) now. This will improve the consistency of cursor themes and sizing, and the interoperability with other compositors.

# Third Party Projects



### Televido [↗](https://github.com/d-k-bo/televido)

Access German-language public TV

[d-k-bo](https://matrix.to/#/@d-k-bo:matrix.org) says

> Televido 0.5.0 is [available on Flathub](https://flathub.org/apps/de.k_bo.Televido).
> 
> Televido is an app to access German-language public broadcasting live streams and archives based on APIs provided by the [MediathekView](https://mediathekview.de/) project.
> 
> As a major change in version 0.5.0, Televido now provides an integrated video player based on [Clapper](https://rafostar.github.io/clapper/).

### Gir.Core [↗](https://gircore.github.io/)

Gir.Core is a project which aims to provide C# bindings for different GObject based libraries.

[Marcel Tiede](https://matrix.to/#/@badcel:matrix.org) announces

> GirCore 0.6.3 was released. This release adds some missing bits to GObject-2.0.Integration, adds `IDisposable` support on interfaces and fixes a bug in several `async` methods. Check the [release notes](https://github.com/gircore/gir.core/releases/tag/0.6.3 ) for details.

### Gameeky [↗](https://github.com/tchx84/Gameeky)

Play, create and learn.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) announces

> [Gameeky](https://flathub.org/apps/dev.tchx84.Gameeky) 0.6.5 is out 🚀
> 
> This new release brings complete translations for Dutch and Hindi, thanks to Heimen Stoffels and Scrambled777 respectively. Additionally, it has upgraded its GNOME runtime and fixed some rendering issues.
> 
> If you're interested in this mix of video games, coding and learning, I invite you to watch Gameeky's [GUADEC presentation](https://www.youtube.com/watch?v=ch9WLQ9ImGY) from last year.

### Archives [↗](https://gitlab.gnome.org/GeopJr/Archives/)

Create and view web archives

[Evangelos "GeopJr" Paterakis](https://matrix.to/#/@geopjr:gnome.org) announces

> [Archives](https://flathub.org/apps/dev.geopjr.Archives) 0.4.0 is out with the ability to archive a right clicked link, all links in text selection and all links from a webpage individually. Additionally, it can now open ZIM files through Kiwix. Lastly, a search bar for searching in page was added, progress bars got redesigned and all third-party tools were updated to their latest versions.
> {{< video src="a589d19d2f75c8aa73ff69a41d0bb24e488a23541895196817314807808.mp4" >}}

# Documentation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> gi-docgen, the GIR-based C documentation generator got a [new release](https://gitlab.gnome.org/GNOME/gi-docgen/-/releases/2025.3). The most important change reflects a change in the GIR data introduced by gobject-introspection that allows "static" virtual functions (functions in the class structure that have no instance parameter). Some small QoL improvements to support narrow layouts, as well as cleanups in the generated HTML and styles. As usual, this release is available on both [download.gnome.org](https://download.gnome.org/sources/gi-docgen/2025/) and on [PyPI](https://pypi.org/project/gi-docgen/).

# Shell Extensions

[Pedro Sader Azevedo](https://matrix.to/#/@toluene:matrix.org) announces

> A few days ago, I released [Blocker](https://extensions.gnome.org/extension/7831/blocker/), my first GNOME Shell Extension.
> 
> It allows users to easily toggle system-wide content blocking. Behind the scenes, it uses a program named [hBlock](https://hblock.molinero.dev/) to change the computer's DNS settings, so it does not connect to domains that are known for serving adverts, trackers, and malware. This strategy of content blocking has its limitations, and you can read more about them [here](https://github.com/pesader/gnome-shell-extension-blocker#what-are-its-limitations).
> 
> Give it a go if that sounds interesting to you!
> ![](bBqBiRqdahxrCnlmqyHZCEPx.png)

# Miscellaneous

[Thib](https://matrix.to/#/@thib:ergaster.org) says

> I published a blog post about how the EU can use GNOME to kickstart an international, transparent, collaborative operating system and reduce its dependency on American corporations, and what role the Foundation and community can play in it.
> 
> https://ergaster.org/posts/2025/02/28-prosthetics-that-dont-betray/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
