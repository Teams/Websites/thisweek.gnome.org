---
title: "#188 Software Fixes"
author: Felix
date: 2025-02-21
tags: ["keypunch", "phosh", "gnome-software", "weather-oclock"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 14 to February 21.<!--more-->

# GNOME Core Apps and Libraries

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Milan Crha has been grinding through and fixing multiple smaller issues and papercuts in gnome-software in the last few weeks, adding polish to the upcoming 48.0 release

# GNOME Incubating Apps

[Pablo Correa Gomez](https://matrix.to/#/@pabloyoyoista:matrix.org) reports

> thanks to work by Om Thorat and Qiu Wenbo on the UI side, and many months of refactoring from all the maintainers, it is now possible to use different styles when highlighting text in PDF documents in Papers! This was one of the most requested features, and one we are very excited to share!
> {{< video src="EsCWeGkhiixLEAXREJgdxNVl.mp4" >}}

# Third Party Projects

[José](https://matrix.to/#/@half_mexican:gnome.org) reports

> 🚀 New Mingle Release: v0.20
> 
> v0.20 is here with some updates: 
> 
> * Tooltips for emojis and their combos: Hover to see what makes up a combo. Easy peasy!
> * Search button fix: Updated it to a toggle button so it stops disappearing on you.
> * New Adw.Spinner: Fancy new loading spinner.
> * Bug fix: Headerbar was ghosting on launch (window couldn’t move)
> * Favorites system: Coming soon™—almost ready! 
> 
> Let me know if I broke something!
> ![](7a40cd1bddb0a417d1f8ecc3caca79659038bb811892769925534056448.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) reports

> [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) 0.45.0 is out:
> 
> Phosh now detects captive Wi-Fi portals and shows a notification that takes you to your favorite browser when activated to log into the portal. When taking screenshots we now save thumbnails right away for file choosers and other apps to display. The compositor switched to wlroots 0.18 and things like debug log domains, damage tracking debugging or touch point debugging can now be configured at run time.
> 
> There's more, see the full details at [here](https://phosh.mobi/releases/rel-0.45.0/)
> ![](EtkdrGFrhwfnjqlFIehciUnf.png)
> ![](NpXYXDrzWVEmWGYftDIdSOsD.png)

### Keypunch [↗](https://github.com/bragefuglseth/keypunch)

Practice your typing skills

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) announces

> This week [Keypunch](https://apps.gnome.org/Keypunch) was accepted into Circle! It's an elegant little typing tutor to help you learn touch typing or improve your typing skills. Congratulations!
> ![](29382591aee0de96daddce9abbd832223a1ff1091891833758051467264.png)

# GNOME Websites

[Jakub Steiner (jimmac)](https://matrix.to/#/@jimmac:gnome.org) reports

> Following the major redesign of www.gnome.org last week, the developer portal at http://developer.gnome.org has also been refreshed with a new look. The update brings it in line with GNOME’s modern design while keeping key developer resources easily accessible.
> ![](2d715cab92263b6c45fa144d0b2ff5bbbbaa09e61892626328931270656.png)

# Shell Extensions

### Weather O'Clock [↗](https://extensions.gnome.org/extension/5470/weather-oclock/)

Display the current weather inside the pill next to the clock.

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) says

> The shell extensions Weather O'Clock and Auto Activities have been ported to GNOME 48.

# Miscellaneous

[Cassidy James (he/him)](https://matrix.to/#/@cassidyjames:gnome.org) says

> I wrote a fairly comprehensive look at the safety story for apps and updates over on [the Flathub blog](https://docs.flathub.org/blog/app-safety-layered-approach-source-to-user).
> 
> If you're an app developer or user who has ever been curious how Flathub helps keep its millions of users safe, give it a read!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
