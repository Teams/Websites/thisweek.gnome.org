---
title: "#187 Triple Buffered Notifications"
author: Felix
date: 2025-02-14
tags: ["gir.core", "mutter", "gnome-contacts", "gnome-shell", "fractal"]
categories: ["twig"]
draft: false
images:
  - posts/2025/02/twig-187/3e081bafb0d32e080c7a9eb2b1d8008701edaba01890467990181249024.png
---

Update on what happened across the GNOME project in the week from February 07 to February 14.<!--more-->

# GNOME Core Apps and Libraries

### Mutter [↗](https://gitlab.gnome.org/GNOME/mutter/)

A Wayland display server and X11 window manager and compositor library.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> ![](3e081bafb0d32e080c7a9eb2b1d8008701edaba01890467990181249024.png)
>
> Today, just in time for this edition of This Week in GNOME and after 5 years, more than a thousand review comments, and multiple massive refactorings and rewrites, the legendary merge request [mutter!1441](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1441) was merged.
> 
> This merge requests introduces an additional render buffer when Mutter is not able to keep up with the frames.
> 
> The technique commonly known as dynamic triple buffering can help in situations where the total time to generate a frame - including CPU and GPU work - is longer than one refresh cycle. This improves the concurrency capabilities of Mutter by letting the compositor start working on the next frame as early as possible, even when the previous frame isn't displayed.
> 
> In practice, this kind of situation can happen with sudden burst of activity in the compositor. For example, when the GNOME Shell overview is opened after a period of low activity.
> 
> This should improve the perceived smoothness of GNOME, with less skipped frames and more fluid animations.

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Julian Sparber (Away till Jan 7th)](https://matrix.to/#/@jsparber:gnome.org) reports

> The long awaited notification grouping was merged this week into GNOME Shell, just in time for GNOME 48. This was a huge effort by multiple parties, especially by Florian Müllner who spend countless hours reviewing code changes. This is probably one of the most visible features added to GNOME thanks to the STF grant.
> {{< video src="9ed4e64fe88305d6b05ffb73b0c8ec306c0191e21890344685436665856.webm" >}}

### GNOME Contacts [↗](https://gitlab.gnome.org/GNOME/gnome-contacts)

Keep and organize your contacts information.

[Adrien Plazas](https://matrix.to/#/@adrien.plazas:gnome.org) announces

> Contacts received some small last minute changes right in time for GNOME 48:
> * its contact editor's spacing have been overhauled to match other GNOME apps,
> * its birthday editing row and dialog got redesigned to not only look better but work better on mobile as well.
> ![](faecfdeef4bd7aa4865ef198f924e246321078fc1889705987762290688.png)

# GNOME Circle Apps and Libraries

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) announces

> This week [Drum Machine](https://apps.gnome.org/DrumMachine) was accepted into Circle! It's a delightful little app to play with drum patterns and prototype track ideas. Congratulations!
> ![](93df6bf9bcc0b756f1ce99eb8615dd7078be79611888998474288988160.png)

# Third Party Projects

[Krafting - Vincent](https://matrix.to/#/@lanseria:matrix.org) reports

> [SemantiK](https://flathub.org/apps/net.krafting.SemantiK) got two releases last week: 1.4.0 and 1.5.0. They both bring new improvements, code refactoring, more translation work (thanks to @johnpetersa19 for the Brazilian Portuguese translation), and a revamped language selector! 
> 
> The next big step would be to create more Language Pack, if you want to help with that, feel free to contact me via Matrix!
> ![](xeoyKRZKsRWDSaFLMdrEIrbb.png)

[Krafting - Vincent](https://matrix.to/#/@lanseria:matrix.org) reports

> Also, last week, I've been hard at work fixing bugs throughout all my apps, and making them fully responsive on small screens, making them perfect for Mobile Linux ! 🎉📱
> 
> [Hex Colordle](https://flathub.org/apps/net.krafting.HexColordle) got some bug fixes and small improvements to message when you lose.
> 
> [Playlifin Voyager](https://flathub.org/apps/net.krafting.PlaylifinVoyager) and [PedantiK](https://flathub.org/apps/net.krafting.PedantiK) got some UI tweaks and bug fixes
> 
> [Reddy](https://flathub.org/apps/net.krafting.Reddy) got some better image scaling, making it way better on small screens, as well as some library version bumps.
> ![](TsuhtwEAdPKgJvNRUsaUbdYH.png)
> ![](JyybeYBkCmoIlPKyXSOROPbA.png)
> ![](LslvxCmKPEdkOFbyglGRNGGI.png)

### Gir.Core [↗](https://gircore.github.io/)

Gir.Core is a project which aims to provide C# bindings for different GObject based libraries.

[Marcel Tiede](https://matrix.to/#/@badcel:matrix.org) says

> GirCore verion [0.6.2](https://github.com/gircore/gir.core/releases/tag/0.6.2) was released. It features support for .NET 9 and modernized the internal binding code resulting in better garbage collector integration and the removal of reflection based code. As a result there are several breaking changes.
>  A new beginner friendly [tutorial](https://gircore.github.io/docs/tutorial/gtk/index.html) was contributed and can be found on the homepage. Please see the release notes for more details.

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) announces

> Due to a couple of unfortunate but important regressions in Fractal 10, we are releasing Fractal 10.1 so our users don’t have to wait too long for them to be addressed. This minor version fixes the following issues:
> 
> * Some rooms were stuck in an unread state, even after reading them or marking them as read.
> * Joining or creating a room would crash the app.
> 
> This version is available right now on [Flathub](https://flathub.org/apps/org.gnome.Fractal).
> 
> If you want to help us avoid regressions like that in the future, you could use [Fractal Nightly](https://gitlab.gnome.org/World/fractal#development-version)! Or even better, you could pick up one of [our issues](https://gitlab.gnome.org/World/fractal/-/issues) and become part of the ~~problem~~ solution.

# Events

[Kristi Progri](https://matrix.to/#/@kristiprogri:gnome.org) announces

> GUADEC 2025 Call for Papers is officially open!
> Submit your paper by March 16th via this link: https://events.gnome.org/event/259/abstracts/#submit-abstract

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
