---
title: "#141 Sleek Documentation"
author: Felix
date: 2024-03-29
tags: ["obfuscate", "fractal", "tuba", "warp", "dejadup", "newsflash", "apostrophe"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 22 to March 29.<!--more-->

# GNOME Circle Apps and Libraries

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) reports

> This week Biblioteca was accepted into GNOME Circle. Biblioteca lets you browse and read GNOME documentation wherever you are in a sleek and convenient way. Congratulations!
>
> https://flathub.org/apps/app.drey.Biblioteca
> ![](1c58069f216c5a55d4129983ab680478b7677bce1772000055922786304.png)

### Warp [↗](https://gitlab.gnome.org/World/warp)

Fast and secure file transfer.

[Fina](https://matrix.to/#/@felinira:matrix.org) reports

> Warp 0.7 has been released with QR Code scanning support 📸. This feature allows to initiate a file transfer just by scanning a code on the receiving device. Thanks a lot to [GNOME Camera](https://apps.gnome.org/Snapshot/) for providing the widgets for a modern camera stack. 🦀
> ![](aqVOWngSSrcVunEPzsaUwQbk.png)

### Obfuscate [↗](https://gitlab.gnome.org/World/obfuscate)

Censor your private information on any image.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> [Obfuscate](https://flathub.org/apps/com.belmoussaoui.Obfuscate) 0.0.10 is out. The app is now adaptive and shows a confirmation when closing the app with unsaved changes.
> {{< video src="6351947fa32c941e9fceff28417364e8ff8538fa1771907455328976896.webm" >}}

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[Jan Lukas](https://matrix.to/#/@jangernert:matrix.org) announces

> Newsflash 3.2 is out. Some highlights are:
> * automatically scraping articles so you can read them offline
> * editing feed URLs
> * some performance improvements
> 
> You can read more here: https://blogs.gnome.org/jangernert/2024/03/25/newsflash-3-2/

### Déjà Dup Backups [↗](https://wiki.gnome.org/Apps/DejaDup/)

A simple backup tool.

[Michael Terry](https://matrix.to/#/@mterry:gnome.org) says

> Déjà Dup Backups 46.beta is out. This adds modern Adwaita dialogs, now uses rclone for cloud support (rather than disparate Python libraries for each service), and adds various smaller UI improvements. Install the beta from [flathub-beta](https://discourse.flathub.org/t/how-to-use-flathub-beta/2111) (`flatpak install flathub-beta org.gnome.DejaDup`) and please report any issues. Thanks!
> ![](027230fb6c221dcb3817ea731c6b85691e76bddb1771943909375606784.png)

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu](https://matrix.to/#/@somas95:gnome.org) reports

> Since rendering random documents can be dangerous, this week I added a security feature to Apostrophe. It'll offer the option to load potentially dangerous files with html and javascript deactivated. I also landed some patches that will unblock the last remaining issue that was preventing a GTK4 release: spellchecking. Hopefully I'll be able to draft a stable release very soon. In the meantime betatesting is more than welcome. An updated build is already available on flathub-beta
> ![](5135404e92a74b0484437f29689131ff02ca0d471773135616649723904.png)

# Third Party Projects

[d-k-bo](https://matrix.to/#/@d-k-bo:matrix.org) announces

> This week [Televido](https://flathub.org/apps/de.k_bo.Televido) received an update to use the new adaptive dialogs that were introduced in GNOME 46.
> 
> Televido is an app to access German-language public broadcasting live streams and archives based on APIs provided by the [MediathekView](https://mediathekview.de/) project.
> 
> Since my first post here, it also got better mobile support, it supports downloading media using an external downloader such as Parabolic and the app is now available in English, German, Dutch and French.
> ![](ljuuMVSItModdfNPSbwsENVy.png)
> ![](erwsRaPFfsAPKJzgDwIwTpRG.png)

[ghani1990](https://matrix.to/#/@ghani1990:matrix.org) says

> [Planify](https://flathub.org/apps/io.github.alainm23.planify) continues to **evolve and enhance** its user experience by introducing several **exciting new features** this week:
> 
> **1. Quick Search Filters:**
>    - Users can now **efficiently filter tasks** by various criteria directly from the Quick Search bar. These filters include:
>      - **"Tomorrow"**: To view tasks scheduled for the next day.
>      - **"Untagged"**: For tasks without specific tags.
>      - **"Repeat"**: To manage recurring tasks.
>      - **"Anytime"**: For tasks with no flexible deadlines.
> 
> **2. Completed View in Sidebar:**
>    - Users can easily access and review their completed tasks, streamlining the process of tracking progress.
> 
> These updates **enhance Planify's overall functionality and usability**, making it an even more **powerful and versatile tool** for efficient task management.
> 
> Additionally, Planify has addressed several bugs and made performance improvements to ensure a smoother experience for users. 🚀📅🔍
> ![](kjDzKkhpsKrsDvwDaWKVzCTH.png)
> ![](ZaciaRuSBEkPbvXnzgpqGgCV.png)

[Michael Terry](https://matrix.to/#/@mterry:gnome.org) says

> [Multiplication Puzzle](https://flathub.org/apps/app.drey.MultiplicationPuzzle) 14.0 is out with modern Adwaita dialogs.
> ![](301f1e45a66ed6b95c3ca6f7b8b3bff8f715d5571771941912136122368.png)

### Tuba [↗](https://github.com/GeopJr/Tuba)

Browse the Fediverse.

[Evangelos "GeopJr" Paterakis](https://matrix.to/#/@geopjr:gnome.org) announces

> [Tuba](https://flathub.org/apps/details/dev.geopjr.Tuba) 0.7 is now available, with many [new features and bug fixes](https://github.com/GeopJr/Tuba/releases/tag/v0.7.0)!
> 
> ✨ Highlights:
> 
> * Filter handling and editing
> * User and Post reporting
> * Ported dialogs to `AdwDialogs`
> * Tracking parameter stripping on paste
> * Syntax highlighting
> * Mutes & Blocks page
> * Recently used emojis in the Custom Emoji Chooser
> * Unread Announcements and Unreviewed Follow Requests banners
> {{< video src="beecda58d9e0284f8156a2d8f0e7d2cb04cc93c31773491675260256256.webm" >}}
> {{< video src="4659fe9608f35a7dc68d2b3bb5f6c49ee553ea311773491710605656064.webm" >}}

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) announces

> Spring is here in Fractal land. Birds chirping, flowers blooming, and a new beta for you to try!
> 
> Staff’s picks for Fractal 7.beta:
> 
> * Encryption support has been extended, with server-side key backup and account recovery.
> * Messages that failed to send can now be retried or discarded.
> * Messages can be reported to server admins for moderation.
> * Room details are now considered complete, with the addition of room address management, permissions, and version upgrade.
> * A new member menu appears when clicking on an avatar in the room history. It offers a quick way to do many actions related to that person, including opening a direct chat with them and moderating them.
> * Pills are clickable and allow to directly go to a room or member profile.
> * Many more improvements on the accessibility front, for better navigability with a screen reader.
> 
> As usual, this release includes other improvements, fixes and new translations thanks to all our contributors, and our upstream projects.
> 
> It is available to install via Flathub Beta, see the [instructions in our README](https://gitlab.gnome.org/World/fractal#installation-instructions).
> 
> As the version implies, there might be a slight risk of regressions, but it should be mostly stable. If all goes well the next step is the release candidate!
> 
> As always, you can try to fix one of our [issues](https://gitlab.gnome.org/World/fractal/-/issues). Any help is greatly appreciated!
> ![](798fe8783051c09a2ea7a88ccdf13a1bbc4bb9bf1773440601593741312.png)

# Miscellaneous

[barthalion](https://matrix.to/#/@barthalion:matrix.org) reports

> To improve transparency, Flathub now shows an explicit "unverified" badge for community-maintained packages.
> 
> Meanwhile, I've been working on improving the configuration of our content delivery network. Mythic Beasts donated a new caching server, which will reduce the load on the origin server. I have enabled shielding in Fastly to further improve the cache hit ratio. I'm also looking into configuring Fastly to use segmented caching to resolve issues with installation of large applications.
> ![](xygPqkDlfikZAdtSKnCpQiei.png)

[psauberz](https://matrix.to/#/@psauberz:matrix.org) announces

> The [Sovereign Tech Fund](<https://www.sovereigntechfund.de>) is designing a pilot program for a fellowship to support open source maintainers working on open digital infrastructure in the public interest. They are asking for input from maintainers in a [survey on their blog](<https://www.sovereigntechfund.de/news/foss-maintainer-fellowship-survey>). If you are a maintainer, please take ten minutes to respond to the survey by Sunday, 7 April 2024.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
