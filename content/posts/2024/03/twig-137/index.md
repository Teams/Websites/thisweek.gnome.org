---
title: "#137 Second Legendary Saturday Edition"
author: Felix
date: 2024-03-02
tags: ["glib", "gnome-calendar", "turtle"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 23 to March 02.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> The GNOME STF team is running too late today for the deadline.  Sorry about that, we'll be back next Friday with 2 weeks worth of updates. Have a great weekend!

# GNOME Core Apps and Libraries

### Calendar [↗](https://gitlab.gnome.org/GNOME/gnome-calendar/)

A simple calendar application.

[Hari Rana | TheEvilSkeleton (any/all)](https://matrix.to/#/@theevilskeleton:fedora.im) reports

> GNOME Calendar 46 beta was released, bringing some exciting changes and additions:
> * [Jeff Fortin](https://fortintam.com/en) worked on [harmonizing the mini calendar's week numbers with GNOME Shell](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/354).
> * [Georges Stavracas](https://feaneron.com/) ported every window to [`AdwDialog`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Dialog.html), and [Dexter Reed](https://sungsphinx.codeberg.page/) ported the about window to [`AdwAboutDialog`](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/411).
> * [Rami Alkawadri](https://gitlab.gnome.org/ramialkawadri) fixed a bug in the creation popover where [full-day events would show "00:00 — 00:00"](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/359).
> * Georges Stavracas ported the [date selector to Settings portal](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/365).
> * [Felipe Kinoshita](https://felipekinoshita.com/) added [debug information to the about dialog](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/380).
> * Georges Stavracas ported from [GtkLabel to GtkInscription for displaying events](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/402). Using GtkInscription for this use case increases performance.
> * Hari Rana added [month separators](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/401), making it easier to distinguish beginning and end of each month.
> * Hari Rana added a [lock icon for read-only calendars](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/393)
> * Hari Rana [improved the handling of event names](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/389), which adds visual feedback to when the event name is invalid.
> ![](5781cb6c4c6a9b317ef58550c264abd8790e34a11761755798108635136.png)
> {{< video src="6d4ed4451f8a1d441a198ed2a807e05a9d01ca3a1761782790996623360.mp4" >}}

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Luca Bacci has improved file info queries on Windows, to improve the responsiveness of file choosers (in GIMP and everywhere else): https://gitlab.gnome.org/GNOME/glib/-/issues/3080

[Michael Catanzaro](https://matrix.to/#/@mcatanzaro:gnome.org) reports

> Last week [I fixed a major performance issue in glib-networking](https://gitlab.gnome.org/GNOME/glib-networking/-/merge_requests/249) causing the system certificate trust to be parsed repeatedly when running under flatpak or in other containerized environments that use p11-kit-server. Epiphany Technology Preview is now able to load https://www.cnn.com in 7-8 seconds for me, down from 31-35 seconds. You should notice network performance improvements in many flatpak apps. This fix will be released in glib-networking 2.80.rc and 2.78.1.

# GNOME Incubating Apps

[Pablo Correa Gomez](https://matrix.to/#/@pabloyoyoista:matrix.org) says

> This week Qiu Wenbo replaced [Papers](https://apps.gnome.org/Papers/) custom [thread scheduler](https://gitlab.gnome.org/GNOME/Incubator/papers/-/merge_requests/81) with GLib's GThreadPool. In consequence, multiple jobs are allowed to be scheduled concurrently (so for example, multiple pages can render at the same time), and we are no longer leaking every thread's memory (the custom scheduler would never call `g_thread_join`)

# Third Party Projects

[Mateus R. Costa](https://matrix.to/#/@MateusRodCosta:matrix.org) says

> For the past few days I have been working on a new thumbnailer that follows the [Freedesktop Thumbnail Managing standard](https://specifications.freedesktop.org/thumbnail-spec/thumbnail-spec-latest.html) for thumbnailing Nintendo handheld systems roms and files.
> 
> While a thumbnailer already existed only for .nds roms as [gnome-nds-thumbnailer](https://gitlab.gnome.org/GNOME/gnome-nds-thumbnailer), I decided to do my own implementation written in Rust and try to add support to the Nintendo 3DS files as well. As you might guess, this was intended to be a way to practice Rust skills on a real-world project.
> 
> The resulting thumbnailer is [bign-handheld-thumbnailer](https://github.com/MateusRodCosta/bign-handheld-thumbnailer). It supports Nintendo DS .nds (although animated DSi icons are not supported and only the standard DS icon is used instead) files and for Nintendo 3DS .cia (3DS installer files, as long as a "Meta" section is present on the file containing a valid SMDH with the large icon data), .smdh (3DS banner metadata) and .3dsx (3DS homebrew, only if a extended header is present with SMDH data) files are currently supported.
> .cci/.3ds (actual 3DS rom) file support will be added in the future, as it seems to rely on mounting a virtual filesystem to extract the icon file.
> 
> The compiled release from the project is just shy of 1 MB of size. It was also a great learning experience, especially around extracting specific data from a file byte's according to the file structure and around Rust error handling.
> Do note that there isn't a .cia mime type installed by default, so I borrowed a definition from the Citra emulator which might need to be installed alongside the thumbnailer. Depending on how the thumbnailer is installed it might run into issues with the fact that nautilus runs thumbnailers inside a sandbox.
> 
> A Fedora RPM is pending to a future moment due to the Fedora repo crate versions being lower than the project's, requiring either waiting for an update or downgrading the project's dependencies.
> 
> **Update from later in the week**: I got some some help from @daknig:matrix.org about the binary size and managed to reduce it from ~990 kB to ~540 kB. This involved moving from clap to pico-args (which is better fit due to being way simpler and adding less size in the final binary) and also adjusting some compilation option from the release build to optimize for a smaller binary.
> ![](aBKJMtuBtQRepNALkyjHMBXP.png)

[Giant Pink Robots!](https://matrix.to/#/@giantpinkrobots:matrix.org) announces

> [Varia](https://giantpinkrobots.github.io/varia/) download manager has received many new features with the v2024.2.29-1 update this week.
> 
> * Support for Firefox and Chrome/ium extension.
> * Initial torrenting support.
> * Remote mode that allows connection to a remote aria2 instance.
> * Background mode that allows the window to be hidden while still downloading.
> * Bug fixes and adjustments.
> ![](UKiGDUOnpidFlziiuNRfoGiR.png)

### Turtle [↗](https://gitlab.gnome.org/philippun1/turtle)

Manage git repositories in Nautilus.

[Philipp](https://matrix.to/#/@philippun:matrix.org) reports

> Turtle 0.7 has been released.
> 
> It now runs a service application in background which will calculate the file manager emblems and is accessed via D-Bus. This drastically improves speed, eliminating roughly 80% overhead.
> 
> The [packaging/debian](https://gitlab.gnome.org/philippun1/turtle/-/tree/packaging/debian) branch has been updated and now can successfully build the deb packages on Debian and Ubuntu. There is also a turtle-nautilus-flatpak package, which comes in handy if you want to use the nautilus plugin combined with the flatpak.
> 
> There were also some minor bug fixes.
> ![](sxSzwyRhEpCcsxNyJomFZrBK.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
