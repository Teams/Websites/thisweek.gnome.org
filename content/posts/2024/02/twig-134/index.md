---
title: "#134 High Contrast"
author: Felix
date: 2024-02-09
tags: ["errands", "secrets", "glib", "graphs"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 02 to February 09.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) project, a number of community members are working on infrastructure related projects. We didn't get around to reporting our progress last week because many of us were at FOSDEM in Brussels, so this is work from the past two weeks.
> 
> Andy updated his GNOME Online Account work from last year
> Andy worked on [Spiel integration with Orca](https://gitlab.gnome.org/GNOME/orca/-/merge_requests/182)
> Sophie added basic filtering of syscalls to glycin bwrap sandboxes via a seccomp allow list
> 
> * https://gitlab.gnome.org/sophie-h/glycin/-/merge_requests/50
> * Currently tested on x86_64, i386, and aarch64
> 
> Hubert implemented high contrast hint portal setting in
> 
> * [xdg-desktop-portal-gnome](https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome/-/merge_requests/143)
> * [xdg-desktop-portal-gtk](https://github.com/flatpak/xdg-desktop-portal-gtk/pull/466)
> * [libadwaita](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1039)
> * [libhandy](https://gitlab.gnome.org/GNOME/libhandy/-/merge_requests/845)
> 
> Sam made a bunch of tweaks to GNOME Shell stylesheet and High Contrast
> Sam made a [new design](https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/2523#note_1983921) for Variable Refresh Rate (VRR) Settings
> Matt implemented the compositor side of [Newton, the new a11y architecture prototype](https://blogs.gnome.org/a11y/2023/10/27/a-new-accessibility-architecture-for-modern-free-desktops/)
> 
> * Current protocol drafts: https://gitlab.freedesktop.org/mwcampbell/wayland-protocols/tree/accessibility
> * Provider implementation in AccessKit: https://github.com/AccessKit/accesskit/tree/unix2-prototype
> * Mutter implementation: https://gitlab.gnome.org/mwcampbell/mutter/tree/wayland-native-a11y
> * Test consumer implementation: https://gitlab.gnome.org/mwcampbell/newton-test-consumer
> 
> Dor made several improvements to Mutter related to VVR
> 
> * https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3549
> * https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3560
> * https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3561
> 
> Dor updated his VVR work and made it ready for review
> 
> * https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1154#note_1998670
> 
> Dor continued experimentation for getting smooth cursor updates with the KMS thread
> 
> Adrian continued his work on systemd-homed integration
> 
> * https://github.com/systemd/systemd/pull/30840
> * https://github.com/systemd/systemd/pull/30226
> * https://github.com/systemd/systemd/pull/31031
> * https://github.com/systemd/systemd/pull/31039
> * https://gitlab.freedesktop.org/accountsservice/accountsservice/-/merge_requests/144
> * https://github.com/systemd/systemd/pull/31039
> * https://github.com/systemd/systemd/pull/31153
> * https://github.com/systemd/systemd/pull/31206
> * https://gitlab.freedesktop.org/accountsservice/accountsservice/-/merge_requests/146
> 
> Adrian is investigating behavior of kernel's page cache when encryption keys are removed
> 
> * https://lore.kernel.org/all/20240116-tagelang-zugnummer-349edd1b5792@brauner/
> 
> Jonas (Dreßler) continued improving Jonas (Ådahl) wayland fractional scaling branch
> Jonas started work on migrating monitor configs to the new format for enabling fractional scaling by default
> 
> Jonas updated the screencast MRs for blocklisting pipelines and hardware encoding
> 
> * https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2976
> * https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2080
> 
> Jonas investigating an issue with variable framerate in screencasts
> 
> * https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/7335
> 
> Julian refactored a lot of the notification API in GNOME Shell as part of https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3103
> 
> Julian landed
> 
> * [using libadwaita Avatar for gnome-initial-setup](https://gitlab.gnome.org/GNOME/gnome-initial-setup/-/merge_requests/215)
> * [using libadwaita Avatar for Settings](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/1939)
> * [Use single notification Source for all system notifications](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3156)
> * [notification: Show symbolic icons in a circle and smaller](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3066)
> * [notifications: Add header for messages](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3103) - GNOME Shell
> * [Set the icon-name property as image hint instead of as app-icon](https://gitlab.gnome.org/GNOME/libnotify/-/merge_requests/35) - libnotify
> 
> Julian submitted an MR to [use the proper hint for setting images for notifications](https://gitlab.gnome.org/GNOME/gnome-settings-daemon/-/merge_requests/352) in gnome-settings-daemon
> 
> Julian is working on image support in notification portal via passing an fd
> 
> ![](d236004a2094ea6cb49efb43a8b3331d0a6a62a61756044844125388800.png)
>
> Georges landed WebKit GTK4 accessibility support
> 
> * https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/6827
> * https://github.com/WebKit/WebKit/pull/23926
> 
> Georges fixed Gamepad support in WebkitGTK / Epiphany
> 
> * https://github.com/WebKit/WebKit/pull/23598
> * https://gitlab.gnome.org/GNOME/epiphany/-/merge_requests/1428
> 
> Alice implemented a window-backed mode for AdwDialog
> 
> * used when the parent window is non-resizable, doesn't support built-in dialogs or is null
> * https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1037
> 
> Alice made a number of AdwDialog improvements
> 
> * https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1035
> * https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1034
> * https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1033
> * https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1032
> * https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1031
> * https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1030
> * https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1041
> * https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1046
> * https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1045
> 
> Alice released libadwaita 1.5 beta https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1049
> 
> Philip worked on splitting platform-specific docs in GLib (proof of concept)
> 
> Philip landed
> 
> * [stack allocation support to libgirepository](https://gitlab.gnome.org/GNOME/glib/-/issues/3217)
> * [GIRepository GIR generation to avoid cyclical dependency](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3797)
> 
> Evan landed
> 
> * [Refactor GIRepository GIR generation to avoid cyclical dependency](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3797)
> * [ girepository: Update gir-compiler and use it to compile GIRs](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3853)
> 
> Sam improved High Contrast support on the lock screen https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3154
> 
> Sam tested and fixed issues with RTL https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3150
> 
> Ivan investigated an input latency issue in Mutter https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/7375
> 
> Joanie replaced autotools with meson in Orca https://gitlab.gnome.org/GNOME/orca/-/commit/3a702c4cf
> 
> Joanie is working on removing more of pyatspi in Orca https://gitlab.gnome.org/GNOME/orca/-/issues/300
> 
> Joanie implemented using window coordinates insetad of screen when getting bounding boxes:
> * https://gitlab.gnome.org/GNOME/orca/-/commit/3f80aee0f
> * Needed for GTK4
> * Needed for Wayland
> 
> Joanie finished removal of Orca's overrides for character names for non-math content https://gitlab.gnome.org/GNOME/orca/-/issues/313

# GNOME Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Maxim Moskalets has added support for `--version` to `GApplication`: just call `g_application_set_version()` in your app to make use of it (https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3889)

# GNOME Circle Apps and Libraries

### Secrets [↗](https://gitlab.gnome.org/World/secrets)

A password manager which makes use of the KeePass v.4 format.

[Maximiliano 🥑](https://matrix.to/#/@msandova:gnome.org) announces

> Secrets have gained numerous updates during this development season. At first, thanks to his work at Volkswagen,
> Jan-Michael Brummer has reworked the architecture to support different key providers beside simple key files. Based on
> this work he added YubiKey and PKCS11 (Smartcard) support. Furthermore Maximiliano revamped
> UI to make use of libadwaita navigation split view widgets. Last but not least Cleo Menezes Jr. OTP added a quick copy action to the entry rows.
> ![](2ca14f06d3ba56536a1e27d743523850c431b4b81755285554737446912.png)

### Errands [↗](https://github.com/mrvladus/Errands)

Manage your tasks.

[ghani1990](https://matrix.to/#/@ghani1990:matrix.org) says

> [Errands](https://flathub.org/apps/io.github.mrvladus.List), shines brighter with its latest update this week, packing exciting new features and bug fixes to enhance your productivity experience.
> **New Features:**
> 
> * Drag and drop tasks between lists
> * Markdown syntax highlighting in task notes
> * Progress bars for each task
> 
> **Bug Fixes:**
> * Reduced sync requests for faster syncing
> * Improved handling of edits with sync enabled
> * Improved translations
> ![](hdrzUEUiKuCytJcKIgOXvNQW.png)
> ![](LdzUIkdpPsFXIlcCbjoMXeKB.png)

### Graphs [↗](https://graphs.sjoerd.se)

Plot and manipulate data

[Sjoerd Stendahl](https://matrix.to/#/@sjoerdb93:matrix.org) announces

> Since Graphs is part of GNOME Circle, we've spent some time on updating the infrastructure. The codebase has now been moved to the [GNOME GitLab](https://gitlab.gnome.org/World/Graphs), and translations are now done using the [GNOME translation platform](https://l10n.gnome.org/module/Graphs/). The GitHub page remains available, but only mirrors the GitLab repository. If you wish to get involved or submit an issue, please turn to the GitLab page instead.
> 
> Furthermore, we've applied some minor quality-of-life changes since last time:
> 
> * Label and title size in the stylesheets can now be set with finer increments.
> * Curves that are hidden are now no longer used when setting the limits of the canvas automatically.
> * The subtitle in the main application now shows the file location without the file name, which is already displayed above.
> * Some minor tweaks under the hood, which should result in slightly faster (about 0.3 s on my setup) start-up times.
> * We updated some strings, and updated the metadata to reflect the move to GNOME GitLab.
> 
> The latest release is available [on Flathub](https://flathub.org/apps/se.sjoerd.Graphs)!
> ![](GBEnHnSCBKhhssRlWmGgONsw.png)

# Third Party Projects

[Gianni Rosato](https://matrix.to/#/@computerbustr:matrix.org) says

> Aviator 0.5.1! TL;DR, mostly SVT-AV1-PSY improvements. But still exciting, in my opinion!
> * The "Open GOP" toggle has been renamed to "oGOP"
> * Speed -1 and -2 are now supported with a warning when you go below Speed 3. Speed -2 can take over 8 hours for a minute of video on a Ryzen 9
> * New "Perceptual Tuning" checkbox toggles some new SVT-AV1-PSY features to "on" by default to improve visual fidelity at the cost of some metric scores
> * Surround sound encodes will now benefit from Opus's multichannel optimizations
> * Video encoding defaults have been modified, re-enabling temporal filtering by default
> ![](GSrUzdbRTbWMMjQrfmLgarxz.webp)

[ghani1990](https://matrix.to/#/@ghani1990:matrix.org) reports

> [Varia](https://flathub.org/apps/io.github.giantpinkrobots.varia) download manager has received a new visual identity this week, new icon and a number of new features and improvements, such as: 
> 
> * A sidebar that hosts buttons for all downloads, downloads in progress, and completed downloads.
> * A setting for adjusting the simultaneous download amount.
> * Bug fixes and adjustments.
> ![](fRYFRPjuhagkbNEIBMfTLPyM.png)

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) announces

> After three months of development, [Gameeky](https://flathub.org/apps/dev.tchx84.Gameeky) reaches its first public release. 
> 
> Gameeky is a learning tool in the shape of a game engine. Therefore, its primary goal is to provide a better learning experience for programming, arts and other STEAM-related skills. More about this rationale [here](https://blogs.gnome.org/tchx84/2023/12/15/gameeky-a-new-learning-tool-to-develop-steam-skills/).
> 
> This first release comes the following:
> 
> * A game launcher to manage projects more easily.
> * A game client to play games cooperatively.
> * A scene editor to create and modify game worlds.
> * An entity editor to create and modify game objects.
> * A Python library for a LOGO-like experience.
> * Plugins support to extend the games logic and entities behavior.
> * An offline beginner’s guide.
> * The first thematic pack to create role-playing games in a medieval fantasy setting.
> * And more…
> 
> Gameeky is available in English and Spanish, including the beginner’s guide.
> 
> Check the release [post](https://blogs.gnome.org/tchx84/2024/02/08/gameeky-released/) for more information about this project and its future plans.
> ![](SzFiYMURDnMfrwUImdVMiVTK.png)
> ![](IbxwWmqAYRMWeyJGItYVxSUv.png)

[alextee](https://matrix.to/#/@alextee:matrix.org) reports

> The [Zrythm](https://www.zrythm.org/en/index.html) team has just released Zrythm 1.0.0-beta.6.3.11 featuring a brand new libadwaita-based greeter window among other improvements. Special thanks to Miró Allard for various contributions!
> 
> * Many UI elements were ported to new libadwaita widgets
> * Visualizers have been moved to bottom bar while the header bar has been simplified/revamped and the secondary header toolbar has been removed
> * The welcome/greeter window has been revamped to look more like GNOME apps
> * Zrythm no longer depends on breeze icons and some icons have changed
> * A bug when copy-pasting audio regions after a BPM change was fixed
> * Zrythm is now fully REUSE 3.0 compliant (license issues reported by `reuse lint` have been fixed)
> ![](JhQKORjmPsbquKwbjtmYhcTq.png)
> ![](pvixBvLNADqgvhfXtRVvwvuR.png)

# Miscellaneous

[ghani1990](https://matrix.to/#/@ghani1990:matrix.org) announces

> [David](https://github.com/david-swift), is embarking on a mission to bridge the gap between the sleek Swift programming language and the established GNOME and Adwaita ecosystem.
> His efforts go beyond simply bringing Swift to GNOME. he's actively building Adwaita for Swift, a framework specifically designed to create user interfaces for GNOME applications using an API remarkably similar to Apple's SwiftUI. This intuitive approach promises to significantly reduce the learning curve for developers familiar with the SwiftUI paradigm, encouraging wider adoption of Swift within the GNOME realm.
> 
> **Exciting New Developments:**
> 
> David's dedication extends beyond the Adwaita for Swift framework. He is actively contributing to the [Adwaita-Swift](https://github.com/AparokshaUI/adwaita-swift) library, introducing a wave of new features designed to further empower developers:
> 
> 	* Support for auto-generated widget bindings
> 	* Support for setting a view's visibility
> 	* Enhanced support for signals
> 	* Improved onClick observer
> 	
> Adwaita for Swift's intuitive API and the continuous improvements to the Adwaita-Swift library hold immense potential to attract new developers, it's worth noting that this bridge also extends to existing macOS developers. This opens up exciting possibilities for attracting talent and fostering cross-platform collaboration, ultimately benefiting the GNOME ecosystem as a whole.
> 
> **Flashcards: A Swift Showcase:**
> 
> David’s recent project, [Flashcards](https://github.com/david-swift/Flashcards) application, demonstrates Swift’s potential. Although still in early stages, Flashcards packs simplicity and power. Key features include creating, editing, viewing, and studying sets. Need to prepare for an exam? The test mode has you covered. Plus, importing existing Quizlet sets is a breeze.
> 
> This journey towards a more diverse and vibrant GNOME development environment continues, fueled by the dedication and foresight of open-source developers.
> ![](SrCVrkZWZCFCDEgsaIYIIZbu.png)
> ![](ISoJIQPgyaItaWOpvHUCtaKU.png)
> {{< video src="JbGgcpjPhSwVXlatsOcFDMdR.webm" >}}

[Dorothy K](https://matrix.to/#/@dorothyk:matrix.org) announces

> As Outreachy interns,we have landed a `gnome_locales` testsuite to test locale changes  for some languages with different alphabets ie. **Russian** and **Japanese** and RTL languages ie. **Arabic**.                                                                  Check it out here: https://openqa.gnome.org/tests/3294#step/gnome_welcome_locales/13
> ![](qCqRqxybRWhmrIHNkWohmJdz.png)

# Events

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> FOSDEM is over, thanks to everyone for attending and organizing. Here is a selection of talks from GNOME contributors
> 
> * [A fully open source stack for MIPI cameras](https://fosdem.org/2024/schedule/event/fosdem-2024-3013-a-fully-open-source-stack-for-mipi-cameras/)
> * [Privacy respecting usage metrics for Free Software projects](https://fosdem.org/2024/schedule/event/fosdem-2024-3648-privacy-respecting-usage-metrics-for-free-software-projects/)
> * [Drop the docs and embrace the model with Gaphor](https://fosdem.org/2024/schedule/event/fosdem-2024-2542-drop-the-docs-and-embrace-the-model-with-gaphor/)
> * [Enhancing Linux Accessibility: A Unified Approach](https://fosdem.org/2024/schedule/event/fosdem-2024-2949-enhancing-linux-accessibility-a-unified-approach/)
> * [Wayland's input-method is broken and it's my fault](https://fosdem.org/2024/schedule/event/fosdem-2024-2972-wayland-s-input-method-is-broken-and-it-s-my-fault/)
> * [GStreamer: State of the Union 2024](https://fosdem.org/2024/schedule/event/fosdem-2024-3590-gstreamer-state-of-the-union-2024/)
> * [The state of video offloading on the Linux Desktop](https://fosdem.org/2024/schedule/event/fosdem-2024-3557-the-state-of-video-offloading-on-the-linux-desktop/)
> * [From Kernel API to Desktop Integration, how do we integrate battery charge limiting in the desktop](https://fosdem.org/2024/schedule/event/fosdem-2024-2123-from-kernel-api-to-desktop-integration-how-do-we-integrate-battery-charge-limiting-in-the-desktop/)

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) reports

> The GUADEC 2024 Call for Participation is still open! We are looking for proposals for both in-person and remote talks, as well as workshops and BoFs. If you have a talk you would like to share with GUADEC attendees make sure to submit it by Feb 18: https://foundation.gnome.org/2024/01/10/guadec-2024-call-for-participation-opens/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

