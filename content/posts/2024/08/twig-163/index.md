---
title: "#163 Public Transit"
author: Felix
date: 2024-08-30
tags: ["blueprint-compiler", "glib", "tuba", "gnome-maps"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 23 to August 30.<!--more-->

# GNOME Core Apps and Libraries

### Maps [↗](https://wiki.gnome.org/Apps/Maps)

Maps gives you quick access to maps all across the world.

[mlundblad](https://matrix.to/#/@mlundblad:matrix.org) says

> Maps now supports Transitous (https://transitous.org/ ), using a crowd-sourcing approach, backed by the MOTIS search engine for public transit routing (for the time being for areas that isn't already supported by existing plugins).
> ![](cLmbDaMzHaFaMMmqvHZldSvz.png)
> ![](tsYrjUlfTrOLKTaOjHRdaUdK.png)
> ![](FSAnionXfckTmZQGmVYpZKZJ.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Thanks to work by Evan Welsh, GLib 2.83.0 (the next unstable release, due in a few months) will have support for sync/async/finish introspection annotations when built and run with a suitably new version of gobject-introspection, https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3746

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Thanks to Luca Bacci for putting in time to improve debuggability of GLib on Windows CI machines, this should help with keeping GLib running on Windows and Windows-like platforms in the future (more help always needed if anyone is interested)

# GNOME Circle Apps and Libraries

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) announces

> I've informally documented the [GNOME Circle review procedure](https://gitlab.gnome.org/Teams/Circle/-/blob/main/review_procedure.md). Existing conventions are laid out and briefly explained, and the ways of which we keep member projects in check are formalized through the concepts of *control reviews* and *group notices*. My goal with this is to increase the transparency and consistency of how the Circle Committee operates.
> 
> If you're already a member of [GNOME Circle](https://circle.gnome.org), a developer considering applying, or just a curious bystander, and have questions about this, don't hesitate to reach out.
> ![](194e3b3d4d6b9e53e75091ae07ee63e86fb5b34a1827821140043628544.png)

### Tuba [↗](https://github.com/GeopJr/Tuba)

Browse the Fediverse.

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) reports

> This week [Tuba](https://flathub.org/apps/dev.geopjr.Tuba) was accepted into GNOME Circle. Tuba lets you explore the federated social web. With its extensive support for popular Fediverse platforms like Mastodon, GoToSocial, Akkoma, and more, it makes it easy to stay connected to your favorite communities, family and friends. Congratulations!
> ![](19b3e2c54f33b8ba4c79bb6d1e5644fd722b70791828153395236569088.png)

# Third Party Projects

[Ronnie Nissan](https://matrix.to/#/@ronniedroid:mozilla.org) reports

> Unix file permissions can be hard to understand, especially for new linux users, that is why I wrote Concessio, an app to help users understand and convert between said permissions. You can get Concessio from [flathub](https://flathub.org/apps/io.github.ronniedroid.concessio)
> ![](0c36166129f6764020340cb47dc64a9c6afc479c1829071275998314496.jpg)
> ![](690230e8d1018fc92f7fdec942f9c53ad8f6c42d1829071280452665344.jpg)

[Alain](https://matrix.to/#/@a23_mh:matrix.org) says

> The new version of Planify 4.11.0 is here, packed with features and improvements that will make your task management even more efficient and personalized!
> 
> **What's New in Planify 4.11.0**
> 
> **1. Collapsible Sections:**
> You can now collapse sections for a cleaner and more focused view of your priority tasks.
> 
> **2. Task Counter per Section:**
> Easily see the number of tasks in each section with the new task counter.
> 
> **3. Pinned Tasks at the Top:**
> Pinned tasks are now displayed at the top of each project, keeping them always in sight.
> 
> **4. Overdue Tasks Indicator:**
> The "Today" button now shows an indicator for overdue tasks, helping you stay on top of what’s important.
> 
> **5. New Quick Add Keyboard Shortcuts:**
> We’ve added new shortcuts to speed up your workflow:
> 
>     - @ to add tags.
>     - # to select a project.
>     - ! to create a reminder.
> 
> **6. Completed Tasks View:**
> Check your completed tasks sorted by date and filter them by sections as needed.
> 
> **7. Task History Log:**
> Access the new task history log, where you can see when a task was completed or moved between projects or sections.
> 
> **8. Improved Drag and Drop:**
> We’ve fixed several bugs with drag and drop, ensuring a smoother experience.
> 
> **9. Quick Find Task Detail:**
> Now, when selecting a task in Quick Find, the task detail will automatically open.
> 
> **10. Markdown Preference:**
> You can now enable or disable the use of Markdown in task details, based on your preference.
> 
> **11. Improved Keyboard Navigation:**
> Keyboard navigation has been optimized to make your experience faster and easier.
> 
> **12. Create Sections in Quick Add:**
> If a section doesn’t exist, you can now create it directly from Quick Add.
> 
> **13. Attachment Number Indicator:**
> We’ve added a numerical indicator to show the number of attachments on each task.
> 
> **14. Preference to Keep Task Detail Pane Open:**
> We’ve added a preference that allows you to keep the task detail pane always open, making navigation faster.
> 
> **15. Quicker Recurring Tasks:**
> The option to repeat a task has been moved to the date and time selection widget, making the process of creating recurring tasks faster.
> 
> **16. Improved Nextcloud Synchronization:**
> We fixed several bugs related to task synchronization with Nextcloud.
> 
> **17. Design and Optimization Improvements:**
> Planify is now faster and consumes fewer resources, thanks to various design and optimization improvements.
> 
> **A Heartfelt Thank You**
> We would like to extend our deepest gratitude to everyone who supports the development of Planify by contributing financially. Your generosity helps us continue to improve and expand Planify, making it a better tool for everyone. Thank you for being a vital part of our journey!
> 
> Update now and enjoy an even more powerful and tailored task management experience. We’re continuously working to bring you the best in every release!
> 
> https://flathub.org/apps/io.github.alainm23.planify
> {{< video src="nBsWmhACUbWcMUKAOsVVKLFt.mp4" >}}
> {{< video src="qvKPPJzLOwfavpcpbWrSUjDB.mp4" >}}
> {{< video src="UbZBqydeRcTLZuLkfeoDBria.mp4" >}}
> {{< video src="QaVrcNuKZNZnsnPsApdMBlDk.mp4" >}}
> {{< video src="oBWovixbscpkgPvKDnqsStfk.mp4" >}}

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> After a year, there's [a new stable release](https://gitlab.gnome.org/GNOME/json-glib/-/releases/1.10.0) of JSON-GLib, the JSON parsing and generation library that tries to be well-integrated with GLib data types and best practices. Version 1.10.0 introduces a new, extensive conformance test suite to ensure that the parsing code is capable of handling both valid and invalid JSON. Additionally, JsonParser has a new "strict" option, for when you need to ensure that the JSON data is strictly conforming to the format specification. The parsing code has been simplified and improved, and passes both static analysis and sanitisers. Finally, there are lots of documentation fixes across the API and manual pages of the command line utilities.

### Blueprint [↗](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/)

A markup language for app developers to create GTK user interfaces.

[James Westman](https://matrix.to/#/@flyingpimonster:matrix.org) announces

> Blueprint 0.14.0 is here! This release includes a bunch of new features, including several from new contributors, but the main highlights are:
> * Greatly improved decompiler (.ui to .blp) support, including a new CLI command for this purpose
> * Syntax support for string arrays and multi-value accessibility relations
> * QoL improvements to the CLI output and language server
> * A ton of bugfixes

# Events

[Daniel Galleguillos Cruz](https://matrix.to/#/@daniel_gc:matrix.org) reports

> GNOME Latam 2024 - Universidad de Antioquia, October 25th and 26th. Medellín, Colombia. A day to celebrate and expand the GNOME community in Latin America. Come and share experiences in the creation and use of GNOME technologies in our region.
> 
> What is GNOME Latam 2024? GNOME Latam 2024 is more than just an event; it’s a movement that brings together developers, users, educators, and free software enthusiasts to share knowledge, experiences, and collaboratively build a more inclusive technological future. Through a series of in-person and online activities, we aim to spread the latest updates within the GNOME community, encourage active participation, and create a space where everyone can contribute, regardless of their experience level or prior knowledge.
> 
> Our mission is clear: to drive the adoption and development of the GNOME environment in Latin America, promoting the values of free software such as freedom, collaboration, and transparency. We believe that access to technology should be a universal right and that everyone, from experts to beginners, has something valuable to contribute.
> 
> Note: The talks will be conducted both in-person and online.
> 
> https://events.gnome.org/event/249/
> 
> This event will be held in Spanish and Portuguese.
> ![](hGBPMUsatbDTKFtaBfxvVEUW.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
