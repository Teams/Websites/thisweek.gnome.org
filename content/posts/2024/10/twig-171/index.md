---
title: "#171 Point of Interest"
author: Felix
date: 2024-10-25
tags: ["biblioteca", "warp", "gnome-maps", "glib", "amberol"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 18 to October 25.<!--more-->

# GNOME Core Apps and Libraries

### Maps [↗](https://apps.gnome.org/Maps)

Maps gives you quick access to maps all across the world.

[mlundblad](https://matrix.to/#/@mlundblad:matrix.org) says

> Maps now has a redesigned UI for editing points-of-interests in OpenStreetMap, using libadwaita widgets, and AdwDialog.
> ![](phfwNfxbGkbcxEtnJLnhUhSP.png)
> ![](QUEHXFGjdRWnVtmRDfWIYHWF.png)
> ![](pyXFgVbivvrqIKRJSyGvkKIh.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Jialu Zhou has renamed the methods in `GUnixMountEntry` in GLib so they can be introspected properly (previously they couldn’t be used easily from introspected languages); see https://gitlab.gnome.org/GNOME/glib/-/merge_requests/4337

# GNOME Incubating Apps

[Pablo Correa Gomez](https://matrix.to/#/@pabloyoyoista:matrix.org) announces

> Qiu Wenbo implemented using colors for highlight annotations in Papers! This was an often requested feature made possible due to the new mockups, and lots of refactoring under-the-hood
> {{< video src="ZPfSGalwTgOeoQufxKfsXrca.mp4" >}}

[Pablo Correa Gomez](https://matrix.to/#/@pabloyoyoista:matrix.org) reports

> @omthorat implemented the new annotation window mockups in Papers, giving annotated documents a great new appearance! You can get the latest development snapshot in gnome-nightly
> ![](tqDuaPfOVRODNgKgNNcBniyd.png)

# GNOME Circle Apps and Libraries

### Warp [↗](https://gitlab.gnome.org/World/warp)

Fast and secure file transfer.

[Fina](https://matrix.to/#/@felinira:matrix.org) says

> Warp 0.8 was released today. Warp allows you to securely send files to each other via the internet or local network by exchanging a word-based code, or scanning a QR code.
> 
> This release features quite a few stability improvements for QR code scanning, and lots of translation updates! The flatpak release was updated to the latest runtime and now supports accent colors.
> ![](lOykYAOiRhswXYdRCfchcIEP.png)

### Biblioteca [↗](https://github.com/workbenchdev/Biblioteca)

Read GNOME documentation offline

[Akshay Warrier](https://matrix.to/#/@akshaywarrier:matrix.org) reports

> [Biblioteca](https://flathub.org/apps/app.drey.Biblioteca) 1.5 is now available on Flathub!
> 
>  The notable changes in this release are:
> * Updated Flatpak runtime to GNOME 47
> * Added gom documentation
> * Updated various library docs (libportal 0.8.1, vte 0.78, libshumate 1.3.0, libspelling 0.4.2)
> * Added support to persist window size across sessions
> * Removed irrelevant context menu entries in the webview

### Amberol [↗](https://gitlab.gnome.org/ebassi/amberol/)

Plays music, and nothing else.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> [Amberol 2024.2](https://gitlab.gnome.org/World/amberol/-/releases/2024.2) is now available on Flathub! Amberol is a small music player with no delusions of grandeur, focused on the task of playing your local music in the simplest way possible. In this new release you'll find:
> * support for external album cover images named `folder` (in both PNG and JPEG image file formats) in the same directory as the songs
> * the file selection dialogs used to add songs and folders will start from the XDG Music directory, if one is set
> * an updated MPRIS implementation, dropping the unmaintained mpris-player crate in favour of the mpris-server one
> * an updated playback implementation, using the GstPlay API instead of the deprecated GstPlayer
> * various style updates to reflect the changes in libadwaita UI elements
> * lots of localisation updates
> 
> As usual, you can install Amberol from [Flathub](https://flathub.org/apps/io.bassi.Amberol).
> ![](801e3b86351ebcf1e5c10d71b9c3342866463eb11849868924460466176.png)

# Third Party Projects

[Konstantin Tutsch](https://matrix.to/#/@konstantin:konstantintutsch.com) says

> [Lock](https://konstantintutsch.com/Lock) is now available on Flathub! Lock is a graphical front-end for GnuPG (GPG) making use of a beautiful LibAdwaita GUI.
> 
> Process text and files:
> 
> * Encryption
> * Decryption
> * Signing
> * Verification
> 
> Manage your GnuPG keyring:
> 
> * Generate new keypairs
> * Import keys
> * Export public keys
> * View expiry dates
> * Remove keys
> 
> Download on [Flathub](https://flathub.org/apps/com.konstantintutsch.Lock).
> ![](CMZuJVCikxcOA7apfoZVRdUHxWwobF6q.png)
> ![](EqoyhNzt52Fc9JsbXr6wJTsdVztszlvf.png)
> ![](K13fbnG9sLNCBfsRRIKWVbohlzA5CMBt.png)

[Hari Rana | TheEvilSkeleton](https://matrix.to/#/@theevilskeleton:fedora.im) says

> Upscaler 1.4.0 was just released! Upscaler is an app that allows you to upscale and enhance images, be it your photos, digital art, and more.
> 
> This release introduces scaling factors, allowing you to upscale between a factor of 2 and 4. The image loading system has been reworked to decrease overall memory consumption. The preview image now has a drop shadow to better make the image distinguishable. Lastly, it fixes a bug where small window sizes made the preview image disappear.
> ![](184f03bdb78a7628d9fae535a2518841ae9b73d11849802576447406080.png)

# Miscellaneous

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) announces

> I blogged about implementing Steam Deck gamepad support in libmanette: https://blogs.gnome.org/alicem/2024/10/24/steam-deck-hid-and-libmanette-adventures/

# GNOME Websites

[Allan Day](https://matrix.to/#/@aday:gnome.org) reports

> GNOME's wiki was officially retired this week. Its functions have already been replaced by other sites and, if you do need information from the old wiki, a static archive of the site is still available at [wiki.gnome.org](https://wiki.gnome.org/). 
> 
> For more information, see the [wiki the migration guide](https://pad.gnome.org/wiki-migration-guide?view).

# Events

[Kristi Progri](https://matrix.to/#/@kristiprogri:gnome.org) announces

> We’re excited to announce that registration for GNOME ASIA 2024 is now open! 
> For more details, feel free to check out our blogpost: https://foundation.gnome.org/2024/10/23/registration-now-open-for-gnome-asia-2024/

# GNOME Foundation

[Allan Day](https://matrix.to/#/@aday:gnome.org) reports

> The GNOME Foundation Board is looking for input on the future of GUADEC. Please comment on the [Discourse thread](https://discourse.gnome.org/t/feedback-wanted-what-do-you-want-from-guadec/24656) with your opinions on where GUADEC should be located, and what its focus should be.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
