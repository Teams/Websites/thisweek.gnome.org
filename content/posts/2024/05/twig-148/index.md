---
title: "#148 Variable Styles"
author: Thib
date: 2024-05-17
tags: ["libadwaita", "phosh"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 10 to May 17.<!--more-->

# Sovereign Tech Fund

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) reports

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure) initiative, a number of community members are working on infrastructure related projects.
> 
> Here are the highlights for the last week:
> 
> Alice [landed CSS variables in GTK](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/6540) 🥳 
> 
> Alice also [ported](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1099) libadwaita to the new CSS variables, [added --window-radius](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1111), [synced AdwComboRow with GTK changes](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1103), added a [default Adw(Application)Window size](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1109), fixed some [dialog focus bugs](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1114) and improved [alert dialog styles](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1115) in addition to various other bug fixes, refactors, and reviews.
> 
> Adrian is working on a new installer for GNOME OS. It's still very early days though, so there isn't a repo yet.
> 
> Felix started investigating how to allow [changing passwords of secret-service collections](https://github.com/bilelmoussaoui/oo7/issues/94).
> 
> Sam worked on the addressing the open questions with the [file chooser designs](https://gitlab.gnome.org/Teams/Design/app-mockups/-/commit/9be6fad519cac79728ed4662d91059e87000a06e).
> 
> Antonio continued work on the Nautilus file chooser, includign some [refactoring](https://gitlab.gnome.org/GNOME/nautilus/-/work_items/3431) to minimize code duplication between Nautilus and file chooser and [implementing an adaptive file chooser layout](https://gitlab.gnome.org/GNOME/nautilus/-/commit/c09c6184000c988fb13712b5a6e77f909c3074ef) following the latest mockups.
> 
> {{< video src="81aa9a62315bccdd50c00111812fd577eba1a5331791570662410682368.webm" >}}
>
> Dorota continued working on the global shortcuts portal, creating a [prototype portal implementation for testing](https://gitlab.gnome.org/dcz/gsimpl), finishing the [GNOME Settings side of the portal](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2485) (waiting for maintainer feedback), and [addressing feedback to the mutter MR](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3680#note_2107354).
> 
> Joanie worked on various regression fixes and cleanups in Orca.
> 
> Matt worked on text support in the [GTK AccessKit implementation](https://gitlab.gnome.org/mwcampbell/gtk/tree/accesskit). The implementation now supports GtkLabel, GtkInscription, and GtkText-based editable widgets (e.g. GtkEntry), including complex cases like bidirectional text and combining characters (e.g. some emoji). The implementation is now complete enough to support full screen reader access to single-line editable widgets. He also added passive key snooping to Newton's AT client protocol (involving [mutter](https://gitlab.gnome.org/mwcampbell/mutter/tree/newton-prototype-2), [newton_consumer](https://gitlab.gnome.org/mwcampbell/newton_consumer), [newton_atspi_compat](https://gitlab.gnome.org/mwcampbell/newton_atspi_compat), and [Orca](https://gitlab.gnome.org/mwcampbell/orca/tree/newton)), and fixed a number of bugs across the stack to get to the point of being able to record a working demo of the whole Newton stack with the GNOME Podcasts app.
>
> {{< video src="newton-demo-2024-05-17.mp4" >}}
> 
> Sam [cleaned up the visuals](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1507) of the floating bar in the file view in Nautilus.
>
> ![](1670e6798e26e22e5ee39ad4d4250c529249e4901791570626159312896.png)
> 
> Dhanuka worked on the oo7-daemon Secret Prompt, exposing [oo7::crypto::decrypt](https://github.com/bilelmoussaoui/oo7/pull/73/commits/c10a62915df8ddb6808af2ef2165ac7101fd86e2) and [oo7::Key::new](https://github.com/bilelmoussaoui/oo7/pull/73/commits/81ee71c6e242a4748e6edfcb83c7394f4cbe3c0a), and implementing [SecretExchange for oo7-daemon](https://github.com/bilelmoussaoui/oo7/pull/73/commits/52e139248f986e1a6873a0dd4b966b3377dfad6f).
> 
> Andy worked on Online Accounts, including [IMAP/SMTP autoconfig](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/229), [SRV lookups](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/227), [content-restricted app passwords](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/226), [WebDAV fixes for file access](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/228), and [WebDAV fixes for Fastmail/mailbox.org](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/225).
> 
> Julian continued work on the [notification portal implementation](https://github.com/flatpak/xdg-desktop-portal/pull/1298), rebased the GNOME Shell [notification grouping MR](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3012) and finished the MR to [add new notification properties to GLib](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/4027).
> 
> Jude worked on the GNOME OS migration from ostree to sysupdate, finishing the [systemd sysupdate dbus service](https://github.com/systemd/systemd/pull/32363) and implementing the [sysupdate plugin in GNOME Software](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/2004).
> 
> Neill worked on security tracking for GNOME OS, opening an MR [make use of freedesktop-sdk's CVE generation utility](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/2873) for reports, add vm and vm-secure reports, and store reports per build branch.
> 
> Sophie landed a D-Bus proxy fix for [no longer requiring monotonic serials](https://github.com/flatpak/xdg-dbus-proxy/pull/57).
> 
> Jonas reviewed and merged a GNOME Shell MR to [allow for 3+ fingers touchpad gestures](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3275), opened an MR to [improve the captive portal dialog a bit](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3307), updated and landed the GNOME Shell [overview spacing improvements](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3278), and worked on GNOME Shell [docs for new contributors](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3322).
> 
> Hub worked on fixing the various issues in the USB portal.
> 
> Evan worked on fixing remaining issues in [TypeScript bindings beta](https://github.com/gjsify/ts-for-gir/issues/171).
> 
> We are very happy to announce our partnership with Codethink to make GNOME OS a viable daily driver for QA and development.
> 
> * Finish the migration from ostree to sysupdate
> * Add support for parallel branches
> * Decrease the image size
> * Tooling for development and testing on immutable OSes
> * Security tracking for GNOME OS
> * Allow testing GNOME Merge Requests
> * OpenQA improvements
> 
> You can ready about systemd-sysupdate integration at [GNOME OS + systemd-sysupdate](https://www.codethink.co.uk/articles/2024/GNOME-OS-systemd-sysupdate/).

[Neill Whillans](https://matrix.to/#/@neillydun:matrix.org) reports

> Working in collaboration with the GNOME Foundation, through the Sovereign Tech Fund (STF), Codethink is currently involved in assisting with the migration from OSTree to systemd-sysupdate for GNOME OS updates.
> 
> Our latest blog [post](https://www.codethink.co.uk/articles/2024/GNOME-OS-systemd-sysupdate/) describes the benefits of using systemd-sysupdate; the migration work already carried out, and the remaining tasks.
> 
> Keep an eye on the Codethink [blog](https://www.codethink.co.uk/updates.html) where we’ll be sharing progress updates.

# GNOME Core Apps and Libraries

[Matthias Clasen](https://matrix.to/#/@matthiasc:gnome.org) announces

> The 2.42.12 release of gdk-pixbuf is now available.
> 
> This release fixes CVE-2022-48622 in the ani loader.
> 
> It also includes a change to the default build setup
> to only build the png, jpeg and tiff loaders by default.
> 
> To build other loaders, use the gif and others options:
> 
> `meson configure -Dgif=true -Dothers=true`

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) announces

> GTK's CSS engine now supports custom properties (aka variables) and libadwaita is now making use of them. This means that apps can now use them instead of old named colors. As such, it's now possible to override them per-widget instead of globally.
> 
> The new colors are documented at https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/css-variables.html - for the most part it's just a change in syntax, not in the available colors - since the old colors still need to work we can't make more drastic changes until 2.0.
> 
> `.error`, `.warning` and `.success` style classes now also change the accent color respectively. For example, this means that a selectable label with .error style class will have a red selection as well, instead of blue.
> 
> The `.opaque` style class for buttons has been deprecated - instead, apps can simply use `.suggested-action` and override the accent color on it. This will also change the focus ring, and similarly, `.destructive-action` buttons have red focus rings now.
> 
> Apps are encouraged to migrate, but the old colors will keep working until 2.0. For example, `@accent_color` can be replaced with `var(--accent-color)`, note the dashes in the names instead of underscores. See the docs for more info.
> ![](7a124173af3605a579a9a75f513ec1ef4890e9241790101807242936320.png)

# GNOME Circle Apps and Libraries

[Liferooter](https://matrix.to/#/@liferooter:vanutp.dev) reports

> After almost two years Text Pieces 4.0 has ben released and now available on Flathub. The application was rewritten and redesigned from scratch.
> 
> New features and improvements:
> 
> * Redesigned interface.
> * Redesigned terminology. Tools are now called actions, arguments are now called parameters.
> * The application became fully asynchronous and now long-running actions do not freeze the interface and also can be cancelled.
> * Added support for typed parameters. Now actions can have boolean or numeric parameters, not only string parameters.
> * Built-in actions are now written in Rust and well-tested, which means that they run faster and less likely to break.

# Third Party Projects

[Alain](https://matrix.to/#/@a23_mh:matrix.org) says

> Planify 4.7.4: New Features and Enhancements for Better Project Management!
> We are excited to announce the release of version 4.7.4 of Planify, our task and project management application. This update brings significant improvements that will enhance your user experience. Here are the key highlights:
> 
> * **Support for Libadwaita 1.5 and Adaptive Dialogs**: All dialogs in Planify are now fully responsive, improving usability on various devices.
> * **Design, Usability, and Performance Improvements**: Enhanced interface and smoother performance for a better user experience.
> * **Updated Translations for Hindi and Chinese**: More accurate and accessible interface for users speaking these languages.
> 
> These are just some of the new features and improvements you'll find in Planify version 4.7.2. We hope you enjoy using these new tools to make your task and project management even more efficient and productive.
> 
> Download the [update](https://github.com/flathub/io.github.alainm23.planify) today and take your productivity to the next level with Planify!
> 
> Thank you for being part of the Planify community!
> ![](WjQnnlxQQbQHqMIoIrUzqZNq.png)
> ![](ZJlZrdmFvJpvZIscZRPrLvKx.png)
> ![](uLmyyOuRZhjspxXWlZujJwKi.png)
> ![](SHJiQDKKVitaKQrqYVSDKAqp.png)
> ![](IYGyGOCsZHtXAHeeDPaqKNcl.png)
> ![](rhDGTEVXpoCeVdSuXDToePWJ.png)
> ![](BQaJmgpqunlLMvmFZjBpEQEI.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) reports

> Phosh 0.39.0 is out:
> 
> [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) now has folder support (to organize your apps) and a night light quick setting. [Phoc](https://gitlab.gnome.org/World/Phosh/phoc) (with the help of [gmobile](https://gitlab.gnome.org/World/Phosh/gmobile)) now gets the keys that should unblank/unidle the device via hwdb/udev. And the on screen keyboard [squeekboard](https://gitlab.gnome.org/World/Phosh/squeekboard) gets new layouts (Portuguese, Slovenian and Turkish (F- and Q-layouts)) as well as improvements to several existing layouts.
> 
> There's more. Check the full details [here](https://phosh.mobi/releases/rel-0.39.0/)
> ![](idqgQJArDCfoMBKSNdfJwpbF.png)

# Documentation



### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[kramo](https://matrix.to/#/@kramo:matrix.org) reports

> Guidelines on using [button rows](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.ButtonRow) have been added to the HIG. The widget is used to present actions with lists in some cases where we would have traditionally used regular or pill buttons. Developers are encouraged to take a look at the [new guidelines](https://developer.gnome.org/hig/patterns/containers/boxed-lists.html#adding-buttons) and start using the widget where appropriate.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
