---
title: "#147 Secure Keys"
author: Felix
date: 2024-05-10
tags: ["letterpress", "gir.core", "glib", "eartag"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 03 to May 10.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> As part of the GNOME STF (Sovereign Tech Fund) initiative, a number of community members are working on infrastructure related projects.
> 
> Here are the highlights for the past week
> 
> Andy is making progress on URL handling for apps. We are planning on advancing and using [the freedesktop intent-apps proposal](https://gitlab.freedesktop.org/xdg/xdg-specs/-/merge_requests/45) which Andy implemented in [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal/pull/1313/commits/11c6af7a7a956c596c64fa1e7e19b49f0f86ac55).
> 
> Felix completed the work to [add keyring collections support to Key Rack](https://gitlab.gnome.org/sophie-h/key-rack/-/merge_requests/14).
> 
> Adrien worked on [replacing deprecated and inaccessible `GtkTreeView` in Disk Usage Analyzer](https://gitlab.gnome.org/GNOME/baobab/-/merge_requests/74) (Baobab)
> 
> Adrien worked on [replacing deperecated and inaccessible `GtkEntryCompletion` in Files](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1497) (Nautilus) 
> 
> Dhanuka [finalized the Keyring implementation in oo7](https://github.com/bilelmoussaoui/oo7/pull/73). 
> 
> Dhanuka [landed rekeying support in oo7](https://github.com/bilelmoussaoui/oo7/pull/77)
> 
> Hubert made good progress on the USB portal and the portal is now able to display a permission dialog.
> 
> Julian [added notifications spec v2 support to GLib GNotification](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/4027)
> 
> Julian [created a draft merge request](https://github.com/flatpak/xdg-desktop-portal-gtk/pull/478) for new notification specs against xdg-desktop-portal-gtk
> 
> Antonio finished preparing nautilus components for reuse in a new FileChooser window. [Ready for review](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1502)
> ![](bf770c356fee5516f85426e58405f225e60a84d61789006741359296512.png)

# GNOME Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> A series of fixes for a GDBus security issue with processes accepting spoofed signal senders has landed. Big thanks to Simon McVittie for putting together the fix (and an impressive set of regression tests), to Alicia Boya García for reporting the issue, and Ray Strode for reviews. https://discourse.gnome.org/t/security-fixes-for-signal-handling-in-gdbus-in-glib/20882

# GNOME Circle Apps and Libraries

### Ear Tag [↗](https://gitlab.gnome.org/World/eartag)

Edit audio file tags.

[knuxify](https://matrix.to/#/@knuxify:cybre.space) says

> Ear Tag [0.6.1](https://gitlab.gnome.org/World/eartag/-/releases/0.6.1) has been released, bringing a few minor quality-of-life improvements and a switch to the new AdwDialog widgets. You can get the latest release from [Flathub](https://flathub.org/apps/app.drey.EarTag).
> 
> (Sidenote - I am looking for contributors who would be willing to help with Ear Tag's testing, bug-fixing and further development, with the goal of potentially finding co-maintainers - if you're interested, see [issue #132](https://gitlab.gnome.org/World/eartag/-/issues/132) for more details.)
> ![](wLZhaFfOlynCfMrJcNpbRGki.png)

### Letterpress [↗](https://gitlab.gnome.org/World/Letterpress)

Create beautiful ASCII art

[Gregor Niehl](https://matrix.to/#/@gregorni:gnome.org) says

> A new minor release of [Letterpress](https://flathub.org/apps/io.gitlab.gregorni.Letterpress) is out! No big UI changes in this 2.1 release, mostly small touches here and there:
> 
> * Images can now be pasted from the clipboard
> * Zoom is now more consistent between different factors
> * The Drag-n-Drop overlay was ~~stolen from Loupe~~ redesigned
> * The GNOME runtime was updated to version 46, which means the Tips Dialog now uses the new [`Adw.Dialog`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Dialog.html), and the About Dialog is now truly an [`About Dialog`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.AboutDialog.html)
> 
> The app has also been translated to Simplified Chinese!
> 
> I'm happy to announce that, in the meantime, @FineFindus has joined the project as maintainer, so it's no longer maintained by a single person.

# Third Party Projects

[Alain](https://matrix.to/#/@a23_mh:matrix.org) announces

> **Planify 4.7.2 is here!**
> 
> We're excited to announce the release of Planify version 4.7.2, with exciting new features and improvements to help you manage your tasks and projects even more efficiently!
> 
> **1. Inbox as Independent Project:** We've completely rebuilt the functionality of Inbox. Now, it's an independent project with the ability to move your tasks between different synchronized services. The Inbox is the default place to add new tasks, allowing you to quickly get your ideas out of your head and then plan them when you're ready.
> 
> **2. Enhanced Task Duplication:** When you duplicate a task now, all subtasks and labels are automatically duplicated, saving you time and effort in managing your projects.
> 
> **3. Duplication of Sections and Projects:** You can now easily duplicate entire sections and projects, making it easier to create new projects based on existing structures.
> 
> **4. Improvements in Quick Add:** We've improved the usability of Quick Add. Now, the "Create More" option is directly displayed in the user interface, making it easier to visualize and configure your new tasks.
> 
> **5. Improvements on Small Screens:** For those working on devices with small screens, we've enhanced the user experience to ensure smooth and efficient navigation.
> 
> **6. Project Expiry Date:** Your project's expiry date now clearly shows the remaining days, helping you keep track of your deadlines more effectively.
> 
> **7. Enhanced Tag Panel:** The tag panel now shows the number of tasks associated with each tag, rather than just the number of tags, giving you a clearer view of your tagged tasks.
> 
> **8. Archiving of Projects and Sections:** You can now archive entire projects and sections! This feature helps you keep your workspace organized and clutter-free.
> 
> **9. New Task Preferences View and Task Details Sidebar:** Introducing a new task preferences view! You can now customize your task preferences with ease. Additionally, we've enabled the option to view task details using the new sidebar view, providing quick access to all the information you need.
> 
> **10. Translation Updates:** We thank @Scrambled777 for the Hindi translation update and @twlvnn for the Bulgarian translation update.
> 
> These are just some of the new features and improvements you'll find in Planify version 4.7.2. We hope you enjoy using these new tools to make your task and project management even more efficient and productive.
> 
> Download the [update](https://flathub.org/apps/io.github.alainm23.planify) today and take your productivity to the next level with Planify!
> 
> Thank you for being part of the Planify community!
> ![](MoHDeCNbVJvWRmOfTalzSiHe.png)
> {{< video src="CtAbRYCHziThspUkmusfyIgL.webm" >}}
> {{< video src="WbBOCwkYjILnSMdFwkbsvTWU.webm" >}}

[Giant Pink Robots!](https://matrix.to/#/@giantpinkrobots:matrix.org) says

> [Varia](https://giantpinkrobots.github.io/varia/) download and torrent manager got a pretty big update.
> 
> * Powerful download scheduling feature that allows the user to specify what times in a week they would like to start or stop downloading, with an unlimited amount of custom timespans.
> * The ability to import a cookies.txt file exported from a browser to support downloads from restricted areas like many cloud storage services.
> * Support for remote timestamps if the user wants the downloaded file to have the original timestamp metadata.
> * Two new filtering options on the sidebar for seeding torrents and failed downloads.
> * An option to automatically quit the application when all downloads are completed.
> * An option to start the app in background mode whenever it's started.
> * Support for Spanish, Persian and Hindi languages.

[Mateus R. Costa](https://matrix.to/#/@MateusRodCosta:matrix.org) announces

> [bign-handheld-thumbnailer](https://github.com/MateusRodCosta/bign-handheld-thumbnailer) (a Nintendo DS and 3DS files thumbnailer) version 0.9.0 was intended to appear on previous week's TWIG but due to performance issues had to be postponed.
> 
> Version 0.9.0 is notable because it finally introduced CXI and CCI (which were deemed too hard to implement for the original 0.1.0 code) and there were a few misc improvements. However it was pointed that the thumbnailer was loading full games to the memory (official 3DS games can weight up to almost 4 GB) even though there were some suggestions on how to improve that I still failed to initiallly make it work.
> At that point [a COPR repo](https://copr.fedorainfracloud.org/coprs/mateusrodcosta/bign-handheld-thumbnailer/) also became available to help distributed the compiled RPM.
> 
> Version 1.0.0 was intended to fix the performance issue once for all, but for more details I recommend reading the blog post about this release at my personal blog: https://www.mateusrodcosta.dev/blog/bign-handheld-thumbnailer-what-i-learned-linux-thumbnailer-rust/
> ![](bUlSMiLdeREtFjkOUxtbSiVH.png)

### Gir.Core [↗](https://gircore.github.io/)

Gir.Core is a project which aims to provide C# bindings for different GObject based libraries.

[badcel](https://matrix.to/#/@badcel:matrix.org) reports

> [Gir.Core 0.5.0](https://github.com/gircore/gir.core/releases/tag/0.5.0) was released. This is one of the biggest releases since the initial release:
> 
> * A lot of of new APIs are supported (especially records)
> * Bugs got squashed
> * The library versions were updated to GNOME SDK 46 and target .NET 8 in addition to .NET 6 and 7
> * New samples were added
> * The homepage got updated
> 
> Anyone interested bringing C# / F# back into the Linux ecosystem is welcome to come by and try out the new version.

# Miscellaneous

[Dan Yeaw](https://matrix.to/#/@danyeaw:gnome.org) says

> [Gvsbuild](https://github.com/wingtk/Gvsbuild), the GTK stack for Windows, version 2024.5.0 is out. Along with the latest GTK version 4.14.4, we also released for the first time a pre-built version of the binaries. To set up a development environment for a GTK app on Windows, you can unzip the package, set a couple of environmental variables, and start coding.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
