---
title: "#146 Editing Markdown"
author: Felix
date: 2024-05-03
tags: ["gnome-mahjongg", "vala", "turtle", "railway", "apostrophe", "fractal", "gnome-software", "workbench", "tracker"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 26 to May 03.<!--more-->

# Sovereign Tech Fund

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) announces

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) initiative, a number of community members are working on infrastructure related projects.
> 
> Here are the highlights for the past two weeks:
> 
> Dorota [created a standalone dialog in GNOME Control Center](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2485) to let users choose/approve/reject when an app requests a Global Shortcuts 
> 
> Dhanuka landed [Add rekeying support for oo7::portal::Keyring](https://github.com/bilelmoussaoui/oo7/pull/77) in oo7.
> 
> Hub [implemented the in-progress USB portal in ashpd](https://github.com/hfiguiere/ashpd/tree/usb-portal) to demo and test
> 
> Sophie landed [libglycin: Add C/glib/gir API for glycin crate](https://gitlab.gnome.org/sophie-h/glycin/-/merge_requests/68). This will let language bindings use Glycin. A first version of the C-API is for glycin is available https://sophie-h.pages.gitlab.gnome.org/glycin/c-api/. Via GObject introspections (https://developer.gnome.org/documentation/guidelines/programming/introspection.html) it is also usable with GJS, Python, and Vala.
> 
> Antonio is making great progress on [using Nautilus as file picker](https://gitlab.gnome.org/GNOME/nautilus/-/work_items/3402).
> 
> Julian [finalized the notification portal specs](https://github.com/flatpak/xdg-desktop-portal/pull/1298), reviews welcome!
> 
> Jonas landed a fix for a long-standing touch bug
> * https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/2946
> * https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/5782
> 
> Jonas [opened a merge request](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3278) to improve GNOME Shell layout on smaller displays
>
> ![](7c67ecc267527fb71ced2e5f44e4df4b7671c2591786457382692323328.png)
> 
> Adrien opened an MR to [replace deprecated and inaccessible GtkEntryCompletion](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1497) in Nautilus.
> 
> The recording for Matt's talk from [Open Source Summit North America](https://ossna2024.sched.com/event/1aBO1/modernizing-accessibility-for-desktop-linux-matt-campbell-gnome-foundation) is now available, you can watch it on [Youtube](https://www.youtube.com/watch?v=w9psDfEFf9c).

# GNOME Circle Apps and Libraries

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu](https://matrix.to/#/@somas95:gnome.org) says

> After two years of development, I'm glad to announce that Apostrophe 3.0 is here! Almost every aspect of the application has seen improvements, from the obvious ones like the port to GTK4 and the refined interface, to several improvements under the hood. Among the new features are:
> 
> * A new toolbar so you don't have to remember markdown syntax
> * A more secure approach when opening and rendering files
> * Autoindentation and autocompletion for lists and braces
> * An improved Hemingway mode
> * The document stats will also show stats for the selected text
> 
> You can download it in [Flathub](https://flathub.org/apps/org.gnome.gitlab.somas.Apostrophe)
> ![](1e116d532e04dc04e3ef8f25d524c15335a3c73b1786047647170166784.png)
> ![](3bf087de8f76d809c7705f740cc51dd767e97e7d1786047443134054400.png)

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> Workbench 46.1 is out!
> 
> See what's new and details at https://blog.sonny.re/workbench-46-1
> ![](1e7d8b3c0d7225853a66b6f218fbce386f18b7c31785589596881420288.png)
> ![](f8298ceed4d7546b492b8d002c4b2271de8b6e211785589616410099712.png)

### Railway [↗](https://gitlab.com/schmiddi-on-mobile/railway)

Travel with all your train information in one place.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) announces

> Railway version 2.5.0 was  released. It contains updates to the GNOME 46 runtime, as well as the addition of the PKP provider (and removal of the INSA provider due to the API failing to search for locations). It furthermore now tries to query the remarks of journeys in the system-language, fixes a crash for the Spanish translation of the app and provides a fix for the RMV provider throwing an error.

# GNOME Core Apps and Libraries

### Vala [↗](https://gitlab.gnome.org/GNOME/vala)

An object-oriented programming language with a self-hosting compiler that generates C code and uses the GObject system.

[lwildberg](https://matrix.to/#/@lw64:gnome.org) reports

> Last month Reuben Thomas completed his port of [Enchant](https://abiword.github.io/enchant/) to [Vala](https://vala.dev)! Enchant is a spellchecking library also used in GNOME. Read the blog post about it and also his experience on porting another project (Zile) to Vala [here](https://vala.dev/blog/c-off-ramp/).

### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) announces

> The Tracker SPARQL developers are very happy to welcome rachle08 and Demigod who will be joining the team as part of Google Summer of Code, working on a project to add a [web-based query editor](https://gitlab.gnome.org/GNOME/tracker/-/issues/421) and generally improve the developer experience.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) reports

> In Tracker SPARQL, Carlos Garnacho worked around a non-backwards compatible change released in SQLite 3.45.3. This change causes errors that look like `ambiguous column name: ROWID (0)`. The fix will be in the next stable release - see [Discourse](https://discourse.gnome.org/t/tracker-sparql-affected-by-behaviour-change-in-sqlite-3-45-3/20772) for more details.

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Automeris naranja has made headway into porting gnome-software to the shiny new `AdwDialog` (and other related new libadwaita APIs)

# Third Party Projects

[José](https://matrix.to/#/@halfmexican:matrix.org) reports

> I've released my first app on Flathub! Mingle is a simple app
> to play with Google's Emoji Kitchen and copy them to your clipboard. It is written in Vala and has been my little pet project these past few months as a learning exercise.
> ![](OpleAVcALIhGrIMZckXsGudN.png)
> ![](dBHjQgpKJwQkoxldklORvVkR.png)

[slomo](https://matrix.to/#/@slomo:matrix.org) reports

> The [GStreamer GTK4 video sink](https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/-/tree/main/video/gtk4) got [support](https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/-/merge_requests/1547) for directly importing video frames as dmabufs on Linux when using GStreamer 1.24 / GTK 4.14, in addition to the already existing support for importing OpenGL textures or video frames in normal system memory. This new feature is available in version 0.12.4 of the sink plugin.
> 
> This is especially useful for video players using hardware decoders or applications that display a video stream from a camera (via v4l2 or pipewire), and allows side-stepping the GL/Vulkan rendering of the video frames inside GTK under certain conditions and let the composition be done by the Wayland compositor or even directly pass the dmabufs to the GPU kernel driver. Doing so would reduce GPU utilization and by that frees resources for other tasks and reduces power consumption. See [Matthias' blog post](https://blog.gtk.org/2024/04/17/graphics-offload-revisited/) on the GTK blog for more details about the dmabuf and graphics offloading support in GTK.

[Alain](https://matrix.to/#/@a23_mh:matrix.org) reports

> Planify 4.7 is Here! Discover the New Features and Improvements
> We're thrilled to announce the arrival of Planify 4.7! This version brings a host of exciting enhancements and new features that will make your task and project management experience smoother and more efficient than ever. Let's take a look at what we've added:
> 
> **Advanced Filtering Function**
> Now in Planify, you can filter your tasks within a project based on priority, due date, and assigned tags. Take control of your tasks like never before!
> 
> **Custom Sorting in Today View**
> Personalize the Today view by sorting your tasks the way you prefer. Make your day more productive by organizing tasks your way!
> 
> **Instant Task Details**
> With our new task detail view in the sidebar, you can quickly access all relevant task information while in the Board view. Keep your workflow uninterrupted!
> 
> **Efficient Management of Completed Tasks**
> Now, deleting completed tasks is easier than ever. Keep your workspace clean and organized with just a few clicks!
> 
> **Attach Files to Your Tasks**
> Never lose track of important files related to your tasks. With the file attachment feature, keep all relevant information in one place.
> 
> **Celebrate Achievements with Sound**
> Want a fun way to celebrate your achievements? Now you can play a sound when completing a task. Make every accomplishment even more satisfying!
> 
> **Bug Fixes and Performance Improvements**
> We've addressed a number of errors, from project duplication to issues with animation when adding subtasks. Additionally, we've updated translations in various languages, including Hindi, Bulgarian, Brazilian Portuguese, and Spanish.
> 
> Download the latest version of Planify on [Flathub](https://flathub.org/apps/io.github.alainm23.planify) now and take your task management to the next level!
> For any feedback, suggestions or bug reports, please file an issue at the [Github](https://github.com/alainm23/planify/issues) issue tracker.
> ![](koPhupZHebLuxBZQrTKSCUtG.png)
> ![](cqwkmxIGCKlYmbwsFNPeueGI.png)

[Link Dupont](https://matrix.to/#/@subpop:matrix.org) says

> Version 0.2.2 of Damask is here. This release has been in the works for a while,
> so contains a lot of bug fixes and UI improvements.
> 
> * Wallhaven: correctly set aspect ratio in search query
> * Reset the refresh timer after a manual refresh
> * Wallhaven: refresh wallpaper when preferences change
> * Refresh wallpaper preview only when a preview is available
> * Set active source by selecting the row in the source list
> * NASA: rename row title to "NASA Astronomy"
> * Sort source list alphabetically
> * Add a setting to disable automatic refresh
> * Improve support for a default "no source" application state
> * Fix preview image dimensions
> * Remove the "manual" source (disable automatic refresh instead)
> * NASA: replace user-defined API key with a value supplied at compilation
> * Unsplash: replace user-defined API key with a value supplied at compilation
> * Wallhaven: add explanatory text for the API key field
> * EarthView: update photo source
> * Slideshow: allow any image type when filtering files
> 
> Download it on [Flathub](https://flathub.org/apps/app.drey.Damask) today!
> ![](HvZjITLyXVxrKQRHiFjDfHmC.png)
> ![](QMcstnECwAmLOegkikDXXvOU.png)
> ![](CzGRbboScWhbKlLHsIeZfnmS.png)

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) reports

> Gameeky 0.6.4 is now available on [Flathub](https://flathub.org/apps/dev.tchx84.Gameeky). This new release brings minor fixes for running Gameeky on other platforms and it's now fully available in Brazilian Portuguese 🇧🇷, thanks to Rafael Fontenelle. As a result of Rafael efforts, the offline documentation can now be translated using regular gettext-based tools and therefore much easier to do so.
> ![](voJfDCQRqCUmCZGgjhYwivyq.png)
> ![](qNRfgRGcwEADhvjuDnIkZMny.png)

### Turtle [↗](https://gitlab.gnome.org/philippun1/turtle)

Manage git repositories in Nautilus.

[Philipp](https://matrix.to/#/@philippun:matrix.org) reports

> [Turtle](https://gitlab.gnome.org/philippun1/turtle) 0.8 has been released.
> 
> Retrieving the log commits and calculating the graph is now much faster. Opening up the log for, i.e. the gnome-shell repo, will now only take some seconds, if "Show all branches" is checked it will take roughly 15 seconds. Before it took roughly 1 minute 40 seconds, depending on your hardware of course.
> 
> There is now also a merge dialog available to merge a branch or commit into the current head. It is also possible to start a merge directly from the log context menu.
> 
> For easier usage, a help output has been added to both the turtle_cli and the turtlevcs python package, a bash completion file has been added and an emblem dialog has been added to the settings dialog.
> 
> And there are many more minor fixes and tweaks, see the [full changelog](https://gitlab.gnome.org/philippun1/turtle/-/releases/0.8).
> ![](iLjdMEWYfGfDhbWWueuCTbGU.png)
> ![](cljTRutvCQhUJkpCcgoKgYWQ.png)

### Mahjongg [↗](https://gitlab.gnome.org/GNOME/gnome-mahjongg)

A solitaire version of the classic Eastern tile game.

[Mat](https://matrix.to/#/@mat:mathias.is) announces

> Mahjongg has received a whole slew of improvements in the last few weeks:
> 
> * Complete dark/light mode support with separate backgrounds for each tileset
> * Faster loading times (almost instant, compared to the previous ~5 seconds for some tile layouts)
> * Moved tile layout switcher to the main menu, for easier access
> * Ported to newer GTK/libadwaita widgets, such as Gtk.ColumnView and Adw.Dialog
> * All known bugs addressed (issue tracker is empty!)
> 
> These changes are not released yet, but are available for testing in the [nightly Flatpak package](https://nightly.gnome.org/):  
> `flatpak install gnome-nightly org.gnome.Mahjongg.Devel`
> ![](efkqZJALYZLfdlnFaYtXrleE.png)
> ![](xcGxzPhbeNDFwuDUozCuvAkf.png)

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) reports

> Here comes ~~the bride~~ Fractal 7, with extended encryption support and improved accessibility. Server-side key backup and account recovery have been added, bringing greater security. Third-party verification has received some bug fixes and improvements. Amongst the many accessibility improvements, navigability has increased, especially in the room history. But that’s not all we’ve been up to in the past three months:
> 
> * Messages that failed to send can now be retried or discarded.
> * Messages can be reported to server admins for moderation.
> * Room details are now considered complete, with the addition of room address management, permissions, and room upgrade.
> * A new member menu appears when clicking on an avatar in the room history. It offers a quick way to do many actions related to that person, including opening a direct chat with them and moderating them.
> * Pills are clickable and allow to directly go to a room or member profile.
> 
> As usual, this release includes other improvements, fixes and new translations thanks to all our contributors, and our upstream projects.
> 
> We want to address special thanks to the translators who worked on this version. We know this is a huge undertaking and have a deep appreciation for what you’ve done. If you want to help with this effort, head over to [Damned Lies](https://l10n.gnome.org/).
> 
> It is available right now on [Flathub](https://flathub.org/apps/org.gnome.Fractal).
> 
> We are already hard at work for our next release, so if you want to give us a hand you can start by looking at our [Newcomer issues](https://gitlab.gnome.org/World/fractal/-/issues/?state=opened&label_name%5B%5D=4.%20Newcomers) or just come say hello in [our Matrix room](https://matrix.to/#/#fractal:gnome.org).
> ![](81089d6351aa86b14333b74e5e8e07bbc35f06061786319054823227392.png)

# Miscellaneous

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) announces

> [Glycin](https://gitlab.gnome.org/sophie-h/glycin) is gaining support for other programming languages. Glycin is a library that features sandboxed and extendable image loading and is used by Image Viewer. It is written in Rust and so far only provided a [Rust API](https://docs.rs/glycin/latest/glycin/). As part of my work for GNOME STF, it has now gained initial support for being used with other languages. The basis for this is [the C-API](https://sophie-h.pages.gitlab.gnome.org/glycin/c-api/). Via [GObject introspections](https://developer.gnome.org/documentation/guidelines/programming/introspection.html) it is now also usable with GJS, Python, and Vala (untested).
> 
> The advantages of Glycin over the well-proven GdkPixbuf are improved security, more reliable and dynamically adjusted memory usage limits, and reliable termination of loading processes. Currently, the drawbacks include a slightly increased overhead and missing support for anything but Linux.

# Google Summer of Code

[Pedro Sader Azevedo](https://matrix.to/#/@toluene:matrix.org) announces

> We are happy to announce that GNOME was assigned eight slots for [Google Summer of Code (GSoC)](https://summerofcode.withgoogle.com/) projects this year!
> 
> GSoC is a program focused on bringing new contributors into open source software development. A number of long term GNOME developers are former GSoC interns, making the program a very valuable entry point for new members in our community.
> 
> In 2024, we will be mentoring the following projects:
> ![](tuXkVbKtYjrHyPXSXGtvFVSD.png)

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) announces

> The GNOME Asia 2024 Call for Locations is open! If you are interested in hosting this year’s conference in your city make sure to submit an intent to bid by May 15, and a final proposal by June 6. More details about how to submit a proposal can be found here: https://foundation.gnome.org/2024/04/30/call-for-gnome-asia-2024-location-proposals/
> 
> The GUADEC 2025 Call for Locations is also open! For next year’s conference, we’re accepting bids from anywhere in the world. If you would like to bring GUADEC to your city make sure to submit an intent to bid today (May 3) and your full proposal by May 31. More details can be found here: https://foundation.gnome.org/2024/04/18/call-for-guadec-2025-location-proposals/
> 
> Registration is open for GUADEC 2024. This year’s conference takes place on July 19-24 in Denver, Colorado, USA. Let us know if you’ll be attending, remotely or in person, by registering on [guadec.org](https://events.gnome.org/event/209/). For anyone attending in person, we’ve organized a social outing to a Colorado Rockies baseball game! You can learn more and register to attend here: https://events.gnome.org/event/209/page/331-colorado-rockies-baseball-game

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
