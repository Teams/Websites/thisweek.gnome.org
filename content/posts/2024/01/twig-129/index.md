---
title: "#129 Hello 2024"
author: Felix
date: 2024-01-05
tags: ["fretboard", "cavalier", "fractal", "glib", "denaro", "parabolic", "crosswords"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 29 to January 05.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) project, a number of community members are working on infrastructure related projects. Many of us were/are on holidays so we skipped last week TWIG. Here are the highlights for the past 2 weeks.
> 
> * Dor Askayo is joining the team to continue their work on variable refresh rate (vrr) support in Mutter https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1154
> * Andy worked on some follow up from the gnome-online-account refactoring
>     - [Remove GOA from gnome-initial setup](https://gitlab.gnome.org/GNOME/gnome-initial-setup/-/merge_requests/221)
>     - [Remove the Fedora provider](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/149)
> * Andy started working and investigating http handlers https://gitlab.freedesktop.org/xdg/shared-mime-info/-/issues/199
> * Jonas sent Kernel patches for the Bluetooth stack
>     - Disconnect bluetooth devices before cutting power to the chip via rfkill: https://lore.kernel.org/linux-bluetooth/20240102181946.57288-1-verdre@v0yd.nl/
>     - Fix a mistake with scans where the wrong flags were checked and scans might not be started https://lore.kernel.org/linux-bluetooth/20240102180810.54515-1-verdre@v0yd.nl/
>     - Improve logic + hardware support for establishing connections to more than a single device concurrently https://lore.kernel.org/linux-bluetooth/20240102185933.64179-1-verdre@v0yd.nl
> * Sam is working on a modern design for Orca settings https://gitlab.gnome.org/Teams/Design/os-mockups/-/issues/240
> ![](1473fdc7848ce4244bf5948347c521b4fce430381743344636052635648.png)
> * Sam improved GNOME Shell theme and High Contrast
> * Sam fixed isues with symbolic icons and synced GTK icons https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/6686
> * Georges made a major contribution to xdg-desktop-portals documentation https://github.com/flatpak/xdg-desktop-portal/pull/1253
> * Evan started working on making GTK 4 accesibility APIs available to languange bindings https://gitlab.gnome.org/GNOME/gjs/-/issues/392
> * Adrian added bulk directory support to systemd homed https://github.com/systemd/systemd/pull/30646
> * Tobias is working on improving the GNOME Shell scrollbars design
>     - https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/7288
>     - https://gitlab.gnome.org/Teams/Design/os-mockups/-/blob/master/notifications-calendar/dynamic-scrollbars.png
> ![](d24c51e106cb0b5abce462a6d612eedd5153ae281743345083316436992.png)
> * Hub is writing documentation for the internals of Flatpak
> * Philip and Matthias hard work of porting GLib docs to gi-docgen made its way into GNOME Nightly and is now available in the SDK
> ![](6588efd17677cee9f258e3845f440bce8bd747571743344777111273472.png)
> * Tait is investiating improving accessibility modifiers for screen readers on Wayland https://github.com/xkbcommon/libxkbcommon/issues/425
> * Following our interest to improve text to speech, we are exploring ways to make it easier to distribute and discover TTS engines and are discussing libspiel with Eitan https://github.com/eeejay/libspiel

# GNOME Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Further work by Thomas Haller has landed in GLib, eliminating some global locks which serialised `g_object_ref()` and `g_object_unref()` calls across all threads. The locking is now per-object. This should speed up multithreaded programs. https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3774

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Thomas Haller’s work to fix a long-standing race in `g_object_unref()` has landed in GLib `main` (see https://gitlab.gnome.org/GNOME/glib/-/issues/3064). Let us know if you spot any new crashes or misbehaviours with object refcounting!

# Third Party Projects

[dabrain34](https://matrix.to/#/@scerveau:igalia.com) reports

> GstPipelineStudio [0.3.5](https://gitlab.freedesktop.org/dabrain34/GstPipelineStudio/-/releases/0.3.5) is here with 2024 . Happy new year GPS !
> 
> 
> Here is the changelog:
> 
>   - logs: receive multiple log sources such as GST logs and messages.
>   - settings: add a log level selection
>   - rename gst_pipeline_studio to gst-pipeline-studio
>   - can open a pipeline from the command line

[دانیال بهزادی](https://matrix.to/#/@danialbehzadi:mozilla.org) announces

> [Carburetor](https://flathub.org/apps/io.frama.tractor.carburetor) 4.4.0 released with a new OverlaySplitView to show detailed output and a set of main icons from Guardian Project's [Orbot](https://github.com/guardianproject/orbot/tree/master/app/src/main/res/drawable).
> 
> Carburetor is an app built upon Libadwaita to let you easily set up a TOR proxy on your session, without getting your hands dirty with system configs. Initially aimed at simplifying life for GNOME enthusiast on their mobiles, it’s now fully usable with mouse and/or keyboard too.
> ![](1bcdabed9522b669be98b1698acda4ab1a9caabf1741213455597174784.png)

[Krafting](https://matrix.to/#/@lanseria:matrix.org) says

> Hello everyone, last year I've released [Playlifin Voyager](https://gitlab.com/Krafting/playlifin-voyager-gtk). It's a tool to export and import playlists from and to your Jellyfin server. So you can reinstall your server and not lose your playlists if the libraries are the same. (You can also use it as a backup solution for your playlists!)
> 
> It is available on [Flathub](https://flathub.org/apps/net.krafting.PlaylifinVoyager) !
> ![](tBxLvliuPpiEDhBHbNaFvORn.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Parabolic [V2023.12.0](https://github.com/NickvisionApps/Parabolic/releases/tag/2023.12.0) is here! This release contains many bug fixes for various issues users were facing across the app :)
> 
> Here's the full changelog:
> * Fixed an issue where split chapters were not renamed correctly
> * Fixed an issue where videos would not download greater than 1080p resolution
> * Fixed an issue where preferring AV1 codec would not properly select av1 videos
> * Parabolic will now remember the chosen file type for generic downloads
> * Redesigned the interface for the Windows app
> * Parabolic no longer depends on psutil
> * Updated translations (Thanks everyone on Weblate!)
> ![](vKPYzxSsYgclLOCnUSdSNLFu.png)

### Fretboard [↗](https://github.com/bragefuglseth/fretboard)

Look up guitar chords

[Gregor Niehl](https://matrix.to/#/@gregorni:gnome.org) reports

> This week, [Fretboard](https://apps.gnome.org/Fretboard/) was accepted into the GNOME Circle, an app for looking up and visualizing guitar chords.
> Congratulations!
> ![](38cc69d619799e5051e022eba31292eb44b71ad11741987614132535296.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) reports

> It is a new year 🎆️, and what better way to celebrate this than to release Fractal 6.rc? It has been only 2 weeks since our latest beta release, but we have been hard at work during the holidays.
> 
> Here is an excerpt:
> 
> * Matrix URIs can be opened with Fractal, it is even registered as a handler for the `matrix` scheme
> * Our Join Room dialog now shows some room details as a preview upon entering an identifier or URI
> * The verification flow was rewritten to rely more on the Matrix Rust SDK, hopefully solving most issues that occurred before
> * Room members now have a profile page that allows, among other things, to kick, ban or ignore them
> * Speaking of ignoring users, the list can be managed from the account settings 
> * The dialog to view an event’s source was reworked to show more details about the event
> 
> … and a lot of other improvements, fixes and new translations thanks to all our contributors, and our upstream projects.
> 
> It is available to install via Flathub Beta, see the [instructions in our README](https://gitlab.gnome.org/GNOME/fractal#installation-instructions).
> 
> As the version implies, it should be mostly stable and we expect to only include minor improvements until the release of Fractal 6.
> 
> If one of your New Year’s resolutions is to contribute to an open source project, take your pick amongst our [issues](https://gitlab.gnome.org/GNOME/fractal/-/issues). Any help is very welcome!
> ![](559f899a22195db1c284e32044066a7362f456571742946522787479552.png)
> ![](94355d383b03230da6cc579dcb17c78bf527beee1742946430667980800.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Denaro [V2024.1.0](https://github.com/NickvisionApps/Denaro/releases/tag/2024.1.0) is here with many bug fixes to ring in the new year!
> 
> Thanks to @JoseBritto for working on the fixes for most of these issues :)
> 
> Here's the full changelog:
> * Fixed an issue where the generated ids of new transactions were incorrect
> * Fixed an issue that caused sort to behave inconsistently
> * Fixed an issue where dragging and dropping an account file was not working
> * Fixed an issue where help documentation was not showing in-app
> * Fixed an issue where some ofx files could not be imported
> * Fixed an issue where the calendar was not showing marked days after pressing the "Today" button
> * Fixed an issue where tag buttons would grow to super long sizes
> * Added more logging to help debug issues
> * Updated and added translations (Thanks to everyone on Weblate)!
> ![](geRafqtQgMNWvLeXaYbxmwPt.png)

### Crosswords [↗](https://gitlab.gnome.org/jrb/crosswords)

A crossword puzzle game and creator.

[jrb](https://matrix.to/#/@jblandford:matrix.org) says

> A new version of [Crosswords](https://gitlab.gnome.org/jrb/crosswords/) was released. This version was focused almost entirely on improvements to the crossword editor, with significant changes under the hood. The editor now features additional panels to help write cryptic clues, as well as grid statistics. Read more details in the release [blog post](https://blogs.gnome.org/jrb/2024/01/05/crosswords-0-3-12-two-toned-editor/)
> ![](dcZWPsscPzoFNrwFuLXjRXHo.png)
> ![](lHDXUjogauMVhbDktrrCmrqx.png)

### Cavalier [↗](https://flathub.org/apps/details/org.nickvision.cavalier)

Visualize audio with CAVA.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Cavalier [V2024.1.0](https://github.com/NickvisionApps/Cavalier/releases/tag/2024.1.0) is here! This release contains some new features and improved drawing algorithms :)
> 
> Let's dance our way into 2024 🕺🕺
> 
> Here's the full changelog:
> * The wave box drawing mode now draws smoother bezier curves (Thanks @OggyP)
> * Users can now specify `--fg aarrggbb` or `--bg aarrggbb` command line arguments to change the foreground and background color of the running Cavalier instance respectively
> * Updated translations (Thanks everyone on Weblate!)
> ![](BuvVcDIMSQXqaXDmWygtPynh.png)

# Google Summer of Code

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> During Google Summer of Code, Dave Patrick worked on rewriting [Bustle](https://flathub.org/apps/org.freedesktop.Bustle).
> The work consisted of:
> 
> * Switching from Haskell to Rust
> * Port to GTK 4
> * Modernize the UI & codebase
> 
> You can read about the GSoC progress at https://seadve.github.io/tags/gsoc-2023/ and the release is available on Flathub!
> ![](a8490cf58fd2f593a299eab82dc9c13a027a47401742280949099397120.png)
> ![](9ac9313e9b2d1bc4717b84eb5efc6c554b9462fa1742280944389193728.png)
> ![](c6f7ce912f4e6164e26a4774937269ab78e722821742280952773607424.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

