---
title: "#144 Better Printing"
author: Felix
date: 2024-04-19
tags: ["flatseal", "gnome-podcasts", "gaphor", "gnome-mahjongg", "gnome-maps", "libadwaita", "fractal", "gnome-software", "dialect"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 12 to April 19.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) initiative, a number of community members are working on infrastructure related projects.
> 
> Here are the highlights for the past week
> 
> We welcome Felix in the team 🎉. Felix is helping towards making [Key Rack](https://gitlab.gnome.org/sophie-h/key-rack) a viable password manager for the GNOME desktop. 
> 
> We welcome Adrien in the team 🎉. Adrien is working on [the initiative to stop using TreeView to improve accessibility](https://gitlab.gnome.org/GNOME/Initiatives/-/issues/49). 
> 
> Sophie [started C bindings for glycin](https://gitlab.gnome.org/sophie-h/glycin/-/merge_requests/68). It's possible to get a GdkTexture for an image.
> 
> Sophie opened an MR to [test bst cargo git with GNOME](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/2825)
> 
> Sophie [fixed an issue in xdg-dbus-proxy](https://github.com/flatpak/xdg-dbus-proxy/pull/57) that affected threaded DBus libraries like godbus or zbus.
> 
> Dorota is making good progress on global shortcuts portal support.
>   * [Send trigger when a key accelerator is deactivated](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3680) (Mutter, review welcome)
>   * [Draf: Add globalshortcuts editing](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2485) (GNOME Settings)
>
> ![](3169beab7d3a6ef6cf76dc3c40f3133f7a59134d1781414257846386688.png) 
>
> Joanie made tons of code cleanup in Orca - as usual - and
>   * Improved Braille support [1](https://gitlab.gnome.org/GNOME/orca/-/commit/2493af3fa) [2](https://gitlab.gnome.org/GNOME/orca/-/commit/a5e2f60c0)
>   * [Converted WebKitGTK support to be based on the web script](https://gitlab.gnome.org/GNOME/orca/-/commit/39ef597e0)
> 
> Tobias [added colorful illustrations to the GNOME Human Design Guidelines](https://gitlab.gnome.org/Teams/Websites/developer.gnome.org-hig/-/merge_requests/119)
> 
> Matt started working on [AccessKit integration in GTK](https://gitlab.gnome.org/mwcampbell/gtk/tree/accesskit#experimental-accesskit-integration) which will bring support for the new [accessibiliity architecture](https://blogs.gnome.org/a11y/2023/10/27/a-new-accessibility-architecture-for-modern-free-desktops/) and potentially a11y support on macOS and Window.
> 
> Matt prepared his talk "Modernizing Accessibility for Desktop Linux" he gave at Open Source Summit North American in Seattle. [Slides available](https://mwcampbell.us/ossna2024-slides.pdf) and the recording should be published soon, we will make sure to twig it.
> 
> Tobias [refreshed the command search/palette concept](https://gitlab.gnome.org/Teams/Design/os-mockups/-/blob/master/command-search/command-search-2024.png).
>
> ![](cebdcb0cd6afafca630c5336fdc276c842073ad21781414452269154304.png) 
>
> Sam submitted a [refreshed concept for the global shortcuts design](https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome/-/issues/47#note_2072317).
> 
> Sam [redesigned the network proxy settings](https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/2995#note_2079397).
> 
> Sam made a website for Orca, it's now live at https://orca.gnome.org/.
> 
> Antonio worked on using Nautilus as the FileChooser portal on GNOME. He's already wrote [the backofice of the portal impelementation](https://gitlab.gnome.org/GNOME/nautilus/-/commit/2ad43d705a42c9df250cdf2c73e86b9b6989fb0e).
> 
> Julian notifications portal v2 work is ready for review! https://github.com/flatpak/xdg-desktop-portal/pull/1298
> 
> Andy released GOA 3.50.1, with backported fixes for WebDAV, OAuth 2.0 URI handling & PKCE: https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/tags/3.50.1
> 
> Alice implemented buttons rows un libadwaita https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1086
> 
> Evan finished the [v4 beta of `ts-for-gir`](https://github.com/gjsify/ts-for-gir/releases/tag/v4.0.0-beta.2) - the new TypeScript bindings 
> 
> Georges landed better printing support in Flatpak / portal
>   * [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal/pull/1322)
>   * [xdg-desktop-portal-gnome](https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome/-/merge_requests/149)
>   * [xdg-desktop-portal-gtk](https://github.com/flatpak/xdg-desktop-portal-gtk/pull/474)
>
> {{< video src="4bd19bcb8d12edf15c17983052cb4f698f18e70a1781414071199858688.webm" >}} 
>
> Felix [landed tons of code improvements](https://gitlab.gnome.org/sophie-h/key-rack/-/merge_requests/13) in Key Rack
> 
> Dhanuka [released gcr 4.3.0](https://gitlab.gnome.org/GNOME/gcr/-/commit/6cefda30376e36bb90bfc274fedfd99e7e84721e)
> 
> Tom submitted a pull request to implement a [dbus service for systemd sysupdate](https://github.com/systemd/systemd/pull/32363).

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) reports

> libadwaita now has [`AdwButtonRow`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.ButtonRow.html), implementing another missing pattern for boxed lists. Additionally, there's a [`.boxed-list-separate`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/style-classes.html#boxed-lists-cards) style class now, that has each row in its own card with spacing between them, and [`AdwPreferencesGroup:separate-rows`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/property.PreferencesGroup.separate-rows.html) to toggle it for preferences groups
> ![](5bb929f127c17ad13a152fa174c89d48f6ef13cb1780028145743691776.png)

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Dawid Osuchowski added app icons in notifications from GNOME Software (https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/1964)
> ![](NiRDWBHWOIKntsZKCCdqDgKu.png)

### Maps [↗](https://wiki.gnome.org/Apps/Maps)

Maps gives you quick access to maps all across the world.

[mlundblad](https://matrix.to/#/@mlundblad:matrix.org) reports

> Maps now has dark mode variants for the default renderings (when no line color is available from a public transit provider)
> ![](hNGpevtfcUxLOegtZAQeoAKx.png)
> ![](lUjNGpikkuAXvTrnkEQJVfsG.png)

# GNOME Circle Apps and Libraries

### Podcasts [↗](https://gitlab.gnome.org/World/podcasts)

Podcast app for GNOME.

[Julian 🍃](https://matrix.to/#/@julianhofer:gnome.org) announces

> Podcasts 0.7.1 is out and can be found on [Flathub](https://flathub.org/apps/org.gnome.Podcasts). Most contributions come from nee. The highlights are: 
> * Replace add button popover with a dedicated page
> * Add streaming support
> * Add additional keyboard shortcuts
> * Several fixes and performance improvements

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) reports

> This week @danyeaw released [Gaphor](https://gaphor.org) 2.25.0. 
> 
> The biggest update is a UI refresh: Gaphor’s UI now better follows the Adwaita style (feedback is welcome). The Property editor has be revised, so it can be used both from diagrams and the model browser. You can read the full list of changes in our [ChangeLog](https://github.com/gaphor/gaphor/blob/main/CHANGELOG.md). Builds for Windows, macOS and Linux Flatpak are available from the [website](https://gaphor.org/download/) and [FlatHub](https://flathub.org/apps/org.gaphor.Gaphor).
> ![](LPJsRCUyGOxLDRyDaYjPhwAg.png)

### Dialect [↗](https://github.com/dialect-app/dialect/)

Translate between languages.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) announces

> We have merged support for the free DeepL API in Dialect. This resolves the most popular and oldest provider request in the app.
> ![](lzzVJvGiDqhcTymWRjNHEYop.png)

# Third Party Projects

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> Retro; the customizable clock widget is now available on Flathub in v2
> 
> https://flathub.org/apps/re.sonny.Retro
> 
> This new release comes with
> 
> Support both 12h and 24h clock format. It follows GNOME Date & Time preference while being sandboxed thanks to [libportal new API for the settings portal](https://github.com/flatpak/libportal/pull/143).
> 
> Energy usage has been improved by using a more efficient method to get the time and by making use of the magic [`GtkWindow.suspended` property](https://docs.gtk.org/gtk4/method.Window.is_suspended.html) to stop updating the clock when the window is not visible.
> 
> Better support for round clocks. The new GTK renderer fixed the visual glitch on transparent corners caused by large border radius. Retro now restores window dimensions and disables the border radius on maximize to make it look good, no matter the shape.
> 
> Controls have been moved to a floating header bar to stay out of the way and prevent interference with customizations.
> ![](b74840e663642b71c003f1911540d0ccfb73e6051780203460667375616.png)
> ![](2987ed385350ab6af4c74e51b57d2139f4ae66c21780203481462734848.png)

### Mahjongg [↗](https://gitlab.gnome.org/GNOME/gnome-mahjongg)

A solitaire version of the classic Eastern tile game.

[Mat](https://matrix.to/#/@mat:mathias.is) reports

> Mahjongg 3.40.1 has been released, and is available on [Flathub](https://flathub.org/apps/org.gnome.Mahjongg).
> 
> This release fixes a few regressions in the GTK 4 port. Most notably, the game no longer freezes after finishing a round, and the tile layout can be changed when using a language other than English.
> 
> I will also maintain Mahjongg from now on. If you want to help out with development, e.g. replace deprecated widgets, feel free to submit a MR on [GitLab](https://gitlab.gnome.org/GNOME/gnome-mahjongg).
> ![](LsPkVawJXpMmhFEcQjoCwMxb.png)

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) reports

> 📣 👀 Fractal 7.rc 🆕 🎉
> 
> * Account recovery, introduced during this cycle, could lead to an unclear situation where it was still incomplete even after successfully going through the process. We added some explanations on how to solve this. Thanks to anyone who tried it in the beta and provided us with feedback!
> * After fixing a focus issue upstream in GtkListView, we got rid of more focus issues in our widgets. That should make the room history completely accessible with keyboard navigation.
> * Third party verification, that happens in a direct chat, was partly broken as the banner about an ongoing verification was not showing up anymore. The culprit was found and we took that opportunity to improve the security instructions. Along with this bugfix, a coat of polish has been applied.
> 
> As usual, this release includes other improvements, fixes and new translations thanks to all our contributors, and our upstream projects.
> 
> It is available to install via Flathub Beta, see the [instructions in our README](https://gitlab.gnome.org/World/fractal#installation-instructions).
> 
> As the version implies, it should be mostly stable and we expect to only include minor improvements until the release of Fractal 7.
> 
> We always welcome [any help we can get](https://gitlab.gnome.org/World/fractal/-/issues), come ask for guidance in [our Matrix room](https://matrix.to/#/%23fractal:gnome.org).

### Flatseal [↗](https://github.com/tchx84/Flatseal)

A graphical utility to review and modify permissions of Flatpak applications.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) reports

> [Flatseal](https://flathub.org/apps/com.github.tchx84.Flatseal) 2.2.0 is out. This new release comes with refined visuals, navigation and adaptive behavior, much more streamlined with the latest GNOME release. It also comes with bug fixes for edge cases involving global overrides and Greek translation by Athanasios Karachalios.
> ![](cFAdYTqvTaXXJoEjfhxjzpaO.png)

# Documentation

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) announces

> The PyGObject Documentation now has a new place at https://pygobject.gnome.org. This URL is easier to remember (and discover). Over time we also want to bring API docs and user guides/tutorials to this site, so we have one central place for everything Python.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) says

> The GNOME API docs for Python over at https://amolenaar.pages.gitlab.gnome.org/pygobject-docs/ have had a revamp over the past week. Thanks to Rafael Mardojai CM, property names are now properly presented. Properties and signals are also cross-referenced in the docs. Method signatures have been improved, based on the latest version of the [pygobject-stubs](https://github.com/pygobject/pygobject-stubs/) project.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

