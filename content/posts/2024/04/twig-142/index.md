---
title: "#142 Portalled Nautilus"
author: Felix
date: 2024-04-05
tags: ["junction"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 29 to April 05.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) initiative, a number of community members are working on infrastructure related projects.
> 
> Here are the highlights for the last two weeks
> 
> We are thrilled to announce António is joining the team. António is a Nautilus (GNOME Files) maintainer and will work on a [FileChooser portal implementation with Nautilus](https://discourse.gnome.org/t/planning-filechooser-portal-implementation-with-nautilus/20335).
>
> ![](0986a0a3dabcb41da5823158529d99e3bc6044811776317521763237888.png) 
>
> Georges opened a draft to [support printing in WebKitGTK using the Print portal](https://github.com/WebKit/WebKit/pull/26765). This allows Epiphany / GNOME Web and other apps to support printing in their Flatpak configuration.
>
> {{< video src="cb769f2fa393c42382c0a06266c5bd1c898fd0f61776317521549328384.webm" >}} 
>
> Georges added a new feature to the Print portal, to let apps tell which file formats they support. [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal/pull/1322) and [xdg-desktop-portal-gnome](https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome/-/merge_requests/149).
> 
> Adrian finished the 1st iteration of his work to [make homed more secure when used in combination with desktop environments](https://github.com/systemd/systemd/pull/31796).
> 
> Adrian implemented homed “secure locking” [in GDM](https://gitlab.gnome.org/GNOME/gdm/-/merge_requests/251) and [GNOME Shell](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3258) in which the home dir is re-encrypted and the key is evicted from memory.
>
> {{< video src="6d1585c3bfd8fad6316f40ca21c156b45825b6fe1776317508010115072.webm" >}} 
>
> Evan released the first beta release of TypeScript bindings for GNOME; see his individual update below.
> 
> Andy improved WebDAV interoperability in GNOME Online accounts
> 
> * [goabackend: fixes for generic WebDAV providers ](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/200)
> * [goabackend: add DAV short-path for fastmail.com ](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/201)
> * [Fixes for mailbox.org DAV](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/196)
> * [Enable PKCE/S256 code challenges for remaining providers](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/199)
> 
> Sam is [investigating UI issues with Microsoft 365](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/issues/317) in GNOME Online Accounts.
> 
> Sam made [initial mockups for an OS installer](https://gitlab.gnome.org/Teams/Design/os-mockups/-/blob/master/installer/os-installer.png).
>
> ![](0ef23995c72aa73ac23361d41f9a661a082fda841776318212338614272.png)
> 
> Sam updated the [mockups for global shortcuts](https://gitlab.gnome.org/Teams/Design/os-mockups/-/blob/master/portals/global-shortcuts.png).
>
> ![](d3c07ea4c2a3736bbe868b37cd8b508319d538ec1776317494047277056.png) 
>
> Dorota got the xdg portal GlobalShortcuts working. There is still a lot of work with integration and UI but this is a great start and we are confident we can ship it in GNOME 47. See her [Mutter](https://gitlab.gnome.org/dcz/mutter/-/tree/gs_draft) and [GNOME Shell](https://gitlab.gnome.org/dcz/gnome-shell/-/commits/gs_draft) branches. She started submitting portions for reviews such as [Send trigger when a key accelerator is deactivated](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3680).
> 
> Joanie added an [`InputEventManager`](https://gitlab.gnome.org/GNOME/orca/-/merge_requests/201) to Orca to consolidate logic throughout the codebase.
> 
> Sophie investigated missing parts to get [Key Rack to feature parity with Seahorse](https://gitlab.gnome.org/sophie-h/key-rack/-/issues/7). By the way, [we are hiring a Rust + GTK developer to work on Key Rack](https://floss.social/@sonny/112196112438951909), don't hesitate to get in touch.
> 
> Sophie added support for [mirroring git repos to cargo bst plugin](https://github.com/apache/buildstream-plugins/pull/62).

# GNOME Circle Apps and Libraries

### Junction [↗](https://github.com/sonnyp/Junction)

Lets you choose the application to open files and links.

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> Junction 1.8 is [available on Flathub](https://flathub.org/apps/re.sonny.Junction).
> 
> Junction pops up automatically when you open a file or link to let you choose which app to open with.
> 
> Highlights of this version:
> 
> * Better portrait/mobile support
> * Better touch support, long press brings desktop actions
> * Fix an issue with certain encoded characters in urls
> * Use GNOME 46
> * The app is now verified on Flathub and has a "High quality app data" rating
> ![](5f16e414bced8a0fbba639a4c458fcdaaad758c21774762445848444928.png)
> ![](7b5c2c6a4ddeb7c9d83c192fdf95b0e34ca692bb1774762526106451968.png)

# Miscellaneous

[ewlsh](https://matrix.to/#/@ewlsh:gnome.org) reports

> We are thrilled to announce the first beta release of new TypeScript definitions for GNOME! These bindings combine the efforts of ts-for-gir and gi.ts into a unified project under the gjsify organization. Since we announced this effort at GUADEC 2023, JumpLink and @ewlsh:gnome.org have been working continuously to identify areas for improvement in the definitions and how best to merge these two sprawling codebases. This fusion marks a significant milestone in our journey towards enhancing the TypeScript ecosystem for GJS and GObject-based libraries. Our collaboration shows the power of community-driven development and the remarkable achievements that can be realized when we join forces towards a common objective :)
> We'd also like to thank the STF initiative for sponsoring some time to explore TypeScript in GNOME. We're excited to see what the future holds for JavaScript and TypeScript in GNOME!
> The new bindings have been published on NPM with the next tag and are ready for testing. We've tried to minimize breaking changes wherever possible and hopefully with new, advanced types the bindings "just work" 💙
> Stay tuned for documentation updates and more!

[JumpLink](https://matrix.to/#/@JumpLink:matrix.org) announces

> Initial TypeScript types [@girs/gnome-shell](https://github.com/gjsify/gnome-shell) for GNOME Shell 46 released on [NPM](https://www.npmjs.com/package/@girs/gnome-shell). Nice to see new contributions from individuals developing their own extensions and utilizing this project :)
> 
> Changelog since the first beta release: https://github.com/gjsify/gnome-shell/compare/45.0.0-beta9...main
> ![](bSSGpaqiaIMmmeJziXZmcJFn.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

