---
title: "#180 Image Editing"
author: Felix
date: 2024-12-27
tags: ["fractal", "loupe"]
categories: ["twig"]
draft: false
images:
  - posts/2024/12/twig-180/dDJkTcDTkBThpchZHPplpZaU.png
---

Update on what happened across the GNOME project in the week from December 20 to December 27.<!--more-->

# GNOME Core Apps and Libraries

### Image Viewer (Loupe) [↗](https://apps.gnome.org/app/org.gnome.Loupe/)

Browse through images and inspect their metadata.

[Sophie 🏳️‍🌈 🏳️‍⚧️ (she/her)](https://matrix.to/#/@sophieherold:gnome.org) announces

> Image editing including crop, rotate, and flip has been [merged](https://gitlab.gnome.org/GNOME/loupe/-/merge_requests/462) into Loupe. Currently, only PNG images are supported. Support for JPEGs is [in the works](https://gitlab.gnome.org/GNOME/glycin/-/issues/104). There is a list of [open issues](https://gitlab.gnome.org/GNOME/loupe/-/issues/409) for the current implementation. You can support my work financially on [several platforms](https://blogs.gnome.org/sophieh/projects/).
> ![](622e6f73d2e656877291fdbd63beed384e4305701872608820698546176.png)
> ![](1936e882b0b65c8af9ef25470c0fe966cfaad1e51872608824242733056.png)

# Third Party Projects

[Sebastian Wiesner](https://matrix.to/#/@swsnr:matrix.org) says

> A new release of TurnOn is available.  The new release adds network scanning: When scanning is on TurnOn now finds devices in the local network and offers to save them so that it can later turn them on.
> 
> [TurnOn](https://github.com/swsnr/turnon) is a small GNOME utility to turn on remote systems such as NAS devices with Wake On LAN. It's available from [Flathub](https://flathub.org/apps/de.swsnr.turnon).
> ![](YRWcmAbCytbwYHuNQiEdNceC.png)

[Vladimir Kosolapov](https://matrix.to/#/@vmkspv:matrix.org) says

> [Netsleuth](https://github.com/vmkspv/netsleuth) 1.1.0 is here! This update introduces a search provider for GNOME Shell, command-line interface and new calculable values.
> 
> The calculation history now stores up to 20 entries, allowing for quick retrieval of specific IP addresses directly from the GNOME Shell search.
> 
> For those who prefer a console experience, the command-line interface enables performing the same calculations as in the graphical version.
> 
> This update also introduces two new calculable values — "IPv4 Mapped Address" and "6to4 Prefix".
> 
> Check it out on [Flathub](https://flathub.org/apps/io.github.vmkspv.netsleuth)!
> ![](xwdoFgnYTIyjjBuBhgzavkNr.png)

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) reports

> 🎶 _Vive le vent ! Vive le vent ! Vive le vent d’hiver !_ 🌲 And _vive_ Fractal 10.beta! While everyone is resting for the holidays, we thought you could use a new release. It focuses on improvements and bug fixes, including:
> 
> * Videos were often not playing after loading in the room history, this was fixed, and we also show properly when an error occurred.
> * Computing the size of media messages was slightly wrong, which meant that sometimes a grey line would appear below them. We rebooted the code and that problem is gone!
> * Our CSS file was a bit too big for our taste, so we decided to make use of SASS and split it.
> * We were downloading too many different sizes for avatar images, which would fill the media cache needlessly. We now only download a couple of sizes. This has the extra benefit of fixing blurry or missing thumbnails in notifications.
> 
> As usual, this release includes other improvements, fixes and new translations thanks to all our contributors, and our upstream projects.
> 
> It is available to install via Flathub Beta, see the [instructions in our README](https://gitlab.gnome.org/World/fractal#installation-instructions).
> 
> As the version implies, there might be a slight risk of regressions, but it should be mostly stable. If all goes well the next step is the release candidate!
> 
> If you have a little bit of time on your hands, you can try to fix one of our [newcomers issues](https://gitlab.gnome.org/World/fractal/-/issues/?label_name%5B%5D=4.%20Newcomers). Anyone can make Fractal better!

# GNOME Websites

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> Thanks to Brage Fuglseth (he/him), thisweek.gnome.org now supports the [OpenGraph protocol](https://gitlab.gnome.org/Teams/Websites/thisweek.gnome.org/-/merge_requests/126), which means you can now get rich social media preview cards on sites that support it (e.g. Mastodon, Discourse, Reddit).
> ![](dDJkTcDTkBThpchZHPplpZaU.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
