---
title: "#179 Reduced Memory Usage"
author: Felix
date: 2024-12-20
tags: ["tuba", "gnome-software", "parabolic", "gnome-calendar", "libadwaita"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 13 to December 20.<!--more-->

# GNOME Core Apps and Libraries

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) says

> Carlos Garnacho implemented a clever optimisation in the LocalSearch filesystem indexer, which reduces memory usage when crawling large folders. See [the MR](https://gitlab.gnome.org/GNOME/localsearch/-/merge_requests/575) for details and a before and after comparison.

[Khalid Abu Shawarib](https://matrix.to/#/@kabushawarib:matrix.org) reports

> [User Sharing](https://gitlab.gnome.org/GNOME/gnome-user-share), is a small package that binds together various free software projects to bring easy to use, user-level file sharing to the
> masses. Earlier this week, a [merge request](https://gitlab.gnome.org/GNOME/gnome-user-share/-/merge_requests/29) has landed that ports all of the application code over from C to Rust! The file sharing service still retains the same functionality.
> 
> See [this post](https://discourse.gnome.org/t/user-sharing-ported-over-to-rust/25682?u=kabushawarib) for more details.

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) says

> libadwaita now has Adaptive Preview in GTK inspector, a way to test layouts on smaller screen sizes - similar to responsive layout mode in Firefox inspector, device toolbar in Chrome etc. See https://blogs.gnome.org/alicem/2024/12/19/mobile-testing-in-libadwaita/ for more details
> ![](3d1c1d6fc60dd4daa712fc224241d7809f6a9b971869739333863342080.png)

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Adrien Plazas](https://matrix.to/#/@adrien.plazas:gnome.org) announces

> Software received a brand new [systemd-sysupdate](https://www.freedesktop.org/software/systemd/man/latest/systemd-sysupdate.html) plugin. That was one of the last pieces needed to complete the migration of GNOME OS from OSTree to systemd-sysupdate. Read more in this [short blog post](https://adrienplazas.com/blog/2024/12/20/a-systemd-sysupdate-plugin-for-gnome-software.html).

### Calendar [↗](https://gitlab.gnome.org/GNOME/gnome-calendar/)

A simple calendar application.

[Titouan Real](https://matrix.to/#/@titouan.real:matrix.org) says

> GNOME Calendar, your favorite calendaring application, got a much improved event editor for GNOME 48!
> * **Adaptive Calendar Selector:** The calendar selector dropdown has been updated to use a standard widget, fixing multiple issues along the way;
> * **Better Date and Time Entries:** entries feature better keyboard navigation and better parsing;
> * **Modernized Layout:** The new layout is more compact, easier to read, and is more in line with modern design patterns.
> 
> Support for timezone editing is also in the testing phase in [this merge request](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/516). If you would like to help, you can grab the Flatpak artifacts (or build yourself using GNOME Builder), and report any bug you may find. We recommend only using a throwaway calendar for testing.
> ![](OYlFlRidYOPZLEpLgqpFYZCr.png)

# GNOME Circle Apps and Libraries



### Tuba [↗](https://github.com/GeopJr/Tuba)

Browse the Fediverse.

[Evangelos "GeopJr" Paterakis](https://matrix.to/#/@geopjr:gnome.org) announces

> [Tuba](https://flathub.org/apps/dev.geopjr.Tuba) 0.9 is now available, with [many new features and bug fixes](https://github.com/GeopJr/Tuba/releases/tag/v0.9.0)!
> 
> ✨ Highlights:
> * Audio Visualizer
> * Accent Colors and other GNOME 47 changes
> * Focus Picker
> * Account Suggestions for new accounts
> * Scheduled & Draft Posts
> * Full emoji reaction support for supported backends
> * FediWrapped
> * Viewing which ones of the people you follow also follow an account
> * Placing favorite lists to the sidebar
> * Poll animations, refresh and show results button
> * Windows installer
> * And [much more](https://floss.social/@Tuba/113657758389560981)!
> 
> Happy Holidays! ❄️
> ![](91d71c9a9978246d4c8cc9b97fdf0f36dbb907431869776860653551616.gif)
> ![](7e6dfd90bb4a893309e818da591fc44447cebddc1869776829728948224.gif)

# Third Party Projects

[دانیال بهزادی](https://matrix.to/#/@danialbehzadi:mozilla.org) announces

> We’re excited to announce the release of [Carburetor](https://flathub.org/apps/io.frama.tractor.carburetor) version 5.0.0, our user-friendly tool for setting up a TOR proxy effortlessly! This release comes with a special significance as we celebrate the longest night of the year, known as [Yalda](https://en.wikipedia.org/wiki/Yald%C4%81_Night) in Persian culture. Yalda symbolizes the endurance of darkness, reminding us that even the longest nights eventually give way to dawn. This resonates deeply with those living under tyranny, as we hold onto hope for a brighter future and the eventual liberation from oppression.
> 
> In a momentous turn of events, we also extend our heartfelt congratulations to the brave Syrians around the world on the departure of Bashar Al-Asad, a symbol of tyranny. We hope this new chapter brings them closer to freedom and access to a truly open Internet. With Carburetor, we aim to empower users to prioritize their privacy and security, ensuring that the tool is used for the right reasons—helping others and safeguarding personal information, rather than as a means of mandatory connectivity.
> 
> Happy Yalda and cheers to a brighter future! 🌟
> ![](f59d9069cfd8300db820dc7e59c9db2bd71c636f1868165179196309504.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Parabolic [V2024.12.0](https://github.com/NickvisionApps/Parabolic/releases/tag/2024.12.0) is here! This update contains a redesigned Qt application, new features and various bug fixes.
> 
> Here's the full changelog:
> * Added the ability to toggle the inclusion of a media's id in its title when validated in the app's settings
> * Added the option to export a download's media description to a separate file
> * Restored the ability for Parabolic to accept a URL to validate via command line arguments
> * Fixed an issue where auto-generated subtitles were not being embed in a media file
> * Fixed an issue where downloading media at certain time frames were not respected
> * Fixed an issue where video medias' thumbnails were also cropped when crop audio thumbnails was enabled
> * Fixed an issue where the previously used download quality was not remembered
> * Redesigned the Qt version's user interface with a more modern style
> * Updated yt-dlp to 2024.12.13
> ![](KcTRZymwQxMiEAdnOCkObxSY.png)

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Parabolic [V2024.12.1](https://github.com/NickvisionApps/Parabolic/releases/tag/2024.12.1) was also released this week! It contains various bug fixes for issues users were experiencing.
> 
> Here's the full changelog:
> * Fixed an issue where generic video downloads would sometimes incorrectly convert to another file type
> * Fixed an issue where subtitles were not downloaded properly
> * Fixed an issue where desktop notifications were not being displayed

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
