---
title: "#177 Scrolling Performance"
author: Felix
date: 2024-12-06
tags: ["gtk", "nautilus", "pipeline"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 29 to December 06.<!--more-->

# GNOME Core Apps and Libraries

### Files [↗](https://gitlab.gnome.org/GNOME/nautilus)

Providing a simple and integrated way of managing your files and browsing your file system.

[Peter Eisenmann](https://matrix.to/#/@p3732:matrix.org) reports

> Khalid Abu Shawarib greatly improved Files' scrolling performance in folders with many thumbnails. The changes resulted in an approximate 10x increase of FPS on tested machines. For details see https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1659

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Jeremy Bicha](https://matrix.to/#/@jbicha:ubuntu.com) says

> GTK's Emoji Chooser has been updated for [Unicode 16](https://blog.emojipedia.org/whats-new-in-unicode-16-0/) . This is included in the new GTK 4.17.1 development release and will also be in 4.16.8.

# Third Party Projects


### Pipeline [↗](https://flathub.org/apps/de.schmidhuberj.tubefeeder)

Follow your favorite video creators.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) reports

> Pipeline version 2.1.0 was released. This release brings some major UI improvements to the channel page and video page, which were mostly implemented by lo. There are also many fixes included in this release, for example very long channel names and video title not allowing the window to shrink to narrow displays, or fixing a bug where the watch-later list was scrolled to the bottom at startup. Compared to the last TWIG announcement, there were also three minor releases fixing many more bugs, like bad video player performance on some devices or errors migrating from the old versions of the application.
> ![](XRRjTerXtXjiqTGdYHFRQOCy.png)

# Miscellaneous

[Sophie 🏳️‍🌈 🏳️‍⚧️ (she/her)](https://matrix.to/#/@sophieherold:gnome.org) reports

> Image Viewer (Loupe) landed in GNOME 45 with its own image loading library, [glycin](https://gitlab.gnome.org/sophie-h/glycin). The reason was that the previously used library GdkPixbuf did not fulfill the security and feature requirements we have for an image loader today.
> 
> In parallel to the [ongoing work on glycin and Loupe](https://blogs.gnome.org/sophieh/category/gnome/image-viewing/glycin/), there have been thoughts on introducing glycin to the rest of GNOME. There are more details available in a new [*Move away from GdkPixbuf*](https://gitlab.gnome.org/GNOME/Initiatives/-/issues/53) GNOME initiative. Contributions and feedback are very welcome.
> 
> You can also [support my work](https://blogs.gnome.org/sophieh/projects/) on glycin and Loupe financially.

# GNOME Foundation

[ramcq](https://matrix.to/#/@ramcq:matrix.org) announces

> The GNOME Foundation is pleased to announce its Request for Proposals for contractors to complete the Digital Wellbeing / Parental Controls and Flathub Payments projects funded by Endless. Please see the GNOME [Desktop-Wide Web/Network Filtering](https://discourse.gnome.org/t/request-for-proposals-gnome-desktop-wide-web-network-filtering-solution/25483) and Flathub [Program Management](https://discourse.flathub.org/t/request-for-proposals-flathub-program-management/8276) posts on the project Discourse forums for the full RFQ details, where you can also ask any questions you have for the project teams. Both roles are open for applications until Wednesday December 18th and we look forward to discussing the projects with prospective applicants and reviewing your proposals.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
