---
title: "#178 Fuzz Testing"
author: Felix
date: 2024-12-13
tags: ["gnome-maps", "tinysparql", "gjs"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from December 06 to December 13.<!--more-->

# GNOME Core Apps and Libraries

### TinySPARQL [↗](https://gitlab.gnome.org/GNOME/tinysparql/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) reports

> [TinySPARQL](http://tinysparql.org/) is the database library that powers GNOME's desktop search. Thanks to work by Carlos Garnacho, it's now enrolled for fuzz testing in the OSS Fuzz project. You can see the current set of fuzz tests [here](https://gitlab.gnome.org/GNOME/tinysparql/-/tree/main/fuzzing).

### Maps [↗](https://apps.gnome.org/Maps)

Maps gives you quick access to maps all across the world.

[mlundblad](https://matrix.to/#/@mlundblad:matrix.org) reports

> Maps now uses AdwSpinner widgets, and the icon for adding a place as a favorite has an animation effect when toggling on and off
> {{< video src="FIaTityCVFwbrgxOceZVMKGw.mp4" >}}

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> In GJS we landed a patch set from Marco Trevisan that gives a big performance boost to accessing GObject properties, like `button.iconName` or `label.useMarkup`. Soon, expect further improvements speeding this up even more!

# GNOME Development Tools

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) announces

> [gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta/) now has
> automatic ref updates, thanks to Jordan Petridis and Abderrahim Kitouni. The updates were previously
> done manually by release team members. This means that continuous integration
> of GNOME modules happens more efficiently than ever.
> 
> An "update refs" merge request is generated [twice daily](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/pipeline_schedules),
> by the gnome-build-meta-bot. The CI tests that this builds across 3 architectures,
> and then one of the gnome-build-meta maintainers simply lets Marge Bot land the
> update.
> 
> We depend on module developers to help keep GNOME building. If you make changes
> in a module that could break the build, such as adding dependencies or changing how the
> build system works, please [check the updates](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests?scope=all&state=opened&author_username=gnome-build-meta-bot),
> and help the release team to fix any issues that come up. Thanks!

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> Maintainers of GNOME modules now have access to a new service, integrated to CI pipelines, for releasing new versions of their projects, courtesy of Stefan Peknik and the Infrastructure team. Instead of creating release archives locally, uploading them to a server with scp, and running a script, we now have a service that takes the release archives built on the GNOME infrastructure using CI. The old system is going to be retired by the end of the week, so make sure to update your CI pipeline before the deadline for the GNOME 48.alpha release on January 4, 2025. More details are available on [Discourse](https://discourse.gnome.org/t/releasing-gnome-modules/25566).

# Third Party Projects

[slaclau](https://matrix.to/#/@slaclau:thedrummond.duckdns.org) says

> I’ve been working on some reusable calendar widgets based on Gnome Calendar and have put them together as a library. I’ve tagged an initial release as 0.1.0 (https://github.com/slaclau/gtkcal/releases/tag/0.1.0) so I can use them in another project but there are no published binaries (yet). The current working name is GtkCal.

[Jan-Michael Brummer](https://matrix.to/#/@jbrummer:matrix.org) reports

> Saldo 0.8.0 has been released. Saldo, an easy way to access your online banking via FinTS, has now an improved user interface and offers support for Quick Unlock and fingerprint unlocking. In addiion the official bank list has been updated alongside bug fixes.
> ![](NyzxQiLuWAHxkrIPYqufkQDw.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
