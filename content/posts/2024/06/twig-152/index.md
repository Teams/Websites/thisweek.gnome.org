---
title: "#152 Bottom Sheets"
author: Felix
date: 2024-06-14
tags: ["libadwaita", "railway"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from June 07 to June 14.<!--more-->

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) announces

> [`AdwBottomSheet`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.BottomSheet.html) is now public, allowing apps to use bottom sheets outside dialogs. It also has an optional bottom bar, which may be useful for music player apps.
> {{< video src="32c286bf53b5bf2edc85b92cb39fcd7f4b6fa6bd1801318200277729280.webm" >}}

# GNOME Incubating Apps

[Pablo Correa Gomez](https://matrix.to/#/@pabloyoyoista:matrix.org) announces

> This week Papers, the Incubator app for viewing documents, had its first release published on [Flathub](https://flathub.org/apps/org.gnome.Papers)! Now you can download a stable version instead of using GNOME nightly. We are hoping to get more testing and feedback from the community!
> 
> In the meanwhile, development continues at a high pace! Qiu Wenbo has continued porting many widgets to libadwaita, UI templates, and Rust, I have focused on cleanups, and as part of GSoC Markus Göllnitz has continued improving the UI! The top headerbar is now split in two, and search happens over the sidebar. Not only allows this to fit the UI in a phone size, but also provides better modularity, making the application more maintainable.
> ![](ZAUbqVgiRKeBAEWCGMvuBwJo.png)
> ![](YFhEOwVHZYCLwucnQDnFRNIN.png)

# GNOME Circle Apps and Libraries

### Railway [↗](https://gitlab.com/schmiddi-on-mobile/railway)

Travel with all your train information in one place.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) says

> Railway version 2.6.0 was now published. This release refactors a lot of the backend of Railway, such that we can now support many more providers. As a start, Railway now supports SBB and the [Transitous](https://transitous.org/) project. Adding new providers should be an easy task if one is proficient with the Rust programming language and the provider has an easy API one can use. To help anybody to contribute their favourite provider, in the case it is not yet implemented, we have extensive documentation on [how to write such an implementation](https://gitlab.com/schmiddi-on-mobile/railway-backend/-/blob/main/docs/writing-a-provider.md?ref_type=heads). In case you want to help out, feel free to [write us a message](https://matrix.to/#/#railwayapp:matrix.org). Even though this was the biggest change with this release, it was not the only one. We improved the user experience in some parts of the application, for example you will now be informed about the option to delete old journeys to clean out these bookmarks automatically. We also made the application more accessible, with support for screen readers for searching for stations.

# Third Party Projects

[DaKnig](https://matrix.to/#/@daknig:matrix.org) says

> [DewDuct v0.2.3](https://github.com/DaKnig/DewDuct/releases/tag/v0.2.3) is released! currently working on behind the scenes things, so not many exciting news besides some bugs squashed and some performance improved ~
> 
> Please, if you have any ideas, contact me on Matrix! (Daknig on Matrix dot org). Even if you have no prior experience in GTK and are just meddling in Rust, I promise to help!
> 
> The situation with invidious being blocked is going to affect DewDuct, the future is uncertain; But, for now, enjoy the new released and see your bugs squashed!
> 
> The release would be available on alpine shortly after this post goes live!

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> I have released [gir-parser](https://crates.io/crates/gir-parser), a complete parser of the GObject Introspection GIR files written in Rust. It can be used as a base for writing a documentation/bindings generator.

# GNOME Websites

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> GNOME's Discourse web forum now publishes topics for various blogs using their RSS feed, like:
> 
> * the Foundation's news blog
> * the GTK developers blog
> * the GNOME Shell developers blog
> * This Week in GNOME (hi!)
> 
> and more will follow in the future. You can use Discourse for comments, feedback, and discussions.

# GNOME Foundation

[Michael Downey](https://matrix.to/#/@michael:downey.net) announces

> Along with our friends at KDE e.V. and Eclipse Foundation, the GNOME Foundation was recently invited to become an Associate Member of the new [Matrix.org Foundation](https://matrix.org/about/) to help provide feedback on making the platform more useful for communities like GNOME to collaborate on free software development.
> 
> Strengthening partnerships between free software communities like ours is an important aspect of project sustainability. We remain grateful to the Matrix.org Foundation for providing the [infrastructure used for GNOME's rooms and Foundation member accounts](https://handbook.gnome.org/get-in-touch/matrix.html).
> 
> As part of their recent Governing Board launch, I'm pleased to share that [Cleo Menezes Jr.](https://gitlab.gnome.org/CleoMenezesJr) has been appointed to be GNOME's first delegate to this group. Cleo will help advocate for GNOME in using and helping others use Matrix, so if you have any feedback or questions, [get in touch](https://matrix.to/#/@cleomenezesjr:matrix.org).

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) announces

> The call for GUADEC 2024 BoFs and Workshops is open for one more day. If you would like to host an in-person or remote session make sure to submit your proposal by tomorrow (June 15): https://foundation.gnome.org/2024/06/04/guadec-2024-call-for-bofs-and-workshops/
> 
> Quick reminder to also register for GUADEC! This year's conference runs from July 19-24, let us know if you're attending in person or remotely by signing up here: https://events.gnome.org/event/209/registrations/
> 
> For anyone attending in person in Denver, we're planning a number of social events so make sure to check the website for more details. We'll be adding details for a pre-registration party on July 19 and options for a social activity on July 24 soon, but you can register for our July 22 family-friendly baseball game (Colorado Rockies vs Boston Red Sox) right now: https://events.gnome.org/event/209/page/331-colorado-rockies-baseball-game

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

