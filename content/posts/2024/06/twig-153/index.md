---
title: "#153 Proudly Colorful"
author: Felix
date: 2024-06-21
tags: ["loupe", "graphs", "workbench", "libadwaita"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from June 14 to June 21.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure) initiative, a number of community members are working on infrastructure related projects.
> 
> Here are the highlights for the past couple of weeks
> 
> [Completed and landed](https://gitlab.gnome.org/GNOME/baobab/-/merge_requests/74) porting Baobab away from GtkTreeView.
> 
> ### Key Rack
> 
> [Key Rack](https://gitlab.gnome.org/sophie-h/key-rack) is a new application to manage desktop secrets and aims to replace [Sea Horse](https://gitlab.gnome.org/GNOME/seahorse).
> 
> Implement [localization support](https://gitlab.gnome.org/sophie-h/key-rack/-/merge_requests/18)
> 
> Add [CI support](https://gitlab.gnome.org/sophie-h/key-rack/-/merge_requests/19)
> 
> Passwords [can now be added](https://gitlab.gnome.org/sophie-h/key-rack/-/merge_requests/20) to a keyring. Thanks to the zxcvbn library, the password strength is displayed directly and guidance is provided on how the password can be improved.
> 
> Both the [keys and the keyrings/apps can now](https://gitlab.gnome.org/sophie-h/key-rack/-/merge_requests/21) be searched.
> 
> [Added Flatpak File Monitor](https://gitlab.gnome.org/sophie-h/key-rack/-/merge_requests/16), which detects changes and automatically updates the views.
> 
> {{< video src="2fb9e17d60852235a8c056986825d4dfacbc42631804240061055631360.webm" >}}
> 
> ### Setup
> 
> Setup is a new OEM style Installer for image based operating systems.
> 
> Fully implemented the EULA page. The License now comes from a file the distro controls. If the file is missing, the page is skipped. It can render markup now! So the EULA can have a bit of (limited) formatting.
> 
> Started work on the disk selection pages
> 
> * Detect & warn about Intel Rapid Storage technology
> * Mocked-up the various possible behaviors of the disk selection pages, and still iterating on it with Sam
> * Started working on reading the data from udisks, instead of the mocked-up data. Not hooked into UI yet
> 
> Here is a video demo showcasing
> 
> * language page from last week
> * EULA page
> * current state of disk selection flows
> 
> {{< video src="ee076f1470ecba9d41eee0fbf5cbf799776059ee1804239804360032256.webm" >}}
> 
> ### Global Shortcuts
> 
> The [Global Shortcuts](https://github.com/flatpak/xdg-desktop-portal/pull/711) portal is a standard API allowing applications to register global shortcuts. These shortcuts can be used by apps to react to the user input, regardless of the input focus state. Besides the obvious advantages to support it in GNOME, its implementation is a prelimary step towards better screen reader support on Wayland with the [Accessibility Shortcuts Portal](https://github.com/flatpak/xdg-desktop-portal/issues/1046)
> 
> Continued asjusting to feedback for GlobalShorcuts in [Settings](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2485) and [Shell](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3343)
> 
> Added GlobalShortcuts portal to [ashpd demo](https://gitlab.gnome.org/dcz/ashpd/-/commits/master).
> 
> Video of Global Shortcuts in ashpd demo
> 
> {{< video src="402285f7679e61e457ffb394fa48b6b2abb50ce21804240890596687872.webm" >}}
> 
> ### Orca
> 
> Orca is the screen reader of the Linux desktop.
> 
> Created a document to help application developers make their application more accessible to screen reader users see [
> Tips for Application Developers](https://gitlab.gnome.org/GNOME/orca/-/blob/main/README-APPLICATION-DEVELOPERS.md).
> 
> Improve presentation of [static text exposed via description](https://gitlab.gnome.org/GNOME/orca/-/issues/508)
> 
> [Completed](https://gitlab.gnome.org/GNOME/orca/-/issues/496) eliminating formatting strings for presentation generation.
> 
> * Branch for user testing: Eliminate use of formatting strings for braille: https://gitlab.gnome.org/GNOME/orca/-/commit/088e39d0
> * Braille is the last thing still using formatting strings, so work in this area will soon be complete
> 
> [Make ARIA posinset and setsize global](https://gitlab.gnome.org/GNOME/orca/-/issues/517)
> 
> * Make posinset and setsize attribute retrieval global: https://gitlab.gnome.org/GNOME/orca/-/commit/eb0b080da
> * This change makes it possible for the GTK developers to fix https://gitlab.gnome.org/GNOME/gtk/-/issues/6765 without having to expose all objects in the accessibility tree
> 
> ### oo7
> 
> oo7 is a new secret service provider and aims to replace gnome-keyring and libsecret.
> 
> [Completed implemention of the prompt](https://github.com/bilelmoussaoui/oo7/pull/73/commits/ce32e3233c8c13a039895d898d33ded7958faafd), oo7-daemon is now able to access the GNOME Shell based prompt to complete org.freedesktop.Secret.Prompt calls
> 
> Video demonstrating oo7 triggering the GNOME Shell prompt to unlock the keyring:
> 
> {{< video src="dbb1e4c41306542c45b34a5457697c4b593a2a2e1804239766808428544.webm" >}}
> 
> ### Newton
> 
> Newton is the new Wayland native protocol for accessibility on the Linux desktop.
> 
> Published a blog post and demo [Update on Newton, the Wayland-native accessibility project](https://blogs.gnome.org/a11y/2024/06/18/update-on-newton-the-wayland-native-accessibility-project/)
> 
> Finished prototype of key grabbing implementation for Orca on Wayland, including support for arbitrary modifiers including Caps Lock. Had to insert key grabbing at a different stage of Mutter event handling to get it right.
> 
> GTK: Implemented support for the most important AccessKit actions, including the ability for ATs to change the caret position or text selection, which is important for the Say All command. Also debugged some crashes. https://gitlab.gnome.org/mwcampbell/gtk/tree/accesskit
> 
> Newton AT client libraries consolidated into a single [repository](https://gitlab.gnome.org/mwcampbell/newton_consumer)
> 
> ### Nautilus File Chooser
> 
> Nautilus is the GNOME file manager. We are working on using it as the [file chooser for the GNOME desktop](https://gitlab.gnome.org/GNOME/nautilus/-/issues/3401).
> 
> 4 of the preparatory merge requests landed
> 
> * [Part 1](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1502)
> * [Part 2 - Keyboard & Pad Shortcuts](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1526)
> * [Part 3 - Toolbar Turbo](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1527)
> * [Part 4 - Sidebar Shuffle](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1530)
> 
> 3 more requests were opened
> 
> * [Part 5 - Go Starred or Go Home](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1532)
> * [Part 6 - Copy and Pasta🤌](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1541)
> * [Part 7 - Deadly Modes](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/1542)
> 
> ### Glycin
> 
> A new library for sandboxed and extandable image loading
> 
> [Fixes and docs](https://gitlab.gnome.org/sophie-h/glycin/-/merge_requests/86) for splitting C bindings into libglycin and libglycin-gtk4.
> 
> [Added](https://gitlab.gnome.org/sophie-h/glycin/-/merge_requests/92) structure for having "editors" as addition to "loaders" for image formats for image editing in glycin. As a first example, JPEG now supports changing the image orientation if a respective Exif tag is present.
> 
> ### libadwaita
> 
> The platform library of GNOME
> 
> [Generate standalone colors from bg colors](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1152)
> 
> [Finished AdwBottomSheet](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1154) and made it public
> 
> [Support nested slots](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1159) in AdwMultiLayoutView
> 
> ### GNOME Shell
> 
> Created a GNOME Shell extension to get feedback about changing notification banner expension.
> 
> * Issue: https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/7713
> * Extension: https://gitlab.gnome.org/jsparber/prototype-for-notification-banners-behavior


# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) says

> After a long wait, we have customizable accent colors!
> 
> If you maintain an app using libadwaita (and don't override its accent color), make sure you don't use hardcoded blue color where system accent color would make sense, or vice versa. (see [docs](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/styles-and-appearance.html#accent-color))
> 
> Libadwaita apps will also pick up accent colors when running in environments such as elementary OS or KDE.
> {{< video src="2eb87b3acd59ece19abd11f55756e97b4900fcb01803818450380914688.mp4" >}}

### Image Viewer (Loupe) [↗](https://apps.gnome.org/app/org.gnome.Loupe/)

Browse through images and inspect their metadata.

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) reports

> Loupe made its first small step towards image editing. Rotating JPEG images that contain an Exiv entry for orientation, the change of orientation is now saved within the image.
> 
> This is made possible by [glycin](https://gitlab.gnome.org/sophie-h/glycin) now supporting changes on images in addition to loading images. In contrast to some other image viewers, Loupe will not rewrite the image or even re-encode it with potential quality loss. Instead, it is just changing the one byte inside the file that's responsible for determining the image orientation.
> 
> Support for more formats and more editing features will hopefully follow over time. If you want to support my work you can join [my Patreon](https://www.patreon.com/sophieh) and get weekly updates.

### Baobab

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) announces

> This week Disk Usage Analyzer (AKA baobab) landed multiple PRs that refresh the UI. Disk Usage Analyzer now uses GtkColumnView for its file tree, uses a new pathbar look inspired by Nautilus, and uses symbolic icons for the Devices & Locations page. We also got rid of some usage of deprecated APIs like GtkStyleContext where possible.
> ![](488821ae9d20698a59a54f9df0c8f5a5d8e962dc1804252011185569792.png)
> ![](23d2a3c8638a6d901ddb9d108991d65557abc6ef1804251962875576320.png)

# GNOME Circle Apps and Libraries

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> Auto suggest/complete landed in Workbench. The feature can be used to discover widgets, properties and write UI faster. You can try it out now in [Workbench nightly](https://blog.sonny.re/workbench-news), testers and feedback welcome. Kudos to James Westman for writing the language server making this possible.
> {{< video src="b4f446fa7d1428f0982f2fbb6066bd86c59601e01802833916097200128.mp4" >}}

### Graphs [↗](https://graphs.sjoerd.se)

Plot and manipulate data

[Sjoerd Stendahl](https://matrix.to/#/@sjoerdb93:matrix.org) announces

> This week we pushed a minor update to Graphs, tackling a bunch of persistent bugs in the current stable release. The most exciting difference in this release is probably a major increase in start-up time, resulting mostly from the way we bundle style-previews now with the build. This change reduces total start-up time of Graphs by 1.5 seconds on my machine, reducing the `do_activate` stage from 1.8s to 0.3s.
> 
> Apart from some minor under-the-hood improvements and other bug fixes, the most likely changes to be noticed are:
> 
> * Updates to translations, including added support for Hungarian
> * Further fixes and improvements to the regex logic in the equation generation. Now nested parentheses are handled correctly, an equation like `sin(x+2(pi-x)²)+2` is now handled properly for example.
> * The minimum vertical height of the main window has been decreased significantly.
> * Delimiters are now handled correctly when importing data, even if an arbritrary amount of whitespace is involved.
> * Fixed an issue where the `New Project` dialog would not activate
> 
> As always, the latest release can be retrieved from [Flathub](https://flathub.org/apps/se.sjoerd.Graphs). Credits for the original graph in the screenshot go to [xkcd](https://xkcd.com/963/).
> ![](OZjcGfqjIhSqsWTVFzWPzLlR.png)

# Third Party Projects

[qwertzuiopy](https://matrix.to/#/@qwertzuiopy:matrix.org) announces

> This week I released my fist app, Libellus!
> Libellus is a small wiki application allowing you to browse through DnD without being distracted by ads or having to open a browser. It supports most of DnD including Classes, Races, Monsters Spells and Items.
> It is available on [Flathub](https://flathub.org/apps/de.hummdudel.Libellus).
> ![](tikXmFKDaDPVmXhOWLZWSGxm.png)

[xjuan](https://matrix.to/#/@xjuan:gnome.org) reports

> New Cambalache development release 0.91.1 is out!
> 
> Key features:
>   - Port to Adwaita
>   - Custom wayland compositor widget
> 
> Read more at https://blogs.gnome.org/xjuan/2024/06/21/new-cambalache-development-release-0-91-1/
> 
> Cambalache is a tool to create UI for Gtk 4/3
> ![](0ff87a0bec1a0245dd4c98b14c504447164956d61804176790957064192.png)

# Documentation

[Dan Yeaw](https://matrix.to/#/@danyeaw:gnome.org) announces

> PyGObject, the Python bindings for using GNOME platform libraries in your applications, received a big improvement to the documentation by adding tutorials. The source of them came from centralizing the magnificent work in the PyGObject-Guide by Rafael Mardojai CM, which was based on the PyGObject-Tutorial by Sebastian Pölsterl. Thanks to all the contributors to both of these projects and a community effort to relicense them under the LGPL. Check out the tutorials at https://pygobject.gnome.org/tutorials/.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
