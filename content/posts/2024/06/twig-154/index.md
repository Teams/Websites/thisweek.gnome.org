---
title: "#154 Pride Day!"
author: Felix
date: 2024-06-28
tags: ["tuba", "apostrophe", "libadwaita", "dejadup", "gnome-shell"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from June 21 to June 28.<!--more-->

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) reports

> as a last minute change before libadwaita 1.6.alpha, we landed a new alert/message dialog style/layout. It covers `AdwAlertDialog`, `AdwMessageDialog` (minus a few features) and even `GtkMessageDialog`/`GtkAlertDialog` (as best as it can). It's now using standard button styles, fixing the long-standing issue where suggested and destructive buttons would look the same when using red accent color, and generally refreshing the look
> ![](65678b483e333dc2a1e80a593ee9c4e7adfc84781806754585696534528.png)

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> After various necessary cleanups in both Mutter and GNOME Shell, you can finally build Mutter as a Wayland-only compositor. GNOME Shell will automatically detect at build time whether Mutter was built with or without X11, with or without Xwayland and adapt to that.

# GNOME Circle Apps and Libraries

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu (he/they/she)](https://matrix.to/#/@somas95:gnome.org) says

> Today is the international LGBTIQA+ pride day. As a queer person myself I wanted to celebrate, especially in a moment where our rights are questioned more and more. It's available now on Flathub a new release of Apostrophe, with some small fixes and updated translations, but first and foremost new pride styles for the save animation. Thanks to Fina Wilke for the original implementation! Check Apostrophe on [Flathub](https://flathub.org/apps/org.gnome.gitlab.somas.Apostrophe) and, if you like it, donate to your favorite local queer association 🏳️‍🌈🏳️‍⚧️
> {{< video src="a9b057d8b542e9ca65554d0de3bb020f2d72c9521806643851184570368.mp4" >}}

### Déjà Dup Backups [↗](https://wiki.gnome.org/Apps/DejaDup/)

A simple backup tool.

[Michael Terry](https://matrix.to/#/@mterry:gnome.org) announces

> Déjà Dup Backups 46 is now available!• New Adwaita dialogs and UI polish
> -	It now uses Rclone for cloud support (more reliable than the Python libraries used before)
> -	Better feedback in several error cases
> -	The Restic backend is a lot more robust
> - Plus the usual bug fixes and translation updates
> Download from flathub: https://flathub.org/apps/org.gnome.DejaDup
> ![](24c2c147d9000610bdcf53f214aba235a3d7d08d1805272801314078720.jpeg)

# Third Party Projects

[Swapnil Devesh](https://matrix.to/#/@sidevesh:matrix.org) reports

> I have released version 1.1.0 of my app Luminance https://github.com/sidevesh/Luminance and now its built using GTK4 and Libadwaita 🎉  TWIG-Bot
> ![](YPGUzNwyOHIcDvRsSXUsIiCo.png)

[d-k-bo](https://matrix.to/#/@d-k-bo:matrix.org) says

> Televido 0.4.0 is [available on Flathub](https://flathub.org/apps/de.k_bo.Televido).
> 
> Televido is an app to access German-language public broadcasting live streams and archives based on APIs provided by the MediathekView project.
> 
> As a major change in version 0.4.0, the mediathek search field now uses MediathekViewWeb's advanced search syntax. Additionally, support for removing and reordering live channels and an option to copy the subtitles URL have been added.

[Izzy (she/they)](https://matrix.to/#/@fizzyizzy05:matrix.org) reports

> Binary 0.3 is officially out!
> 
> New features and improvements include:
> 
> * Conversions to and from octal (base 8) numbers
> * A cleaner headerbar with flat dropdowns for the base selectors.
> * Reworked number conversions to be more reliable and quicker.
> * Added translations for Finnish and German.
> 
> Get it from Flathub here: https://flathub.org/apps/io.github.fizzyizzy05.binary
> ![](igtQrJSjYyvlUBiqkWcyWDLT.png)

### Tuba [↗](https://github.com/GeopJr/Tuba)

Browse the Fediverse.

[Evangelos "GeopJr" Paterakis](https://matrix.to/#/@geopjr:gnome.org) reports

> [Tuba](https://flathub.org/apps/details/dev.geopjr.Tuba) 0.8 is now available, with many [new features and bug fixes](https://github.com/GeopJr/Tuba/releases/tag/v0.8.0)!
> 
> ✨ Highlights:
> 
> * Admin Dashboard
> * Advanced Search Dialog
> * Accessibility improvements
> * Graceful network recovery & in-app proxy settings
> * Post translations
> * Profile notes
> * Notification requests & filtering
> * Mini profiles when clicking avatars
> * Optional Clapper support
> 
> 🏳️‍🌈 Happy International LGBTQIA+ Pride Day! 🏳️‍⚧️
> {{< video src="87ef221a6ad2e2865cfc931a4f95b445dccc7ede1806718773894316032.webm" >}}

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) reports

> GUADEC 2024 is only a few weeks away! Make sure to let us know how you’re attending by registering. 
> 
> **Remote Registration**
>
> If you plan to attend from home or at a satellite event such as in Berlin, please remember to register as a remote participant at https://events.gnome.org/event/209/registrations/181/. Knowing who participates in GUADEC (and where they take part) helps us make events as relevant and engaging as possible, and helps explain the reach and impact of our events to sponsors. Knowing how many remote participants we have also helps us continue to make our events accessible with a lower carbon footprint by justifying costs such as AV Teams and live streaming.
> 
> **In-Person Registration**
>
> If you are joining us in Denver, Colorado make sure to register as an in-person attendee at https://events.gnome.org/event/209/registrations/177/. Signing up ahead of the conference helps ensure you receive all the important updates and information we email to registered participants. It also allows us to print your conference badge in time for early pick up at our [July 18th pre-registration party](https://events.gnome.org/event/209/page/330-social-events)!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!