---
title: "#165 Signing Documents"
author: Felix
date: 2024-09-13
tags: ["libadwaita", "pipeline"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 06 to September 13.<!--more-->

# Sovereign Tech Fund

[Thib](https://matrix.to/#/@thib:ergaster.org) says

> This post is a digest of what happened in August in the STF team (a little late). You might have seen some updates from the team members directly earlier in TWIG! They say August is a productivity blackhole, but _whoa_ look at this _massive_ update!
> 
> I'm very grateful to the team for its dedication, and to the Sovereign Tech Fund who enables this work. The Foundation is preparing a crowdfunding platform to allow _you_ to support this kind of work as well! More on this next month.
> 
> ## Encrypt user home directories individually
> 
> On Linux systems, users are traditionally managed in a specific file called `/etc/passwd`. It doesn't make a difference between technical and human accounts. It also only stores very basic information about users, and can only be modified by root. It's a very rigid users database.
> 
> AccountsService is a "temporary" daemon (since 2010) bringing to manage users and store more extensive information about them: profile picture, parental control settings, preferred session to log into, languages, etc. It manages `/etc/passwd` and its own database.
> 
> homed is a modern replacement for AccountsService. It provides encryption for user data, and paves the way for future exciting platform security and flexibility improvements.
> 
> Adrian worked with a user through some remote debugging about systemd-homed ([systemd/systemd#33541](https://github.com/systemd/systemd/issues/33541#issuecomment-2313253491) and following comments).
>
> ## Modernize platform infrastructure
> 
> ### libadwaita
> 
> GTK is a popular development toolkit to create graphical apps in the free desktop. libadwaita is a library based on GTK providing the building blocks for modern GNOME applications.
> 
> Alice did libadwaita releases: [1.4.7](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1235), [1.5.3](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1234), [1.6.beta](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1236), and [1.6.rc](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1237). She also worked on Dialog fixes before beta ([here](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1231) and [here](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1230)), a [CI fix](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1232) as well as several reviews.
> 
> She shared thoughts on a common interface for platform libraries support in GTK [in this issue](https://gitlab.gnome.org/GNOME/gtk/-/issues/6821).
> 
> Alice also [worked on toggle groups](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/727)
> * She added crossfade support to the view stack so we can use it in contexts where you'd want an inline switcher
> * She switched inline stack switcher to view stack, renamed to inline view switcher
> * And she started reworking toggle group api. That work is still ongoing
> * inline view switcher now supports needs-attention/badges.
> * Alice also he gave the tests (including accessibility tests) some love to make sure libadwaita is as reliable as ever and implemented a new demo.
> * She reworked documentation to make her work easier to use
> * Finally she spent quite a bit of time on reviews for PRs have been merged ([here](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1199), [here](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1241), [here](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1242), and [here](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1246) and quite a few more) and on PRs that have not.
> 
> Toggle groups should be ready, waiting for 47 to branch now.
> 
> ### Shell notifications
> 
> If you have followed Julian's GUADEC talk about notifications on the free desktop, you know that the current situation is suboptimal. Fortunately, the design team and Julian are coming to the rescue, and are also working on grouped notifications to make them less in your face.
> 
> Julian [split out refactor part of of the notification grouping MR](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3429#note_2180643), so that it can be merged sooner. He also addressed comments and got [his cleanup MR](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3387) merged into GNOME Shell.
> 
> Julian rewrote glsl shader for fading notifications when a group is expanded. He looked into round clipping for mutter, which would be really nice for grouping. This will require a significant amount of non trivial work to do correctly.
> 
> Finally, he worked on notification grouping for GNOME Shell: after addressing the style suggestions he marked the [notification grouping MR](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3012) as ready.
> ## Improve QA and development tooling
> 
> ### gjs bindings
> 
> Evan finished work on getting [async support for GJS](https://gitlab.gnome.org/GNOME/gjs/-/merge_requests/863), compiling again and is debugging test failures.
> 
> He is working on [updating async tests for the TypeScript bindings](https://github.com/gjsify/ts-for-gir/pull/177).
> 
> Finally, [GLib async support](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3746) needs additional work to fix Windows CI (hopefully the last roadblock 🤞).
> 
> ### systemd-sysupdated
> 
> systemd-sysupdate is an update system allowing immutable systems to apply lightweight delta updates. It helps image-based systems support immutability, auto-updates, adaptability, factory reset, uniformity and providing a trust chain from the bootloader all the way up.
> 
> sysupdate is a CLI tool. In order to be able to use systemd-sysupdate, sturdier services are needed. sysupdated is a service that provides a dbus API for sysupdate.
> 
> sysupdated got a [final re-review, and got merged](https://github.com/systemd/systemd/pull/32363) into systemd. Adrian [followed-up fixes](https://github.com/systemd/systemd/pull/34079) after merging.
> 
> He rebased the [incomplete versions bugfix](https://github.com/systemd/systemd/pull/33570), and got it merged, worked on [optional features](https://github.com/systemd/systemd/pull/33398), [major os upgrades](https://github.com/systemd/systemd/pull/33706), `updatectl` (a user facing CLI for sysupdate) [bugfixes](https://github.com/systemd/systemd/pull/34198) and [more](https://github.com/systemd/systemd/pull/34202).
> 
> ### systemd-repart
> 
> systemd-repart is a tool that systemd runs on boot that non-destructively repartitions disks.
> 
> Adrian taught it to [dynamically decide whether or not to create a separate boot partition](https://github.com/systemd/systemd/pull/34040). Depending on the existing layout of disk, repart will skip creating a dedicated boot partition if it can get away with it, which is one of the big missing features in repart before it can be an OS installer.
> 
> ## Improve the state and compatibility of accessibility
> 
> Joanie has been working on a metric tonne of updates in Orca. Most of the changes are rather technical and would be rather wordy in a TWIG update, but thanks Joanie for rejuvenating Orca!
> 
> ## Maintenance and modernization of security components
> 
> The free desktop standardises the storage and usage of secrets (such as passphrases or SSH keys) via the secrets specification. gnome-keyring was the backend implementation, and libsecret the client-side of said specification.
> 
> gnome-keyring and libsecret are written in C and lack maintenance. oo7 is a modern Rust client-side library that respects the secrets specification. Dhanuka is extending oo7 to implement the backend side of secrets management, and ultimately replace gnome-keyring, has been at it in August as well!
> 
> ## Secure APIs for Wayland/Flatpak
> 
> ### Flatpak
> 
> Georges handled Flatpak / Bubblewrap [CVE-2024-42472](https://github.com/flatpak/flatpak/security/advisories/GHSA-7hgv-f2j8-xw87), released xdg-dbus-proxy 0.1.6 with important fixes, coordinated more merges, and released Flatpak 1.16.0.
> 
> He also implemented a solution to the "impossible" Flatpak a11y issue ([here](https://github.com/WebKit/WebKit/pull/32247), [here](https://github.com/flatpak/flatpak/pull/5898) and [here](https://github.com/flatpak/flatpak-xdg-utils/pull/67)). This accessibility issue revolved around the Flatpak sandbox preventing WebKit and Epiphany's accessibility trees from being connected.
> 
> ### USB Portal
> 
> Flatpak doesn't have USB permission. To access USB currently, the `device=all` permission must be used. This exposes all the devices on the machine.
> 
> We want to provide a way to allow selective access to USB devices from inside a sandbox like flatpak. This is access to the actual USB, not the devices build on top, like audio, input or mass storage.
> 
> Hub rebased the [xdg-desktop-portal PR](https://github.com/flatpak/xdg-desktop-portal/pull/1354) for the USB portal, and rebased t[he flatpak PR for USB](https://github.com/flatpak/flatpak/pull/5620). Finally, https://github.com/flatpak/flatpak/pull/5855 landed.
> 
> ## Closing thoughts
> 
> Again, a massive _massive_ thank you to the whole team who has been working on the STF grant, and our gratitude to the Sovereign Tech Fund who makes this possible. The team is working with all their heart to bring not only GNOME but also the free desktop as a whole forward.

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) says

> libadwaita 1.6.0 is out! see the [blog post](https://blogs.gnome.org/alicem/2024/09/13/libadwaita-1-6/) for details.

# GNOME Incubating Apps

[Pablo Correa Gomez](https://matrix.to/#/@pabloyoyoista:matrix.org) reports

> Thanks to Jan-Michael Brummer (Volkswagen), Papers now supports signing documents with digital certificates! If you have a digital certificate stored in your computer or in an Smart Card, as those stored in the national IDs of some states like Estonia or Spain, you no longer need an additional program to digitally sign your documents! See the Merge Request [description](https://gitlab.gnome.org/GNOME/Incubator/papers/-/merge_requests/296) for a walk-through of the UX!
> ![](UFhYOKYfLrKRjGBkDEXESJME.png)

# Third Party Projects

[Hari Rana | TheEvilSkeleton](https://matrix.to/#/@theevilskeleton:fedora.im) announces

> [Upscaler version 1.3.0](https://gitlab.gnome.org/World/Upscaler/-/tree/1.3.0?ref_type=tags) was just released! The release is pretty large and includes some nice features and visual improvements:
> 
> * Introduce queue system to allow users to upscale images right after the other
> * Add branding colors
> * Port to latest Adwaita widgets
> * Allow dropping remote images
> * Allow pasting images from clipboard
> * Make window draggable from anywhere
> * Delete temporary file when unused
> * Switch to Upscayl-NCNN from Real-ESRGAN ncnn Vulkan
> * Update and rework translation
> * Add Bulgarian translation
> 
> Unfortunately, I completely forgot that the GNOME 47 runtime isn't released yet. The update will be available later once the GNOME 47 runtime is out. If you want to try it out, you can build it from source (roughly a few minutes) using GNOME Builder: https://gitlab.gnome.org/World/Upscaler#gnome-builder
> ![](cd7ded2efaf2f9bbcca8529b8a78d8f7113f58f01834682897890541568.png)
> {{< video src="081d4ced878cb5dabdd49b95016cbae2f7ae50111834682704709287936.mp4" >}}

[Krafting](https://matrix.to/#/@lanseria:matrix.org) says

> Version 1.2.0 of [SemantiK](https://flathub.org/apps/net.krafting.SemantiK) is out now. It is a word-guessing game. 
> 
> This new version includes the ability to download language packs. The first language pack to be available is the English language pack, which you can download on Flathub through your software center of choice.
> 
> The UI is still in French, but I'll be adding a translated UI in the next version!
> 
> You can download [SemantiK](https://flathub.org/apps/net.krafting.SemantiK) from Flathub
> 
> If people want to help build more language packs, feel free to contact me!

[xjuan](https://matrix.to/#/@xjuan:gnome.org) says

> Introducing Casilda - A Wayland compositor widget!
> 
> A simple Wayland compositor widget for Gtk 4 which can be used to embed other processes windows in your Gtk 4 application.
> It was originally created for Cambalache's workspace using wlroots, a modular library to create Wayland compositors.
> Following Wayland tradition, this library is named after my hometown in Santa Fe, Argentina
> 
> Read more about it at https://blogs.gnome.org/xjuan/2024/09/13/introducing-casilda-wayland-compositor-widget/
> ![](e8b0e23b47ba842ca4e36b6d6744e79137a578241834647665917296640.gif)

### Pipeline [↗](https://flathub.org/apps/de.schmidhuberj.tubefeeder)

Follow your favorite video creators.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) reports

> Pipeline version 2.0.0 was released. Pipeline lets you follow your favorite video creators, both on YouTube and PeerTube. Version 2.0.0 is a complete rewrite of the application, in order allow for easier maintenance, more features (some included in this version, some in future versions) and improve the user experience in general.
> 
> The highlights of this version are the ability to search YouTube and PeerTube for videos and channels and playing videos inside the application using the [Clapper](https://github.com/Rafostar/clapper) video player (while the option to play with any other external player is still available). Besides those highlights, the application should have feature-parity with previous releases of Pipeline. This includes in particular:
> 
> * Displaying videos of your subscriptions in a single feed,
> * Filtering out unwanted videos, for example Shorts or videos from a series you don't like,
> * Managing videos you want to watch later,
> * Importing your previous subscriptions from [NewPipe](https://newpipe.net/) or YouTube,
> * Download videos you want to watch offline.
> 
> You can get the latest release on [Flathub](https://flathub.org/apps/de.schmidhuberj.tubefeeder), and feel free to chat with us in our [Matrix room](https://matrix.to/#/%23pipelineapp:matrix.org).
> ![](CzzgVDHTrNKOUCLRJASNSnOE.png)
> ![](NvVLumjFrqQadUVnjDdKhVHH.png)

# Miscellaneous

[Jan-Michael Brummer](https://matrix.to/#/@jbrummer:matrix.org) says

> Software development without Linux is no longer possible within an automotive environment. Therefore Volkswagen Group IT created and maintains a Linux distribution for their developers and contributes to many upstream projects. Jan-Michael Brummer speaks at OpenSUSE 2024 (https://media.ccc.de/v/4486-linux-at-volkswagen/oembed) about the starting goal to integrate into the existing environment, and highlights their integration problems and solutions with contributing to upstream: libproxy, OneDrive (gvfs/goa), PKCS11 (Secrets / Web), Digital Signing (Papers), ...

# GNOME Foundation

[Allan Day](https://matrix.to/#/@aday:gnome.org) announces

> The GNOME Foundation has announced that hiring is open for its next Executive Director. Details about the position can be found on [the GNOME Foundation website](https://foundation.gnome.org/careers/), at [OpenSource Job Hub](https://opensourcejobhub.com/job/19044/executive-director/), and [fossjobs.net](https://www.fossjobs.net/job/11451/executive-director-%e2%80%94-gnome-foundation-at-gnome-foundation/). Please share the post with your networks and reach out to any candidates you might know.
> In other Foundation news, the Board of Directors had its regular monthly meeting this week, where it focused on budget planning for the upcoming financial year. We are also pleased to announce that Richard Littauer, the Foundation's current interim executive director, will be continuing in his position until the end of November.

[Rosanna](https://matrix.to/#/@zana:gnome.org) reports

> The big news this week from the Foundation is our opening up the search for a new Executive Director. Our current Interim Executive Director, Richard Littauer, is helping us with this search. More details about this job opening is available https://foundation.gnome.org/2024/09/13/search-for-new-executive-director/.
> 
> Earlier this week, I attended the monthly board meeting and went through a very rough, preliminary draft of a budget for the Board. Not intended to be a working budget, this draft is just a starting point to begin discussion. While it is up to the Finance Committee to hammer this draft into something usable, this exercise of going over it with the board at large was useful in showing our directors the format and structure of our yearly budget, and in explaining what the groupings and categories cover. I am looking forward with the rest of the Finance Committee on the next steps.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

