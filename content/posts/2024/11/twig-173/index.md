---
title: "#173 Text Annotations"
author: Felix
date: 2024-11-08
tags: ["gir.core"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 01 to November 08.<!--more-->

# GNOME Incubating Apps

[Pablo Correa Gomez](https://matrix.to/#/@pabloyoyoista:matrix.org) says

> Papers' UX process for adding text annotations has changed. Before (left), it required two clicks to add an annotation. Now (right) the annotation will be added where the user right clicked. Additionally, this makes it now a lot more intuitive to add annotations on devices with touchscreens! Even further, annotations are now created centered and not with a displacement to the bottom right!
> {{< video src="FXuPSBdqXfOCYGlFWrWLRjaM.mp4" >}}

# Third Party Projects

[Peter Eisenmann](https://matrix.to/#/@p3732:matrix.org) says

> Last week os-installer version 0.4 was released featuring these changes:
> 
> * Use AdwNavigationView (♥️) and other modern libadwaita widgets
> * Added Desktop selection page
> * Multiple-choice options for software/feature choices
> * Password/pin can have confirmation fields and minimum length
> * Optional username field added
> * Fixes and interal refactorings for performance improvements
> * Added translations (Arabic, Portuguese (Portugal), Romanian, Russian, Turkish, Urdu) and updated existing translations
> 
> Many thanks to all translators and everyone providing feedback! More features are already on their way.
> 
> If you are interested in designing graphics for the page headers, feel free to reach out in our [Matrix chat](https://matrix.to/#/#os-installer:matrix.org).
> ![](dqyUjjBdOHcgRJVFiTgampWX.jpg)

[Heliguy](https://matrix.to/#/@heliguy:matrix.org) reports

> I'm happy to announce that Warehouse has a major update! The user interface has been significantly improved, making the more powerful actions easier to use. This redesign also allows for Warehouse to make better use of larger screen sizes, showing content side-by-side. Improvements also include the ability to name Snapshots (application user data backups), support for custom installations, and of course, overall performance benefits.
> 
> https://flathub.org/apps/io.github.flattool.Warehouse
> https://github.com/flattool/warehouse
> ![](HUnUPlWuhLEnRsYdSLjBvJcZ.png)
> ![](rVHodjxhpDdxqpDpQrIXGNhW.png)

[Giant Pink Robots!](https://matrix.to/#/@giantpinkrobots:matrix.org) announces

> Varia download manager has gotten a new update this week. The main UI has been tweaked, with a new button to add .torrent files and a better layout for download progress cards. This update is mostly polishing what was already there, with important bug fixes, performance improvements and many other changes regarding general usability.
> 
> https://flathub.org/apps/io.github.giantpinkrobots.varia
> https://giantpinkrobots.github.io/varia/
> ![](fhAMULvghpytLQYGYRXJuKeN.png)

### Gir.Core [↗](https://gircore.github.io/)

Gir.Core is a project which aims to provide C# bindings for different GObject based libraries.

[badcel](https://matrix.to/#/@badcel:matrix.org) reports

> The [GirCore](https://github.com/gircore/gir.core/tree/main) project released version [0.6.0-preview.1](https://github.com/gircore/gir.core/releases/tag/0.6.0-preview.1) containing updated C# bindings for the GObject stack inclding GTK 4.16 and libadwaita 1.6.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
