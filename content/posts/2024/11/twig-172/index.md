---
title: "#172 Valencia"
author: Felix
date: 2024-11-01
tags: ["libadwaita", "fractal", "parabolic"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 25 to November 01.<!--more-->

Open source is more than just writing code; it's about people and the community. Right now, the world faces numerous crises, and this past week, another tragedy occurred - one that also affects members of the GNOME community. 

[Manu (he/they/she)](https://matrix.to/#/@somas95:gnome.org) says

> The Valencian Country, among other Spanish autonomies has been hit by the worst natural disaster in its history. Entire villages have been completely flooded. There are more than 200 deaths so far that we know of, and more than 2000 people missing.
>
> If you wish to help, Caritas is a trustful organization to donate to: 
>
> **ES02 2100 8734 6113 0064 8236**
>
> Any [donation](https://www.paypal.me/manuelgenoves) [Apostrophe](https://apps.gnome.org/Apostrophe/) receives the next two months will be also donated to one of the local Horta Sud associations that are working on the field. I will be also helping were help is needed. 

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) reports

> Peter Eisenmann added the [`:visible-page-tag`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/property.NavigationView.visible-page-tag.html) property to `AdwNavigationView` - a helper for checking the current page by its tag

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) reports

> the style class `.dim-label` that has always had a misleading name has been soft deprecated in favor of [`.dimmed`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/style-classes.html#dimmed). The old style still works same as before

# Third Party Projects

[Jan-Willem](https://matrix.to/#/@jwharm:matrix.org) announces

> This week I released Java-GI version 0.11.0! Java-GI is a GObject-Introspection bindings generator for Java, using the brand new foreign function interface of OpenJDK 22. It can be used to develop GNOME apps in Java.
> 
> This release features a lot of fixes and improvements, so make sure to check out the [release notes](https://github.com/jwharm/java-gi/releases/tag/0.11.0).
> 
> For more information about Java-GI, visit the [website](https://jwharm.github.io/java-gi/), where you will find code samples in both Java and Kotlin. Additionally, the Gtk "Getting started guide" has been ported to Java and is now [available here](https://jwharm.github.io/java-gi/getting-started/getting_started_00/), and a couple new examples were added to the [java-gi-examples repository](https://github.com/jwharm/java-gi-examples).

[Gianni Rosato](https://matrix.to/#/@computerbustr:matrix.org) says

> We’ve got a new Aviator release for you! It’s packed with small bug fixes and an [SVT-AV1-PSY](https://svt-av1-psy.com) update.
> 
> Bug Fixes:
> 
> * We fixed the hicolor icon that was mislabeled as scalable.
> * We also fixed the audio bitrate resetting when you open a new file.
> 
> SVT-AV1-PSY v2.3.0 Improvements for Aviator:
> 
> * You can now encode with odd (non-mod2) dimensions.
> * You can also encode at resolutions lower than 64x64, all the way down to 4x4.
> * The color reproduction and overall picture quality will be better when you disable “Perceptual Tuning.”
> * The color reproduction and overall picture quality with “Perceptual Tuning.” disabled has improved.
> * There will be general perceptual fidelity improvements when you enable “Perceptual Tuning.”
> * There will be general performance improvements, especially on ARM platforms.
> 
> Other Changes:
> 
> * We removed the sharpness usage when “Perceptual Tuning” is enabled.
> * We’ve updated FFmpeg to version 7.1.
> * We’ve updated llvm to version 19 for project compilation.
> * We’ve updated to GNOME SDK 47.
> 
> Enjoy the new Aviator release! ✈️

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Parabolic [V2024.10.3](https://github.com/NickvisionApps/Parabolic/releases/tag/2024.10.3) is here! This update introduces some new features, including the ability to select a batch txt file with multiple URLs for validation, and fixes many bugs regarding website validation, localization of the app on Windows, and crashes on Linux.
> 
> Here's the full changelog:
> * Added support for selecting a batch file with multiple URLs to validate instead of validating a single URL at a time
> * Added a recovery mode where downloads that were running/queued will be restored when the application is restarted after a crash
> * User entered file names will now be correctly normalized and validated in the Add Download dialog
> * Fixed an issue where YouTube tabs were not correctly validated
> * Fixed an issue where the app's documentation was not accessible
> * Fixed an issue where UTF-8 characters were not displayed correctly on Windows
> * Fixed an issue where playlist names were not normalized on Windows
> * Fixed an issue where the row animations were choppy using aria2c on Linux
> * Fixed an issue where the app would crash when stopping all downloads on Linux
> * Updated yt-dlp to 2024.10.22
> ![](cemRJMfNalXfbBLEwKuxromG.png)

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) reports

> 😱 What’s that behind you⁉️ Oh, that’s the new Fractal 9 release❣️ 😁 🎃
> 
> * We switched to the glycin library (the same one used by GNOME Image Viewer) to load images, allowing us to fix several issues, like supporting more animated formats and SVGs and respecting EXIF orientation.
> * The annoying bug where some rooms would stay as unread even after opening them is now a distant memory.
> * The media cache uses its own database that you can delete if you want to free some space on your system. It will also soon be able to clean up unused media files to prevent it from growing indefinitely.
> * Sometimes the day separators would show up with the wrong date, not anymore!
> * We migrated to the new GTK 4.16 and libadwaita 1.6 APIs, including CSS variables, AdwButtonRow and AdwSpinner.
> * We used to only rely on the secrets provider to tell us which Matrix accounts are logged-in, which caused issues for people sharing their secrets between devices. Now we also make sure that there is a data folder for a given session before trying to restore it.
> * Our notifications are categorized as coming from an instant messenger, so graphical shells that support it, such as Phosh, can play a sound for them.
> * Some room settings are hidden for direct chats, because it does not make sense to change them in this type of room.
> * The size of the headerbar would change depending on whether the room has a topic or not. This will not happen anymore.
> 
> As usual, this release includes other improvements and fixes thanks to all our contributors, and our upstream projects.
> 
> We want to address special thanks to the translators who worked on this version. We know this is a huge undertaking and have a deep appreciation for what you’ve done. If you want to help with this effort, head over to [Damned Lies](https://l10n.gnome.org/).
> 
> This version is available right now on [Flathub](https://flathub.org/apps/org.gnome.Fractal).
> 
> We have a lot of improvements in mind for our next release, but if you want a particular feature to make it, the surest way is to implement it yourself! Start by looking at our [issues](https://gitlab.gnome.org/World/fractal/-/issues/) or just come say hello in [our Matrix room](https://matrix.to/#/#fractal:gnome.org).

### Dev Toolbox [↗](https://github.com/aleiepure/devtoolbox)

Dev tools at your fingertips

[Alessandro Iepure](https://matrix.to/#/@aleiepure:matrix.org) announces

> After a long year of coding on and off (and balancing a lot of real life and university work!), I'm happy to finally share a new **Dev Toolbox** update! 🎉
> 
> This release packs a completely revamped UI for a smoother experience and new search functionality, making it even easier to find exactly the tool you need. You can also now mark your favorite tools to keep them in their own special menu, ready for quick access. And the fun doesn’t stop there; let me introduce three new tools:
> 
> * **JavaScript & CSS Minifiers** to help you shrink those files down
> * A handy **Base64 Encoder** (huge thanks to [@amersaw](https://github.com/amersaw) for the contribution!)
> 
> Plus, a handful of smaller improvements and bug fixes are sprinkled in to make your experience even better.
> 
> A huge shoutout to the translators who helped make this app accessible to more people around the world! 🌍 
> 
> **[Dev Toolbox](https://flathub.org/apps/me.iepure.devtoolbox)** is available right now on Flathub.
> ![](lPgtLIFkYyhafUsafxyzizNG.png)

# Events

[devrtz](https://matrix.to/#/@devrtz:fortysixandtwo.eu) reports

> 📢 🎉.This week the FOSS on mobile devices [Call for Proposals](https://gitlab.com/fosdem_mobile/devroom/-/blob/main/README.md) has been opened for FOSDEM 2025 🎉. 📢
> 
> We are excited to have your presentations, demos and more! 📈
> Showcase (and witness) the latest and greatest in Mobile Linux technologies ☎️
> next year in Brussels 🚀 
> 
> For more information, see this post on [devrtz fosstodon post](https://fosstodon.org/@devrtz/113404243460072632)
> 
> \o/ We hope to see you there \o/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!