---
title: "#176 Command History"
author: Felix
date: 2024-11-29
tags: ["gjs"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 22 to November 29.<!--more-->

# GNOME Core Apps and Libraries

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) reports

> This week, Gary added a feature to the GJS command-line interpreter: command history is now saved between runs. Look for this in GNOME 48.

# Third Party Projects

[xjuan](https://matrix.to/#/@xjuan:gnome.org) says

> Cambalache version 0.94.0 is out!Release notes: 
> - Gtk 4 and Gtk 3 accessibility support 
> - Support property subclass override defaults 
> - AdwDialog placeholder support 
> - Improved object description in hierarchy 
> - Lots of bug fixes and minor UI improvements
>
> Read more about it at https://blogs.gnome.org/xjuan/2024/11/26/cambalache-0-94-released/
> ![](c73f47bdf3855716376aac52f994e5b3052397f21862592117377859584.jpeg)

[nokyan](https://matrix.to/#/@nokyan:matrix.org) announces

> Resources [1.7](https://github.com/nokyan/resources/releases/tag/v1.7.0) has been released with support for Neural Processing Units (NPUs), the ability to select multiple processes and swap usage columns for the Apps and Processes views.
> 
> Additionally, temperatures are now also displayed as graphs and there are a couple of improvements for AMD GPUs regarding media engine and compute utilization.
> 
> The update is of course available on [Flathub](https://flathub.org/apps/net.nokyan.Resources). Enjoy!
> ![](CNHqVNuYNRTQRZuDglKZWRPA.png)

[Jan-Michael Brummer](https://matrix.to/#/@jbrummer:matrix.org) says

> TwoFun 0.5.1 has been released. It's a two player game for touch devices featuring smaller game to kill some time.
> This time user interface gained some smaller improvements and bugfixes. Enjoy!
> ![](WkqIIGqqQjLBdpcFqxxrhQEY.png)

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) announces

> Some sad news to report this week: the GNOME shop is currently closed to new orders. If you have an outstanding order that has not yet arrived and have not already contacted me, please let me know by forwarding the order to info@gnome.org. If you have any experience with running an online shop like the one we have and have the time and patience to help me troubleshoot and explain it to me, please reach out as well! I would be most grateful.
> 
> Hope everyone in the US celebrating this week has had a wonderful Thanksgiving!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
