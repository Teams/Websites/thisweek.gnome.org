---
title: "#174 Choosing Formats"
author: Felix
date: 2024-11-15
tags: ["parabolic", "sysprof", "gnome-control-center"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 08 to November 15.<!--more-->

# GNOME Core Apps and Libraries

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Hari Rana | TheEvilSkeleton](https://matrix.to/#/@theevilskeleton:fedora.im) reports

> The Format Chooser dialog in GNOME Settings has been entirely revamped with [merge request !2778](https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2778), which implements Allan Day's [mockup](https://gitlab.gnome.org/Teams/Design/settings-mockups/-/blob/master/region-and-language/region-and-language.png?ref_type=heads). The dialog was ported from [`AdwWindow`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Window.html) to [`AdwDialog`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Dialog.html), as well as from the deprecated [`AdwLeaflet`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Leaflet.html) to [`AdwOverlaySplitView`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.OverlaySplitView.html), making it adapt better on mobile form factors. The style should look less heavy thanks to the flat design in the format previewer pane.
> {{< video src="02348294b629111613edaae2ab29e9ef401a89441857103268140810240.webm" >}}

# GNOME Development Tools

### Sysprof [↗](https://gitlab.gnome.org/GNOME/sysprof)

A profiling tool that helps in finding the functions in which a program uses most of its time.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> Sysprof received a round of improvements to the Marks Waterfall view, [the hover tooltip now show the duration of the mark](https://gitlab.gnome.org/GNOME/sysprof/-/merge_requests/113). The Graphics view also [received some visual improvements](https://gitlab.gnome.org/GNOME/sysprof/-/merge_requests/112), such as taller graphs and line rendering without cutoffs. Finally, Sysprof collector is now [able to handle multiprocess scenarios better](https://gitlab.gnome.org/GNOME/sysprof/-/merge_requests/111).
> 
> A new tool for Sysprof was added: sysprof-cat. It takes a capture file, and dumps it in textual form.
> 
> This is all in preparation to further profiler integration in WebKit on Linux.
> ![](1597416efe22278e435e1cd56f763107ecc137071857362842844397568.png)
> ![](d6862457f2836cba17bd2b96935fa31c17834d4c1857362786011578368.png)

# Third Party Projects

[Vladimir Kosolapov](https://matrix.to/#/@vmkspv:matrix.org) announces

> [Netsleuth](https://github.com/vmkspv/netsleuth) 1.0.5 has just been released on [Flathub](https://flathub.org/apps/io.github.vmkspv.netsleuth)!
> 
> This version features an adaptive interface that uses a split view on wide screens. The app utilizes screen space more efficiently across mobile devices, tablets and desktops.
> 
> For advanced users and specialists, a hexadecimal output format has been added. Now it's possible to perform more complex calculations while still remaining offline.
> ![](HxYboVoGyFljJVTdHiTCperS.png)

[dabrain34](https://matrix.to/#/@scerveau:igalia.com) announces

> 🎉 New Release Announcement: GstPipelineStudio v0.3.6 🎉
> 
> It's a great pleasure to announce the release of GstPipelineStudio version 0.3.6 after nearly a year of hard work, enhancements, and testing! This release is packed with new features, performance improvements, and bug fixes that make your GStreamer pipeline design experience smoother and more efficient.
> 🆕
> 🚀 Upgrade Now!
> 
> To get the latest version of GstPipelineStudio, visit the project's [page](https://dabrain34.pages.freedesktop.org/GstPipelineStudio) and check out the release notes for more details. Whether you're building multimedia applications, experimenting with GStreamer, or just exploring the possibilities of media streaming, this release has something for everyone.
> 
> Happy streaming! 🎬📡

[Krafting](https://matrix.to/#/@lanseria:matrix.org) reports

> Hey people! I've just released [Hex Colordle](https://flathub.org/apps/net.krafting.HexColordle). It is a game inspired from [Hexcodle](https://hexcodle.com), where you need to guess the hex code of the color displayed on screen.
> 
> You can adjust the number max. of guesses to your liking, and play as much as you want.
> 
> It is available now on [Flathub](https://flathub.org/apps/net.krafting.HexColordle)
> ![](hgBrouMUsdEJZpUmGTZZKjhx.png)

[Krafting](https://matrix.to/#/@lanseria:matrix.org) says

> I've also release versions 1.1.0 and 1.2.0 of [PedantiK](https://flathub.org/apps/net.krafting.PedantiK). 
> 
> There are a lot of bugfixes and refactoring, alongside some new features, such as the ability to show the page once you find it, close guessed words are now in gray and current guesses are in color.
> 
> More importantly, I started work to bring more languages to the application, in the next version you will see an English language pack, the same way I've done for [SemantiK](https://flathub.org/apps/net.krafting.SemantiK), for now the app is still French-only.
> 
> It is available on [Flathub](https://flathub.org/apps/net.krafting.PedantiK)
> ![](fmdfrcBCiPPJlAAdkqBhBlOC.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) announces

> [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) 0.43.0 is out:
>
> The quick settings in phosh got massively overhauled giving us nicer status pages and doing away with the long press to open them also custom quick settings can now have status pages too. Phosh now also picks up the accent color, we added a Pomodoro timer quick setting and save screenshots to the screenshots folder. There's more fixes to the notification system, CSS and other parts of the shell and the compositor.
>
> Check the full details [here](https://phosh.mobi/releases/rel-0.43.0/)
>
> ![](image.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Parabolic [V2024.11.0](https://github.com/NickvisionApps/Parabolic/releases/tag/2024.11.0) is here!!
> 
> This update introduces some new features, including the ability to select a preferred subtitle format and a generic file type, and fixes many issues with downloads failing and not resuming correctly as well as other minor bugs.
> 
> Here's the full changelog:
> * Added the ability to turn on and off yt-dlp's verbose logging in the app's settings
> * Added the ability to select a preferred subtitle file format in the app's settings
> * Added the ability to select a generic file type that will prevent conversions of media files
> * Fixed an issue where some webm conversions failed
> * Fixed an issue where some files related to a download were being overwritten when they should not have been
> * Fixed an issue where the app crashed when handling too long file names
> * Fixed an issue where the app would not open due to infinitely attempting to recover downloads
> * Fixed an issue where the speed limit option was not configurable
> * Fixed an issue where the app did not self update on Windows
> * Updated yt-dlp to 2024.11.04
> ![](GvNrQaSwOVlszsJncADvzaVc.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
