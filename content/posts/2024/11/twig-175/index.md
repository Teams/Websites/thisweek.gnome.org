---
title: "#175 Magic"
author: Felix
date: 2024-11-22
tags: ["parabolic", "gjs", "turtle"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 15 to November 22.<!--more-->

# GNOME Core Apps and Libraries

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> Gary Li added support for source maps to GJS. If you use build tools such as TypeScript for source code transformation, you can ship source map files alongside your built JS files and make sure your build tool adds the magic source map comment. You will then get the original source locations printed in stack traces and in the debugger.

# Third Party Projects

[Konstantin Tutsch](https://matrix.to/#/@konstantin:konstantintutsch.com) announces

> [Lock](https://konstantintutsch.com/Lock) v1.1.0 is here!
> 
> The user experience of encrypting data has drastically improved with the manual entering of a key's UID being obsolete. You can now simply choose the key you want to encrypt for from a list of all available keys with just a single click!
> 
> Fingerprint access of keys has also improved. You can now copy a key's fingerprint by clicking on its row during keyring management.
> 
> Available on [Flathub](https://flathub.org/apps/com.konstantintutsch.Lock).
> ![](tjaBcuru9PyRMpO3nDRQKa1HsDsh2Jby.png)

[Mateus R. Costa](https://matrix.to/#/@MateusRodCosta:matrix.org) reports

> Today I have released version 1.1.1 of bign-handheld-thumbnailer. This new version is a patch release with some small tweaks and I believe there won't be much to add to the program for a while.
> 
> bign-handheld-thumbnailer, for those who haven't heard about, is a thumbnailer for Nintendo DS and 3DS roms. It was created as a replacement for [gnome-nds-thumbnailer](https://gitlab.gnome.org/Archive/gnome-nds-thumbnailer) (which only created thumbnails for NDS roms and has been recently archived), but also gained the ability to generate thumbnails for 3DS roms.
> 
> The new 1.1.1 version is available in [a copr for Fedora 40, 41 and Rawhide](https://copr.fedorainfracloud.org/coprs/mateusrodcosta/bign-handheld-thumbnailer/) (Fedora 39 is left out as it will be EoL in a few days).
> 
> For the next steps, and since gnome-nds-thumbnailer has been archived, it should be a good moment to try to get the project added into official distro repos.
> I will be attempting to going through the Fedora process, if you are on a different distro and this thumbnailer is useful for you, consider asking for an official package.
> 
> (Another possibility is shipping the thumbnailer as a Flatpak in the near future, if that functionality is ever added...)
> ![](hSaNwdElfvVDDBSmDnjXbnsS.png)

### Turtle [↗](https://gitlab.gnome.org/philippun1/turtle)

Manage git repositories in Nautilus.

[Philipp](https://matrix.to/#/@philippun:matrix.org) says

> [Turtle](https://flathub.org/apps/de.philippun1.turtle) 0.11 has been released.
> 
> ### Clean and reset dialog
> 
> A clean and reset dialog has been added to clean a repository from untracked files or reset the current branch to a specific reference.
> 
> ### Diff and log updates
> 
> It is now possible to compare the working directory to a reference in the diff dialog.
> 
> Renamed files are now shown as one entry in the commit table instead of a removed and added entry. Additionally the context menu in the log dialog has been extended by a push action to push the selected branch.
> 
> ### Minor updates
> 
> There are more minor updates, for the full list see the [changelog](https://gitlab.gnome.org/philippun1/turtle/-/releases/0.11)
> ![](FqHQTjlsSVAeNbOXRNVzelDB.png)
> ![](FSJMXyeSVLJSBDfARopHrBzd.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Parabolic [V2024.11.1](https://github.com/NickvisionApps/Parabolic/releases/tag/2024.11.1) is here!
> 
> This update contains fixes for various bugs users were experiencing.
> 
> Here's the full changelog:
> * Fixed an issue where file names that included a period were truncated
> * Fixed an issue where long file names caused the application to crash
> * Fixed an issue where external subtitle files were created although embedding was supported
> * Fixed an issue where generic downloads were unable to be opened upon completion
> * Updated yt-dlp to 2024.11.18
> ![](LAwKFgTmIGISMnCfesxjHRlB.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
