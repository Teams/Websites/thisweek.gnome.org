---
title: "#158 Mini Edition"
author: Felix
date: 2024-07-26
tags: ["railway", "gnome-calendar"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 19 to July 26.<!--more-->

# GNOME Core Apps and Libraries

### Calendar [↗](https://gitlab.gnome.org/GNOME/gnome-calendar/)

A simple calendar application.

[Hari Rana | TheEvilSkeleton](https://matrix.to/#/@theevilskeleton:fedora.im) reports

> GNOME Calendar users, rejoice! After 7 months of pain and suffering, we finally reworked the event details popover, which will be available in GNOME 47!
> 
> The new event details popover builds on top of the existing UI/code, while adding a few improvements and behavioral changes:
> 
> * The popover displays the changes-prevent (lock) icon when the event is read-only.
> * Each section is properly separated with separators, with equal amount of margins.
> * Detected video meeting links no longer appear in both the location and meeting sections, but only in the meeting section.
> * When an event has no event note, the popover will always explicitly display that there's no event description.
> * The action button adapts its icon and tooltip text depending on the event permission.
> ![](74154a851712e02ef51ac5f34a8437b917f2094d1815549578791878656.png)
> ![](c9bae2b7691b10a65ca17aaf76a7b32e8b1f0e901815549562987741184.png)
> ![](d9d55f1135f5e975cea17fa2c88fad2a562014501815549598404444160.png)

# GNOME Circle Apps and Libraries

### Railway [↗](https://gitlab.com/schmiddi-on-mobile/railway)

Travel with all your train information in one place.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) reports

> Railway version 2.7.0 was released. This includes an improved dialog listing the providers, which now highlights the currently selected provider and furthermore separates the providers by region. One can now furthermore search for journeys by arrival time (took only 9 months for it to get merged). This release also contains minor fixes with some providers.
> ![](OCfgEMUHhEvWiMsbwuDITCWg.png)

# Third Party Projects

[pieterdd](https://matrix.to/#/@pieterdd:matrix.org) says

> [Rclone Shuttle](https://github.com/pieterdd/RcloneShuttle) is a libadwaita GUI for rclone to interact with a wide variety of cloud storage providers. The latest update (0.1.4) adds support for downloading a local copy of certain files/folders.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
