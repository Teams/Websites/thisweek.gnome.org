---
title: "#157 GUADEC 2024"
author: Felix
date: 2024-07-19
tags: ["authenticator", "workbench", "keypunch", "fractal"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 12 to July 19.<!--more-->

![GUADEC Banner](image.png)

GUADEC is already happening! You can find [the complete timetable here](https://events.gnome.org/event/209/timetable/#20240719). 

Register on https://guadec.org to get all the details on how to join the event remotely.


# GNOME Circle Apps and Libraries

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> After adding TypeScript support, [Angelo](https://mas.to/@vixalientoots) made Workbench able to use TypeScript for JavaScript diagnostics and completions.
> ![](4ba68cd02cd2fb6440d0c66736dbdc7996715ba11812595243283906560.png)

### Authenticator [↗](https://gitlab.gnome.org/World/Authenticator)

Simple application for generating Two-Factor Authentication Codes.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) reports

> I have released [Authenticator](https://flathub.org/apps/com.belmoussaoui.Authenticator) 4.5.0. The release consists of the following:
> 
> * backup: Add import of Raivo OTP files
> * qr scanner: Use back camera by default
> * search provider: Withdraw notification once the code is no longer valid
> * accounts: Refresh completion model when scanning from qr code
> * use latest libadwaita widgets
> 
> Without forgetting all the improvements brought to the camera widget through [aperture](https://gitlab.gnome.org/GNOME/snapshot/-/tree/main/aperture?ref_type=heads) library

# Third Party Projects

[Ronnie Nissan](https://matrix.to/#/@ronniedroid:mozilla.org) announces

> This week I released Embellish, a simple nerd fonts manager, the features include:
> 
> * A beautiful Libadwaita design
> * Download and install nerd fonts from a list
> * Update installed fonts
> * Uninstall fonts
> * Fuzzy search
> * A CLI/TUI companion app (embellish-tui) for scripting and/or people who prefer the terminal
> 
> This is my first ever gnome app (and desktop app) and I am happy to be joining the community.
> You can get Embellish on [flathub](https://flathub.org/apps/io.github.getnf.embellish)
> ![](c97fdc5a86486dbbadd16cfaab10edbca38502431814021156601069568.png)

[JumpLink](https://matrix.to/#/@JumpLink:matrix.org) reports

> In preparation for GUADEC, we have released version [4.0.0-beta.7](https://github.com/gjsify/ts-for-gir/releases/tag/4.0.0-beta.7) of the TypeScript type definition generator `ts-for-gir` this friday evening. 
> 
> With this version, it is now possible to generate types locally without `package.json` support. This was a highly requested feature from the GNOME community to avoid the need for NPM. You can now easily generate this types, for example, with:
> 
> ```
> npx @ts-for-gir/cli generate Adw-1.0
> ```
> 
> Additionally, we have released a preview version of the upcoming GNOME Shell 47 types [here](https://github.com/gjsify/gnome-shell/pull/49) and on [NPM](https://www.npmjs.com/package/@girs/gnome-shell/v/47.0.0-next.1). This should hopefully make it easier for GNOME Shell extension projects to prepare their extensions for the upcoming version.
> {{< video src="AETUFuRctrgGtgNoMWlewkcU.mp4" >}}

### Keypunch [↗](https://github.com/bragefuglseth/keypunch)

Practice your typing skills

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) reports

> This week I released Keypunch 3.0. If you appreciate accuracy and legibility, you are going to love this update!
> 
> * Monospaced font in the main text view
> * Text generation support for Bangla and Hebrew
> * Guillemet quotation marks for the French text generation
> * User interface translations for Hebrew and Italian, making Keypunch available in a total of 14 languages
> 
> You can get Keypunch on [Flathub](https://flathub.org/apps/dev.bragefuglseth.Keypunch).
> ![](0c3c9984c0e4d70f121d7e782a3c46b5ead81c611813383271300464640.png)

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) reports

> Did you miss us? Only two weeks after our beta release, we are ready to release Fractal 8.rc.
> 
> Here are the main goodies since Fractal 8.beta:
> 
> * Draft messages are persisted in the database, allowing to restore them after a restart
> * A banner appears when synchronization with the homeserver fails too many times in a row
> * Links in room descriptions are detected in most places
> * Collapsed categories in the sidebar are remembered between restarts, with the “Historical” category collapsed by default
> * The history media viewers show properly when they are loading or empty
> 
> As usual, this release includes other improvements, fixes and new translations thanks to all our contributors, and our upstream projects.
> 
> It is available to install via Flathub Beta, see the [instructions in our README](https://gitlab.gnome.org/World/fractal#installation-instructions).
> 
> As the version implies, it should be mostly stable and we expect to only include minor improvements until the release of Fractal 8.
> 
> If you want to spend your summer or winter coding, solving our [issues](https://gitlab.gnome.org/World/fractal/-/issues) is a great place to start!

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) reports

> Today is the first official day at GUADEC, but for our Board of Directors, Our Interim Executive Director, and myself, it's day three of GUADEC-related events. Traditionally, the board meets in person two days before GUADEC, followed the next day with a meeting with our Advisory Board. This year, with some directors unable to be here in person in Denver, we have had some scheduling challenges and the Board meeting has spread out across three mornings. It has been satisfying helping the new Directors as they work together as a Board for the first time.
> 
> At the meetings I presented financials, gave input on Foundation procedures, and explained proposals on behalf of the staff. While all-day meetings are always tiring, it was good to be able to work through the backlog of issues that usually are decided at the GUADEC Board meetings.
> 
> In addition, I have had some fruitful discussions with Richard, our Interim ED, on the priorities and logistics of Foundation work.
> 
> Last night was the pre-registration party here in Denver, and it was wonderful seeing so many folks again! It was truly lovely to see familiar faces that have not been able to come to GUADEC in a while due to the distance.
> 
> While GUADEC has taken up most of my time and mental energies, regular Foundation work doesn't stop. I have also been in contact with our accountants to make sure our finalized financial review is signed, worked on paperwork for other tax forms, made sure invoices are being paid, along with other minutiae.
> 
> If you're in Denver please drop by and say hi! I am excited GUADEC is about to begin and looking forward to the coming days of talks, BoFs, and social events!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
